package pt.isel.tese.smartParkingIot.api.repository;

import org.springframework.stereotype.Component;
import pt.isel.tese.smartParkingIot.api.repository.contracts.*;

@Component
public class RepositoryFacade {
    private IZoneRepository zoneRepository;
    private ISensorRepository sensorRepository;
    private IUserRepository userRepository;
    private ISensorTypeRepository sensorTypeRepository;
    private IRoleRepository roleRepository;
    private IOutboundMessageTypeRepository outboundMessageTypeRepository;
    private IOutboundMessageRepository outboundMessageRepository;

    public RepositoryFacade(IZoneRepository zoneRepository, ISensorRepository sensorRepository,
                            IUserRepository userRepository, ISensorTypeRepository sensorTypeRepository,
                            IRoleRepository roleRepository, IOutboundMessageTypeRepository outboundMessageTypeRepository,
                            IOutboundMessageRepository outboundMessageRepository) {
        this.zoneRepository = zoneRepository;
        this.sensorRepository = sensorRepository;
        this.userRepository = userRepository;
        this.sensorTypeRepository = sensorTypeRepository;
        this.roleRepository = roleRepository;
        this.outboundMessageTypeRepository = outboundMessageTypeRepository;
        this.outboundMessageRepository = outboundMessageRepository;
    }

    public IZoneRepository getZoneRepository() {
        return zoneRepository;
    }

    public ISensorRepository getSensorRepository() {
        return sensorRepository;
    }

    public IUserRepository getUserRepository() {
        return userRepository;
    }

    public ISensorTypeRepository getSensorTypeRepository() {
        return sensorTypeRepository;
    }

    public IRoleRepository getRoleRepository() {
        return roleRepository;
    }

    public IOutboundMessageTypeRepository getOutboundMessageTypeRepository() {
        return outboundMessageTypeRepository;
    }

    public IOutboundMessageRepository getOutboundMessageRepository() {
        return outboundMessageRepository;
    }
}
