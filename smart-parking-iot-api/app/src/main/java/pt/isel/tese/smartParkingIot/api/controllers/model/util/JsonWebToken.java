package pt.isel.tese.smartParkingIot.api.controllers.model.util;

import java.util.Date;

public class JsonWebToken {

    private int subject;
    private int role;
    private Date issued;
    private Date expiration;

    public JsonWebToken(int subject, int role, Date issued, Date expiration) {
        this.subject = subject;
        this.role = role;
        this.issued = issued;
        this.expiration = expiration;
    }

    public int getSubject() {
        return subject;
    }

    public void setSubject(int subject) {
        this.subject = subject;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public Date getIssued() {
        return issued;
    }

    public void setIssued(Date issued) {
        this.issued = issued;
    }

    public Date getExpiration() {
        return expiration;
    }

    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }
}
