package pt.isel.tese.smartParkingIot.api.controllers.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;

import javax.servlet.http.HttpServletRequest;

@Validated
public class ErrorResponse {

    @JsonProperty("detail")
    private String detail = null;

    @JsonProperty("instance")
    private String instance = "";

    @JsonProperty("status")
    private Integer status = null;

    @JsonProperty("title")
    private String title = null;

    @JsonProperty("type")
    private String type = "about:blank";

    public ErrorResponse(String title, String detail, HttpStatus status) {
        this.title = title;
        this.detail = detail;
        this.status = status.value();
    }

    public ErrorResponse(String message, Throwable ex, HttpServletRequest request, HttpStatus status) {
        this(message, ex.getMessage(), status);
        this.instance = request.getRequestURI();
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getInstance() {
        return instance;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
