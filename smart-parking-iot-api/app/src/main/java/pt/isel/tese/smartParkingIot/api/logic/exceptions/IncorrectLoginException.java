package pt.isel.tese.smartParkingIot.api.logic.exceptions;

public class IncorrectLoginException extends LogicException {

    public IncorrectLoginException() {
        super("Invalid login.", "The user does not exist, or the password in incorrect.");
    }

    public IncorrectLoginException(String title, String detail) {
        super(title, detail);
    }
}
