package pt.isel.tese.smartParkingIot.api.logic.implementations;

import org.springframework.stereotype.Service;
import pt.isel.tese.smartParkingIot.api.logic.exceptions.CreateZoneException;
import pt.isel.tese.smartParkingIot.api.model.Sensor;
import pt.isel.tese.smartParkingIot.api.model.Zone;
import pt.isel.tese.smartParkingIot.api.model.util.Prediction;
import pt.isel.tese.smartParkingIot.api.repository.RepositoryFacade;
import pt.isel.tese.smartParkingIot.api.repository.contracts.ISensorRepository;
import pt.isel.tese.smartParkingIot.api.repository.contracts.IZoneRepository;
import pt.isel.tese.smartParkingIot.api.logic.contracts.IZoneService;
import pt.isel.tese.smartParkingIot.api.logic.managers.algorithm.contracts.IPercentageAlgorithm;
import pt.isel.tese.smartParkingIot.api.logic.managers.geometry.contracts.IGeometryManager;
import pt.isel.tese.smartParkingIot.api.utils.ListUtils;

import java.util.*;

@Service
public class ZoneService implements IZoneService {

    private RepositoryFacade repositoryFacade;
    private IZoneRepository zoneRepository;
    private ISensorRepository sensorRepository;
    private IGeometryManager geometryManager;
    private IPercentageAlgorithm percentageAlgorithm;
    private static final int MILLISECONDS_IN_MINUTES = 60000;
    private static final int MIN_ZONE_PERIMETER_POINTS = 4;

    public ZoneService(RepositoryFacade repositoryFacade, IGeometryManager geometryManager, IPercentageAlgorithm percentageAlgorithm) {
        this.repositoryFacade = repositoryFacade;
        this.zoneRepository = repositoryFacade.getZoneRepository();
        this.sensorRepository = repositoryFacade.getSensorRepository();
        this.geometryManager = geometryManager;
        this.percentageAlgorithm = percentageAlgorithm;
    }

    @Override
    public int create(Zone object) {
        if(object.getPerimeter().getCoordinates().get(0).size() < MIN_ZONE_PERIMETER_POINTS) {
            throw new CreateZoneException();
        }
        object.setCreated(new Date());
        object.setModified(object.getCreated());
        handleAreaIntersection(object);
        return zoneRepository.create(object);
    }

    @Override
    public List<Zone> read(Map<String, String> filters) {
        List<Zone> zones = zoneRepository.read(filters);
        return ListUtils.mapList(zones, this::addPercentageToZone);
    }

    @Override
    public Zone read(int id) {
        Zone zone = zoneRepository.read(id);
        return addPercentageToZone(zone);
    }

    @Override
    public void update(Zone object) {
        Zone savedZone = read(object.getId());

        savedZone.setName(object.getName());
        savedZone.setMaxCount(object.getMaxCount());
        savedZone.setPerimeter(object.getPerimeter());
        savedZone.setModified(new Date());

        zoneRepository.update(savedZone);
        handleAreaIntersection(object);
    }

    @Override
    public void delete(int id) {
        // delete zone association with sensors
        deleteZoneSensors(id);
        // delete sensors that don't have any zone associated with them
        deleteSensorsWithoutZone();
        Zone zone = new Zone(id);
        zoneRepository.delete(zone);
    }

    @Override
    public List<Prediction> getPredictions(int zone, Date start, Date end, int interval) {
        Date startCopy = new Date(start.getTime());
        Date endCopy = new Date(end.getTime());
        List<Integer> predictionCounts = percentageAlgorithm.calculate(repositoryFacade, startCopy, endCopy, interval, zone);
        List<Prediction> result = new LinkedList<>();

        int predictionCountIndex = 0;
        while(start.before(end) && predictionCountIndex < predictionCounts.size()) {
            int predictionCount = predictionCounts.get(predictionCountIndex);
            Prediction prediction = new Prediction(new Date(start.getTime()), predictionCount);
            result.add(prediction);

            ++predictionCountIndex;
            start.setTime(start.getTime() + (interval * MILLISECONDS_IN_MINUTES));
        }

        return result;
    }

    private Zone addPercentageToZone(Zone zone) {
        int occupiedPercentage = percentageAlgorithm.calculate(repositoryFacade, zone.getId());
        zone.setOccupiedPercentage(occupiedPercentage);
        return zone;
    }

    private void handleAreaIntersection(Zone zone) {
        zoneRepository.read(new HashMap<>())
                .stream()
                .filter(z -> z.getId() != zone.getId())
                .filter(z -> geometryManager.intersects(z.getPerimeter(), zone.getPerimeter()))
                .forEach(z -> {
                    z.setPerimeter(geometryManager.difference(z.getPerimeter(), zone.getPerimeter()));
                    zoneRepository.update(z);
                });
    }

    private void deleteZoneSensors(int zone) {
        // delete zone association with sensors
        sensorRepository.read(new HashMap<>())
                .stream()
                // get each sensor
                .map(sensor -> sensorRepository.read(sensor.getId()))
                // filters which sensors have the zone
                .filter(sensor -> isSensorInZone(sensor, zone))
                // removes zone from sensors
                .map(sensor -> removeZoneFromSensor(sensor, zone))
                // updates sensor
                .forEach(sensor -> sensorRepository.update(sensor));
    }

    private boolean isSensorInZone(Sensor sensor, int zone) {
        return sensor
                .getZones()
                .stream()
                .anyMatch(sensorZone -> sensorZone.getId() == zone);
    }

    private Sensor removeZoneFromSensor(Sensor sensor, int zone) {
        sensor
                .getZones()
                .removeIf(sensorZone -> sensorZone.getId() == zone);
        return sensor;
    }

    private void deleteSensorsWithoutZone() {
        sensorRepository.read(new HashMap<>())
                .stream()
                // get each sensor
                .map(sensor -> sensorRepository.read(sensor.getId()))
                // filters sensors that don't have any zone associated with them
                .filter(sensor -> sensor.getZones().size() == 0)
                // deletes those sensors
                .forEach(sensor -> sensorRepository.delete(sensor));
    }
}
