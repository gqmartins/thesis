package pt.isel.tese.smartParkingIot.api.logic.managers.geometry.contracts;

import pt.isel.tese.smartParkingIot.api.model.util.GeoJsonPolygon;

public interface IGeometryManager {

    boolean intersects(GeoJsonPolygon firstPolygon, GeoJsonPolygon secondPolygon);
    GeoJsonPolygon difference(GeoJsonPolygon firstPolygon, GeoJsonPolygon secondPolygon);
}
