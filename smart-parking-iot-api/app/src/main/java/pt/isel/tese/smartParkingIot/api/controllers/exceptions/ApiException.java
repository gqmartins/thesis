package pt.isel.tese.smartParkingIot.api.controllers.exceptions;

import org.springframework.http.HttpStatus;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ErrorResponse;

public class ApiException extends RuntimeException {

    private ErrorResponse error;
    private Exception exception;

    public ApiException (String title, String detail, HttpStatus status, String instance, String type, Exception ex) {
        super(title);
        this.error = new ErrorResponse(title, detail, status);
        this.exception = ex;
        if(type!= null && !type.equals("")) {
            this.error.setType(type);
        }
        if(instance!= null && !instance.equals("")) {
            this.error.setInstance(instance);
        }
    }

    public ApiException(String title, String detail, HttpStatus status) {
        this(title, detail, status, null, null,null);
    }

    public ApiException(String title, String detail, HttpStatus status, Exception ex) {
        this(title, detail, status, "string", "about:blank", ex);
    }

    public ErrorResponse getError() {
        return this.error;
    }

    public String getExceptionDetail() {
        return this.exception != null ? exception.getMessage() : this.error.getDetail();
    }
}
