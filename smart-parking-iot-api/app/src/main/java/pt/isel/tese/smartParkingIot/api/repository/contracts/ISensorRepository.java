package pt.isel.tese.smartParkingIot.api.repository.contracts;

import pt.isel.tese.smartParkingIot.api.model.Sensor;

public interface ISensorRepository extends IBaseRepository<Sensor> { }
