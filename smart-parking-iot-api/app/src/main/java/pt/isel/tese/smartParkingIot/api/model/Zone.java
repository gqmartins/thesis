package pt.isel.tese.smartParkingIot.api.model;

import pt.isel.tese.smartParkingIot.api.model.util.GeoJsonPolygon;

import java.util.Date;

public class Zone {

    private int id;
    private String name;
    private int maxCount;
    private int currentCount;
    private int occupiedPercentage;
    private GeoJsonPolygon perimeter;
    private Date created;
    private Date modified;

    public Zone(int id) {
        this.id = id;
    }

    public Zone(String name, int maxCount, int currentCount, GeoJsonPolygon perimeter) {
        this.name = name;
        this.maxCount = maxCount;
        this.currentCount = currentCount;
        this.perimeter = perimeter;
    }

    public Zone(int id, String name, int maxCount, int currentCount, int occupiedPercentage, GeoJsonPolygon perimeter, Date created, Date modified) {
        this(name, maxCount, currentCount, perimeter);
        this.id = id;
        this.occupiedPercentage = occupiedPercentage;
        this.created = created;
        this.modified = modified;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(int maxCount) {
        this.maxCount = maxCount;
    }

    public int getCurrentCount() {
        return currentCount;
    }

    public void setCurrentCount(int currentCount) {
        this.currentCount = currentCount;
    }

    public int getOccupiedPercentage() {
        return occupiedPercentage;
    }

    public void setOccupiedPercentage(int occupiedPercentage) {
        this.occupiedPercentage = occupiedPercentage;
    }

    public GeoJsonPolygon getPerimeter() {
        return perimeter;
    }

    public void setPerimeter(GeoJsonPolygon perimeter) {
        this.perimeter = perimeter;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }
}
