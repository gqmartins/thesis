package pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.mappers;

import pt.isel.tese.smartParkingIot.api.model.SensorType;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto.SensorTypeDTO;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.utils.DateUtils;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.utils.FieldUtils;

import java.sql.Timestamp;
import java.util.List;

public class SensorTypeRepositoryMapper implements IRepositoryMapper<SensorType, SensorTypeDTO> {

    @Override
    public SensorType transformToObject(SensorTypeDTO dto) {
        List<String> fields = FieldUtils.transformToList(dto.getFields());
        return new SensorType(dto.getId(), dto.getName(), fields, dto.getCreated(), dto.getModified());
    }

    @Override
    public SensorTypeDTO transformToDto(SensorType model) {
        Timestamp created = DateUtils.convertDateToTimestamp(model.getCreated());
        Timestamp modified = DateUtils.convertDateToTimestamp(model.getModified());
        String fields = FieldUtils.transformToString(model.getFields());
        return new SensorTypeDTO(model.getId(), model.getName(), fields, created, modified);
    }

    @Override
    public int getIdentifier(SensorType model) {
        return model.getId();
    }
}
