package pt.isel.tese.smartParkingIot.api.repository.contracts;

import pt.isel.tese.smartParkingIot.api.model.Zone;

public interface IZoneRepository extends IBaseRepository<Zone> { }
