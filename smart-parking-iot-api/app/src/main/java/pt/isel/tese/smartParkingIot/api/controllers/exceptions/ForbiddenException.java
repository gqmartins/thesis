package pt.isel.tese.smartParkingIot.api.controllers.exceptions;

import org.springframework.http.HttpStatus;

public class ForbiddenException extends ApiException {

    public ForbiddenException(String title, String detail) {
        super(title, detail, HttpStatus.FORBIDDEN);
    }

    public ForbiddenException(String title, String detail, HttpStatus status, String instance, String type, Exception ex) {
        super(title, detail, status, instance, type,ex);
    }
}

