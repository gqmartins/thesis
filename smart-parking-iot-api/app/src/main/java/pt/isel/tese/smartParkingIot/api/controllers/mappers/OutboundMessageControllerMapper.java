package pt.isel.tese.smartParkingIot.api.controllers.mappers;

import org.springframework.stereotype.Component;
import pt.isel.tese.smartParkingIot.api.controllers.config.UriConfiguration;
import pt.isel.tese.smartParkingIot.api.controllers.model.request.OutboundMessageRequest;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ListItemResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.OutboundMessageResponse;
import pt.isel.tese.smartParkingIot.api.controllers.utils.ResponseUtils;
import pt.isel.tese.smartParkingIot.api.controllers.utils.URIBuilder;
import pt.isel.tese.smartParkingIot.api.model.OutboundMessage;

@Component
public class OutboundMessageControllerMapper {

    public OutboundMessage transformToModel(OutboundMessageRequest request, int sensorId) {
        return new OutboundMessage(request.getFieldValues(), sensorId, request.getType());
    }

    public OutboundMessageResponse transformToResponse(OutboundMessage model) {
        String created = ResponseUtils.convertDateToString(model.getCreated());
        return new OutboundMessageResponse(model.getBody(), model.getType(), model.getId(), created);
    }

    public ListItemResponse transformToListItem(OutboundMessage model) {
        String href = URIBuilder.getCreatedURI(UriConfiguration.OUTBOUND_MESSAGE_ID_URI, model.getSensor(), model.getId()).toString();
        String created = ResponseUtils.convertDateToString(model.getCreated());
        return new ListItemResponse(model.getId(), null, created, null, href);
    }
}
