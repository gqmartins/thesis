package pt.isel.tese.smartParkingIot.api.controllers.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.util.Map;

public class OutboundMessageRequest {

    @JsonProperty("fieldValues")
    private Map<String, String> fieldValues = null;

    @NotNull(message = "An outbound message type must be provided.")
    @JsonProperty("type")
    private Integer type = null;

    public OutboundMessageRequest() { }

    public OutboundMessageRequest(Map<String, String> fieldValues, Integer type) {
        this.fieldValues = fieldValues;
        this.type = type;
    }

    /**
     * Get type
     * @return type
     **/
    @ApiModelProperty(example = "1", value = "")
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * Get fieldValues
     * @return fieldValues
     **/
    @ApiModelProperty(example = "", value = "")
    public Map<String, String> getFieldValues() {
        return fieldValues;
    }

    public void setFieldValues(Map<String, String> fieldValues) {
        this.fieldValues = fieldValues;
    }
}
