package pt.isel.tese.smartParkingIot.api.controllers.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

public class UserRequest {

    @JsonProperty("name")
    @NotNull(message = "A name must be provided.")
    private String name = null;

    @JsonProperty("username")
    @NotNull(message = "A username must be provided.")
    private String username = null;

    @JsonProperty("role")
    @NotNull(message = "A role must be provided.")
    private int role = 0;

    @JsonProperty("password")
    @NotNull(message = "A password must be provided.")
    private String password = null;

    public UserRequest() { }

    public UserRequest(String name, String username, int role) {
        this.name = name;
        this.username = username;
        this.role = role;
    }

    /**
     * Get name
     * @return name
     **/
    @ApiModelProperty(example = "carlos jorge", required = true, value = "")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get username
     * @return username
     **/
    @ApiModelProperty(example = "c_jorge", required = true, value = "")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Get role
     * @return role
     **/
    @ApiModelProperty(example = "1", required = true, value = "")
    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    /**
     * Get password
     * @return password
     **/
    @ApiModelProperty(example = "*****", required = true, value = "")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
