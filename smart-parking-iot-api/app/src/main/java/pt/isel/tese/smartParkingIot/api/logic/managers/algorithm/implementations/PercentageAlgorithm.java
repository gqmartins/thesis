package pt.isel.tese.smartParkingIot.api.logic.managers.algorithm.implementations;

import org.hibernate.query.criteria.internal.expression.function.AggregationFunction;
import org.springframework.stereotype.Component;
import pt.isel.tese.smartParkingIot.api.model.Zone;
import pt.isel.tese.smartParkingIot.api.repository.RepositoryFacade;
import pt.isel.tese.smartParkingIot.api.logic.managers.algorithm.contracts.IPercentageAlgorithm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Component
public class PercentageAlgorithm implements IPercentageAlgorithm {

    private static final int COUNT_VALUE = 10;
    private static final int MILLISECONDS_IN_MINUTES = 60000;
    private static final int ADD_LIMIT = 50;

    @Override
    public int calculate(RepositoryFacade repositoryFacade, int zone) {
        Zone currentZone = repositoryFacade.getZoneRepository().read(zone);
        return (currentZone.getCurrentCount() * 100) / currentZone.getMaxCount();
    }

    @Override
    public List<Integer> calculate(RepositoryFacade repositoryFacade, Date start, Date end, int interval, int zone) {
        Zone savedZone = repositoryFacade.getZoneRepository().read(zone);
        List<Integer> percentageList = new ArrayList<>();
        int currentCount = savedZone.getMaxCount() / 2;
        while(start.before(end)) {
            int calculatedPercentage = calculatePercentageValue(currentCount, savedZone.getMaxCount());
            percentageList.add(calculatedPercentage);
            start.setTime(start.getTime() + (interval * MILLISECONDS_IN_MINUTES));
            currentCount = calculatedPercentage;
        }
        return percentageList;
    }

    private int calculatePercentageValue(int currentCount, int maxCount) {
        int randomNum = ThreadLocalRandom.current().nextInt(0, 99 + 1);
        if((randomNum < ADD_LIMIT && (currentCount + COUNT_VALUE < maxCount)) || currentCount - COUNT_VALUE <= 0) {
            return currentCount + COUNT_VALUE;
        }
        return currentCount - COUNT_VALUE;
    }
}
