package pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Uri")
public class UriDTO {

    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "uri")
    private String uri;

    @Column(name = "method")
    private String method;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "uris")
    private List<RoleDTO> roles = new ArrayList<>();

    public UriDTO() { }

    public UriDTO(int id) {
        this.id = id;
    }

    public UriDTO(int id, String uri, String method, List<RoleDTO> roles) {
        this.id = id;
        this.uri = uri;
        this.method = method;
        this.roles = roles;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getMethod() {
        return method;
    }

    public List<RoleDTO> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleDTO> roles) {
        this.roles = roles;
    }
}
