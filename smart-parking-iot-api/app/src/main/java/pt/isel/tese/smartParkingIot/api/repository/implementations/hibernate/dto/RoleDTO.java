package pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Role")
public class RoleDTO {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "Role_Uri", joinColumns = { @JoinColumn(name = "role_id") }, inverseJoinColumns = { @JoinColumn(name = "uri_id") })
    private List<UriDTO> uris = new ArrayList<>();

    public RoleDTO() { }

    public RoleDTO(int id) {
        this.id = id;
    }

    public RoleDTO(int id, String name, List<UriDTO> uris) {
        this.id = id;
        this.name = name;
        this.uris = uris;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<UriDTO> getUris() {
        return uris;
    }

    public void setUris(List<UriDTO> uris) {
        this.uris = uris;
    }
}
