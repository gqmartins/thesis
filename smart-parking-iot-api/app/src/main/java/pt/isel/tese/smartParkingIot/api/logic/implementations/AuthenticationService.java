package pt.isel.tese.smartParkingIot.api.logic.implementations;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pt.isel.tese.smartParkingIot.api.logic.exceptions.IncorrectLoginException;
import pt.isel.tese.smartParkingIot.api.model.User;
import pt.isel.tese.smartParkingIot.api.repository.RepositoryFacade;
import pt.isel.tese.smartParkingIot.api.repository.contracts.IUserRepository;
import pt.isel.tese.smartParkingIot.api.logic.contracts.IAuthenticationService;

import java.util.HashMap;

@Service
public class AuthenticationService implements IAuthenticationService {

    private IUserRepository userRepository;

    public AuthenticationService(RepositoryFacade repositoryFacade) {
        this.userRepository = repositoryFacade.getUserRepository();
    }

    @Override
    public User login(String username, String password) {
        int userId = getUserId(username);
        User savedUser = userRepository.read(userId);

        if(!verifyPassword(password, savedUser.getPassword())) {
            throw new IncorrectLoginException();
        }

        return savedUser;
    }

    @Override
    public void logout(String username) {

    }

    private int getUserId(String username) {
        return userRepository
                .read(new HashMap<>())
                .stream()
                .filter(user -> user.getUsername().equals(username))
                .findFirst()
                .get()
                .getId();
    }

    private boolean verifyPassword(String rawPassword, String encodedPassword) {
        BCryptPasswordEncoder passwordDecoder = new BCryptPasswordEncoder();
        return passwordDecoder.matches(rawPassword, encodedPassword);
    }
}
