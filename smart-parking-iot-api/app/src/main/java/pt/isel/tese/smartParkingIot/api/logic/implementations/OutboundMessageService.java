package pt.isel.tese.smartParkingIot.api.logic.implementations;

import org.springframework.stereotype.Service;
import pt.isel.tese.smartParkingIot.api.logic.managers.messageSender.SensorIoTManagementSupplier;
import pt.isel.tese.smartParkingIot.api.logic.managers.messageSender.contract.ISensorMessageSender;
import pt.isel.tese.smartParkingIot.api.logic.utils.FieldUtils;
import pt.isel.tese.smartParkingIot.api.model.OutboundMessage;
import pt.isel.tese.smartParkingIot.api.model.OutboundMessageType;
import pt.isel.tese.smartParkingIot.api.model.Sensor;
import pt.isel.tese.smartParkingIot.api.repository.RepositoryFacade;
import pt.isel.tese.smartParkingIot.api.repository.contracts.IOutboundMessageRepository;
import pt.isel.tese.smartParkingIot.api.repository.contracts.IOutboundMessageTypeRepository;
import pt.isel.tese.smartParkingIot.api.repository.contracts.ISensorRepository;
import pt.isel.tese.smartParkingIot.api.logic.contracts.IOutboundMessageService;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OutboundMessageService implements IOutboundMessageService {

    private IOutboundMessageRepository outboundMessageRepository;
    private IOutboundMessageTypeRepository outboundMessageTypeRepository;
    private ISensorRepository sensorRepository;
    private SensorIoTManagementSupplier sensorManagementSupplier;

    public OutboundMessageService(RepositoryFacade repositoryFacade, SensorIoTManagementSupplier sensorManagementSupplier) {
        this.outboundMessageRepository = repositoryFacade.getOutboundMessageRepository();
        this.outboundMessageTypeRepository = repositoryFacade.getOutboundMessageTypeRepository();
        this.sensorRepository = repositoryFacade.getSensorRepository();
        this.sensorManagementSupplier = sensorManagementSupplier;
    }

    @Override
    public int create(OutboundMessage object) {
        OutboundMessageType outboundMessageType = outboundMessageTypeRepository.read(object.getType());
        FieldUtils.checkForFields(outboundMessageType.getFields(), object.getBody());

        Sensor sensor = sensorRepository.read(object.getSensor());
        ISensorMessageSender sensorManagement = sensorManagementSupplier.getSensorManagement(sensor.getSensorType());
        sensorManagement.send(sensor, object);

        object.setCreated(new Date());
        return outboundMessageRepository.create(object);
    }

    @Override
    public List<OutboundMessage> read(Map<String, String> filters) {
        return outboundMessageRepository.read(new HashMap<>());
    }

    @Override
    public OutboundMessage read(int id) {
        return outboundMessageRepository.read(id);
    }

    @Override
    public void update(OutboundMessage object) {
        throw new NotImplementedException();
    }

    @Override
    public void delete(int id) {
        throw new NotImplementedException();
    }
}
