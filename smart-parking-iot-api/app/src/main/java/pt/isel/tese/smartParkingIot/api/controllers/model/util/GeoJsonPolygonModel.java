package pt.isel.tese.smartParkingIot.api.controllers.model.util;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class GeoJsonPolygonModel {

    @JsonProperty("type")
    private String type = "Polygon";

    @JsonProperty("coordinates")
    private List<List<List<Float>>> coordinates = new ArrayList<List<List<Float>>>();

    public GeoJsonPolygonModel() { }

    public GeoJsonPolygonModel(List<List<List<Float>>> coordinates) {
        this.coordinates = coordinates;
    }

    /**
     * The array for each coordinate must have at least two points.
     * @return coordinates
     **/
    @ApiModelProperty(required = true, value = "The array for each coordinate must have at least two points.")
    @NotNull
    public List<List<List<Float>>> getCoordinates() {
        return coordinates;
    }

    /**
     * Get type
     * @return type
     **/
    @ApiModelProperty(example = "Polygon", required = true, value = "")
    @NotNull
    public String getType() {
        return type;
    }
}
