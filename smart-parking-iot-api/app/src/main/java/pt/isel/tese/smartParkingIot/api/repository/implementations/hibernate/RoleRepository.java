package pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate;

import org.springframework.stereotype.Repository;
import pt.isel.tese.smartParkingIot.api.model.Role;
import pt.isel.tese.smartParkingIot.api.model.util.PermissionUri;
import pt.isel.tese.smartParkingIot.api.repository.contracts.IRoleRepository;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto.RoleDTO;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.mappers.RoleRepositoryMapper;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class RoleRepository extends BaseRepository<Role, RoleDTO> implements IRoleRepository {

    private static final Class<RoleDTO> ROLE_DTO_CLASS = RoleDTO.class;
    private static final RoleRepositoryMapper roleRepositoryMapper = new RoleRepositoryMapper();

    public RoleRepository() {
        super(ROLE_DTO_CLASS, roleRepositoryMapper);
    }

    @Override
    public List<PermissionUri> getUriByRole(int role) {
        RoleDTO roleDTO = map(session -> session.get(ROLE_DTO_CLASS, role));
        return roleDTO
                .getUris()
                .stream()
                .map(roleRepositoryMapper::transformToPermissionUri)
                .collect(Collectors.toList());
    }


}
