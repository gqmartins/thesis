package pt.isel.tese.smartParkingIot.api.model.util;

public class SensorZone {

    private int id;
    private boolean enterExit;

    public SensorZone(int id, boolean enterExit) {
        this.id = id;
        this.enterExit = enterExit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isEnterExit() {
        return enterExit;
    }

    public void setEnterExit(boolean enterExit) {
        this.enterExit = enterExit;
    }
}
