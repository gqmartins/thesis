package pt.isel.tese.smartParkingIot.api.controllers.model.util;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

public class OutboundMessageFieldModel {

    @JsonProperty("name")
    private String name = null;

    public OutboundMessageFieldModel() { }

    public OutboundMessageFieldModel(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    /**
     * Get name
     * @return name
     **/
    @ApiModelProperty(example = "time", required = true, value = "")
    @NotNull
    public void setName(String name) {
        this.name = name;
    }
}
