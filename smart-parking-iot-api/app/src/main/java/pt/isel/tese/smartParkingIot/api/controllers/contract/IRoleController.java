package pt.isel.tese.smartParkingIot.api.controllers.contract;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ErrorResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.RoleResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ZoneResponse;

import javax.validation.Valid;
import java.util.List;

import static pt.isel.tese.smartParkingIot.api.controllers.config.UriConfiguration.ROLES_URI;
import static pt.isel.tese.smartParkingIot.api.controllers.config.UriConfiguration.ROLES_URI_ID;

@Api(value = "sensor", description = "the sensor API")
public interface IRoleController {

    @ApiOperation(value = "returns roles added to the system", nickname = "readRoles", notes = "returns all the roles that were added to the system, ordered by creation date", response = RoleResponse.class, responseContainer = "List", tags={ "Role" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "roles result matching search criteria", response = RoleResponse.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad request parameter", response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class) })
    @GetMapping(value = ROLES_URI, produces = { "application/json" })
    ResponseEntity<List<RoleResponse>> readRoles(@ApiParam(value = "number of page for pagination results") @Valid @RequestParam(value = "page", required = false) Integer page, @ApiParam(value = "number of elements to return in a page") @Valid @RequestParam(value = "size", required = false) Integer size);

    @ApiOperation(value = "returns role with the specified role-id", nickname = "readRole", notes = "returns zone with the specified zone-id", response = RoleResponse.class, tags={ "Role" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "role with the specified role-id", response = RoleResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "The specified role does not exist", response = ErrorResponse.class) })
    @GetMapping(value = ROLES_URI_ID, produces = { "application/json" })
    ResponseEntity<RoleResponse> readRole(@ApiParam(value = "role id",required=true) @PathVariable("role-id") Integer roleId);
}
