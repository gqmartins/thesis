package pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate;

import org.springframework.stereotype.Repository;
import pt.isel.tese.smartParkingIot.api.model.SensorType;
import pt.isel.tese.smartParkingIot.api.repository.contracts.ISensorTypeRepository;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto.SensorTypeDTO;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.mappers.SensorTypeRepositoryMapper;

@Repository
public class SensorTypeRepository extends BaseRepository<SensorType, SensorTypeDTO> implements ISensorTypeRepository {

    public SensorTypeRepository(OutboundMessageTypeRepository outboundMessageTypeRepository) {
        super(SensorTypeDTO.class, new SensorTypeRepositoryMapper());
    }
}
