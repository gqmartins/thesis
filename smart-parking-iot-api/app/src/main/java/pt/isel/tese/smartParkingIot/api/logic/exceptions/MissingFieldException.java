package pt.isel.tese.smartParkingIot.api.logic.exceptions;

public class MissingFieldException extends LogicException {

    public MissingFieldException(String field) {
        super("Missing field value.", "There's no field value for field " + field + ".");
    }

    public MissingFieldException(String title, String detail) {
        super(title, detail);
    }
}
