package pt.isel.tese.smartParkingIot.api.controllers.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import pt.isel.tese.smartParkingIot.api.controllers.model.util.GeoJsonPolygonModel;

import javax.validation.constraints.NotNull;

public class ZoneRequest {

    @JsonProperty("name")
    @NotNull(message = "A name must be provided.")
    private String name = null;

    @JsonProperty("maxCount")
    @NotNull(message = "A max count must be provided.")
    private Integer maxCount = null;

    @JsonProperty("currentCount")
    private Integer currentCount = 0;

    @JsonProperty("perimeter")
    @NotNull(message = "A perimeter must be provided.")
    private GeoJsonPolygonModel perimeter = null;

    public ZoneRequest() { }

    public ZoneRequest(String name, Integer maxCount, Integer currentCount, GeoJsonPolygonModel perimeter) {
        this.name = name;
        this.maxCount = maxCount;
        this.currentCount = currentCount;
        this.perimeter = perimeter;
    }

    /**
     * Get name
     * @return name
     **/
    @ApiModelProperty(example = "zone 1", required = true, value = "")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get maxCount
     * @return maxCount
     **/
    @ApiModelProperty(example = "150", required = true, value = "")
    public Integer getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(Integer maxCount) {
        this.maxCount = maxCount;
    }

    /**
     * Get currentCount
     * @return currentCount
     **/
    @ApiModelProperty(example = "75", value = "")
    public Integer getCurrentCount() {
        return currentCount;
    }

    public void setCurrentCount(Integer currentCount) {
        this.currentCount = currentCount;
    }

    /**
     * Get perimeter
     * @return perimeter
     **/
    @ApiModelProperty(required = true, value = "")
    public GeoJsonPolygonModel getPerimeter() {
        return perimeter;
    }

    public void setPerimeter(GeoJsonPolygonModel perimeter) {
        this.perimeter = perimeter;
    }
}
