package pt.isel.tese.smartParkingIot.api.controllers.exceptions;

import org.springframework.http.HttpStatus;

public class BadRequestException extends ApiException {

    public BadRequestException(String title, String detail, Exception ex) {
        super(title, detail, HttpStatus.BAD_REQUEST, ex);
    }

    public BadRequestException(String title, String detail) {
        this(title, detail, null);
    }

    public BadRequestException(String title, String detail, HttpStatus status, String instance, String type, Exception ex) {
        super(title, detail, status, instance, type, ex);
    }
}
