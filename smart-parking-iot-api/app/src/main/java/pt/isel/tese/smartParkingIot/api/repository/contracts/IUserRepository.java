package pt.isel.tese.smartParkingIot.api.repository.contracts;

import pt.isel.tese.smartParkingIot.api.model.User;

public interface IUserRepository extends IBaseRepository<User> { }
