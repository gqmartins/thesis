package pt.isel.tese.smartParkingIot.api.model.util;

public class PermissionUri {

    private String method;
    private String path;

    public PermissionUri(String method, String path) {
        this.method = method;
        this.path = path;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
