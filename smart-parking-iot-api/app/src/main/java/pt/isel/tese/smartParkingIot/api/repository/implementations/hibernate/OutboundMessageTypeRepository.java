package pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate;

import org.springframework.stereotype.Repository;
import pt.isel.tese.smartParkingIot.api.model.OutboundMessageType;
import pt.isel.tese.smartParkingIot.api.repository.contracts.IOutboundMessageTypeRepository;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto.OutboundMessageTypeDTO;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.mappers.OutboundMessageTypeRepositoryMapper;

import java.util.HashMap;

@Repository
public class OutboundMessageTypeRepository extends BaseRepository<OutboundMessageType, OutboundMessageTypeDTO> implements IOutboundMessageTypeRepository {

    private static final Class<OutboundMessageTypeDTO> OUTBOUND_MESSAGE_TYPE_CLASS = OutboundMessageTypeDTO.class;

    protected OutboundMessageTypeRepository() {
        super(OUTBOUND_MESSAGE_TYPE_CLASS, new OutboundMessageTypeRepositoryMapper());
    }
    /*
    @Override
    public int create(OutboundMessageType object) {
        int createdId = super.create(object);
        object.setId(createdId);
        createOutboundMessageFields(object);
        return createdId;
    }

    @Override
    public void update(OutboundMessageType object) {
        super.update(object);
        deleteOutboundMessageFields(object);
        createOutboundMessageFields(object);
    }

    @Override
    public void delete(OutboundMessageType object) {
        deleteOutboundMessageFields(object);
        super.delete(object);
    }

    private void createOutboundMessageFields(OutboundMessageType object) {
        object
                .getFields()
                .forEach(fieldModel -> {
                    OutboundMessageFieldDTO field = OutboundMessageFieldMapper.transformToOutboundMessageFieldDTO(fieldModel, object.getId());
                    outboundMessageFieldRepository.create(field);
                });
    }

    private void deleteOutboundMessageFields(OutboundMessageType object) {
        outboundMessageFieldRepository
                .read(new HashMap<>())
                .stream()
                .filter(field -> field.getOutboundMessageType().getId() == object.getId())
                .forEach(outboundMessageFieldRepository::delete);
    }
    */
}
