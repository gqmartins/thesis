package pt.isel.tese.smartParkingIot.api.controllers.contract;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.isel.tese.smartParkingIot.api.controllers.model.request.SensorRequest;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ErrorResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ListItemResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.SensorResponse;

import javax.validation.Valid;
import java.util.List;

import static pt.isel.tese.smartParkingIot.api.controllers.config.UriConfiguration.SENSORS_ID_URI;
import static pt.isel.tese.smartParkingIot.api.controllers.config.UriConfiguration.SENSORS_URI;

@Api(value = "sensor", description = "the sensor API")
public interface ISensorController {

    @ApiOperation(value = "adds a sensor to the system", nickname = "createSensor", notes = "adds a sensor to the system", tags={ "Sensor" })
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "sensor successfully added to the system. to get the sensor added to the system make a get request to the response header location URI."),
            @ApiResponse(code = 400, message = "Bad request parameter", response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class) })
    @PostMapping(value = SENSORS_URI, produces = { "application/json" }, consumes = { "application/json" })
    ResponseEntity<Void> createSensor(@ApiParam(value = "Sensor to add"  )  @Valid @RequestBody SensorRequest body);

    @ApiOperation(value = "deletes the sensor with the specified sensor-id", nickname = "deleteSensor", notes = "delete the sensor with the specified sensor-id", tags={ "Sensor", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "sensor successfully deleted."),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "The specified sensor does not exist", response = ErrorResponse.class) })
    @DeleteMapping(value = SENSORS_ID_URI, produces = { "application/json" })
    ResponseEntity<Void> deleteSensor(@ApiParam(value = "sensor id",required=true) @PathVariable("sensor-id") Integer sensorId);

    @ApiOperation(value = "edits the sensor with the specified sensor-id", nickname = "editSensor", notes = "edits the sensor with the specified sensor-id", tags={ "Sensor" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "sensor successfully edited. to get the sensor edited make a GET request to the URI used to edit the sensor."),
            @ApiResponse(code = 400, message = "Bad request parameter", response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "The specified sensor does not exist", response = ErrorResponse.class) })
    @PutMapping(value = SENSORS_ID_URI, produces = { "application/json" }, consumes = { "application/json" })
    ResponseEntity<Void> editSensor(@ApiParam(value = "sensor id",required=true) @PathVariable("sensor-id") Integer sensorId,@ApiParam(value = "sensor to edit"  )  @Valid @RequestBody SensorRequest body);

    @ApiOperation(value = "returns sensor with the specified sensor-id", nickname = "readSensor", notes = "returns sensor with the specified sensor-id", response = SensorResponse.class, tags={ "Sensor" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "sensor with the specified sensor-id", response = SensorResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "The specified sensor does not exist", response = ErrorResponse.class) })
    @GetMapping(value = SENSORS_ID_URI, produces = { "application/json" })
    ResponseEntity<SensorResponse> readSensor(@ApiParam(value = "sensor id",required=true) @PathVariable("sensor-id") Integer sensorId);

    @ApiOperation(value = "returns sensors added to the system", nickname = "readSensors", notes = "returns all the sensors that were added to the system, ordered by creation date", response = ListItemResponse.class, responseContainer = "List", tags={ "Sensor" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "sensor result matching search criteria", response = ListItemResponse.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad request parameter", response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class) })
    @GetMapping(value = SENSORS_URI, produces = { "application/json" })
    ResponseEntity<List<ListItemResponse>> readSensors(@ApiParam(value = "number of page for pagination results") @Valid @RequestParam(value = "page", required = false) Integer page, @ApiParam(value = "number of elements to return in a page") @Valid @RequestParam(value = "size", required = false) Integer size);
}
