package pt.isel.tese.smartParkingIot.api.logic.exceptions;

public class DeleteAdminUserException extends LogicException {

    public DeleteAdminUserException() {
        super("Can't delete user.", "There has to be at least one user with the role Admin in the system.");
    }

    public DeleteAdminUserException(String title, String detail) {
        super(title, detail);
    }
}
