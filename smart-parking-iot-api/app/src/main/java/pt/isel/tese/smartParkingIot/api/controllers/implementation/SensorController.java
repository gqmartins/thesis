package pt.isel.tese.smartParkingIot.api.controllers.implementation;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pt.isel.tese.smartParkingIot.api.controllers.config.UriConfiguration;
import pt.isel.tese.smartParkingIot.api.controllers.contract.ISensorController;
import pt.isel.tese.smartParkingIot.api.controllers.mappers.SensorControllerMapper;
import pt.isel.tese.smartParkingIot.api.controllers.model.request.SensorRequest;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ListItemResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.SensorResponse;
import pt.isel.tese.smartParkingIot.api.controllers.utils.URIBuilder;
import pt.isel.tese.smartParkingIot.api.model.Sensor;
import pt.isel.tese.smartParkingIot.api.logic.ServiceFacade;
import pt.isel.tese.smartParkingIot.api.logic.contracts.ISensorService;
import pt.isel.tese.smartParkingIot.api.utils.ListUtils;

import javax.validation.Valid;
import java.net.URI;
import java.util.HashMap;
import java.util.List;

@RestController
public class SensorController implements ISensorController {

    private ISensorService sensorService;
    private SensorControllerMapper sensorMapper;

    public SensorController(ServiceFacade serviceFacade, SensorControllerMapper sensorMapper) {
        this.sensorService = serviceFacade.getSensorService();
        this.sensorMapper = sensorMapper;
    }

    @Override
    public ResponseEntity<Void> createSensor(@Valid @RequestBody SensorRequest body) {
        Sensor sensor = sensorMapper.transformToModel(body);
        int createdSensorId = sensorService.create(sensor);
        URI createdUri = URIBuilder.getCreatedURI(UriConfiguration.SENSORS_ID_URI, createdSensorId);
        return ResponseEntity.created(createdUri).build();
    }

    @Override
    public ResponseEntity<Void> deleteSensor(@PathVariable("sensor-id") Integer sensorId) {
        sensorService.delete(sensorId);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<Void> editSensor(@PathVariable("sensor-id") Integer sensorId, @Valid @RequestBody SensorRequest body) {
        Sensor sensor = sensorMapper.transformToModel(body);
        sensor.setId(sensorId);
        sensorService.update(sensor);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<SensorResponse> readSensor(@PathVariable("sensor-id") Integer sensorId) {
        Sensor sensor = sensorService.read(sensorId);
        SensorResponse response = sensorMapper.transformToResponse(sensor);
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<List<ListItemResponse>> readSensors(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size) {
        List<Sensor> sensors = sensorService.read(new HashMap<>());
        List<ListItemResponse> response = ListUtils.mapList(sensors, sensorMapper::transformToListItem);
        return ResponseEntity.ok(response);
    }
}
