package pt.isel.tese.smartParkingIot.api.model;

import pt.isel.tese.smartParkingIot.api.model.util.OutboundMessageField;

import java.util.Date;
import java.util.List;

public class OutboundMessageType {

    private int id;
    private String name;
    private List<String> fields;
    private int sensorTypeId;
    private Date created;
    private Date modified;

    public OutboundMessageType(int id) {
        this.id = id;
    }

    public OutboundMessageType(String name, List<String> fields, int sensorTypeId) {
        this.name = name;
        this.fields = fields;
        this.sensorTypeId = sensorTypeId;
    }

    public OutboundMessageType(int id, String name, List<String> fields, int sensorTypeId, Date created, Date modified) {
        this(name, fields, sensorTypeId);
        this.id = id;
        this.created = created;
        this.modified = modified;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    public int getSensorTypeId() {
        return sensorTypeId;
    }

    public void setSensorTypeId(int sensorTypeId) {
        this.sensorTypeId = sensorTypeId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }
}
