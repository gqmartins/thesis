package pt.isel.tese.smartParkingIot.api.logic.managers.messageSender.contract;

import pt.isel.tese.smartParkingIot.api.model.OutboundMessage;
import pt.isel.tese.smartParkingIot.api.model.Sensor;

public interface ISensorMessageSender {

    void send(Sensor sensor, OutboundMessage message);
}
