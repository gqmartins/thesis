package pt.isel.tese.smartParkingIot.api.controllers.mappers;

import org.springframework.stereotype.Component;
import pt.isel.tese.smartParkingIot.api.controllers.config.UriConfiguration;
import pt.isel.tese.smartParkingIot.api.controllers.model.request.UserRequest;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ListItemResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.RoleResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.UserResponse;
import pt.isel.tese.smartParkingIot.api.controllers.utils.ResponseUtils;
import pt.isel.tese.smartParkingIot.api.controllers.utils.URIBuilder;
import pt.isel.tese.smartParkingIot.api.model.User;

@Component
public class UserControllerMapper implements IControllerMapper<User, UserRequest, UserResponse> {

    @Override
    public User transformToModel(UserRequest request) {
        return new User(request.getUsername(), request.getName(), request.getRole(), request.getPassword());
    }

    @Override
    public UserResponse transformToResponse(User model) {
        String created = ResponseUtils.convertDateToString(model.getCreated());
        String modified = ResponseUtils.convertDateToString(model.getModified());
        return new UserResponse(model.getId(), model.getName(), model.getUsername(), new RoleResponse(model.getRole(), ""),
                created, modified);
    }

    @Override
    public ListItemResponse transformToListItem(User model) {
        String href = URIBuilder.getCreatedURI(UriConfiguration.USERS_ID_URI, model.getId()).toString();
        String created = ResponseUtils.convertDateToString(model.getCreated());
        String modified = ResponseUtils.convertDateToString(model.getModified());
        return new ListItemResponse(model.getId(), model.getName(), created, modified, href);
    }
}
