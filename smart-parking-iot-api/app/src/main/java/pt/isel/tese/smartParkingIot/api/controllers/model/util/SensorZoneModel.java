package pt.isel.tese.smartParkingIot.api.controllers.model.util;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

public class SensorZoneModel {

    @JsonProperty("id")
    private Integer id = null;

    @JsonProperty("enterExit")
    private Boolean enterExit = null;

    public SensorZoneModel() { }

    public SensorZoneModel(Integer id, Boolean enterExit) {
        this.id = id;
        this.enterExit = enterExit;
    }

    /**
     * Get id
     * @return id
     **/
    @ApiModelProperty(example = "5", required = true, value = "")
    @NotNull
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get enterExit
     * @return enterExit
     **/
    @ApiModelProperty(example = "false", required = true, value = "")
    @NotNull
    public Boolean getEnterExit() {
        return enterExit;
    }

    public void setEnterExit(Boolean enterExit) {
        this.enterExit = enterExit;
    }
}
