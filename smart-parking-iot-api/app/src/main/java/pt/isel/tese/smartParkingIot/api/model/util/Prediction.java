package pt.isel.tese.smartParkingIot.api.model.util;

import java.util.Date;

public class Prediction {

    private Date date;
    private int percentage;

    public Prediction(Date date, int percentage) {
        this.date = date;
        this.percentage = percentage;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }
}
