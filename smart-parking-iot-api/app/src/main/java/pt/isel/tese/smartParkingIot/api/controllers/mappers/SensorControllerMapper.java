package pt.isel.tese.smartParkingIot.api.controllers.mappers;

import org.springframework.stereotype.Component;
import pt.isel.tese.smartParkingIot.api.controllers.config.UriConfiguration;
import pt.isel.tese.smartParkingIot.api.controllers.model.request.SensorRequest;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ListItemResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.SensorResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.util.GeoJsonPointModel;
import pt.isel.tese.smartParkingIot.api.controllers.model.util.SensorZoneModel;
import pt.isel.tese.smartParkingIot.api.controllers.utils.ResponseUtils;
import pt.isel.tese.smartParkingIot.api.controllers.utils.URIBuilder;
import pt.isel.tese.smartParkingIot.api.model.Sensor;
import pt.isel.tese.smartParkingIot.api.model.util.GeoJsonPoint;
import pt.isel.tese.smartParkingIot.api.model.util.SensorZone;
import pt.isel.tese.smartParkingIot.api.utils.ListUtils;

import java.util.List;

@Component
public class SensorControllerMapper implements IControllerMapper<Sensor, SensorRequest, SensorResponse> {

    @Override
    public Sensor transformToModel(SensorRequest request) {
        GeoJsonPoint location = new GeoJsonPoint(request.getLocation().getCoordinates());
        List<SensorZone> sensorZones = ListUtils.mapList(request.getZones(), this::transformToSensorZone);
        return new Sensor(request.getName(), location, sensorZones, request.getFieldValues(), request.getType());
    }

    @Override
    public SensorResponse transformToResponse(Sensor model) {
        GeoJsonPointModel location = new GeoJsonPointModel(model.getLocation().getCoordinates());
        String created = ResponseUtils.convertDateToString(model.getCreated());
        String modified = ResponseUtils.convertDateToString(model.getModified());
        List<SensorZoneModel> sensorZones = ListUtils.mapList(model.getZones(), this::transformToSensorZoneModel);
        return new SensorResponse(model.getName(), location, sensorZones, model.getFieldValues(), model.getSensorType(), model.getId(), created, modified);
    }

    @Override
    public ListItemResponse transformToListItem(Sensor model) {
        String href = URIBuilder.getCreatedURI(UriConfiguration.SENSORS_ID_URI, model.getId()).toString();
        String created = ResponseUtils.convertDateToString(model.getCreated());
        String modified = ResponseUtils.convertDateToString(model.getModified());
        return new ListItemResponse(model.getId(), model.getName(), created, modified, href);
    }

    private SensorZone transformToSensorZone(SensorZoneModel sensorZoneModel) {
        return new SensorZone(sensorZoneModel.getId(), sensorZoneModel.getEnterExit());
    }

    private SensorZoneModel transformToSensorZoneModel(SensorZone sensorZone) {
        return new SensorZoneModel(sensorZone.getId(), sensorZone.isEnterExit());
    }
}
