package pt.isel.tese.smartParkingIot.api.controllers.contract;

import io.swagger.annotations.*;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.isel.tese.smartParkingIot.api.controllers.model.request.ZoneRequest;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ErrorResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ListItemResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.PredictionResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ZoneResponse;

import javax.validation.Valid;
import java.util.List;

import static pt.isel.tese.smartParkingIot.api.controllers.config.UriConfiguration.*;

@Api(value = "zone", description = "the zone API")
public interface IZoneController {

    @ApiOperation(value = "adds a zone to the system", nickname = "createZone", notes = "adds a zone to the system", tags={ "Zone" })
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "zone successfully added to the system. to get the zone added to the system make a get request to the response header location URI."),
            @ApiResponse(code = 400, message = "Bad request parameter", response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class) })
    @PostMapping(value = ZONES_URI, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<Void> createZone(@ApiParam(value = "Zone to add"  )  @Valid @RequestBody ZoneRequest body);

    @ApiOperation(value = "deletes the zone with the specified zone-id", nickname = "deleteZone", notes = "delete the zone with the specified zone-id", tags={ "Zone" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "zone successfully deleted."),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "The specified zone does not exist", response = ErrorResponse.class) })
    @DeleteMapping(value = ZONES_ID_URI, produces = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<Void> deleteZone(@ApiParam(value = "zone id",required=true) @PathVariable("zone-id") Integer zoneId);

    @ApiOperation(value = "edits the zone with the specified zone-id", nickname = "editZone", notes = "edits the zone with the specified zone-id", tags={ "Zone" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "zone successfully edited. to get the zone edited make a GET request to the URI used to edit the zone."),
            @ApiResponse(code = 400, message = "Bad request parameter", response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "The specified zone does not exist", response = ErrorResponse.class) })
    @PutMapping(value = ZONES_ID_URI, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<Void> editZone(@ApiParam(value = "zone id",required=true) @PathVariable("zone-id") Integer zoneId, @ApiParam(value = "zone to edit"  )  @Valid @RequestBody ZoneRequest body);

    @ApiOperation(value = "returns zone with the specified zone-id", nickname = "readZone", notes = "returns zone with the specified zone-id", response = ZoneResponse.class, tags={ "Zone" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "zone with the specified zone-id", response = ZoneResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "The specified zone does not exist", response = ErrorResponse.class) })
    @GetMapping(value = ZONES_ID_URI, produces = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<ZoneResponse> readZone(@ApiParam(value = "zone id",required=true) @PathVariable("zone-id") Integer zoneId);

    @ApiOperation(value = "returns zones added to the system", nickname = "readZones", notes = "returns all the zones that were added to the system, ordered by creation date", response = ListItemResponse.class, responseContainer = "List", tags={ "Zone" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "zone result matching search criteria", response = ListItemResponse.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad request parameter", response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class) })
    @GetMapping(value = ZONES_URI, produces = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<List<ListItemResponse>> readZones(@ApiParam(value = "number of page for pagination results") @Valid @RequestParam(value = "page", required = false) Integer page, @ApiParam(value = "number of elements to return in a page") @Valid @RequestParam(value = "size", required = false) Integer size);

    @ApiOperation(value = "returns predictions for the zone with the specified zone-id, a start, end date and an interval", nickname = "readZonePredictions", notes = "returns zone predictions with the specified zone-id", response = ZoneResponse.class, tags={ "Zone" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "zone predictions with the specified zone-id", response = ZoneResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "The specified zone does not exist", response = ErrorResponse.class) })
    @GetMapping(value = ZONES_ID_PREDICTION_URI, produces = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<List<PredictionResponse>> readZonePredictions(@ApiParam(value = "zone id",required=true) @PathVariable("zone-id") Integer zoneId, @ApiParam(value = "start date in ISO 8601 format") @RequestParam(value = "start", required = false) String start, @ApiParam(value = "end date in ISO 8601 format") @RequestParam(value = "end", required = false) String end, @RequestParam(value = "interval", required = false) int interval);
}
