package pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.mappers;

import pt.isel.tese.smartParkingIot.api.model.Sensor;
import pt.isel.tese.smartParkingIot.api.model.util.GeoJsonPoint;
import pt.isel.tese.smartParkingIot.api.model.util.SensorZone;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto.SensorDTO;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto.SensorTypeDTO;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto.SensorZoneDTO;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto.ZoneDTO;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.utils.DateUtils;
import pt.isel.tese.smartParkingIot.api.utils.JsonUtils;
import pt.isel.tese.smartParkingIot.api.utils.ListUtils;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SensorRepositoryMapper implements IRepositoryMapper<Sensor, SensorDTO> {

    @Override
    public Sensor transformToObject(SensorDTO dto) {
        GeoJsonPoint location = JsonUtils.transformToObject(dto.getLocation(), GeoJsonPoint.class);
        Timestamp created = DateUtils.convertDateToTimestamp(dto.getCreated());
        Timestamp modified = DateUtils.convertDateToTimestamp(dto.getModified());
        Map<String, String> fieldValues = JsonUtils.transformToMap(dto.getFieldValues());
        return new Sensor(dto.getId(), dto.getName(), location, new ArrayList<>(), fieldValues, dto.getSensorType().getId(),
                created, modified);
    }

    @Override
    public SensorDTO transformToDto(Sensor model) {
        String location = JsonUtils.transformToString(model.getLocation());
        Timestamp created = DateUtils.convertDateToTimestamp(model.getCreated());
        Timestamp modified = DateUtils.convertDateToTimestamp(model.getModified());
        String fieldValues = JsonUtils.transformToString(model.getFieldValues());
        return new SensorDTO(model.getId(), model.getName(), location, fieldValues, created, modified,
                new SensorTypeDTO(model.getSensorType()));
    }

    @Override
    public int getIdentifier(Sensor model) {
        return model.getId();
    }
}
