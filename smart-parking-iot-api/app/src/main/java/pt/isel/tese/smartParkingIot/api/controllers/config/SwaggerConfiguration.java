package pt.isel.tese.smartParkingIot.api.controllers.config;

import com.google.common.base.Predicate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static com.google.common.base.Predicates.or;

@Configuration
@EnableSwagger2
@Controller
public class SwaggerConfiguration {

    @RequestMapping(value = "/swagger-info")
    public String index() {
        return "redirect:swagger-ui.html";
    }

    ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("SmartparkingIoT Admin API")
                .license("")
                .licenseUrl("")
                .termsOfServiceUrl("")
                .version("1.0.0")
                .contact(new Contact("","", ""))
                .build();
    }

    @Bean
    public Docket customImplementation(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(getApis())
                .build()
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo());
    }

    private Predicate<RequestHandler> getApis() {
        // Defines the packages to be shown in the UI
        return or(
                RequestHandlerSelectors.basePackage("pt.isel.tese.smartParkingIot.api.controllers.implementation")
        );
    }
}
