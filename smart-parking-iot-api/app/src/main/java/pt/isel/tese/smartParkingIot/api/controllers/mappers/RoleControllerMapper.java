package pt.isel.tese.smartParkingIot.api.controllers.mappers;

import org.springframework.stereotype.Component;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.RoleResponse;
import pt.isel.tese.smartParkingIot.api.model.Role;

@Component
public class RoleControllerMapper {

    public RoleResponse transformToResponse(Role role) {
        return new RoleResponse(role.getId(), role.getName());
    }
}
