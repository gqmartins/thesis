package pt.isel.tese.smartParkingIot.api.logic.implementations;

import org.springframework.stereotype.Service;
import pt.isel.tese.smartParkingIot.api.model.OutboundMessageType;
import pt.isel.tese.smartParkingIot.api.repository.RepositoryFacade;
import pt.isel.tese.smartParkingIot.api.repository.contracts.IOutboundMessageTypeRepository;
import pt.isel.tese.smartParkingIot.api.logic.contracts.IOutboundMessageTypeService;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class OutboundMessageTypeService implements IOutboundMessageTypeService {

    private IOutboundMessageTypeRepository outboundMessageTypeRepository;

    public OutboundMessageTypeService(RepositoryFacade repositoryFacade) {
        this.outboundMessageTypeRepository = repositoryFacade.getOutboundMessageTypeRepository();
    }

    @Override
    public int create(OutboundMessageType object) {
        object.setCreated(new Date());
        object.setModified(object.getCreated());
        return outboundMessageTypeRepository.create(object);
    }

    @Override
    public List<OutboundMessageType> read(Map<String, String> filters) {
        return outboundMessageTypeRepository.read(new HashMap<>());
    }

    @Override
    public OutboundMessageType read(int id) {
        return outboundMessageTypeRepository.read(id);
    }

    @Override
    public void update(OutboundMessageType object) {
        OutboundMessageType savedModel = outboundMessageTypeRepository.read(object.getId());

        savedModel.setName(object.getName());
        savedModel.setSensorTypeId(object.getSensorTypeId());
        savedModel.setFields(object.getFields());
        savedModel.setModified(new Date());

        outboundMessageTypeRepository.update(savedModel);
    }

    @Override
    public void delete(int id) {
        OutboundMessageType outboundMessageType = new OutboundMessageType(id);
        outboundMessageTypeRepository.delete(outboundMessageType);
    }

    @Override
    public List<OutboundMessageType> read(int sensorId, Map<String, String> filters) {
        return read(filters)
                .stream()
                .filter(outboundMessageType -> outboundMessageType.getSensorTypeId() == sensorId)
                .collect(Collectors.toList());
    }
}
