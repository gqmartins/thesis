package pt.isel.tese.smartParkingIot.api.logic.contracts;

import pt.isel.tese.smartParkingIot.api.model.util.PermissionUri;

import java.util.List;

public interface IAuthorizationService {

    List<PermissionUri> getUrisForRole(int role);
}
