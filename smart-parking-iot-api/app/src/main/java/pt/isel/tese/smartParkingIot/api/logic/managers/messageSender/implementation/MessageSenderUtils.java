package pt.isel.tese.smartParkingIot.api.logic.managers.messageSender.implementation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class MessageSenderUtils {

    private static final int DEVICE_ID_PAYLOAD_SIZE = 2;
    private static final int MESSAGE_TYPE_PAYLOAD_SIZE = 1;
    // payload indexes
    private static final int SENSOR_ID_HIGH_BYTE_INDEX = 0;
    private static final int SENSOR_ID_LOW_BYTE_INDEX = 1;
    private static final int MESSAGE_TYPE_INDEX = 2;

    public static byte[] buildPayload(int sensorId, int messageType, Map<String, String> body) {
        byte[] payload = getPayload(body);
        insertDeviceId(sensorId, payload);
        insertMessageType(messageType, payload);
        insertValuesFromMessage(body, payload);
        return payload;
    }

    private static byte[] getPayload(Map<String, String> body) {
        int payloadSize = DEVICE_ID_PAYLOAD_SIZE + MESSAGE_TYPE_PAYLOAD_SIZE + body.keySet().size();
        return new byte[payloadSize];
    }

    private static void insertDeviceId(int sensorId, byte[] payload) {
        payload[SENSOR_ID_HIGH_BYTE_INDEX] = (byte) ((sensorId >>> 8) & 0xFF);
        payload[SENSOR_ID_LOW_BYTE_INDEX] = (byte) (sensorId & 0xFF);
    }

    private static void insertMessageType(int messageType, byte[] payload) {
        payload[MESSAGE_TYPE_INDEX] = (byte) messageType;
    }

    private static void insertValuesFromMessage(Map<String, String> body, byte[] payload) {
        List<String> keys = new ArrayList<>(body.keySet());
        Collections.sort(keys);
        int beginSize = DEVICE_ID_PAYLOAD_SIZE + MESSAGE_TYPE_PAYLOAD_SIZE;
        for(int i = beginSize; i < payload.length; ++i) {
            String value = body.get(keys.get(i - beginSize));
            int intValue = Integer.parseInt(value);
            byte byteValue = (byte) (intValue & 0xFF);
            payload[i] = byteValue;
        }
    }
}
