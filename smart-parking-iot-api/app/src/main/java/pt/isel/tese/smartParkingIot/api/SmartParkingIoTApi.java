package pt.isel.tese.smartParkingIot.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
public class SmartParkingIoTApi {

    public static void main(String[] args) {
        System.out.println("::STARTING::");
        SpringApplication.run(SmartParkingIoTApi.class, args);
        System.out.println("::RUNNING::");
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry
                        .addMapping("/**")
                        .allowedOrigins("*")
                        .exposedHeaders("Location", "Authorization")
                        .allowedMethods("*");
            }
        };
    }
}
