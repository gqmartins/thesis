package pt.isel.tese.smartParkingIot.api.controllers.mappers;

import pt.isel.tese.smartParkingIot.api.controllers.model.response.ListItemResponse;

public interface IControllerMapper<T, E, R> {

    T transformToModel(E request);
    R transformToResponse(T model);
    ListItemResponse transformToListItem(T model);
}
