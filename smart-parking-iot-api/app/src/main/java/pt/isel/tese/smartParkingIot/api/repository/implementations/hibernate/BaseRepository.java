package pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.exception.ConstraintViolationException;
import pt.isel.tese.smartParkingIot.api.repository.exceptions.EntityDoesNotExistException;
import pt.isel.tese.smartParkingIot.api.repository.exceptions.NameAlreadyExistsException;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.mappers.IRepositoryMapper;
import pt.isel.tese.smartParkingIot.api.utils.ListUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;

public class BaseRepository<T, R> {

    private Class<R> dtoClass;
    private IRepositoryMapper<T, R> mapper;
    private static SessionFactory sessionFactory;

    protected BaseRepository(Class<R> dtoClass, IRepositoryMapper<T, R> mapper) {
        this.dtoClass = dtoClass;
        this.mapper = mapper;
        if(sessionFactory == null) {
            final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                    .configure()
                    .build();
            sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        }
    }

    public int create(T object) {
        R dto = mapper.transformToDto(object);
        return map(session -> (int) session.save(dto));
    }

    // TODO: VER DO FILTER
    public List<T> read(Map<String, String> filter) {
        return map(session -> {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<R> criteriaQuery = criteriaBuilder.createQuery(dtoClass);
            criteriaQuery.from(dtoClass);
            List<R> data = session.createQuery(criteriaQuery).list();
            return ListUtils.mapList(data, mapper::transformToObject);
        });
    }

    public T read(int id) {
        R dto = map(session -> session.get(dtoClass, id));
        if(dto == null) {
            throw new EntityDoesNotExistException();
        }
        return mapper.transformToObject(dto);
    }

    public void update(T object) {
        R dto = mapper.transformToDto(object);
        execute(session -> session.update(dto));
    }

    public void delete(T object) {
        execute(session -> {
            R dto = session.load(dtoClass, mapper.getIdentifier(object));
            session.delete(dto);
        });
    }

    protected void execute(Consumer<Session> consumer) {
        try(Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            consumer.accept(session);
            session.getTransaction().commit();
        } catch (ConstraintViolationException ex) {
            throw new NameAlreadyExistsException();
        } catch (HibernateException ex) {
            System.out.println("A database related error occurred.");
            ex.printStackTrace();
        } catch (Exception ex) {
            throw new NameAlreadyExistsException();
        }

    }

    protected <U> U map(Function<Session, U> mapper) {
        try(Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            U object = mapper.apply(session);
            session.getTransaction().commit();
            return object;
        } catch (ConstraintViolationException ex) {
            throw new NameAlreadyExistsException();
        } catch(HibernateException ex) {
            System.out.println("A database related error occurred.");
            ex.printStackTrace();
            return null;
        }
    }
}
