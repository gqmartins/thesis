package pt.isel.tese.smartParkingIot.api.controllers.contract;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.isel.tese.smartParkingIot.api.controllers.model.request.UserRequest;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ErrorResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ListItemResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.UserResponse;

import javax.validation.Valid;
import java.util.List;

import static pt.isel.tese.smartParkingIot.api.controllers.config.UriConfiguration.USERS_ID_URI;
import static pt.isel.tese.smartParkingIot.api.controllers.config.UriConfiguration.USERS_URI;

@Api(value = "user", description = "the user API")
public interface IUserController {

    @ApiOperation(value = "adds a user to the system", nickname = "createUser", notes = "adds a user to the system", tags={ "User" })
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "user successfully added to the system. to get the user added to the system make a get request to the response header location URI."),
            @ApiResponse(code = 400, message = "Bad request parameter", response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class) })
    @PostMapping(value = USERS_URI, produces = { "application/json" }, consumes = { "application/json" })
    ResponseEntity<Void> createUser(@ApiParam(value = "User to add"  )  @Valid @RequestBody UserRequest body);

    @ApiOperation(value = "deletes the user with the specified user-id", nickname = "deleteUser", notes = "delete the user with the specified user-id", tags={ "User" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "user successfully deleted."),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "The specified user does not exist", response = ErrorResponse.class) })
    @DeleteMapping(value = USERS_ID_URI, produces = { "application/json" })
    ResponseEntity<Void> deleteUser(@ApiParam(value = "user id",required=true) @PathVariable("user-id") Integer userId);

    @ApiOperation(value = "edits the user with the specified user-id", nickname = "editUser", notes = "edits the user with the specified user-id", tags={ "User" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "user successfully edited. to get the user edited make a GET request to the URI used to edit the user."),
            @ApiResponse(code = 400, message = "Bad request parameter", response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "The specified user does not exist", response = ErrorResponse.class) })
    @PutMapping(value = USERS_ID_URI, produces = { "application/json" }, consumes = { "application/json" })
    ResponseEntity<Void> editUser(@ApiParam(value = "user id",required=true) @PathVariable("user-id") Integer userId,@ApiParam(value = "user to edit"  )  @Valid @RequestBody UserRequest body);

    @ApiOperation(value = "returns user with the specified user-id", nickname = "readUser", notes = "returns user with the specified user-id", response = UserResponse.class, tags={ "User" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "user with the specified user-id", response = UserResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "The specified user does not exist", response = ErrorResponse.class) })
    @GetMapping(value = USERS_ID_URI, produces = { "application/json" })
    ResponseEntity<UserResponse> readUser(@ApiParam(value = "user id",required=true) @PathVariable("user-id") Integer userId);

    @ApiOperation(value = "returns users added to the system", nickname = "readUsers", notes = "returns all the users that were added to the system, ordered by creation date", response = ListItemResponse.class, responseContainer = "List", tags={ "User" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "user result matching search criteria", response = ListItemResponse.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad request parameter", response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class) })
    @GetMapping(value = USERS_URI, produces = { "application/json" })
    ResponseEntity<List<ListItemResponse>> readUsers(@ApiParam(value = "number of page for pagination results") @Valid @RequestParam(value = "page", required = false) Integer page, @ApiParam(value = "number of elements to return in a page") @Valid @RequestParam(value = "size", required = false) Integer size);
}
