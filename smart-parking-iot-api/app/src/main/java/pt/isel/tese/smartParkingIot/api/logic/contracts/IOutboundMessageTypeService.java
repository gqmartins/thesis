package pt.isel.tese.smartParkingIot.api.logic.contracts;

import pt.isel.tese.smartParkingIot.api.model.OutboundMessageType;

import java.util.List;
import java.util.Map;

public interface IOutboundMessageTypeService extends IBaseService<OutboundMessageType> {
    List<OutboundMessageType> read(int sensorId, Map<String, String> filters);
}
