package pt.isel.tese.smartParkingIot.api.controllers.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

    private static final String DATE_FORMAT = "yyyy-MM-dd'T'kk:mm";
    private static final int START_DATE_HOUR = 0;
    private static final int START_DATE_MINUTE = 0;
    private static final int END_DATE_HOUR = 23;
    private static final int END_DATE_MINUTE = 59;
    private static final int DATE_MILLISECOND = 0;

    public static Date parseDate(String date, boolean startEndDate) {
        if(date == null) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, startEndDate ? START_DATE_HOUR : END_DATE_HOUR);
            calendar.set(Calendar.MINUTE, startEndDate ? START_DATE_MINUTE : END_DATE_MINUTE);
            calendar.set(Calendar.SECOND, DATE_MILLISECOND);
            return calendar.getTime();
        }

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
            return simpleDateFormat.parse(date);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
