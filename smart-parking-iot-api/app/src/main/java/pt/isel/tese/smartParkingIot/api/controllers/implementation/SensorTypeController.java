package pt.isel.tese.smartParkingIot.api.controllers.implementation;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pt.isel.tese.smartParkingIot.api.controllers.config.UriConfiguration;
import pt.isel.tese.smartParkingIot.api.controllers.contract.ISensorTypeController;
import pt.isel.tese.smartParkingIot.api.controllers.mappers.SensorTypeControllerMapper;
import pt.isel.tese.smartParkingIot.api.controllers.model.request.SensorTypeRequest;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ListItemResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.SensorTypeResponse;
import pt.isel.tese.smartParkingIot.api.controllers.utils.URIBuilder;
import pt.isel.tese.smartParkingIot.api.model.SensorType;
import pt.isel.tese.smartParkingIot.api.logic.ServiceFacade;
import pt.isel.tese.smartParkingIot.api.logic.contracts.ISensorTypeService;
import pt.isel.tese.smartParkingIot.api.utils.ListUtils;

import javax.validation.Valid;
import java.net.URI;
import java.util.HashMap;
import java.util.List;

@RestController
public class SensorTypeController implements ISensorTypeController {

    private ISensorTypeService sensorTypeService;
    private SensorTypeControllerMapper sensorTypeMapper;

    public SensorTypeController(ServiceFacade serviceFacade, SensorTypeControllerMapper sensorTypeMapper) {
        this.sensorTypeService = serviceFacade.getSensorTypeService();
        this.sensorTypeMapper = sensorTypeMapper;
    }

    @Override
    public ResponseEntity<Void> createSensorType(@Valid @RequestBody SensorTypeRequest body) {
        SensorType sensorType = sensorTypeMapper.transformToModel(body);
        int createdId = sensorTypeService.create(sensorType);
        URI createdUri = URIBuilder.getCreatedURI(UriConfiguration.SENSOR_TYPES_ID_URI, createdId);
        return ResponseEntity.created(createdUri).build();
    }

    @Override
    public ResponseEntity<Void> deleteSensorType(@PathVariable("sensor-type-id") Integer sensorTypeId) {
        sensorTypeService.delete(sensorTypeId);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<Void> editSensorType(@PathVariable("sensor-type-id") Integer sensorTypeId, @Valid @RequestBody SensorTypeRequest body) {
        SensorType sensorType = sensorTypeMapper.transformToModel(body);
        sensorType.setId(sensorTypeId);
        sensorTypeService.update(sensorType);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<SensorTypeResponse> readSensorType(@PathVariable("sensor-type-id") Integer sensorTypeId) {
        SensorType sensorType = sensorTypeService.read(sensorTypeId);
        SensorTypeResponse sensorResponse = sensorTypeMapper.transformToResponse(sensorType);
        return ResponseEntity.ok(sensorResponse);
    }

    @Override
    public ResponseEntity<List<ListItemResponse>> readSensorTypes(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size) {
        List<SensorType> sensorTypes = sensorTypeService.read(new HashMap<>());
        List<ListItemResponse> response = ListUtils.mapList(sensorTypes, sensorTypeMapper::transformToListItem);
        return ResponseEntity.ok(response);
    }
}
