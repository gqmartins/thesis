package pt.isel.tese.smartParkingIot.api.model;

import java.util.Date;
import java.util.List;

public class SensorType {

    private int id;
    private String name;
    private List<String> fields;
    private Date created;
    private Date modified;

    public SensorType(int id) {
        this.id = id;
    }

    public SensorType(String name, List<String> fields) {
        this.name = name;
        this.fields = fields;
    }

    public SensorType(int id, String name, List<String> fields, Date created, Date modified) {
        this(name, fields);
        this.id = id;
        this.created = created;
        this.modified = modified;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }
}
