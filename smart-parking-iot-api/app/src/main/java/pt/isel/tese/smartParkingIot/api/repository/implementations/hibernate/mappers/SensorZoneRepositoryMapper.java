package pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.mappers;

import pt.isel.tese.smartParkingIot.api.model.util.SensorZone;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto.SensorDTO;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto.SensorZoneDTO;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto.ZoneDTO;

public class SensorZoneRepositoryMapper implements IRepositoryMapper<SensorZoneDTO, SensorZoneDTO> {

    @Override
    public SensorZoneDTO transformToObject(SensorZoneDTO dto) {
        return dto;
    }

    @Override
    public SensorZoneDTO transformToDto(SensorZoneDTO object) {
        return object;
    }

    @Override
    public int getIdentifier(SensorZoneDTO model) {
        return model.getId();
    }

    public static SensorZoneDTO transformToSensorZoneDTO(int sensorId, SensorZone sensorZone) {
        return new SensorZoneDTO(sensorZone.getId(), sensorId, sensorZone.isEnterExit());
    }

    public static SensorZone transformToSensorZone(SensorZoneDTO dto) {
        return new SensorZone(dto.getZone(), dto.isEnterExit());
    }
}
