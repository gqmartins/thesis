package pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.mappers;

public interface IRepositoryMapper<T, R> {

    T transformToObject(R dto);
    R transformToDto(T model);
    int getIdentifier(T model);
}
