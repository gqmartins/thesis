package pt.isel.tese.smartParkingIot.api.logic.contracts;

import pt.isel.tese.smartParkingIot.api.model.Role;

public interface IRoleService extends IBaseService<Role> { }
