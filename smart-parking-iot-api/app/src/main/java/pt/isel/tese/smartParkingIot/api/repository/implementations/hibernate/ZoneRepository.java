package pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate;

import org.springframework.stereotype.Repository;
import pt.isel.tese.smartParkingIot.api.model.Zone;
import pt.isel.tese.smartParkingIot.api.repository.contracts.IZoneRepository;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto.ZoneDTO;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.mappers.ZoneRepositoryMapper;

@Repository
public class ZoneRepository extends BaseRepository<Zone, ZoneDTO> implements IZoneRepository {

    public ZoneRepository() {
        super(ZoneDTO.class, new ZoneRepositoryMapper());
    }
}
