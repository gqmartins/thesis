package pt.isel.tese.smartParkingIot.api.controllers.model.util;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class GeoJsonPointModel {

    @JsonProperty("type")
    private String type = "Point";

    @JsonProperty("coordinates")
    private List<Float> coordinates = new ArrayList<>();

    public GeoJsonPointModel() { }

    public GeoJsonPointModel(List<Float> coordinates) {
        this.coordinates = coordinates;
    }

    /**
     * Get type
     * @return type
     **/
    @ApiModelProperty(example = "Point", required = true, value = "")
    @NotNull
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    /**
     * The array for each coordinate must have at least two points.
     * @return coordinates
     **/
    @ApiModelProperty(required = true, value = "The array for each coordinate must have at least two points.")
    @NotNull
    public List<Float> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<Float> coordinates) {
        this.coordinates = coordinates;
    }
}
