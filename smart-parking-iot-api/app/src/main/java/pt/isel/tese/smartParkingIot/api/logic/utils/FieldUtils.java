package pt.isel.tese.smartParkingIot.api.logic.utils;

import pt.isel.tese.smartParkingIot.api.logic.exceptions.MissingFieldException;

import java.util.List;
import java.util.Map;

public class FieldUtils {

    public static void checkForFields(List<String> fields, Map<String, String> fieldValues) {
        for(String field : fields) {
            if(!fieldValues.containsKey(field)) {
                throw new MissingFieldException(field);
            }
        }
    }
}
