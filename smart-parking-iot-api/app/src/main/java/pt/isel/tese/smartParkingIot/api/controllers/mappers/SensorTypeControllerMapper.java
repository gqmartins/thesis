package pt.isel.tese.smartParkingIot.api.controllers.mappers;

import org.springframework.stereotype.Component;
import pt.isel.tese.smartParkingIot.api.controllers.config.UriConfiguration;
import pt.isel.tese.smartParkingIot.api.controllers.model.request.SensorTypeRequest;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ListItemResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.SensorTypeResponse;
import pt.isel.tese.smartParkingIot.api.controllers.utils.ResponseUtils;
import pt.isel.tese.smartParkingIot.api.controllers.utils.URIBuilder;
import pt.isel.tese.smartParkingIot.api.model.SensorType;

import static pt.isel.tese.smartParkingIot.api.controllers.config.UriConfiguration.OUTBOUND_MESSAGE_TYPES_URI;

@Component
public class SensorTypeControllerMapper implements IControllerMapper<SensorType, SensorTypeRequest, SensorTypeResponse> {

    @Override
    public SensorType transformToModel(SensorTypeRequest request) {
        return new SensorType(request.getName(), request.getFields());
    }

    public SensorTypeResponse transformToResponse(SensorType model) {
        String created = ResponseUtils.convertDateToString(model.getCreated());
        String modified = ResponseUtils.convertDateToString(model.getModified());
        String outbountMessageTypesHref = URIBuilder.getCreatedURIString(OUTBOUND_MESSAGE_TYPES_URI, model.getId());
        return new SensorTypeResponse(model.getId(), model.getName(), model.getFields(), created, modified, outbountMessageTypesHref);
    }

    @Override
    public ListItemResponse transformToListItem(SensorType model) {
        String href = URIBuilder.getCreatedURI(UriConfiguration.SENSOR_TYPES_ID_URI, model.getId()).toString();
        String created = ResponseUtils.convertDateToString(model.getCreated());
        String modified = ResponseUtils.convertDateToString(model.getModified());
        return new ListItemResponse(model.getId(), model.getName(), created, modified, href);
    }
}
