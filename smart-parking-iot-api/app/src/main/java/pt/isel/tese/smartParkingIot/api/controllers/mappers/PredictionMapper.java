package pt.isel.tese.smartParkingIot.api.controllers.mappers;

import org.springframework.stereotype.Component;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.PredictionResponse;
import pt.isel.tese.smartParkingIot.api.controllers.utils.ResponseUtils;
import pt.isel.tese.smartParkingIot.api.model.util.Prediction;

@Component
public class PredictionMapper {

    public PredictionResponse getPredictionResponse(Prediction prediction) {
        String date = ResponseUtils.convertDateToString(prediction.getDate());
        return new PredictionResponse(date, prediction.getPercentage());
    }
}
