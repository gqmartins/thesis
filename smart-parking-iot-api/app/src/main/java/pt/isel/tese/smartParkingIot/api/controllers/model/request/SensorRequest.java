package pt.isel.tese.smartParkingIot.api.controllers.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import pt.isel.tese.smartParkingIot.api.controllers.model.util.GeoJsonPointModel;
import pt.isel.tese.smartParkingIot.api.controllers.model.util.SensorZoneModel;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

public class SensorRequest {

    @JsonProperty("name")
    @NotNull(message = "A name must be provided.")
    private String name = null;

    @JsonProperty("location")
    @NotNull(message = "A location must be provided.")
    private GeoJsonPointModel location = null;

    @JsonProperty("zones")
    private List<SensorZoneModel> zones = null;

    @JsonProperty("fieldValues")
    private Map<String, String> fieldValues = null;

    @JsonProperty("type")
    @NotNull(message = "A sensor type must be provided.")
    private Integer type = null;

    public SensorRequest() { }

    public SensorRequest(String name, GeoJsonPointModel location, List<SensorZoneModel> zones, Map<String, String> fieldValues, Integer type) {
        this.name = name;
        this.location = location;
        this.zones = zones;
        this.fieldValues = fieldValues;
        this.type = type;
    }

    /**
     * Get name
     * @return name
     **/
    @ApiModelProperty(example = "sensor 1", required = true, value = "")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get location
     * @return location
     **/
    @ApiModelProperty(required = true, value = "")
    public GeoJsonPointModel getLocation() {
        return location;
    }

    public void setLocation(GeoJsonPointModel location) {
        this.location = location;
    }

    /**
     * Get zones associated with the sensor
     * @return zones
     **/
    @ApiModelProperty(required = true, value = "")
    public List<SensorZoneModel> getZones() {
        return zones;
    }

    public void setZones(List<SensorZoneModel> zones) {
        this.zones = zones;
    }

    /**
     * Get type
     * @return type
     **/
    @ApiModelProperty(example = "1", value = "")
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * Get fieldValues
     * @return fieldValues
     **/
    @ApiModelProperty(example = "", value = "")
    public Map<String, String> getFieldValues() {
        return fieldValues;
    }

    public void setFieldValues(Map<String, String> fieldValues) {
        this.fieldValues = fieldValues;
    }
}
