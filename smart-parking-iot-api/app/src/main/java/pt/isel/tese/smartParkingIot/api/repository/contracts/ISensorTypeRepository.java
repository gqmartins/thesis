package pt.isel.tese.smartParkingIot.api.repository.contracts;

import pt.isel.tese.smartParkingIot.api.model.SensorType;

public interface ISensorTypeRepository extends IBaseRepository<SensorType> { }
