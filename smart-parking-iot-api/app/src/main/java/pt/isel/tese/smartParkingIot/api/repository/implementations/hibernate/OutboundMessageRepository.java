package pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate;

import org.springframework.stereotype.Repository;
import pt.isel.tese.smartParkingIot.api.model.OutboundMessage;
import pt.isel.tese.smartParkingIot.api.repository.contracts.IOutboundMessageRepository;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto.OutboundMessageDTO;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.mappers.OutboundMessageRepositoryMapper;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

@Repository
public class OutboundMessageRepository extends BaseRepository<OutboundMessage, OutboundMessageDTO> implements IOutboundMessageRepository {

    public OutboundMessageRepository() {
        super(OutboundMessageDTO.class, new OutboundMessageRepositoryMapper());
    }

    @Override
    public void update(OutboundMessage object) {
        throw new NotImplementedException();
    }
}
