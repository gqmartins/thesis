package pt.isel.tese.smartParkingIot.api.logic.implementations;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pt.isel.tese.smartParkingIot.api.logic.exceptions.DeleteAdminUserException;
import pt.isel.tese.smartParkingIot.api.model.User;
import pt.isel.tese.smartParkingIot.api.model.util.Roles;
import pt.isel.tese.smartParkingIot.api.repository.RepositoryFacade;
import pt.isel.tese.smartParkingIot.api.repository.contracts.IRoleRepository;
import pt.isel.tese.smartParkingIot.api.repository.contracts.IUserRepository;
import pt.isel.tese.smartParkingIot.api.logic.contracts.IUserService;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import org.springframework.security.crypto.scrypt.SCryptPasswordEncoder;

@Service
public class UserService implements IUserService {

    private IUserRepository userRepository;
    private IRoleRepository roleRepository;

    public UserService(RepositoryFacade repositoryFacade) {
        this.userRepository = repositoryFacade.getUserRepository();
        this.roleRepository = repositoryFacade.getRoleRepository();
    }

    @Override
    public int create(User object) {
        String generatedPassword = generatePassword(object.getPassword());
        object.setPassword(generatedPassword);
        object.setCreated(new Date());
        object.setModified(object.getCreated());
        return userRepository.create(object);
    }

    @Override
    public List<User> read(Map<String, String> filters) {
        return userRepository.read(new HashMap<>());
    }

    @Override
    public User read(int id) {
        return userRepository.read(id);
    }

    @Override
    public void update(User object) {
        User savedUser = userRepository.read(object.getId());
        int adminRoleId = getRoleAdminId();
        int numberOfAdminUsers = countAdminUsers(adminRoleId);

        // the app has to have at least a user with the Admin role
        if(numberOfAdminUsers == 1 && savedUser.getRole() == adminRoleId && object.getRole() != savedUser.getRole()) {
            throw new DeleteAdminUserException();
        }

        if(!object.getPassword().equals("")) {
            String newPassword = generatePassword(object.getPassword());
            savedUser.setPassword(newPassword);
        }
        savedUser.setName(object.getName());
        savedUser.setUsername(object.getUsername());
        savedUser.setRole(object.getRole());
        savedUser.setModified(new Date());
        userRepository.update(savedUser);
    }

    @Override
    public void delete(int id) {
        User savedUser = userRepository.read(id);
        int adminRoleId = getRoleAdminId();
        int numberOfAdminUsers = countAdminUsers(adminRoleId);

        // the app has to have at least a user with the Admin role
        if(numberOfAdminUsers == 1 && savedUser.getRole() == adminRoleId) {
            throw new DeleteAdminUserException();
        }

        userRepository.delete(savedUser);
    }
    /*
    @Override
    public void updatePassword(int user, String newPassword, String confirmPassword) {
        if (!newPassword.equals(confirmPassword)) {
            throw new RuntimeException("The passwords don't match");
        }

        String generatedPassword = generatePassword(newPassword);
        User savedUser = userRepository.read(user);
        savedUser.setPassword(generatedPassword);
        userRepository.update(savedUser);
    }
    */

    private String generatePassword(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }

    private int countAdminUsers(int adminRoleId) {
        return (int) userRepository
                .read(new HashMap<>())
                .stream()
                .filter(user -> user.getRole() == adminRoleId)
                .count();
    }

    private int getRoleAdminId() {
        return roleRepository
                .read(new HashMap<>())
                .stream()
                .filter(role -> role.getName().equals(Roles.ADMIN.getRole()))
                .findFirst()
                .get()
                .getId();
    }

}
