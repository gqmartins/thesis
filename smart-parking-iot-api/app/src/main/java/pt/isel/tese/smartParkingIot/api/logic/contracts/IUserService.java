package pt.isel.tese.smartParkingIot.api.logic.contracts;

import pt.isel.tese.smartParkingIot.api.model.User;

public interface IUserService extends IBaseService<User> {

    //void updatePassword(int user, String newPassword, String confirmPassword);
}
