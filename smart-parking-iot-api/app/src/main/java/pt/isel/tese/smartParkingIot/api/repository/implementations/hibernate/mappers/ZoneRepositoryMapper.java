package pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.mappers;

import com.google.gson.Gson;
import pt.isel.tese.smartParkingIot.api.model.Zone;
import pt.isel.tese.smartParkingIot.api.model.util.GeoJsonPolygon;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto.ZoneDTO;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.utils.DateUtils;

import java.sql.Timestamp;
import java.util.ArrayList;

public class ZoneRepositoryMapper implements IRepositoryMapper<Zone, ZoneDTO> {

    private final Gson gson = new Gson();

    @Override
    public Zone transformToObject(ZoneDTO dto) {
        GeoJsonPolygon perimeter = gson.fromJson(dto.getPerimeter(), GeoJsonPolygon.class);
        return new Zone(dto.getId(), dto.getName(), dto.getMaxCount(), dto.getCurrentCount(), 0,
                perimeter, dto.getCreated(), dto.getModified());
    }

    @Override
    public ZoneDTO transformToDto(Zone object) {
        String perimeter = gson.toJson(object.getPerimeter());
        Timestamp created = DateUtils.convertDateToTimestamp(object.getCreated());
        Timestamp modified = DateUtils.convertDateToTimestamp(object.getModified());
        return new ZoneDTO(object.getId(), object.getName(), object.getMaxCount(), object.getCurrentCount(), created,
                modified, perimeter, new ArrayList<>());
    }

    @Override
    public int getIdentifier(Zone model) {
        return model.getId();
    }
}
