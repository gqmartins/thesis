package pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate;

import org.springframework.stereotype.Repository;
import pt.isel.tese.smartParkingIot.api.model.Sensor;
import pt.isel.tese.smartParkingIot.api.model.util.SensorZone;
import pt.isel.tese.smartParkingIot.api.repository.contracts.ISensorRepository;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto.SensorDTO;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.mappers.SensorRepositoryMapper;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.mappers.SensorZoneRepositoryMapper;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class SensorRepository extends BaseRepository<Sensor, SensorDTO> implements ISensorRepository {

    private static final Class<SensorDTO> SENSOR_CLASS = SensorDTO.class;
    private SensorZoneRepository sensorZoneRepository;
    private OutboundMessageRepository outboundMessageRepository;

    public SensorRepository(SensorZoneRepository sensorZoneRepository, OutboundMessageRepository outboundMessageRepository) {
        super(SENSOR_CLASS, new SensorRepositoryMapper());
        this.sensorZoneRepository = sensorZoneRepository;
        this.outboundMessageRepository = outboundMessageRepository;
    }

    @Override
    public int create(Sensor object) {
        int createdId = super.create(object);
        // associates zones with sensor
        object.setId(createdId);
        createSensorZones(object);
        return createdId;
    }

    @Override
    public Sensor read(int id) {
        Sensor sensor = super.read(id);
        sensor.setZones(getAllSensorZones(sensor.getId()));
        return sensor;
    }

    @Override
    public void update(Sensor object) {
        super.update(object);
        // deletes and creates sensor zones
        deleteSensorZones(object.getId());
        createSensorZones(object);
    }

    @Override
    public void delete(Sensor object) {
        // delete all sensor zones associated with the current sensor
        deleteSensorZones(object.getId());
        // delete outbound messages
        deleteOutboundMessages(object.getId());
        super.delete(object);
    }

    private void createSensorZones(Sensor sensor) {
        sensor
                .getZones()
                .forEach(sensorZone -> sensorZoneRepository.create(SensorZoneRepositoryMapper.transformToSensorZoneDTO(sensor.getId(), sensorZone)));
    }

    private List<SensorZone> getAllSensorZones(int sensorId) {
        return sensorZoneRepository
                .read(new HashMap<>())
                .stream()
                .filter(sensorZoneDTO -> sensorZoneDTO.getSensor() == sensorId)
                .map(SensorZoneRepositoryMapper::transformToSensorZone)
                .collect(Collectors.toList());
    }

    private void deleteSensorZones(int sensorId) {
        sensorZoneRepository
                .read(new HashMap<>())
                .stream()
                .filter(sensorZoneDTO -> sensorZoneDTO.getSensor() == sensorId)
                .forEach(sensorZoneRepository::delete);
    }

    private void deleteOutboundMessages(int sensorId) {
        outboundMessageRepository
                .read(new HashMap<>())
                .stream()
                .filter(outboundMessage -> outboundMessage.getSensor() == sensorId)
                .forEach(outboundMessageRepository::delete);
    }
}
