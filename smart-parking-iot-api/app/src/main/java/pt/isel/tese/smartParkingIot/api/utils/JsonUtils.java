package pt.isel.tese.smartParkingIot.api.utils;

import com.google.gson.Gson;

import java.util.Map;

public class JsonUtils {

    private static final Gson gson = new Gson();

    public static <T> T transformToObject(String json, Class<T> type) {
        return gson.fromJson(json, type);
    }

    public static <T> String transformToString(T object) {
        return gson.toJson(object);
    }

    public static Map<String, String> transformToMap(String json) {
        return gson.fromJson(json, Map.class);
    }
}
