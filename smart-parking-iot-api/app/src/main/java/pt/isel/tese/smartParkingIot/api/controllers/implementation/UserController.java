package pt.isel.tese.smartParkingIot.api.controllers.implementation;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pt.isel.tese.smartParkingIot.api.controllers.config.UriConfiguration;
import pt.isel.tese.smartParkingIot.api.controllers.contract.IUserController;
import pt.isel.tese.smartParkingIot.api.controllers.mappers.UserControllerMapper;
import pt.isel.tese.smartParkingIot.api.controllers.model.request.UserRequest;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ListItemResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.UserResponse;
import pt.isel.tese.smartParkingIot.api.controllers.utils.URIBuilder;
import pt.isel.tese.smartParkingIot.api.model.User;
import pt.isel.tese.smartParkingIot.api.logic.ServiceFacade;
import pt.isel.tese.smartParkingIot.api.logic.contracts.IUserService;
import pt.isel.tese.smartParkingIot.api.utils.ListUtils;

import javax.validation.Valid;
import java.net.URI;
import java.util.HashMap;
import java.util.List;

@RestController
public class UserController implements IUserController {

    private IUserService userService;
    private UserControllerMapper userMapper;

    public UserController(ServiceFacade serviceFacade, UserControllerMapper userMapper) {
        this.userService = serviceFacade.getUserService();
        this.userMapper = userMapper;
    }

    @Override
    public ResponseEntity<Void> createUser(@Valid @RequestBody UserRequest body) {
        User user = userMapper.transformToModel(body);
        int createdUserId = userService.create(user);
        URI createdUri = URIBuilder.getCreatedURI(UriConfiguration.USERS_ID_URI, createdUserId);
        return ResponseEntity.created(createdUri).build();
    }

    @Override
    public ResponseEntity<Void> deleteUser(@PathVariable("user-id") Integer userId) {
        userService.delete(userId);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<Void> editUser(@PathVariable("user-id") Integer userId, @Valid @RequestBody UserRequest body) {
        User user = userMapper.transformToModel(body);
        user.setId(userId);
        userService.update(user);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<UserResponse> readUser(@PathVariable("user-id") Integer userId) {
        User user = userService.read(userId);
        UserResponse response = userMapper.transformToResponse(user);
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<List<ListItemResponse>> readUsers(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size) {
        List<User> users = userService.read(new HashMap<>());
        List<ListItemResponse> response = ListUtils.mapList(users, userMapper::transformToListItem);
        return ResponseEntity.ok(response);
    }
}
