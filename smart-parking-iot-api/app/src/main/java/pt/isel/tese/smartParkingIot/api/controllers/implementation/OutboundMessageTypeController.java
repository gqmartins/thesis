package pt.isel.tese.smartParkingIot.api.controllers.implementation;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pt.isel.tese.smartParkingIot.api.controllers.config.UriConfiguration;
import pt.isel.tese.smartParkingIot.api.controllers.contract.IOutboundMessageTypeController;
import pt.isel.tese.smartParkingIot.api.controllers.mappers.OutboundMessageTypeControllerMapper;
import pt.isel.tese.smartParkingIot.api.controllers.model.request.OutboundMessageTypeRequest;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ListItemResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.OutboundMessageTypeResponse;
import pt.isel.tese.smartParkingIot.api.controllers.utils.URIBuilder;
import pt.isel.tese.smartParkingIot.api.model.OutboundMessageType;
import pt.isel.tese.smartParkingIot.api.logic.ServiceFacade;
import pt.isel.tese.smartParkingIot.api.logic.contracts.IOutboundMessageTypeService;
import pt.isel.tese.smartParkingIot.api.utils.ListUtils;

import javax.validation.Valid;
import java.net.URI;
import java.util.HashMap;
import java.util.List;

@RestController
public class OutboundMessageTypeController implements IOutboundMessageTypeController {

    private IOutboundMessageTypeService outboundMessageTypeService;
    private OutboundMessageTypeControllerMapper outboundMessageTypeControllerMapper;

    public OutboundMessageTypeController(ServiceFacade serviceFacade, OutboundMessageTypeControllerMapper outboundMessageTypeControllerMapper) {
        this.outboundMessageTypeService = serviceFacade.getOutboundMessageTypeService();
        this.outboundMessageTypeControllerMapper = outboundMessageTypeControllerMapper;
    }

    @Override
    public ResponseEntity<Void> createOutboundMessageType(@PathVariable("sensor-type-id") Integer sensorTypeId, @Valid @RequestBody OutboundMessageTypeRequest body) {
        OutboundMessageType outboundMessageType = outboundMessageTypeControllerMapper.transformToModel(body, sensorTypeId);
        int createdId = outboundMessageTypeService.create(outboundMessageType);
        URI createdUri = URIBuilder.getCreatedURI(UriConfiguration.OUTBOUND_MESSAGE_TYPES_ID_URI, sensorTypeId, createdId);
        return ResponseEntity.created(createdUri).build();
    }

    @Override
    public ResponseEntity<Void> deleteOutboundMessageType(@PathVariable("sensor-type-id") Integer sensorTypeId, @PathVariable("outbound-message-type-id") Integer outboundMessageTypeId) {
        outboundMessageTypeService.delete(outboundMessageTypeId);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<Void> editOutboundMessageType(@PathVariable("sensor-type-id") Integer sensorTypeId, @PathVariable("outbound-message-type-id") Integer outboundMessageTypeId, @Valid @RequestBody OutboundMessageTypeRequest body) {
        OutboundMessageType outboundMessageType = outboundMessageTypeControllerMapper.transformToModel(body, sensorTypeId);
        outboundMessageType.setId(outboundMessageTypeId);
        outboundMessageTypeService.update(outboundMessageType);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<OutboundMessageTypeResponse> readOutboundMessageType(@PathVariable("sensor-type-id") Integer sensorTypeId, @PathVariable("outbound-message-type-id") Integer outboundMessageTypeId) {
        OutboundMessageType outboundMessageType = outboundMessageTypeService.read(outboundMessageTypeId);
        OutboundMessageTypeResponse response = outboundMessageTypeControllerMapper.transformToResponse(outboundMessageType);
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<List<ListItemResponse>> readOutboundMessageTypes(@PathVariable("sensor-type-id") Integer sensorTypeId, Integer page, Integer size) {
        List<OutboundMessageType> outboundMessageTypes = outboundMessageTypeService.read(sensorTypeId, new HashMap<>());
        List<ListItemResponse> response = ListUtils.mapList(outboundMessageTypes, outboundMessageTypeControllerMapper::transformToListItem);
        return ResponseEntity.ok(response);
    }
}
