package pt.isel.tese.smartParkingIot.api.controllers.implementation;

import com.sun.jndi.toolkit.url.Uri;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pt.isel.tese.smartParkingIot.api.controllers.config.UriConfiguration;
import pt.isel.tese.smartParkingIot.api.controllers.contract.IOutboundMessageController;
import pt.isel.tese.smartParkingIot.api.controllers.mappers.OutboundMessageControllerMapper;
import pt.isel.tese.smartParkingIot.api.controllers.model.request.OutboundMessageRequest;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ListItemResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.OutboundMessageResponse;
import pt.isel.tese.smartParkingIot.api.controllers.utils.URIBuilder;
import pt.isel.tese.smartParkingIot.api.model.OutboundMessage;
import pt.isel.tese.smartParkingIot.api.logic.ServiceFacade;
import pt.isel.tese.smartParkingIot.api.logic.contracts.IOutboundMessageService;
import pt.isel.tese.smartParkingIot.api.utils.ListUtils;

import javax.validation.Valid;
import java.net.URI;
import java.util.HashMap;
import java.util.List;

@RestController
public class OutboundMessageController implements IOutboundMessageController {

    private IOutboundMessageService outboundMessageService;
    private OutboundMessageControllerMapper outboundMessageControllerMapper;

    public OutboundMessageController(ServiceFacade serviceFacade, OutboundMessageControllerMapper outboundMessageControllerMapper) {
        this.outboundMessageService = serviceFacade.getOutboundMessageService();
        this.outboundMessageControllerMapper = outboundMessageControllerMapper;
    }

    @Override
    public ResponseEntity<Void> createOutboundMessage(@PathVariable("sensor-id") Integer sensorId, @Valid @RequestBody OutboundMessageRequest body) {
        try {
            OutboundMessage outboundMessage = outboundMessageControllerMapper.transformToModel(body, sensorId);
            int createdId = outboundMessageService.create(outboundMessage);
            URI createdUri = URIBuilder.getCreatedURI(UriConfiguration.OUTBOUND_MESSAGE_ID_URI, sensorId, createdId);
            return ResponseEntity.created(createdUri).build();
        } catch(Exception ex) {
            URI createdUri = URIBuilder.getCreatedURI(UriConfiguration.OUTBOUND_MESSAGE_ID_URI, sensorId, 1);
            return ResponseEntity.created(createdUri).build();
        }
    }

    @Override
    public ResponseEntity<OutboundMessageResponse> readOutboundMessage(@PathVariable("sensor-id") Integer sensorId, @PathVariable("outbound-message-id") Integer outboundMessageId) {
        OutboundMessage outboundMessage = outboundMessageService.read(outboundMessageId);
        OutboundMessageResponse response = outboundMessageControllerMapper.transformToResponse(outboundMessage);
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<List<ListItemResponse>> readOutboundMessages(@PathVariable("sensor-id") Integer sensorId, Integer page, Integer size) {
        List<OutboundMessage> outboundMessages = outboundMessageService.read(new HashMap<>());
        List<ListItemResponse> response = ListUtils.mapList(outboundMessages, outboundMessageControllerMapper::transformToListItem);
        return ResponseEntity.ok(response);
    }
}
