package pt.isel.tese.smartParkingIot.api.controllers.implementation;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pt.isel.tese.smartParkingIot.api.controllers.config.UriConfiguration;
import pt.isel.tese.smartParkingIot.api.controllers.contract.IZoneController;
import pt.isel.tese.smartParkingIot.api.controllers.mappers.PredictionMapper;
import pt.isel.tese.smartParkingIot.api.controllers.mappers.ZoneControllerMapper;
import pt.isel.tese.smartParkingIot.api.controllers.model.request.ZoneRequest;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ListItemResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.PredictionResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ZoneResponse;
import pt.isel.tese.smartParkingIot.api.controllers.utils.DateUtils;
import pt.isel.tese.smartParkingIot.api.controllers.utils.URIBuilder;
import pt.isel.tese.smartParkingIot.api.model.Zone;
import pt.isel.tese.smartParkingIot.api.model.util.Prediction;
import pt.isel.tese.smartParkingIot.api.logic.ServiceFacade;
import pt.isel.tese.smartParkingIot.api.logic.contracts.IZoneService;
import pt.isel.tese.smartParkingIot.api.utils.ListUtils;

import javax.validation.Valid;
import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@RestController
public class ZoneController implements IZoneController {

    private IZoneService zoneService;
    private ZoneControllerMapper zoneMapper;
    private PredictionMapper predictionMapper;

    public ZoneController(ServiceFacade serviceFacade, ZoneControllerMapper zoneMapper, PredictionMapper predictionMapper) {
        this.zoneService = serviceFacade.getZoneService();
        this.zoneMapper = zoneMapper;
        this.predictionMapper = predictionMapper;
    }

    @Override
    public ResponseEntity<Void> createZone(@Valid @RequestBody ZoneRequest body) {
        Zone zone = zoneMapper.transformToModel(body);
        int createdZoneId = zoneService.create(zone);
        URI createdUri = URIBuilder.getCreatedURI(UriConfiguration.ZONES_ID_URI, createdZoneId);
        return ResponseEntity.created(createdUri).build();
    }

    @Override
    public ResponseEntity<Void> deleteZone(@PathVariable("zone-id") Integer zoneId) {
        zoneService.delete(zoneId);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<Void> editZone(@PathVariable("zone-id") Integer zoneId, @Valid @RequestBody ZoneRequest body) {
        Zone zone = zoneMapper.transformToModel(body);
        zone.setId(zoneId);
        zoneService.update(zone);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<ZoneResponse> readZone(@PathVariable("zone-id") Integer zoneId) {
        Zone zone = zoneService.read(zoneId);
        ZoneResponse response = zoneMapper.transformToResponse(zone);
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<List<ListItemResponse>> readZones(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size) {
        List<Zone> zones = zoneService.read(new HashMap<>());
        List<ListItemResponse> response = ListUtils.mapList(zones, zoneMapper::transformToListItem);
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<List<PredictionResponse>> readZonePredictions(@PathVariable(value = "zone-id") Integer zoneId, @RequestParam(value = "start", required = false) String start, @RequestParam(value = "end", required = false) String end, @RequestParam(value = "interval", required = false, defaultValue = "20") int interval) {
        Date startDate = DateUtils.parseDate(start, true);
        Date endDate = DateUtils.parseDate(end, false);
        List<Prediction> predictions = zoneService.getPredictions(zoneId, startDate, endDate, interval);
        List<PredictionResponse> response = ListUtils.mapList(predictions, predictionMapper::getPredictionResponse);
        return ResponseEntity.ok(response);
    }
}
