package pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate;

import org.springframework.stereotype.Repository;
import pt.isel.tese.smartParkingIot.api.model.User;
import pt.isel.tese.smartParkingIot.api.repository.contracts.IUserRepository;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto.UserDTO;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.mappers.UserRepositoryMapper;

@Repository
public class UserRepository extends BaseRepository<User, UserDTO> implements IUserRepository {

    public UserRepository() {
        super(UserDTO.class, new UserRepositoryMapper());
    }
}
