package pt.isel.tese.smartParkingIot.api.model.util;

public enum Roles {

    ADMIN("Admin"),
    VIEWER("Viewer"),
    MANAGER("Manager");

    private String role;

    Roles(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }
}
