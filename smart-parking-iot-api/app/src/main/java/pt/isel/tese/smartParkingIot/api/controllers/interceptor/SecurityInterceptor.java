package pt.isel.tese.smartParkingIot.api.controllers.interceptor;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.SignatureException;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.UriTemplate;
import pt.isel.tese.smartParkingIot.api.controllers.config.AppConfiguration;
import pt.isel.tese.smartParkingIot.api.controllers.config.UriConfiguration;
import pt.isel.tese.smartParkingIot.api.controllers.exceptions.ForbiddenException;
import pt.isel.tese.smartParkingIot.api.controllers.exceptions.UnauthorizedException;
import pt.isel.tese.smartParkingIot.api.controllers.model.util.JsonWebToken;
import pt.isel.tese.smartParkingIot.api.controllers.utils.JwtUtils;
import pt.isel.tese.smartParkingIot.api.model.util.PermissionUri;
import pt.isel.tese.smartParkingIot.api.logic.contracts.IAuthorizationService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@CrossOrigin
@Component
public class SecurityInterceptor extends HandlerInterceptorAdapter {

    private AppConfiguration appConfiguration;
    private IAuthorizationService authorizationService;
    private static final String AUTHORIZATION_HEADER = "Authorization";

    public SecurityInterceptor(AppConfiguration appConfiguration, IAuthorizationService authorizationService) {
        this.appConfiguration = appConfiguration;
        this.authorizationService = authorizationService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String requestUri = getUri(request.getRequestURI());
        if(requestUri.equals(UriConfiguration.LOGIN_URI) || requestUri.equals(UriConfiguration.ERROR_URI)
                || requestUri.equals(UriConfiguration.RENEW_TOKEN_URI) || requestUri.equals(UriConfiguration.LOGOUT_URI)
                || requestUri.equals(UriConfiguration.SWAGGER_URI)) {
            return true;
        }

        // get jwt
        String authorizationHeader = request.getHeader(AUTHORIZATION_HEADER);
        JsonWebToken token = JwtUtils.parseToken(authorizationHeader, appConfiguration.jwtSecret);
        // get permitted uris for role
        List<PermissionUri> roleUris = authorizationService.getUrisForRole(token.getRole());
        // validate if user role can access the uri
        validateUris(request.getMethod(), requestUri, roleUris);

        return super.preHandle(request, response, handler);
    }

    private String getUri(String requestUri) {
        int baseUriLength = appConfiguration.serverContextPath.length();
        return requestUri.substring(baseUriLength);
    }

    private void validateUris(String requestMethod, String requestUri, List<PermissionUri> roleUris) {
        for(PermissionUri permissionUri : roleUris) {
            UriTemplate uriTemplate = new UriTemplate(permissionUri.getPath());
            if(requestMethod.equals(permissionUri.getMethod()) &&  uriTemplate.matches(requestUri)) {
                return;
            }
        }
        throw new ForbiddenException("Invalid request.", "User is not allowed to make such request.");
    }
}
