package pt.isel.tese.smartParkingIot.api.logic.contracts;

import pt.isel.tese.smartParkingIot.api.model.OutboundMessage;

public interface IOutboundMessageService extends IBaseService<OutboundMessage> { }
