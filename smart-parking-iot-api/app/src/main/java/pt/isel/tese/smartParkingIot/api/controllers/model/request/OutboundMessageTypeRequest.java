package pt.isel.tese.smartParkingIot.api.controllers.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.util.List;

public class OutboundMessageTypeRequest {

    @NotNull(message = "A name must be provided.")
    @JsonProperty("name")
    private String name = null;

    @JsonProperty("fields")
    private List<String> fields = null;

    public OutboundMessageTypeRequest() { }

    public OutboundMessageTypeRequest(String name, List<String> fields) {
        this.name = name;
        this.fields = fields;
    }

    /**
     * Get name
     * @return name
     **/
    @ApiModelProperty(example = "set message time", required = true, value = "")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get fields associated with message type
     * @return zones
     **/
    @ApiModelProperty(required = true, value = "")
    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }
}
