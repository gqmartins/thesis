package pt.isel.tese.smartParkingIot.api.logic.implementations;

import org.springframework.stereotype.Service;
import pt.isel.tese.smartParkingIot.api.model.Role;
import pt.isel.tese.smartParkingIot.api.repository.RepositoryFacade;
import pt.isel.tese.smartParkingIot.api.repository.contracts.IRoleRepository;
import pt.isel.tese.smartParkingIot.api.logic.contracts.IRoleService;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class RoleService implements IRoleService {

    private IRoleRepository roleRepository;

    public RoleService(RepositoryFacade repositoryFacade) {
        this.roleRepository = repositoryFacade.getRoleRepository();
    }

    @Override
    public int create(Role object) {
        throw new NotImplementedException();
    }

    @Override
    public List<Role> read(Map<String, String> filters) {
        return roleRepository.read(new HashMap<>());
    }

    @Override
    public Role read(int id) {
        return roleRepository.read(id);
    }

    @Override
    public void update(Role object) {
        throw new NotImplementedException();
    }

    @Override
    public void delete(int id) {
        throw new NotImplementedException();
    }
}
