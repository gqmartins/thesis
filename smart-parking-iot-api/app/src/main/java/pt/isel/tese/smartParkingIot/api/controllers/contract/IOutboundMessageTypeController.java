package pt.isel.tese.smartParkingIot.api.controllers.contract;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.isel.tese.smartParkingIot.api.controllers.model.request.OutboundMessageTypeRequest;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ErrorResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ListItemResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.OutboundMessageTypeResponse;

import javax.validation.Valid;
import java.util.List;

import static pt.isel.tese.smartParkingIot.api.controllers.config.UriConfiguration.OUTBOUND_MESSAGE_TYPES_ID_URI;
import static pt.isel.tese.smartParkingIot.api.controllers.config.UriConfiguration.OUTBOUND_MESSAGE_TYPES_URI;

@Api(value = "outbound message type", description = "the outbound message type API")
public interface IOutboundMessageTypeController {

    @ApiOperation(value = "adds a outbound message type to the system", nickname = "createOutboundMessageType", notes = "adds a outbound message type to the system", tags={ "Outbound message type" })
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "outbound message type successfully added to the system. to get the outbound message type added to the system make a get request to the response header location URI."),
            @ApiResponse(code = 400, message = "Bad request parameter", response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class) })
    @PostMapping(value = OUTBOUND_MESSAGE_TYPES_URI, produces = { "application/json" }, consumes = { "application/json" })
    ResponseEntity<Void> createOutboundMessageType(@ApiParam(value = "sensor type id",required=true) @PathVariable("sensor-type-id") Integer sensorTypeId, @ApiParam(value = "Outbound message type to add") @Valid @RequestBody OutboundMessageTypeRequest body);

    @ApiOperation(value = "deletes the outbound message type with the specified outboundMessageType-id", nickname = "deleteOutboundMessageType", notes = "delete the outbound message type with the specified outbound-message-type-id", tags={ "Outbound message type", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "outbound message type successfully deleted."),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "The specified outbound message type does not exist", response = ErrorResponse.class) })
    @DeleteMapping(value = OUTBOUND_MESSAGE_TYPES_ID_URI, produces = { "application/json" })
    ResponseEntity<Void> deleteOutboundMessageType(@ApiParam(value = "sensor type id",required=true) @PathVariable("sensor-type-id") Integer sensorTypeId, @ApiParam(value = "outbound message type id",required=true) @PathVariable("outbound-message-type-id") Integer outboundMessageTypeId);

    @ApiOperation(value = "edits the outbound message type with the specified outbound-message-type-id", nickname = "editOutboundMessageType", notes = "edits the outbound message type with the specified outbound-message-type-id", tags={ "Outbound message type", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "outbound message type successfully edited. to get the outbound message type edited make a GET request to the URI used to edit the outbound message type."),
            @ApiResponse(code = 400, message = "Bad request parameter", response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "The specified outbound message type does not exist", response = ErrorResponse.class) })
    @PutMapping(value = OUTBOUND_MESSAGE_TYPES_ID_URI, produces = { "application/json" }, consumes = { "application/json" })
    ResponseEntity<Void> editOutboundMessageType(@ApiParam(value = "sensor type id",required=true) @PathVariable("sensor-type-id") Integer sensorTypeId, @ApiParam(value = "outbound message type id",required=true) @PathVariable("outbound-message-type-id") Integer outboundMessageTypeId,@ApiParam(value = "outbound message type to edit"  )  @Valid @RequestBody OutboundMessageTypeRequest body);

    @ApiOperation(value = "returns outbound message type with the specified outbound-message-type-id", nickname = "readOutboundMessageType", notes = "returns outbound message type with the specified outbound-message-type-id", response = OutboundMessageTypeResponse.class, tags={ "Outbound message type", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "outbound message type with the specified outbound-message-type-id", response = OutboundMessageTypeResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "The specified outbound message type does not exist", response = ErrorResponse.class) })
    @GetMapping(value = OUTBOUND_MESSAGE_TYPES_ID_URI, produces = { "application/json" })
    ResponseEntity<OutboundMessageTypeResponse> readOutboundMessageType(@ApiParam(value = "sensor type id",required=true) @PathVariable("sensor-type-id") Integer sensorTypeId, @ApiParam(value = "outbound message type id",required=true) @PathVariable("outbound-message-type-id") Integer outboundMessageTypeId);

    @ApiOperation(value = "returns outbound message types added to the system", nickname = "readOutboundMessageTypes", notes = "returns all the outbound message types that were added to the system, ordered by creation date", response = ListItemResponse.class, responseContainer = "List", tags={ "Outbound message type", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "outbound message type result matching search criteria", response = ListItemResponse.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad request parameter", response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class) })
    @GetMapping(value = OUTBOUND_MESSAGE_TYPES_URI, produces = { "application/json" })
    ResponseEntity<List<ListItemResponse>> readOutboundMessageTypes(@ApiParam(value = "sensor type id",required=true) @PathVariable("sensor-type-id") Integer sensorTypeId, @ApiParam(value = "number of page for pagination results") @Valid @RequestParam(value = "page", required = false) Integer page, @ApiParam(value = "number of elements to return in a page") @Valid @RequestParam(value = "size", required = false) Integer size);
}
