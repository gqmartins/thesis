package pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Zone")
public class ZoneDTO {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "max_count")
    private int maxCount;

    @Column(name = "current_count")
    private int currentCount;

    @Column(name = "created")
    private Timestamp created;

    @Column(name = "modified")
    private Timestamp modified;

    @Column(name = "perimeter")
    private String perimeter;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "sensor")
    private List<SensorZoneDTO> sensors = new ArrayList<>();

    public ZoneDTO() { }

    public ZoneDTO(int id) {
        this.id = id;
    }

    public ZoneDTO(int id, String name, int maxCount, int currentCount, Timestamp created, Timestamp modified, String perimeter, List<SensorZoneDTO> sensors) {
        this.id = id;
        this.name = name;
        this.maxCount = maxCount;
        this.currentCount = currentCount;
        this.created = created;
        this.modified = modified;
        this.perimeter = perimeter;
        this.sensors = sensors;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(int maxCount) {
        this.maxCount = maxCount;
    }

    public int getCurrentCount() {
        return currentCount;
    }

    public void setCurrentCount(int currentCount) {
        this.currentCount = currentCount;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    public String getPerimeter() {
        return perimeter;
    }

    public void setPerimeter(String perimeter) {
        this.perimeter = perimeter;
    }

    public List<SensorZoneDTO> getSensors() {
        return sensors;
    }

    public void setSensors(List<SensorZoneDTO> sensors) {
        this.sensors = sensors;
    }
}
