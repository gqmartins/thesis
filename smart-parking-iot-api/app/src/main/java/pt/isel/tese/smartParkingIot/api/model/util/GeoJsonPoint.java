package pt.isel.tese.smartParkingIot.api.model.util;

import java.util.List;

public class GeoJsonPoint {

    private String type = "Point";
    private List<Float> coordinates;

    public GeoJsonPoint(List<Float> coordinates) {
        this.coordinates = coordinates;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Float> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<Float> coordinates) {
        this.coordinates = coordinates;
    }
}
