package pt.isel.tese.smartParkingIot.api.logic.implementations;

import org.springframework.stereotype.Service;
import pt.isel.tese.smartParkingIot.api.logic.exceptions.DeleteSensorTypeException;
import pt.isel.tese.smartParkingIot.api.model.Sensor;
import pt.isel.tese.smartParkingIot.api.model.SensorType;
import pt.isel.tese.smartParkingIot.api.repository.RepositoryFacade;
import pt.isel.tese.smartParkingIot.api.repository.contracts.IOutboundMessageTypeRepository;
import pt.isel.tese.smartParkingIot.api.repository.contracts.ISensorRepository;
import pt.isel.tese.smartParkingIot.api.repository.contracts.ISensorTypeRepository;
import pt.isel.tese.smartParkingIot.api.logic.contracts.ISensorTypeService;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SensorTypeService implements ISensorTypeService {

    private ISensorTypeRepository sensorTypeRepository;
    private ISensorRepository sensorRepository;
    private IOutboundMessageTypeRepository outboundMessageTypeRepository;

    public SensorTypeService(RepositoryFacade repositoryFacade) {
        this.sensorTypeRepository = repositoryFacade.getSensorTypeRepository();
        this.sensorRepository = repositoryFacade.getSensorRepository();
        this.outboundMessageTypeRepository = repositoryFacade.getOutboundMessageTypeRepository();
    }

    @Override
    public int create(SensorType object) {
        object.setCreated(new Date());
        object.setModified(object.getCreated());
        return sensorTypeRepository.create(object);
    }

    @Override
    public List<SensorType> read(Map<String, String> filters) {
        return sensorTypeRepository.read(new HashMap<>());
    }

    @Override
    public SensorType read(int id) {
        return sensorTypeRepository.read(id);
    }

    @Override
    public void update(SensorType object) {
        SensorType savedSensorType = sensorTypeRepository.read(object.getId());

        savedSensorType.setName(object.getName());
        savedSensorType.setFields(object.getFields());
        savedSensorType.setModified(new Date());

        sensorTypeRepository.update(savedSensorType);
    }

    @Override
    public void delete(int id) {
        if(isSensorsAssociatedWithSensorType(id)) {
            throw new DeleteSensorTypeException();
        }

        SensorType sensorType = new SensorType(id);
        deleteOutboundMessageTypes(sensorType);
        sensorTypeRepository.delete(sensorType);
    }

    private boolean isSensorsAssociatedWithSensorType(int sensorTypeId) {
        List<Sensor> sensors = sensorRepository.read(new HashMap<>());
        return sensors
                .stream()
                .anyMatch(sensor -> sensor.getSensorType() == sensorTypeId);
    }

    private void deleteOutboundMessageTypes(SensorType sensorType) {
        outboundMessageTypeRepository.read(new HashMap<>())
                .stream()
                .filter(messageType -> messageType.getSensorTypeId() == sensorType.getId())
                .forEach(outboundMessageTypeRepository::delete);
    }
}
