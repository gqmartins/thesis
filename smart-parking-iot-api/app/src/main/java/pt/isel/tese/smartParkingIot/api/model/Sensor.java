package pt.isel.tese.smartParkingIot.api.model;

import pt.isel.tese.smartParkingIot.api.model.util.GeoJsonPoint;
import pt.isel.tese.smartParkingIot.api.model.util.SensorZone;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class Sensor {

    private int id;
    private String name;
    private GeoJsonPoint location;
    private List<SensorZone> zones;
    private Map<String, String> fieldValues;
    private int sensorType;
    private Date created;
    private Date modified;

    public Sensor(int id) {
        this.id = id;
    }

    public Sensor(String name, GeoJsonPoint location, List<SensorZone> zones, Map<String, String> fieldValues, int sensorType) {
        this.name = name;
        this.location = location;
        this.zones = zones;
        this.fieldValues = fieldValues;
        this.sensorType = sensorType;
    }

    public Sensor(int id, String name, GeoJsonPoint location, List<SensorZone> zones, Map<String, String> fieldValues, int sensorType, Date created, Date modified) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.zones = zones;
        this.fieldValues = fieldValues;
        this.sensorType = sensorType;
        this.created = created;
        this.modified = modified;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GeoJsonPoint getLocation() {
        return location;
    }

    public void setLocation(GeoJsonPoint location) {
        this.location = location;
    }

    public List<SensorZone> getZones() {
        return zones;
    }

    public void setZones(List<SensorZone> zones) {
        this.zones = zones;
    }

    public int getSensorType() {
        return sensorType;
    }

    public void setSensorType(int sensorType) {
        this.sensorType = sensorType;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public Map<String, String> getFieldValues() {
        return fieldValues;
    }

    public void setFieldValues(Map<String, String> fieldValues) {
        this.fieldValues = fieldValues;
    }
}
