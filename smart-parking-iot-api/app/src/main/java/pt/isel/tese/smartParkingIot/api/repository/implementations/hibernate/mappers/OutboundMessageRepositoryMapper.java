package pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.mappers;

import pt.isel.tese.smartParkingIot.api.model.OutboundMessage;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto.OutboundMessageDTO;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto.OutboundMessageTypeDTO;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto.SensorDTO;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.utils.DateUtils;
import pt.isel.tese.smartParkingIot.api.utils.JsonUtils;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

public class OutboundMessageRepositoryMapper implements IRepositoryMapper<OutboundMessage, OutboundMessageDTO> {

    @Override
    public OutboundMessage transformToObject(OutboundMessageDTO dto) {
        Date created = DateUtils.convertDateToTimestamp(dto.getCreated());
        Map<String, String> fieldValues = JsonUtils.transformToMap(dto.getBody());
        return new OutboundMessage(dto.getId(), fieldValues, dto.getSensor().getId(),
                dto.getOutboundMessageType().getId(), created);
    }

    @Override
    public OutboundMessageDTO transformToDto(OutboundMessage model) {
        Timestamp created = DateUtils.convertDateToTimestamp(model.getCreated());
        SensorDTO sensor = new SensorDTO(model.getSensor());
        OutboundMessageTypeDTO outboundMessageType = new OutboundMessageTypeDTO(model.getType());
        String fieldValues = JsonUtils.transformToString(model.getBody());
        return new OutboundMessageDTO(model.getId(), fieldValues, created, sensor, outboundMessageType);
    }

    @Override
    public int getIdentifier(OutboundMessage model) {
        return model.getId();
    }
}
