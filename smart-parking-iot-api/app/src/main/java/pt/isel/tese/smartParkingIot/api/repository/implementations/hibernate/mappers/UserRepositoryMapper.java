package pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.mappers;

import pt.isel.tese.smartParkingIot.api.model.User;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto.RoleDTO;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto.UserDTO;

public class UserRepositoryMapper implements IRepositoryMapper<User, UserDTO> {

    @Override
    public User transformToObject(UserDTO dto) {
        return new User(dto.getId(), dto.getUsername(), dto.getName(), dto.getRole().getId(), dto.getPassword(),
                dto.getCreated(), dto.getModified());
    }

    @Override
    public UserDTO transformToDto(User object) {
        return new UserDTO(object.getId(), object.getUsername(), object.getName(), new RoleDTO(object.getRole()),
                object.getPassword(), object.getCreated(), object.getModified());
    }

    @Override
    public int getIdentifier(User model) {
        return model.getId();
    }
}
