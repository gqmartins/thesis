package pt.isel.tese.smartParkingIot.api.logic.contracts;

import pt.isel.tese.smartParkingIot.api.model.SensorType;

public interface ISensorTypeService extends IBaseService<SensorType> { }
