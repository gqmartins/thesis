package pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "OutboundMessageType")
public class OutboundMessageTypeDTO {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "fields")
    private String fields;

    @Column(name = "created")
    private Timestamp created;

    @Column(name = "modified")
    private Timestamp modified;

    @ManyToOne
    @JoinColumn(name = "sensor_type_id")
    private SensorTypeDTO sensorType;

    public OutboundMessageTypeDTO() { }

    public OutboundMessageTypeDTO(int id) {
        this.id = id;
    }

    public OutboundMessageTypeDTO(int id, String name, String fields, SensorTypeDTO sensorType, Timestamp created, Timestamp modified) {
        this.id = id;
        this.name = name;
        this.fields = fields;
        this.sensorType = sensorType;
        this.created = created;
        this.modified = modified;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFields() {
        return fields;
    }

    public void setFields(String fields) {
        this.fields = fields;
    }

    public SensorTypeDTO getSensorType() {
        return sensorType;
    }

    public void setSensorType(SensorTypeDTO sensorType) {
        this.sensorType = sensorType;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }
}
