package pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Sensor")
public class SensorDTO {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "location")
    private String location;

    @Column(name = "field_values")
    private String fieldValues;

    @Column(name = "created")
    private Timestamp created;

    @Column(name = "modified")
    private Timestamp modified;

    @ManyToOne
    @JoinColumn(name = "sensor_type_id")
    private SensorTypeDTO sensorType;

    public SensorDTO() { }

    public SensorDTO(int id) {
        this.id = id;
    }

    public SensorDTO(int id, String name, String location, String fieldValues, Timestamp created, Timestamp modified,
                     SensorTypeDTO sensorType) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.fieldValues = fieldValues;
        this.created = created;
        this.modified = modified;
        this.sensorType = sensorType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    public SensorTypeDTO getSensorType() {
        return sensorType;
    }

    public void setSensorType(SensorTypeDTO sensorType) {
        this.sensorType = sensorType;
    }

    public String getFieldValues() {
        return fieldValues;
    }

    public void setFieldValues(String fieldValues) {
        this.fieldValues = fieldValues;
    }
}
