package pt.isel.tese.smartParkingIot.api.model;

import java.util.Date;

public class User {

    private int id;
    private String username;
    private String name;
    private int role;
    private String password;
    private Date created;
    private Date modified;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User(String username, String name, int role, String password) {
        this(username, password);
        this.name = name;
        this.role = role;
    }

    public User(int id, String username, String name, int role, String password, Date created, Date modified) {
        this(username, name, role, password);
        this.id = id;
        this.created = created;
        this.modified = modified;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }
}
