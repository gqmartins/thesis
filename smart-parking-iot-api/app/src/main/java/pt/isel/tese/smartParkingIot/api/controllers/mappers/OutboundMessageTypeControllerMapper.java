package pt.isel.tese.smartParkingIot.api.controllers.mappers;

import org.springframework.stereotype.Component;
import pt.isel.tese.smartParkingIot.api.controllers.config.UriConfiguration;
import pt.isel.tese.smartParkingIot.api.controllers.model.request.OutboundMessageTypeRequest;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ListItemResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.OutboundMessageTypeResponse;
import pt.isel.tese.smartParkingIot.api.controllers.utils.ResponseUtils;
import pt.isel.tese.smartParkingIot.api.controllers.utils.URIBuilder;
import pt.isel.tese.smartParkingIot.api.model.OutboundMessageType;

@Component
public class OutboundMessageTypeControllerMapper {

    public OutboundMessageType transformToModel(OutboundMessageTypeRequest request, int sensorTypeId) {
        return new OutboundMessageType(request.getName(), request.getFields(), sensorTypeId);
    }

    public OutboundMessageTypeResponse transformToResponse(OutboundMessageType model) {
        String created = ResponseUtils.convertDateToString(model.getCreated());
        String modified = ResponseUtils.convertDateToString(model.getModified());
        return new OutboundMessageTypeResponse(model.getId(), model.getName(), model.getFields(), model.getSensorTypeId(), created, modified);
    }

    public ListItemResponse transformToListItem(OutboundMessageType model) {
        String href = URIBuilder.getCreatedURI(UriConfiguration.OUTBOUND_MESSAGE_TYPES_ID_URI, model.getSensorTypeId(), model.getId()).toString();
        String created = ResponseUtils.convertDateToString(model.getCreated());
        String modified = ResponseUtils.convertDateToString(model.getModified());
        return new ListItemResponse(model.getId(), model.getName(), created, modified, href);
    }
}
