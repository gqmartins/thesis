package pt.isel.tese.smartParkingIot.api.controllers.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pt.isel.tese.smartParkingIot.api.controllers.config.AppConfiguration;

import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;

@Component
public class URIBuilder {

    @Autowired
    private AppConfiguration appConfiguration;
    private static String BASE_PATH;

    @Autowired
    public void setFullURI() {
        try {
            String protocol = "http://";
            URIBuilder.BASE_PATH = protocol + getUri();
        } catch (UnknownHostException e) {
            throw new RuntimeException("Error building full URI.");
        }
    }

    public static URI getCreatedURI(String baseUri, Object ... params) {
        try {
            return new URI(getCreatedURI(BASE_PATH + baseUri, 0, params));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getCreatedURIString(String baseUri, Object ... params) {
        try {
            return getCreatedURI(BASE_PATH + baseUri, 0, params);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String getCreatedURI(String baseUri, int currentIndex, Object ... params) throws URISyntaxException {
        if(currentIndex == params.length) {
            return baseUri;
        }
        int firstBracesIndex = baseUri.indexOf("{");
        int lastBracesIndex = baseUri.indexOf("}");
        String toReplace = baseUri.substring(firstBracesIndex, lastBracesIndex + 1);
        String uri = baseUri.replace(toReplace, String.valueOf(params[currentIndex]));
        return getCreatedURI(uri, currentIndex + 1, params);
    }

    private String getUri() throws UnknownHostException {
        String hostAddress = InetAddress.getLocalHost().getHostAddress();
        String port = ":" + appConfiguration.serverPort;
        String context = appConfiguration.serverContextPath;
        return hostAddress + port + context;
    }
}
