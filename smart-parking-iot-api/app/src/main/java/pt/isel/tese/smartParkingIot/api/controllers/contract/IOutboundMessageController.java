package pt.isel.tese.smartParkingIot.api.controllers.contract;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.isel.tese.smartParkingIot.api.controllers.model.request.OutboundMessageRequest;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ErrorResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ListItemResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.OutboundMessageResponse;

import javax.validation.Valid;
import java.util.List;

import static pt.isel.tese.smartParkingIot.api.controllers.config.UriConfiguration.OUTBOUND_MESSAGE_ID_URI;
import static pt.isel.tese.smartParkingIot.api.controllers.config.UriConfiguration.OUTBOUND_MESSAGE_URI;

@Api(value = "outbound message", description = "the outbound message API")
public interface IOutboundMessageController {

    @ApiOperation(value = "adds a outbound message to the system", nickname = "createOutboundMessage", notes = "adds a outbound message to the system", tags={ "Outbound message", })
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "outbound message successfully added to the system. to get the outbound message added to the system make a get request to the response header location URI."),
            @ApiResponse(code = 400, message = "Bad request parameter", response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class) })
    @PostMapping(value = OUTBOUND_MESSAGE_URI, produces = { "application/json" }, consumes = { "application/json" })
    ResponseEntity<Void> createOutboundMessage(@ApiParam(value = "sensor id",required=true) @PathVariable("sensor-id") Integer sensorId, @ApiParam(value = "Outbound message to add"  )  @Valid @RequestBody OutboundMessageRequest body);

    @ApiOperation(value = "returns outbound message with the specified outbound-message-id", nickname = "readOutboundMessage", notes = "returns outbound message with the specified outbound-message-id", response = OutboundMessageResponse.class, tags={ "Outbound message", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "outbound message with the specified outbound-message-id", response = OutboundMessageResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "The specified outbound message does not exist", response = ErrorResponse.class) })
    @GetMapping(value = OUTBOUND_MESSAGE_ID_URI, produces = { "application/json" })
    ResponseEntity<OutboundMessageResponse> readOutboundMessage(@ApiParam(value = "sensor id",required=true) @PathVariable("sensor-id") Integer sensorId, @ApiParam(value = "outbound message id",required=true) @PathVariable("outbound-message-id") Integer outboundMessageId);

    @ApiOperation(value = "returns outbound messages added to the system", nickname = "readOutboundMessages", notes = "returns all the outbound messages that were added to the system, ordered by creation date", response = ListItemResponse.class, responseContainer = "List", tags={ "Outbound message", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "outbound message result matching search criteria", response = ListItemResponse.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad request parameter", response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class) })
    @GetMapping(value = OUTBOUND_MESSAGE_URI, produces = { "application/json" })
    ResponseEntity<List<ListItemResponse>> readOutboundMessages(@ApiParam(value = "sensor id",required=true) @PathVariable("sensor-id") Integer sensorId, @ApiParam(value = "number of page for pagination results") @Valid @RequestParam(value = "page", required = false) Integer page, @ApiParam(value = "number of elements to return in a page") @Valid @RequestParam(value = "size", required = false) Integer size);
}
