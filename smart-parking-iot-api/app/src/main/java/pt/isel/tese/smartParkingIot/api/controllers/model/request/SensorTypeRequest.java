package pt.isel.tese.smartParkingIot.api.controllers.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.util.List;

public class SensorTypeRequest {

    @JsonProperty("name")
    @NotNull(message = "A name must be provided.")
    private String name = null;

    @JsonProperty("fields")
    private List<String> fields = null;

    public SensorTypeRequest() { }

    public SensorTypeRequest(String name, List<String> fields) {
        this.name = name;
        this.fields = fields;
    }

    /**
     * Get name
     * @return name
     **/
    @ApiModelProperty(example = "sensor type 1", required = true, value = "")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get fields associated with sensor type
     * @return zones
     **/
    @ApiModelProperty(required = true, value = "")
    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }
}
