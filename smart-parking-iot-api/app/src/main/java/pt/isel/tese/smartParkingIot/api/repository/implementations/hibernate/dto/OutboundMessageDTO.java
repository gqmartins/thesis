package pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "OutboundMessage")
public class OutboundMessageDTO {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "body")
    private String body;

    @Column(name = "created")
    private Timestamp created;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sensor_id")
    private SensorDTO sensor;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "outbound_message_type_id")
    private OutboundMessageTypeDTO outboundMessageType;

    public OutboundMessageDTO() { }

    public OutboundMessageDTO(int id) {
        this.id = id;
    }

    public OutboundMessageDTO(int id, String body, Timestamp created, SensorDTO sensor, OutboundMessageTypeDTO outboundMessageType) {
        this.id = id;
        this.body = body;
        this.created = created;
        this.sensor = sensor;
        this.outboundMessageType = outboundMessageType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public SensorDTO getSensor() {
        return sensor;
    }

    public void setSensor(SensorDTO sensor) {
        this.sensor = sensor;
    }

    public OutboundMessageTypeDTO getOutboundMessageType() {
        return outboundMessageType;
    }

    public void setOutboundMessageType(OutboundMessageTypeDTO outboundMessageType) {
        this.outboundMessageType = outboundMessageType;
    }
}
