package pt.isel.tese.smartParkingIot.api.logic.exceptions;

public class DeleteSensorTypeException extends LogicException {

    public DeleteSensorTypeException() {
        super("Sensor type can't be deleted.", "There are sensors associated with the sensor type.");
    }

    public DeleteSensorTypeException(String title, String detail) {
        super(title, detail);
    }
}
