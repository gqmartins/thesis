package pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate;

import org.springframework.stereotype.Repository;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto.SensorZoneDTO;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.mappers.SensorZoneRepositoryMapper;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

@Repository
public class SensorZoneRepository extends BaseRepository<SensorZoneDTO, SensorZoneDTO> {

    public SensorZoneRepository() {
        super(SensorZoneDTO.class, new SensorZoneRepositoryMapper());
    }

    @Override
    public void update(SensorZoneDTO object) {
        throw new NotImplementedException();
    }
}
