package pt.isel.tese.smartParkingIot.api.logic.managers.algorithm.contracts;

import pt.isel.tese.smartParkingIot.api.repository.RepositoryFacade;

import java.util.Date;
import java.util.List;

public interface IPercentageAlgorithm {

    int calculate(RepositoryFacade repositoryFacade, int zone);
    List<Integer> calculate(RepositoryFacade repositoryFacade, Date start, Date end, int interval, int zone);
}
