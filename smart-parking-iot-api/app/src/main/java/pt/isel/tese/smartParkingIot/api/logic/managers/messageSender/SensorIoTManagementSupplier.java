package pt.isel.tese.smartParkingIot.api.logic.managers.messageSender;

import org.springframework.stereotype.Component;
import pt.isel.tese.smartParkingIot.api.controllers.config.AppConfiguration;
import pt.isel.tese.smartParkingIot.api.logic.managers.messageSender.contract.ISensorMessageSender;
import pt.isel.tese.smartParkingIot.api.logic.managers.messageSender.implementation.TTNSensorMessageSender;

import java.util.HashMap;
import java.util.Map;

@Component
public class SensorIoTManagementSupplier {

    private static final Map<Integer, ISensorMessageSender> sensorManagementMap = new HashMap<>();

    public SensorIoTManagementSupplier(AppConfiguration appConfiguration) {
        buildMap(appConfiguration);
    }

    public ISensorMessageSender getSensorManagement(int sensorType) {
        return sensorManagementMap.get(sensorType);
    }

    private void buildMap(AppConfiguration appConfiguration) {
        sensorManagementMap.put(1, new TTNSensorMessageSender(appConfiguration.ttnRegion, appConfiguration.ttnAppId,
                appConfiguration.ttnAccessKey));
    }
}
