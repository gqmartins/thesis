package pt.isel.tese.smartParkingIot.api.repository.contracts;

import pt.isel.tese.smartParkingIot.api.model.OutboundMessage;

public interface IOutboundMessageRepository extends IBaseRepository<OutboundMessage> { }
