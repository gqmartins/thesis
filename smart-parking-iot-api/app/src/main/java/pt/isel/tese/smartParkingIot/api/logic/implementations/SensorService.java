package pt.isel.tese.smartParkingIot.api.logic.implementations;

import org.springframework.stereotype.Service;
import pt.isel.tese.smartParkingIot.api.logic.managers.messageSender.SensorIoTManagementSupplier;
import pt.isel.tese.smartParkingIot.api.logic.managers.messageSender.contract.ISensorMessageSender;
import pt.isel.tese.smartParkingIot.api.logic.utils.FieldUtils;
import pt.isel.tese.smartParkingIot.api.model.Sensor;
import pt.isel.tese.smartParkingIot.api.model.SensorType;
import pt.isel.tese.smartParkingIot.api.repository.RepositoryFacade;
import pt.isel.tese.smartParkingIot.api.repository.contracts.IOutboundMessageRepository;
import pt.isel.tese.smartParkingIot.api.repository.contracts.ISensorRepository;
import pt.isel.tese.smartParkingIot.api.logic.contracts.ISensorService;
import pt.isel.tese.smartParkingIot.api.repository.contracts.ISensorTypeRepository;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SensorService implements ISensorService {

    private ISensorRepository sensorRepository;
    private ISensorTypeRepository sensorTypeRepository;
    private IOutboundMessageRepository outboundMessageRepository;

    public SensorService(RepositoryFacade repositoryFacade) {
        this.sensorRepository = repositoryFacade.getSensorRepository();
        this.sensorTypeRepository = repositoryFacade.getSensorTypeRepository();
        this.outboundMessageRepository = repositoryFacade.getOutboundMessageRepository();
    }

    @Override
    public int create(Sensor object) {
        SensorType sensorType = sensorTypeRepository.read(object.getSensorType());
        FieldUtils.checkForFields(sensorType.getFields(), object.getFieldValues());

        object.setCreated(new Date());
        object.setModified(object.getCreated());
        return sensorRepository.create(object);
    }

    @Override
    public List<Sensor> read(Map<String, String> filters) {
        return sensorRepository.read(new HashMap<>());
    }

    @Override
    public Sensor read(int id) {
        return sensorRepository.read(id);
    }

    @Override
    public void update(Sensor object) {
        Sensor savedSensor = sensorRepository.read(object.getId());

        savedSensor.setName(object.getName());
        savedSensor.setLocation(object.getLocation());
        savedSensor.setZones(object.getZones());
        savedSensor.setFieldValues(object.getFieldValues());
        savedSensor.setModified(new Date());

        sensorRepository.update(savedSensor);
    }

    @Override
    public void delete(int id) {
        Sensor sensor = new Sensor(id);
        deleteOutboundMessages(sensor);
        sensorRepository.delete(sensor);
    }

    private void deleteOutboundMessages(Sensor sensor) {
        outboundMessageRepository.read(new HashMap<>())
                .stream()
                .filter(message -> message.getSensor() == sensor.getId())
                .forEach(outboundMessageRepository::delete);
    }

}
