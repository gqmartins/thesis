package pt.isel.tese.smartParkingIot.api.controllers.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import pt.isel.tese.smartParkingIot.api.controllers.model.request.SensorRequest;
import pt.isel.tese.smartParkingIot.api.controllers.model.util.GeoJsonPointModel;
import pt.isel.tese.smartParkingIot.api.controllers.model.util.SensorZoneModel;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

public class SensorResponse extends SensorRequest {

    @JsonProperty("id")
    private Integer id = null;

    @JsonProperty("created")
    private String created = null;

    @JsonProperty("modified")
    private String modified = null;

    public SensorResponse() { }

    public SensorResponse(String name, GeoJsonPointModel location, List<SensorZoneModel> zones, Map<String, String> fieldValues, Integer type, Integer id, String created, String modified) {
        super(name, location, zones, fieldValues, type);
        this.id = id;
        this.created = created;
        this.modified = modified;
    }

    /**
     * Get id
     * @return id
     **/
    @ApiModelProperty(example = "5", required = true, value = "")
    @NotNull
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get created
     * @return created
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull
    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    /**
     * Get modified
     * @return modified
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull
    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }
}
