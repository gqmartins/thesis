package pt.isel.tese.smartParkingIot.api.repository.contracts;

import pt.isel.tese.smartParkingIot.api.model.Role;
import pt.isel.tese.smartParkingIot.api.model.util.PermissionUri;

import java.util.List;

public interface IRoleRepository extends IBaseRepository<Role> {

    List<PermissionUri> getUriByRole(int role);
}
