package pt.isel.tese.smartParkingIot.api.logic.exceptions;

public class CreateZoneException extends LogicException {

    public CreateZoneException() {
        super("Can't create zone.", "The zone perimeter must have at least 3 sides.");
    }

    public CreateZoneException(String title, String detail) {
        super(title, detail);
    }
}
