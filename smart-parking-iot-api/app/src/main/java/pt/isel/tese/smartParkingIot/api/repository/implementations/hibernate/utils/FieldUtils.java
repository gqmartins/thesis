package pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FieldUtils {

    private static final String FIELD_SEPARATOR = "&";
    private static final int BEGIN_INDEX = 1;

    public static String transformToString(List<String> fields) {
        if(fields.size() == 0) {
            return "";
        }
        return fields
                .stream()
                .reduce("", (subtotal, element) -> subtotal + FIELD_SEPARATOR + element)
                .substring(BEGIN_INDEX);
    }

    public static List<String> transformToList(String fields) {
        String[] fieldSplit = fields.split(FIELD_SEPARATOR);
        return new ArrayList<>(Arrays.asList(fieldSplit));
    }
}
