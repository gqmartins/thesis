package pt.isel.tese.smartParkingIot.api.model;

import java.util.Date;
import java.util.Map;

public class OutboundMessage {

    private int id;
    private Map<String, String> body;
    private int sensor;
    private int type;
    private Date created;

    public OutboundMessage(int id) {
        this.id = id;
    }

    public OutboundMessage(Map<String, String> body, int sensor, int type) {
        this.body = body;
        this.sensor = sensor;
        this.type = type;
    }

    public OutboundMessage(int id, Map<String, String> body, int sensor, int type, Date created) {
        this(body, sensor, type);
        this.id = id;
        this.created = created;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Map<String, String> getBody() {
        return body;
    }

    public void setBody(Map<String, String> body) {
        this.body = body;
    }

    public int getSensor() {
        return sensor;
    }

    public void setSensor(int sensor) {
        this.sensor = sensor;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
