package pt.isel.tese.smartParkingIot.api.controllers.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import pt.isel.tese.smartParkingIot.api.controllers.model.request.OutboundMessageRequest;

import javax.validation.constraints.NotNull;
import java.util.Map;

public class OutboundMessageResponse extends OutboundMessageRequest {

    @JsonProperty("id")
    private Integer id = null;

    @JsonProperty("created")
    private String created = null;

    public OutboundMessageResponse() { }

    public OutboundMessageResponse(Map<String, String> fieldsValues, Integer type, Integer id, String created) {
        super(fieldsValues, type);
        this.id = id;
        this.created = created;
    }

    /**
     * Get id
     * @return id
     **/
    @ApiModelProperty(example = "5", required = true, value = "")
    @NotNull
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get created
     * @return created
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull
    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }
}
