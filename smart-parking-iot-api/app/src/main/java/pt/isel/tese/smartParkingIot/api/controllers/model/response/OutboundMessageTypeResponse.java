package pt.isel.tese.smartParkingIot.api.controllers.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import pt.isel.tese.smartParkingIot.api.controllers.model.request.OutboundMessageRequest;
import pt.isel.tese.smartParkingIot.api.controllers.model.request.OutboundMessageTypeRequest;
import pt.isel.tese.smartParkingIot.api.controllers.model.util.OutboundMessageFieldModel;

import javax.validation.constraints.NotNull;
import java.util.List;

public class OutboundMessageTypeResponse extends OutboundMessageTypeRequest {

    @JsonProperty("id")
    private Integer id = null;

    @JsonProperty("sensorType")
    private Integer sensorType = null;

    @JsonProperty("created")
    private String created = null;

    @JsonProperty("modified")
    private String modified = null;

    public OutboundMessageTypeResponse(Integer id, String name, List<String> fields, Integer sensorType, String created, String modified) {
        super(name, fields);
        this.id = id;
        this.sensorType = sensorType;
        this.created = created;
        this.modified = modified;
    }

    /**
     * Get id
     * @return id
     **/
    @ApiModelProperty(example = "5", required = true, value = "")
    @NotNull
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get sensorType
     * @return sensorType
     **/
    @ApiModelProperty(example = "1", required = true, value = "")
    @NotNull
    public Integer getSensorType() {
        return sensorType;
    }

    public void setSensorType(Integer sensorType) {
        this.sensorType = sensorType;
    }

    /**
     * Get created
     * @return created
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull
    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    /**
     * Get modified
     * @return modified
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull
    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }
}
