package pt.isel.tese.smartParkingIot.api.logic.contracts;

import pt.isel.tese.smartParkingIot.api.model.User;

public interface IAuthenticationService {

    User login(String username, String password);
    void logout(String username);
}
