package pt.isel.tese.smartParkingIot.api.controllers.config;

public class UriConfiguration {

    // Zone
    public static final String ZONES_URI = "/zone";
    public static final String ZONES_ID_URI = "/zone/{zone-id}";
    public static final String ZONES_ID_PREDICTION_URI = "/zone/{zone-id}/prediction";

    // Sensor
    public static final String SENSORS_URI = "/sensor";
    public static final String SENSORS_ID_URI = "/sensor/{sensor-id}";

    // Users
    public static final String USERS_URI = "/user";
    public static final String USERS_ID_URI = "/user/{user-id}";

    // Sensor types
    public static final String SENSOR_TYPES_URI = "/sensor-type";
    public static final String SENSOR_TYPES_ID_URI = "/sensor-type/{sensor-type-id}";

    // Roles
    public static final String ROLES_URI = "/role";
    public static final String ROLES_URI_ID = "/role/{role-id}";

    // OutboundMessageType
    public static final String OUTBOUND_MESSAGE_TYPES_URI = "/sensor-type/{sensor-type-id}/outbound-message-type";
    public static final String OUTBOUND_MESSAGE_TYPES_ID_URI = "/sensor-type/{sensor-type-id}/outbound-message-type/{outbound-message-type-id}";

    // OutboundMessage
    public static final String OUTBOUND_MESSAGE_URI = "/sensor/{sensor-id}/outbound-message";
    public static final String OUTBOUND_MESSAGE_ID_URI = "/sensor/{sensor-id}/outbound-message/{outbound-message-id}";

    // Authentication
    public static final String LOGIN_URI = "/login";
    public static final String LOGOUT_URI = "/logout";
    public static final String RENEW_TOKEN_URI = "/renew-token";

    // Error
    public static final String ERROR_URI = "/error";

    // Swagger
    public static final String SWAGGER_URI = "/swagger";
}
