package pt.isel.tese.smartParkingIot.api.logic.managers.geometry.implementations;

import com.esri.core.geometry.ogc.OGCGeometry;
import com.esri.core.geometry.ogc.OGCPolygon;
import com.google.gson.Gson;
import org.springframework.stereotype.Component;
import pt.isel.tese.smartParkingIot.api.model.util.GeoJsonPolygon;
import pt.isel.tese.smartParkingIot.api.logic.managers.geometry.contracts.IGeometryManager;

@Component
public class EsriGeometryManager implements IGeometryManager {

    private final Gson gson = new Gson();

    @Override
    public boolean intersects(GeoJsonPolygon firstPolygon, GeoJsonPolygon secondPolygon) {
        OGCGeometry firstGeometry = convertToOGCGeometry(firstPolygon);
        OGCGeometry secondGeometry = convertToOGCGeometry(secondPolygon);
        return firstGeometry.intersects(secondGeometry);
    }

    @Override
    public GeoJsonPolygon difference(GeoJsonPolygon firstPolygon, GeoJsonPolygon secondPolygon) {
        OGCGeometry firstGeometry = convertToOGCGeometry(firstPolygon);
        OGCGeometry secondGeometry = convertToOGCGeometry(secondPolygon);
        OGCGeometry differenceGeometry = firstGeometry.difference(secondGeometry);
        String geoJson = differenceGeometry.asGeoJson();
        return gson.fromJson(geoJson, GeoJsonPolygon.class);
    }

    private OGCGeometry convertToOGCGeometry(GeoJsonPolygon polygon) {
        String geoJson = gson.toJson(polygon);
        return OGCPolygon.fromGeoJson(geoJson);
    }
}
