package pt.isel.tese.smartParkingIot.api.controllers.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

public class PredictionResponse {

    @JsonProperty("date")
    private String date = null;

    @JsonProperty("percentage")
    private Integer percentage = null;

    public PredictionResponse(String date, Integer percentage) {
        this.date = date;
        this.percentage = percentage;
    }

    /**
     * Get date
     * @return date
     **/
    @ApiModelProperty(example = "", required = true, value = "")
    @NotNull
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    /**
     * Get percentage
     * @return percentage
     **/
    @ApiModelProperty(example = "10", required = true, value = "")
    @NotNull
    public Integer getPercentage() {
        return percentage;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }
}
