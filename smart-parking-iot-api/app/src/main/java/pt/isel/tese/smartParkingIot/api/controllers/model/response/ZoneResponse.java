package pt.isel.tese.smartParkingIot.api.controllers.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import pt.isel.tese.smartParkingIot.api.controllers.model.request.ZoneRequest;
import pt.isel.tese.smartParkingIot.api.controllers.model.util.GeoJsonPolygonModel;

import javax.validation.constraints.NotNull;

public class ZoneResponse extends ZoneRequest {

    @JsonProperty("id")
    private Integer id = null;

    @JsonProperty("occupiedPercentage")
    private Integer occupiedPercentage = null;

    @JsonProperty("created")
    private String created = null;

    @JsonProperty("modified")
    private String modified = null;

    public ZoneResponse(String name, Integer maxCount, Integer currentCount, GeoJsonPolygonModel perimeter, Integer id, Integer occupiedPercentage, String created, String modified) {
        super(name, maxCount, currentCount, perimeter);
        this.id = id;
        this.occupiedPercentage = occupiedPercentage;
        this.created = created;
        this.modified = modified;
    }

    /**
     * Get id
     * @return id
     **/
    @ApiModelProperty(example = "5", required = true, value = "")
    @NotNull
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get occupiedPercentage
     * @return occupiedPercentage
     **/
    @ApiModelProperty(example = "75", required = true, value = "")
    @NotNull

    public Integer getOccupiedPercentage() {
        return occupiedPercentage;
    }

    public void setOccupiedPercentage(Integer occupiedPercentage) {
        this.occupiedPercentage = occupiedPercentage;
    }

    /**
     * Get created
     * @return created
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull
    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    /**
     * Get modified
     * @return modified
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull
    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }
}
