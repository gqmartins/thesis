package pt.isel.tese.smartParkingIot.api.logic.managers.messageSender.implementation;

import org.thethingsnetwork.data.common.Connection;
import org.thethingsnetwork.data.common.messages.DownlinkMessage;
import org.thethingsnetwork.data.mqtt.Client;
import pt.isel.tese.smartParkingIot.api.logic.managers.messageSender.contract.ISensorMessageSender;
import pt.isel.tese.smartParkingIot.api.model.OutboundMessage;
import pt.isel.tese.smartParkingIot.api.model.Sensor;

import java.util.*;

public class TTNSensorMessageSender implements ISensorMessageSender {

    private Client client;
    private static final String DEVICE_ID = "ttnSensorId";
    private static final int DOWNLINK_MESSAGE_PORT = 1;

    public TTNSensorMessageSender(String region, String appId, String accessKey) {
        initClient(region, appId, accessKey);
    }

    @Override
    public void send(Sensor sensor, OutboundMessage message) {
        try {
            String ttnDeviceId = sensor.getFieldValues().get(DEVICE_ID);
            byte[] payload = MessageSenderUtils.buildPayload(sensor.getId(), message.getType(), message.getBody());
            DownlinkMessage downlinkMessage = new DownlinkMessage(DOWNLINK_MESSAGE_PORT, payload);
            client.send(ttnDeviceId, downlinkMessage);
        } catch (Exception ex) {
            throw new RuntimeException("Error while trying to send message to TTN");
        }
    }

    private void initClient(String region, String appId, String accessKey) {
        try {
            client = new Client(region, appId, accessKey);

            client.onActivation((deviceId, data) -> System.out.println("TTNBaseMessageSupplier -> activation: " + deviceId + ", data: " + data.getDevAddr()));
            client.onError((Throwable _error) -> System.err.println("TTNBaseMessageSupplier ->  error: " + _error.getMessage()));
            client.onConnected((Connection _client) -> System.out.println("TTNBaseMessageSupplier -> connected !"));

            client.start();
        } catch (Exception e) {
            throw new RuntimeException("Error initializing TTN Client.");
        }
    }
}
