package pt.isel.tese.smartParkingIot.api.controllers.mappers;

import org.springframework.stereotype.Component;
import pt.isel.tese.smartParkingIot.api.controllers.config.UriConfiguration;
import pt.isel.tese.smartParkingIot.api.controllers.model.request.ZoneRequest;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ListItemResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ZoneResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.util.GeoJsonPolygonModel;
import pt.isel.tese.smartParkingIot.api.controllers.utils.ResponseUtils;
import pt.isel.tese.smartParkingIot.api.controllers.utils.URIBuilder;
import pt.isel.tese.smartParkingIot.api.model.Zone;
import pt.isel.tese.smartParkingIot.api.model.util.GeoJsonPolygon;

@Component
public class ZoneControllerMapper implements IControllerMapper<Zone, ZoneRequest, ZoneResponse> {

    @Override
    public Zone transformToModel(ZoneRequest request) {
        GeoJsonPolygon perimeter = new GeoJsonPolygon(request.getPerimeter().getCoordinates());
        return new Zone(request.getName(), request.getMaxCount(), request.getCurrentCount(), perimeter);
    }

    @Override
    public ZoneResponse transformToResponse(Zone model) {
        GeoJsonPolygonModel perimeter = new GeoJsonPolygonModel(model.getPerimeter().getCoordinates());
        String created = ResponseUtils.convertDateToString(model.getCreated());
        String modified = ResponseUtils.convertDateToString(model.getModified());
        return new ZoneResponse(model.getName(), model.getMaxCount(), model.getCurrentCount(), perimeter,
                model.getId(), model.getOccupiedPercentage(), created, modified);
    }

    @Override
    public ListItemResponse transformToListItem(Zone model) {
        String href = URIBuilder.getCreatedURI(UriConfiguration.ZONES_ID_URI, model.getId()).toString();
        String created = ResponseUtils.convertDateToString(model.getCreated());
        String modified = ResponseUtils.convertDateToString(model.getModified());
        return new ListItemResponse(model.getId(), model.getName(), created, modified, href);
    }
}
