package pt.isel.tese.smartParkingIot.api.controllers.contract;

import io.swagger.annotations.*;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.isel.tese.smartParkingIot.api.controllers.model.request.SensorTypeRequest;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ErrorResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.ListItemResponse;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.SensorTypeResponse;

import javax.validation.Valid;
import java.util.List;

import static pt.isel.tese.smartParkingIot.api.controllers.config.UriConfiguration.SENSOR_TYPES_ID_URI;
import static pt.isel.tese.smartParkingIot.api.controllers.config.UriConfiguration.SENSOR_TYPES_URI;

@Api(value = "sensor type", description = "the sensor type API")
public interface ISensorTypeController {

    @ApiOperation(value = "adds a sensor type to the system", nickname = "createSensorType", notes = "adds a sensor type to the system", tags={ "Sensor type" })
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "sensor type successfully added to the system. to get the sensor type added to the system make a get request to the response header location URI."),
            @ApiResponse(code = 400, message = "Bad request parameter", response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class) })
    @PostMapping(value = SENSOR_TYPES_URI, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<Void> createSensorType(@ApiParam(value = "SensorType to add"  )  @Valid @RequestBody SensorTypeRequest body);

    @ApiOperation(value = "deletes the sensor type with the specified sensor-type-id", nickname = "deleteSensorType", notes = "delete the sensor type with the specified sensor-type-id", tags={ "Sensor type" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "sensor type successfully deleted."),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "The specified sensor type does not exist", response = ErrorResponse.class) })
    @DeleteMapping(value = SENSOR_TYPES_ID_URI, produces = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<Void> deleteSensorType(@ApiParam(value = "sensor type id",required=true) @PathVariable("sensor-type-id") Integer sensorTypeId);

    @ApiOperation(value = "edits the sensor type with the specified sensor-type-id", nickname = "editSensorType", notes = "edits the sensor type with the specified sensor-type-id", tags={ "Sensor type" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "sensor type successfully edited. to get the sensor type edited make a GET request to the URI used to edit the sensor type."),
            @ApiResponse(code = 400, message = "Bad request parameter", response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "The specified sensor type does not exist", response = ErrorResponse.class) })
    @PutMapping(value = SENSOR_TYPES_ID_URI, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<Void> editSensorType(@ApiParam(value = "sensor type id",required=true) @PathVariable("sensor-type-id") Integer sensorTypeId, @ApiParam(value = "sensor type to edit")  @Valid @RequestBody SensorTypeRequest body);

    @ApiOperation(value = "returns sensor type with the specified sensor-type-id", nickname = "readSensorType", notes = "returns sensor type with the specified sensor-type-id", response = SensorTypeResponse.class, tags={ "Sensor type" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "sensor type with the specified sensor-type-id", response = SensorTypeResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "The specified sensor type does not exist", response = ErrorResponse.class) })
    @GetMapping(value = SENSOR_TYPES_ID_URI, produces = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<SensorTypeResponse> readSensorType(@ApiParam(value = "sensor type id",required=true) @PathVariable("sensor-type-id") Integer sensorTypeId);

    @ApiOperation(value = "returns types of sensors added to the system", nickname = "readSensorTypes", notes = "returns all the sensor types that were added to the system, ordered by creation date", response = SensorTypeResponse.class, responseContainer = "List", tags={ "Sensor type" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "sensor type result matching search criteria", response = SensorTypeResponse.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad request parameter", response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class) })
    @GetMapping(value = SENSOR_TYPES_URI, produces = { "application/json" })
    ResponseEntity<List<ListItemResponse>> readSensorTypes(@ApiParam(value = "number of page for pagination results") @Valid @RequestParam(value = "page", required = false) Integer page, @ApiParam(value = "number of elements to return in a page") @Valid @RequestParam(value = "size", required = false) Integer size);
}
