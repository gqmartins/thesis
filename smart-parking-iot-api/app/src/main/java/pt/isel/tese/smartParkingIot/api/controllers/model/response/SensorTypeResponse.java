package pt.isel.tese.smartParkingIot.api.controllers.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import pt.isel.tese.smartParkingIot.api.controllers.model.request.SensorTypeRequest;

import javax.validation.constraints.NotNull;
import java.util.List;

public class SensorTypeResponse extends SensorTypeRequest {

    @JsonProperty("id")
    private Integer id = null;

    @JsonProperty("created")
    private String created = null;

    @JsonProperty("modified")
    private String modified = null;

    @JsonProperty("outboundMessageTypesHref")
    private String outboundMessageTypesHref = null;

    public SensorTypeResponse(Integer id, String name, List<String> fields, String created, String modified, String outboundMessageTypesHref) {
        super(name, fields);
        this.id = id;
        this.created = created;
        this.modified = modified;
        this.outboundMessageTypesHref = outboundMessageTypesHref;
    }

    /**
     * Get id
     * @return id
     **/
    @ApiModelProperty(example = "5", required = true, value = "")
    @NotNull
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get created
     * @return created
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull
    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    /**
     * Get modified
     * @return modified
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull
    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    /**
     * Get outboundMessageTypesHref
     * @return outboundMessageTypesHref
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull
    public String getOutboundMessageTypesHref() {
        return outboundMessageTypesHref;
    }

    public void setOutboundMessageTypesHref(String outboundMessageTypesHref) {
        this.outboundMessageTypesHref = outboundMessageTypesHref;
    }
}
