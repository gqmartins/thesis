package pt.isel.tese.smartParkingIot.api.logic;

import org.springframework.stereotype.Component;
import pt.isel.tese.smartParkingIot.api.logic.contracts.*;

@Component
public class ServiceFacade {

    private IZoneService zoneService;
    private ISensorService sensorService;
    private ISensorTypeService sensorTypeService;
    private IRoleService roleService;
    private IUserService userService;
    private IAuthenticationService authenticationService;
    private IOutboundMessageTypeService outboundMessageTypeService;
    private IOutboundMessageService outboundMessageService;

    public ServiceFacade(IZoneService zoneService, ISensorService sensorService, ISensorTypeService sensorTypeService,
                         IRoleService roleService, IUserService userService,IAuthenticationService authenticationService,
                         IOutboundMessageTypeService outboundMessageTypeService, IOutboundMessageService outboundMessageService) {
        this.zoneService = zoneService;
        this.sensorService = sensorService;
        this.sensorTypeService = sensorTypeService;
        this.roleService = roleService;
        this.userService = userService;
        this.authenticationService = authenticationService;
        this.outboundMessageTypeService = outboundMessageTypeService;
        this.outboundMessageService = outboundMessageService;
    }

    public IZoneService getZoneService() {
        return zoneService;
    }

    public ISensorService getSensorService() {
        return sensorService;
    }

    public ISensorTypeService getSensorTypeService() {
        return sensorTypeService;
    }

    public IRoleService getRoleService() {
        return roleService;
    }

    public IUserService getUserService() {
        return userService;
    }

    public IAuthenticationService getAuthenticationService() {
        return authenticationService;
    }

    public IOutboundMessageTypeService getOutboundMessageTypeService() {
        return outboundMessageTypeService;
    }

    public void setOutboundMessageTypeService(IOutboundMessageTypeService outboundMessageTypeService) {
        this.outboundMessageTypeService = outboundMessageTypeService;
    }

    public IOutboundMessageService getOutboundMessageService() {
        return outboundMessageService;
    }
}
