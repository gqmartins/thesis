package pt.isel.tese.smartParkingIot.api.controllers.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import pt.isel.tese.smartParkingIot.api.controllers.model.request.UserRequest;

import javax.validation.constraints.NotNull;

public class UserResponse extends UserRequest {

    @JsonProperty("id")
    private Integer id = null;

    @JsonProperty("role")
    private RoleResponse roleResponse;

    @JsonProperty("created")
    private String created = null;

    @JsonProperty("modified")
    private String modified = null;

    public UserResponse(Integer id, String name, String username, RoleResponse roleResponse, String created, String modified) {
        super(name, username, roleResponse.getId());
        this.id = id;
        this.roleResponse = roleResponse;
        this.created = created;
        this.modified = modified;
    }

    /**
     * Get id
     * @return id
     **/
    @ApiModelProperty(example = "5", required = true, value = "")
    @NotNull
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Get role
     * @return role
     **/
    @ApiModelProperty(example = "", required = true, value = "")
    @NotNull
    public RoleResponse getRoleResponse() {
        return roleResponse;
    }

    public void setRoleResponse(RoleResponse role) {
        this.roleResponse = role;
    }

    /**
     * Get created
     * @return created
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull
    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    /**
     * Get modified
     * @return modified
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull
    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }
}
