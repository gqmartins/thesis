package pt.isel.tese.smartParkingIot.api.repository.contracts;

import pt.isel.tese.smartParkingIot.api.model.OutboundMessageType;

public interface IOutboundMessageTypeRepository extends IBaseRepository<OutboundMessageType> {
}
