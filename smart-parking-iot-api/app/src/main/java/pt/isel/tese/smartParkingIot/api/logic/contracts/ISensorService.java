package pt.isel.tese.smartParkingIot.api.logic.contracts;

import pt.isel.tese.smartParkingIot.api.model.Sensor;

public interface ISensorService extends IBaseService<Sensor> { }
