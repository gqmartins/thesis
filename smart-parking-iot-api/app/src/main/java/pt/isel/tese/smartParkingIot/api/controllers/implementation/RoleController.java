package pt.isel.tese.smartParkingIot.api.controllers.implementation;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pt.isel.tese.smartParkingIot.api.controllers.contract.IRoleController;
import pt.isel.tese.smartParkingIot.api.controllers.mappers.RoleControllerMapper;
import pt.isel.tese.smartParkingIot.api.controllers.model.response.RoleResponse;
import pt.isel.tese.smartParkingIot.api.model.Role;
import pt.isel.tese.smartParkingIot.api.logic.ServiceFacade;
import pt.isel.tese.smartParkingIot.api.logic.contracts.IRoleService;
import pt.isel.tese.smartParkingIot.api.utils.ListUtils;

import java.util.HashMap;
import java.util.List;

@RestController
public class RoleController implements IRoleController {

    private IRoleService roleService;
    private RoleControllerMapper roleMapper;

    public RoleController(ServiceFacade serviceFacade, RoleControllerMapper roleMapper) {
        this.roleService = serviceFacade.getRoleService();
        this.roleMapper = roleMapper;
    }

    @Override
    public ResponseEntity<List<RoleResponse>> readRoles(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size) {
        List<Role> roles = roleService.read(new HashMap<>());
        List<RoleResponse> response = ListUtils.mapList(roles, roleMapper::transformToResponse);
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<RoleResponse> readRole(@PathVariable("role-id") Integer roleId) {
        Role role = roleService.read(roleId);
        RoleResponse roleResponse = roleMapper.transformToResponse(role);
        return ResponseEntity.ok(roleResponse);
    }
}
