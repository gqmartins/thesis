package pt.isel.tese.smartParkingIot.api.logic.contracts;

import pt.isel.tese.smartParkingIot.api.model.Zone;
import pt.isel.tese.smartParkingIot.api.model.util.Prediction;

import java.util.Date;
import java.util.List;

public interface IZoneService extends IBaseService<Zone> {

    List<Prediction> getPredictions(int zone, Date start, Date end, int interval);
}
