package pt.isel.tese.smartParkingIot.api.model.util;

public class OutboundMessageField {

    private int id;
    private String name;

    public OutboundMessageField(String name) {
        this.name = name;
    }

    public OutboundMessageField(int id, String name) {
        this(name);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
