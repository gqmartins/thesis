package pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.mappers;

import pt.isel.tese.smartParkingIot.api.model.OutboundMessageType;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto.OutboundMessageTypeDTO;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto.SensorTypeDTO;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.utils.DateUtils;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.utils.FieldUtils;

import java.sql.Timestamp;
import java.util.List;

public class OutboundMessageTypeRepositoryMapper implements IRepositoryMapper<OutboundMessageType, OutboundMessageTypeDTO> {

    @Override
    public OutboundMessageType transformToObject(OutboundMessageTypeDTO dto) {
        List<String> fields = FieldUtils.transformToList(dto.getFields());
        return new OutboundMessageType(dto.getId(), dto.getName(), fields, dto.getSensorType().getId(), dto.getCreated(), dto.getModified());
    }

    @Override
    public OutboundMessageTypeDTO transformToDto(OutboundMessageType model) {
        SensorTypeDTO sensorTypeDTO = new SensorTypeDTO(model.getSensorTypeId());
        String fields = FieldUtils.transformToString(model.getFields());
        Timestamp created = DateUtils.convertDateToTimestamp(model.getCreated());
        Timestamp modified = DateUtils.convertDateToTimestamp(model.getModified());
        return new OutboundMessageTypeDTO(model.getId(), model.getName(), fields, sensorTypeDTO, created, modified);
    }

    @Override
    public int getIdentifier(OutboundMessageType model) {
        return model.getId();
    }
}
