package pt.isel.tese.smartParkingIot.api.controllers.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfiguration {

    @Value("${server.contextPath}")
    public String serverContextPath;

    @Value("${server.port}")
    public String serverPort;

    @Value("2F997032E19611D2EAC46B9441E230932A67B70EB0794CA9EA9D821D00521E39")
    public String jwtSecret;

    @Value("${tokenExpirationMinutes}")
    public String jwtExpirationMinutes;

    @Value("${useSecurity}")
    public String useSecurity;

    @Value("${ttn-region}")
    public String ttnRegion;

    @Value("${ttn-app-id}")
    public String ttnAppId;

    @Value("${ttn-access-key}")
    public String ttnAccessKey;
}
