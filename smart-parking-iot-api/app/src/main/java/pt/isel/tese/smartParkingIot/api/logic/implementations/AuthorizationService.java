package pt.isel.tese.smartParkingIot.api.logic.implementations;

import org.springframework.stereotype.Service;
import pt.isel.tese.smartParkingIot.api.model.util.PermissionUri;
import pt.isel.tese.smartParkingIot.api.repository.RepositoryFacade;
import pt.isel.tese.smartParkingIot.api.repository.contracts.IRoleRepository;
import pt.isel.tese.smartParkingIot.api.logic.contracts.IAuthorizationService;

import java.util.List;

@Service
public class AuthorizationService implements IAuthorizationService {

    private IRoleRepository roleRepository;

    public AuthorizationService(RepositoryFacade repositoryFacade) {
        this.roleRepository = repositoryFacade.getRoleRepository();
    }

    @Override
    public List<PermissionUri> getUrisForRole(int role) {
        return roleRepository.getUriByRole(role);
    }
}
