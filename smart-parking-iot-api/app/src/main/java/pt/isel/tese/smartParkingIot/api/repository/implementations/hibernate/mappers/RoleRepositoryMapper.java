package pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.mappers;

import pt.isel.tese.smartParkingIot.api.model.Role;
import pt.isel.tese.smartParkingIot.api.model.util.PermissionUri;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto.RoleDTO;
import pt.isel.tese.smartParkingIot.api.repository.implementations.hibernate.dto.UriDTO;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class RoleRepositoryMapper implements IRepositoryMapper<Role, RoleDTO> {

    @Override
    public Role transformToObject(RoleDTO dto) {
        return new Role(dto.getId(), dto.getName());
    }

    @Override
    public RoleDTO transformToDto(Role object) {
        throw new NotImplementedException();
    }

    @Override
    public int getIdentifier(Role model) {
        return model.getId();
    }

    public PermissionUri transformToPermissionUri(UriDTO dto) {
        return new PermissionUri(dto.getMethod(), dto.getUri());
    }
}
