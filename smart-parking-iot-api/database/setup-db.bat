SET DBNAME=smartparkingiotdb
SET USERNAME=smartparkingiotdb_user
SET USERPASSWORD=smartparkingiotdb_password

psql -h localhost -p 5432 -d postgres -U postgres -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'smartparkingiotdb' AND pid <> pg_backend_pid()";
psql -h localhost -p 5432 -d postgres -U postgres -c "DROP DATABASE IF EXISTS \"%DBNAME%\";"
psql -h localhost -p 5432 -d postgres -U postgres -c "DROP OWNED BY \"%USERNAME%\" CASCADE;"
psql -h localhost -p 5432 -d postgres -U postgres -c "DROP USER IF EXISTS \"%USERNAME%\";"
psql -h localhost -p 5432 -d postgres -U postgres -c "CREATE database \"%DBNAME%\";"
psql -h localhost -p 5432 -d %DBNAME% -U postgres -c "CREATE USER \"%USERNAME%\" WITH PASSWORD '%USERPASSWORD%';"
psql -h localhost -p 5432 -d %DBNAME% -U postgres -c "GRANT ALL PRIVILEGES ON DATABASE \"%DBNAME%\" to \"%USERNAME%\";"

psql -h localhost -p 5432 -d %DBNAME% -U postgres -f tables\create-tables.sql
psql -h localhost -p 5432 -d %DBNAME% -U postgres -f data\insert-base-data.sql

psql -h localhost -p 5432 -d %DBNAME% -U postgres -c "GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO \"%USERNAME%\";"
psql -h localhost -p 5432 -d %DBNAME% -U postgres -c "GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO \"%USERNAME%\";"

pause