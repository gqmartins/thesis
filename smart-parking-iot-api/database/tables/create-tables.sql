BEGIN;

CREATE TABLE Zone (
	id SERIAL,
	name TEXT NOT NULL UNIQUE,
	max_count INT NOT NULL,
	current_count INT NOT NULL,
	perimeter TEXT NOT NULL,
	created TIMESTAMP NOT NULL,
	modified TIMESTAMP NOT NULL,

	CONSTRAINT PK_Zone PRIMARY KEY (id)
);

CREATE TABLE SensorType (
	id SERIAL,
	name TEXT NOT NULL,
	fields TEXT NULL,
	created TIMESTAMP NOT NULL,
	modified TIMESTAMP NOT NULL,

	CONSTRAINT PK_SensorType PRIMARY KEY (id)
);

CREATE TABLE OutboundMessageType (
	id SERIAL,
	name TEXT NOT NULL,
	fields TEXT NULL,
	sensor_type_id INT NOT NULL,
	created TIMESTAMP NOT NULL,
	modified TIMESTAMP NOT NULL,
	
	CONSTRAINT PK_OutboundMessageType PRIMARY KEY (id),
	CONSTRAINT FK_OutboundMessageType_SensorType FOREIGN KEY (sensor_type_id) REFERENCES SensorType (id)
);

CREATE TABLE Sensor (
	id SERIAL,
	name TEXT NOT NULL UNIQUE,
	location TEXT NOT NULL,
	field_values TEXT NOT NULL,
	created TIMESTAMP NOT NULL,
	modified TIMESTAMP NOT NULL,
	sensor_type_id INT NOT NULL,

	CONSTRAINT PK_Sensor PRIMARY KEY (id),
	CONSTRAINT FK_Sensor_SensorType FOREIGN KEY (sensor_type_id) REFERENCES SensorType (id)
);

CREATE TABLE Sensor_Zone (
	id SERIAL,
	zone_id INT NOT NULL,
	sensor_id INT NOT NULL,
	enter_exit BOOLEAN NOT NULL,

	CONSTRAINT PK_Zone_Sensor PRIMARY KEY (id, sensor_id, zone_id),
	CONSTRAINT FK_Zone_Sensor_Zone FOREIGN KEY (zone_id) REFERENCES Zone (id),
	CONSTRAINT FK_Zone_Sensor_Sensor FOREIGN KEY (sensor_id) REFERENCES Sensor (id)
);

CREATE TABLE OutboundMessage (
	id SERIAL,
	body TEXT NOT NULL,
	created TIMESTAMP NOT NULL,
	sensor_id INT NOT NULL,
	outbound_message_type_id INT NULL,

	CONSTRAINT PK_OutboundMessage PRIMARY KEY (id),
	CONSTRAINT FK_OutboundMessage_Sensor FOREIGN KEY (sensor_id) REFERENCES Sensor (id)
);

CREATE TABLE InboundMessage (
	id SERIAL,
	sensor_id INT NOT NULL,
	count INT NOT NULL,
	created TIMESTAMP NOT NULL,
	
	CONSTRAINT PK_InboundMessage PRIMARY KEY (id),
	CONSTRAINT FK_InboundMessage_Sensor FOREIGN KEY (sensor_id) REFERENCES Sensor (id)
);

-- Authentication and Authorization

CREATE TABLE Role (
	id INT NOT NULL,
	name TEXT NOT NULL UNIQUE,

	CONSTRAINT PK_Role PRIMARY KEY (id)
);

CREATE TABLE AppUser (
	id SERIAL,
	username TEXT NOT NULL UNIQUE,
	name TEXT NOT NULL,
	password TEXT NOT NULL,
	created TIMESTAMP NOT NULL,
	modified TIMESTAMP NOT NULL,
	role_id INT NOT NULL,

	CONSTRAINT PK_AppUser PRIMARY KEY (id),
	CONSTRAINT FK_AppUser_Role FOREIGN KEY (role_id) REFERENCES Role (id)
);

CREATE TABLE Uri (
	id INT,
	uri TEXT NOT NULL,
	method TEXT NOT NULL,

	CONSTRAINT PK_Uri PRIMARY KEY (id)
);

CREATE TABLE Role_Uri (
	id SERIAL,
	role_id INT NOT NULL,
	uri_id INT NOT NULL,

	CONSTRAINT PK_Role_Uri PRIMARY KEY (id),
	CONSTRAINT FK_Role_Uri_Role FOREIGN KEY (role_id) REFERENCES Role (id),
	CONSTRAINT FK_Role_Uri_Uri FOREIGN KEY (uri_id) REFERENCES Uri (id)
);

COMMIT;