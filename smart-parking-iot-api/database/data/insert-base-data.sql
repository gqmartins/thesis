BEGIN;

INSERT INTO SensorType (name, fields, created, modified) VALUES ('TTN Sensor', 'ttnSensorId', current_timestamp, current_timestamp);

INSERT INTO Role (id, name) VALUES (1, 'Admin'),
                                   (2, 'Manager'),
                                   (3, 'Viewer'),
                                   (4, 'MessageProducer');

INSERT INTO AppUser (username, name, password, created, modified, role_id) VALUES ('gmartins', 'G Martins', '$2a$10$zmJVWuEYV24yROBTI338nOhPu.P666PiWKwGaWf10ObH3vBu/GZz6', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1);
																				  --('messageproducer', 'M Producer', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 4);

INSERT INTO Uri (id, uri, method) VALUES (1, '/message', 'POST'),
										 (2, '/zone', 'POST'),
										 (3, '/zone', 'GET'),
										 (4, '/zone/{zone-id}', 'GET'),
										 (5,'/zone/{zone-id}', 'PUT'),
										 (6,'/zone/{zone-id}', 'DELETE'),
										 (7,'/sensor', 'POST'),
										 (8,'/sensor', 'GET'),
										 (9,'/sensor/{sensor-id}', 'GET'),
										 (10,'/sensor/{sensor-id}', 'PUT'),
										 (11,'/sensor/{sensor-id}', 'DELETE'),
										 (12,'/user', 'POST'),
										 (13,'/user', 'GET'),
										 (14,'/user/{user-id}', 'GET'),
										 (15,'/user/{user-id}', 'PUT'),
										 (16,'/user/{user-id}', 'DELETE'),
										 (17,'/sensor-type', 'GET'),
				                         (18,'/role', 'GET');

INSERT INTO Role_Uri (role_id, uri_id) VALUES (1, 2),
											  (1, 3),
											  (1, 4),
											  (1, 5),
											  (1, 6),
											  (1, 7),
											  (1, 8),
											  (1, 9),
											  (1, 10),
											  (1, 11),
											  (1, 12),
											  (1, 13),
											  (1, 14),
											  (1, 15),
											  (1, 16),
											  (1, 17),
											  (1, 18),
											  (2, 2),
                                              (2, 3),
                                              (2, 4),
                                              (2, 5),
                                              (2, 6),
                                              (2, 7),
                                              (2, 8),
                                              (2, 9),
                                              (2, 10),
                                              (2, 11),
                                              (2, 17),
                                              (2, 18),
                                              (3, 3),
                                              (3, 4),
                                              (3, 8),
                                              (3, 9),
                                              (3, 17),
                                              (3, 18),
                                              (4, 1);

COMMIT;