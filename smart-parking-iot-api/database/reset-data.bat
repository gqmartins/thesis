SET DBNAME=smartparkingiotdb
SET USERNAME=smartparkingiotdb_user
SET USERPASSWORD=smartparkingiotdb_password

psql -h localhost -p 5432 -d %DBNAME% -U postgres -f data\delete-data.sql
psql -h localhost -p 5432 -d %DBNAME% -U postgres -f data\insert-base-data.sql

pause