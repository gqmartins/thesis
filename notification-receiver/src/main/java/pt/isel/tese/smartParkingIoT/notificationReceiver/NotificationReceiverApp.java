package pt.isel.tese.smartParkingIoT.notificationReceiver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NotificationReceiverApp {

    public static void main(String[] args) {
        System.out.println("::STARTING::");
        SpringApplication.run(NotificationReceiverApp.class, args);
        System.out.println("::RUNNING::");
    }
}
