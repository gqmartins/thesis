package pt.isel.tese.smartParkingIoT.notificationReceiver.controller.implementation;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pt.isel.tese.smartParkingIoT.notificationReceiver.controller.contract.INotificationReceiverController;
import pt.isel.tese.smartParkingIoT.notificationReceiver.controller.model.request.ZoneNotificationRequest;

@RestController
public class NotificationReceiverController implements INotificationReceiverController {

    @Override
    public ResponseEntity<Void> receiveZoneNotification(@RequestBody ZoneNotificationRequest body) {
        System.out.println(body);
        return ResponseEntity.ok().build();
    }
}
