package pt.isel.tese.smartParkingIoT.notificationReceiver.controller.config;

public class UriConfiguration {

    public static final String NOTIFICATION_URL = "/notification";
}
