package pt.isel.tese.smartParkingIoT.notificationReceiver.controller.contract;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import pt.isel.tese.smartParkingIoT.notificationReceiver.controller.config.UriConfiguration;
import pt.isel.tese.smartParkingIoT.notificationReceiver.controller.model.request.ZoneNotificationRequest;

public interface INotificationReceiverController {

    @PostMapping(value = UriConfiguration.NOTIFICATION_URL, produces = { "application/json" }, consumes = { "application/json" })
    ResponseEntity<Void> receiveZoneNotification(@RequestBody ZoneNotificationRequest body);
}
