package pt.isel.tese.smartParkingIoT.notificationReceiver.controller.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import pt.isel.tese.smartParkingIoT.notificationReceiver.controller.model.request.util.GeoJsonPolygonModel;

public class ZoneNotificationRequest {

    @JsonProperty("id")
    private String id;

    @JsonProperty("type")
    private String type;

    @JsonProperty("name")
    private String name;

    @JsonProperty("occupiedPercentage")
    private int occupiedPercentage;

    @JsonProperty("location")
    private GeoJsonPolygonModel location;

    public ZoneNotificationRequest() { }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOccupiedPercentage() {
        return occupiedPercentage;
    }

    public void setOccupiedPercentage(int occupiedPercentage) {
        this.occupiedPercentage = occupiedPercentage;
    }

    public GeoJsonPolygonModel getLocation() {
        return location;
    }

    public void setLocation(GeoJsonPolygonModel location) {
        this.location = location;
    }
}
