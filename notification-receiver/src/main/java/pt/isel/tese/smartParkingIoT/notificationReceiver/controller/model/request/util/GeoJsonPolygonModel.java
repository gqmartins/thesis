package pt.isel.tese.smartParkingIoT.notificationReceiver.controller.model.request.util;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class GeoJsonPolygonModel {

    @JsonProperty("type")
    private String type = "Polygon";

    @JsonProperty("coordinates")
    private List<List<List<Float>>> coordinates = new ArrayList<List<List<Float>>>();

    public GeoJsonPolygonModel() { }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<List<List<Float>>> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<List<List<Float>>> coordinates) {
        this.coordinates = coordinates;
    }
}
