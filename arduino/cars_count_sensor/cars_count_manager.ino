int x, y, z, count = 0;

void carsCountManagerProcess() {
  sensorInputRead(&x, &y, &z);
  bool carPassed = adaptiveThresholdDetectionFunction(x, y, z);

  if(carPassed) {
    count += 1;
    Serial.println("cars_count_manager -> a car has passed !!!");
    Serial.print("cars_count_manager -> number of cars counted: ");
    Serial.println(count);
  }

  if (sendDataToTTN(count)) {
    count = 0;
  }
}
