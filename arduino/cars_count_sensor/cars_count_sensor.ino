#include <math.h>
#include <TheThingsNetwork.h>

const int READ_RATE = 10; // rate that the values will be read when using the sensor, in hz
const int SENSOR_ID = 1;

// TTN constants
#define FREQ_PLAN TTN_FP_EU868
#define loraSerial Serial1
#define debugSerial Serial

const char *appEui = "70B3D57ED001A008";
const char *appKey = "95129A50C9A9AF285AFB9E48166846B9";

TheThingsNetwork ttn(loraSerial, debugSerial, FREQ_PLAN);

void setup() {
  Serial.begin(9600);
  while(!Serial)
    ;

  ttn.join(appEui, appKey);

  sensorInputInit(READ_RATE);
  adaptiveThresholdDetectionInit();
  ttnManagerInit(SENSOR_ID);
}

void loop() {
  carsCountManagerProcess();
}
