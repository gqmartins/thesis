const int TRIGGER_THRESHOLD = 1800;
const int THRESHOLD_LIMIT = 800;
const int CALIBRATION_SAMPLES = 100;

// STATES
const int CAR_PASSING = 0;
const int CAR_NOT_PASSING = 1;

// LIMIT_CROSSED
const int UP_LIMIT = 0;
const int DOWN_LIMIT = 1;

int calibrationValues[CALIBRATION_SAMPLES];
int average, currentState = CAR_NOT_PASSING, currentLimit;
long sum = 0;

void adaptiveThresholdDetectionInit() {
  Serial.println("adaptive_threshold_detection -> Init...");
  _calibrate();
}

bool adaptiveThresholdDetectionFunction(int x, int y, int z) {
  int maxTriggerLimit = average + TRIGGER_THRESHOLD;
  int minTriggerLimit = average - TRIGGER_THRESHOLD;

  int maxThresholdLimit = maxTriggerLimit - THRESHOLD_LIMIT;
  int minThresholdLimit = minTriggerLimit + THRESHOLD_LIMIT;
  bool carPassed = false;

  // car has passed the maxTriggerlimit OR the minTrggerLimit WHEN a car is not passing
  if((y >= maxTriggerLimit || y <= minTriggerLimit) && currentState == CAR_NOT_PASSING) {
    currentState = CAR_PASSING;
    // defines if the car is passing below or up calibration values
    currentLimit = y >= maxTriggerLimit ? UP_LIMIT : DOWN_LIMIT;
  }
  // the car has passed the down limit
  else if(currentState == CAR_PASSING && ((currentLimit == UP_LIMIT && y < maxThresholdLimit) || (currentLimit == DOWN_LIMIT && y > minThresholdLimit))) {
      currentState = CAR_NOT_PASSING;
      carPassed = true;
  }

  if(currentState == CAR_NOT_PASSING) {
    _updateAverage(y);
  }

  return carPassed;
}

void _calibrate() {
  Serial.println("adaptive_threshold_detection -> begin calibration...");
  int x, y, z, index;
  for(index = 0; index < CALIBRATION_SAMPLES; ++index) {
    Serial.print(index);
    fileInputRead(&x, &y, &z);
    calibrationValues[index] = y;
  }
  average = _calculateAverage();
  Serial.print("adaptive_threshold_detection -> average value: ");
  Serial.println(average);
  Serial.println("adaptive_threshold_detection -> end calibration...");
}

void _updateAverage(int newValue) {
  int firstValue = calibrationValues[0], index;
  for(index = 0; index < CALIBRATION_SAMPLES - 1; ++index) {
    calibrationValues[index] = calibrationValues[index + 1];
  }
  calibrationValues[CALIBRATION_SAMPLES - 1] = newValue;
  
  sum -= firstValue;
  sum += newValue;
  average = sum / CALIBRATION_SAMPLES;
}

int _calculateAverage() {
  int index;
  for(index = 0; index < CALIBRATION_SAMPLES; ++index) {
    sum += calibrationValues[index];
  }
  return sum / CALIBRATION_SAMPLES;
}
