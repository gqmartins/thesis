#include <SPI.h>
#include <SD.h>

const int CHIP_SELECT_PIN = 10;

File dataFile;

void fileInputInit() {
  _initSDCard();
  _checkIfFileExists();
  dataFile = SD.open("input.csv");
  // read .csv header
  _readLine();
  Serial.println("file_input -> initialization done...");
}

void fileInputRead(int *x, int *y, int *z) {
  String line = _readLine();
  int values[3];
  int index;
  for(index = 0; index < sizeof(values) / sizeof(values[0]); ++index) {
    line = line.substring(line.indexOf(",") + 1);
    String value = line.substring(0, line.indexOf(","));
    values[index] = value.toInt();
  }
  
  *x = values[0];
  *y = values[1];
  *z = values[2];
}

void _initSDCard() {
  Serial.println("file_input -> initializing sd card...");
  pinMode(CHIP_SELECT_PIN, OUTPUT);
  if(!SD.begin()) {
    Serial.println("file_input -> error Initializing SD card...");
    while(1)
      ;
  }
}

void _checkIfFileExists() {
  if(SD.exists("input.csv")) {
    Serial.println("file_input -> the file exists...");
  } else {
    Serial.println("file_input -> the file doesn't exist...");
    while(1);
  }
}

String _readLine() {
  return dataFile.readStringUntil('\n');
}
