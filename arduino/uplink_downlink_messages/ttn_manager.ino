// payload indexes
const int SENSOR_ID_HIGH_BYTE_INDEX = 0;
const int SENSOR_ID_LOW_BYTE_INDEX = 1;
const int MESSAGE_TYPE_INDEX = 2;

// uplink data indexes
const int COUNTER_INDEX = 3;
// uplink message types
const int COUNTER_DATA_MESSAGE_TYPE = 1;

// downlink data indexes
const int SEND_DATA_INTERVAL_INDEX = 3;
// downlink message types
const int CHANGE_SEND_DATA_INTERVAL_MESSAGE_TYPE = 1;

unsigned long sendDataInterval = 15000;

unsigned long sendDatapreviousTime = 0;
int sensorId;

void ttnManagerInit(int ttnSensorId) {
  sensorId = ttnSensorId;
  ttn.onMessage(_downlinkMessageHandler);
}

bool sendDataToTTN(int count) {
  unsigned long currentTime = millis();
  
  if((currentTime - sendDatapreviousTime) > sendDataInterval) {
    _uplinkMessageHandler(count);
    sendDatapreviousTime = currentTime;
    // check for downlink messages
    ttn.poll();
    return true;
  }
  
  return false;
}

void _uplinkMessageHandler(int count) {
    byte payload[4]; 
    payload[SENSOR_ID_HIGH_BYTE_INDEX] = ((sensorId >> 8) & 0xFF);
    payload[SENSOR_ID_LOW_BYTE_INDEX] = (sensorId & 0xFF);
    payload[MESSAGE_TYPE_INDEX] = COUNTER_DATA_MESSAGE_TYPE;
    payload[COUNTER_INDEX] = count;
    
    ttn.sendBytes(payload, sizeof(payload));
}

void _downlinkMessageHandler(const uint8_t *payload, size_t size, port_t port) {
  int sensorMessageId = payload[SENSOR_ID_LOW_BYTE_INDEX] | payload[SENSOR_ID_HIGH_BYTE_INDEX] << 8;
  if(sensorMessageId != sensorId) {
    return;
  }
  
  uint8_t messageType = payload[MESSAGE_TYPE_INDEX];
  switch (messageType) {
    case CHANGE_SEND_DATA_INTERVAL_MESSAGE_TYPE:
      sendDataInterval = payload[SEND_DATA_INTERVAL_INDEX] * 60000;
      break;
    default:
      break;
  }
}
