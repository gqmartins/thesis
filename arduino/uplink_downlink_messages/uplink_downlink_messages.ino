#include <TheThingsNetwork.h>

const int SENSOR_ID = 1;

// TTN constants
#define FREQ_PLAN TTN_FP_EU868
#define loraSerial Serial1
#define debugSerial Serial

const char *APP_EUI = "70B3D57ED001F69A";
const char *APP_KEY = "48E6E71827DD022F2BF481860272610A";

TheThingsNetwork ttn(loraSerial, debugSerial, FREQ_PLAN);

void setup() {
  loraSerial.begin(57600);
  debugSerial.begin(9600);
  
  while(!debugSerial)
    ;

  ttn.join(APP_EUI, APP_KEY);
  
  ttnManagerInit(SENSOR_ID);
}

void loop() {
  carsCountManagerProcess();
  delay(1000);
}
