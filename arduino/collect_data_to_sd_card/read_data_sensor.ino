//I2C Arduino Library
#include <Wire.h>

// addresses for QMC5883L magnometer
const int QMC5883L_ADDRESS = 0x0D;
// ADDRESS for reset period
const int RESET_ADDRESS = 11;
// ADDRESS for configuration
const int CONFIG_ADDRESS = 9;
// ADDRESS for X less significant bit
const int X_LSB_ADDRESS = 0;
// ADDRESS for status
const int STATUS_ADDRESS = 6;
// ADDRESS for soft reset
const int SOFT_RESET_ADDRESS = 0b00001010;

// values used to write on QMC5883L magnometer
// diferent rates to read values
const int CONFIG_200HZ = 0b00001100;
// gauss values
const int CONFIG_8GAUSS = 0b00010000;
const int CONFIG_2GAUSS = 0b00000000;
// continuous reading value
const int CONFIG_CONT = 0b00000001;
// reset period value
const int CONFIG_RESET = 0b00000001;
// soft reset value
const int SOFT_RESET_VALUE = 0b10000000;

// number of bytes to read, 6 for each axis
# define BYTES_TO_READ = 6;

double delayTime;

void sensorInputInit(int rate) {
  _writeRegister(SOFT_RESET_ADDRESS, SOFT_RESET_VALUE);
  _writeRegister(RESET_ADDRESS, CONFIG_RESET);
  setReadDataRate(rate);
}

// reads x, y and z values from sensor
bool sensorInputRead(int *x, int *y, int *z) {
  delay(delayTime);
  if(_readRegister(X_LSB_ADDRESS,6)) {
    *x = Wire.read() | (Wire.read()<<8);
    *y = Wire.read() | (Wire.read()<<8);
    *z = Wire.read() | (Wire.read()<<8);

    return true;
  }
  return false;
}

// changes the rate that the sensor reads the values
void setReadDataRate(int rate) {
  delayTime = ((double)1/rate) * 1000;
  _writeRegister(CONFIG_ADDRESS, CONFIG_2GAUSS | CONFIG_200HZ | CONFIG_CONT);
}

// writes value in register with address reg
void _writeRegister(int reg, int value) {
  Wire.beginTransmission(QMC5883L_ADDRESS);
  Wire.write(reg);
  Wire.write(value);
  Wire.endTransmission();
}

// reads count bytes
int _readRegister(int reg, int count)
{
  Wire.beginTransmission(QMC5883L_ADDRESS);
  Wire.write(reg);
  Wire.endTransmission();
  
  Wire.requestFrom(QMC5883L_ADDRESS,count);
  int n = Wire.available();
  return n <= count;
}
