const int FINISH_PIN = 7;
const int RATE = 10;
const String FILENAME = "10hz.csv";

int x, y, z;

void setup() {
  //while(!Serial)
    //;

  sensorInputInit(RATE);
  initSDCard(FILENAME);

  pinMode(LED_BUILTIN, OUTPUT);

  //pinMode(START_STOP_PIN, INPUT_PULLUP);
  pinMode(FINISH_PIN, INPUT_PULLUP);

  Serial.println("BEGINING...");
}

void loop() {
  //Serial.println("READING...");
  if(digitalRead(FINISH_PIN) == HIGH) {
    changeFile("dummy.txt");
    digitalWrite(LED_BUILTIN, HIGH);
    //Serial.println("FINISHED");
    while(1)
      ;
  }
  if(sensorInputRead(&x, &y, &z)) {
    char line[30];
    sprintf(line, "%ld,%d,%d,%d", millis(), x, y, z);
    //Serial.print("x: ");
    //Serial.print(x);
    //Serial.print(", y: ");
    //Serial.print(y);
    //Serial.print(", z: ");
    //Serial.println(z);
    writeLine(line);
  }
}
