#include <SD.h>
#include <SPI.h>

const int CHIP_SELECT_PIN = 10;
File dataFile;

void initSDCard(String filename) {
  _initCard();
  _createFile(filename);
}

// writes line to file
void writeLine(String line) {
  dataFile.println(line);
}

// changes files to write on
void changeFile(String filename) {
  dataFile.close();
  _createFile(filename);
}

void _initCard() {
  //Serial.println("Initializing SD card...");
  pinMode(CHIP_SELECT_PIN, OUTPUT);
  if(!SD.begin()) {
    //Serial.println("ERROR Initializing SD card...");
    while(1)
      ;
  }
  //Serial.println("SD Card Initialization DONE...");
}

// creates file, removing another if it has the same name
void _createFile(String filename) {
  if(SD.exists(filename)) {
    SD.remove(filename);
  }
  dataFile = SD.open(filename, FILE_WRITE);
}
