int count = 0;

void carsCountManagerProcess() {
  String sample = fileHandlerRead();
  bool carPassed = adaptiveThresholdDetectionFunction(sample);

  if(carPassed) {
    count += 1;
    Serial.println("cars_count_manager -> a car has passed !!!");
    Serial.print("cars_count_manager -> number of cars counted: ");
    Serial.println(count);
  }
}
