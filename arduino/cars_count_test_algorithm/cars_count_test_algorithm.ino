const String READ_FILENAME = "input.csv"; // filename where the data will be read
const String WRITE_FILENAME = "output.csv"; // filename where the results will be written

void setup() {
  Serial.begin(9600);
  while(!Serial)
    ;

  fileHandlerInit(READ_FILENAME, WRITE_FILENAME);
  adaptiveThresholdDetectionInit();
}

void loop() {
  carsCountManagerProcess();
}
