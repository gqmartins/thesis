#include <SPI.h>
#include <SD.h>

const int CHIP_SELECT_PIN = 10;

File readDataFile;
File writeDataFile;

void fileHandlerInit(String readFilename, String writeFilename) {
  _initSDCard();
  _checkIfFileExists(readFilename, writeFilename);
  readDataFile = SD.open(readFilename);
  writeDataFile = SD.open(writeFilename, FILE_WRITE);
  // read .csv header
  fileHandlerRead();
  fileHandlerWrite("start-time,end-time");
  Serial.println("file_handler -> initialization done...");
}

String fileHandlerRead() {
  return readDataFile.readStringUntil('\n');
}

void fileHandlerWrite(String line) {
  writeDataFile.println(line);
  writeDataFile.flush();
}

void _initSDCard() {
  Serial.println("file_handler -> initializing sd card...");
  pinMode(CHIP_SELECT_PIN, OUTPUT);
  if(!SD.begin()) {
    Serial.println("file_handler -> error Initializing SD card...");
    while(1)
      ;
  }
}

void _checkIfFileExists(String readFilename, String writeFilename) {
  if(SD.exists(readFilename)) {
    Serial.println("file_handler -> the file exists...");
  } else {
    Serial.println("file_handler -> the file doesn't exist...");
    while(1);
  }

  if(SD.exists(writeFilename)) {
    SD.remove(writeFilename);
  }
}
