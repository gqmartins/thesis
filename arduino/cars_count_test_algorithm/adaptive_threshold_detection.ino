// threshold limit to consider the start of a passing car
const int TRIGGER_THRESHOLD = 1800;
// threshold limit to consider the stop of a passing car
const int THRESHOLD_LIMIT = 800;
const int CALIBRATION_SAMPLES = 100;

// STATES
const int CAR_PASSING = 0;
const int CAR_NOT_PASSING = 1;

// LIMIT_CROSSED
const int UP_LIMIT = 0;
const int DOWN_LIMIT = 1;

int calibrationValues[CALIBRATION_SAMPLES];
int average, currentState = CAR_NOT_PASSING, currentLimit;
long sum = 0;
String startPassingTimestamp, endPassingTimestamp;

void adaptiveThresholdDetectionInit() {
  Serial.println("adaptive_threshold_detection -> Init...");
  _calibrate();
}

bool adaptiveThresholdDetectionFunction(String sample) {
  int x, y, z;
  String timestamp;
  _readResult(sample, &timestamp, &x, &y, &z);
  
  int maxTriggerLimit = average + TRIGGER_THRESHOLD;
  int minTriggerLimit = average - TRIGGER_THRESHOLD;
  int maxThresholdLimit = maxTriggerLimit > 0 ? maxTriggerLimit - THRESHOLD_LIMIT : maxTriggerLimit + THRESHOLD_LIMIT;
  int minThresholdLimit = minTriggerLimit > 0 ? minTriggerLimit - THRESHOLD_LIMIT : minTriggerLimit + THRESHOLD_LIMIT;
  bool carPassed = false;

  // car has passed the maxTriggerlimit OR the minTrggerLimit WHEN a car is not passing
  if((y >= maxTriggerLimit || y <= minTriggerLimit) && currentState == CAR_NOT_PASSING) {
    currentState = CAR_PASSING;
    currentLimit = y >= maxTriggerLimit ? UP_LIMIT : DOWN_LIMIT;
    startPassingTimestamp = timestamp;
  } 
  // the car has passed the down limit
  else if(currentState == CAR_PASSING && ((currentLimit == UP_LIMIT && y < maxThresholdLimit) || (currentLimit == DOWN_LIMIT && y > minThresholdLimit))) {
      currentState = CAR_NOT_PASSING;
      carPassed = true;
      endPassingTimestamp = timestamp;
      _writeResult();
  }

  if(currentState == CAR_NOT_PASSING) {
    _updateAverage(y);
  }

  return carPassed;
}

void _calibrate() {
  Serial.println("adaptive_threshold_detection -> begin calibration...");
  int x, y, z, index;
  String timestamp;
  for(index = 0; index < CALIBRATION_SAMPLES; ++index) {
    String sample = fileHandlerRead();
    _readResult(sample, &timestamp, &x, &y, &z);
    calibrationValues[index] = y;
  }
  average = _calculateAverage();
  Serial.print("adaptive_threshold_detection -> average value: ");
  Serial.println(average);
  Serial.println("adaptive_threshold_detection -> end calibration...");
}

void _updateAverage(int newValue) {
  int firstValue = calibrationValues[0], index;
  for(index = 0; index < CALIBRATION_SAMPLES - 1; ++index) {
    calibrationValues[index] = calibrationValues[index + 1];
  }
  calibrationValues[CALIBRATION_SAMPLES - 1] = newValue;
  
  sum -= firstValue;
  sum += newValue;
  average = sum / CALIBRATION_SAMPLES;
}

int _calculateAverage() {
  int index;
  for(index = 0; index < CALIBRATION_SAMPLES; ++index) {
    sum += calibrationValues[index];
  }
  return sum / CALIBRATION_SAMPLES;
}

void _readResult(String line, String *timestamp, int *x, int *y, int*z) {
  int values[3];
  int index;
  *timestamp = line.substring(0, line.indexOf(","));
  for(index = 0; index < sizeof(values) / sizeof(values[0]); ++index) {
    line = line.substring(line.indexOf(",") + 1);
    String value = line.substring(0, line.indexOf(","));
    values[index] = value.toInt();
  }
  
  *x = values[0];
  *y = values[1];
  *z = values[2];
}

void _writeResult() {
  fileHandlerWrite(startPassingTimestamp + "," + endPassingTimestamp);
}
