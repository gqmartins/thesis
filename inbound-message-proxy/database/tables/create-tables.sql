BEGIN;

CREATE TABLE Message (
	id SERIAL,
	consumer_type TEXT NOT NULL,
	content TEXT NOT NULL,
	content_type TEXT NOT NULL,
	created TIMESTAMP NOT NULL,
	last_sent_try TIMESTAMP NOT NULL,
	
	CONSTRAINT PK_Message PRIMARY KEY (id)
);

COMMIT;