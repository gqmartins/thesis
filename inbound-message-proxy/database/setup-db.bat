SET DBNAME=messagereceiverdb
SET USERNAME=messagereceiverdb_user
SET USERPASSWORD=messagereceiverdb_password

psql -h localhost -p 5432 -d postgres -U postgres -c "DROP DATABASE IF EXISTS \"%DBNAME%\";"
psql -h localhost -p 5432 -d postgres -U postgres -c "DROP OWNED BY \"%USERNAME%\" CASCADE;"
psql -h localhost -p 5432 -d postgres -U postgres -c "DROP USER IF EXISTS \"%USERNAME%\";"
psql -h localhost -p 5432 -d postgres -U postgres -c "CREATE database \"%DBNAME%\";"
psql -h localhost -p 5432 -d %DBNAME% -U postgres -c "CREATE USER \"%USERNAME%\" WITH PASSWORD '%USERPASSWORD%';"
psql -h localhost -p 5432 -d %DBNAME% -U postgres -c "GRANT ALL PRIVILEGES ON DATABASE \"%DBNAME%\" to \"%USERNAME%\";"

psql -h localhost -p 5432 -d %DBNAME% -U postgres -f tables\create-tables.sql

psql -h localhost -p 5432 -d %DBNAME% -U postgres -c "GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO \"%USERNAME%\";"
psql -h localhost -p 5432 -d %DBNAME% -U postgres -c "GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO \"%USERNAME%\";"

pause