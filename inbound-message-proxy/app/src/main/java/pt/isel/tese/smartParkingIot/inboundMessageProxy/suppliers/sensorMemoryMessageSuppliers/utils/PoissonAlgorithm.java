package pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.sensorMemoryMessageSuppliers.utils;

public class PoissonAlgorithm {

    public static int calculateValue(double median) {
        double L = Math.exp(-median);
        double p = 1.0;
        int k = 0;

        do {
            k++;
            p *= Math.random();
        } while (p > L);

        return k - 1;
    }
}
