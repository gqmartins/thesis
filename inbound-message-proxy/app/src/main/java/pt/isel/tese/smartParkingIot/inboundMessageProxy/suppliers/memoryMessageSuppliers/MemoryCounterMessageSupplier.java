package pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.memoryMessageSuppliers;

import pt.isel.tese.smartParkingIot.inboundMessageProxy.messages.CounterMessage;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.IMessageSupplier;

import java.util.Random;
import java.util.function.Consumer;

public class MemoryCounterMessageSupplier implements IMessageSupplier<CounterMessage> {

    private static final int SENSOR_BOUND = 15;
    private static final int COUNTER_BOUND = 100;
    private static final int SLEEP_TIME = 10000;

    @Override
    public void setup(Consumer<CounterMessage> messageConsumer) throws Exception {
        final Random random = new Random();
        new Thread(() -> {
            while(true) {
                int sensorId = random.nextInt(SENSOR_BOUND);
                int count = random.nextInt(COUNTER_BOUND);
                CounterMessage message = new CounterMessage(sensorId, count);
                messageConsumer.accept(message);
                try {
                    Thread.sleep(SLEEP_TIME);
                } catch (InterruptedException e) {
                    System.out.println("Thread failed to sleep");
                }
            }
        }).start();
    }
}