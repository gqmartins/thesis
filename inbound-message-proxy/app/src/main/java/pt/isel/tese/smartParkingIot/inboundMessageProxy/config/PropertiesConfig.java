package pt.isel.tese.smartParkingIot.inboundMessageProxy.config;

import java.io.InputStream;
import java.util.Properties;

public class PropertiesConfig {

    private Properties propertiesFile;

    public PropertiesConfig(String filename) {
        getPropertiesFile(filename);
    }

    public String getValue(String key) {
        return propertiesFile.getProperty(key);
    }

    private void getPropertiesFile(String filename) {
        try  {
            propertiesFile = new Properties();
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(filename);
            propertiesFile.load(inputStream);
        } catch(Exception ex) {
            System.out.println("Couldn't read properties file: " + ex.getMessage());
        }
    }
}
