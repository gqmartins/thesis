package pt.isel.tese.smartParkingIot.inboundMessageProxy.config;

public class PropertiesFileKeys {

    // filename
    public static final String PROPERTIES_FILE = "config.properties";

    // properties file values
    // values to access TTN
    public static final String TTN_REGION_KEY = "ttn-region";
    public static final String TTN_APP_ID = "ttn-app-id";
    public static final String TTN_ACCESS_KEY = "ttn-access-key";

    // values to access API
    public static final String INBOUND_MESSAGE_API_URI = "inbound-message-api-uri";
    public static final String INBOUND_MESSAGE_API_KEY = "inbound-message-api-key";

    // values to create sensor memory supplier
    public static final String SENSOR_CONFIG_FILE = "sensor-config-file";
    public static final String TIME_COUNTS_FILE = "time-counts.csv";
    public static final String MINUTES_INTERVAL_KEY = "real-minutes-interval";
    public static final String SECONDS_INTERVAL_KEY = "fake-seconds-interval";
}
