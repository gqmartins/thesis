package pt.isel.tese.smartParkingIot.inboundMessageProxy.handlers;

import java.util.function.Function;

public interface IMessageHandler<T> extends Function<T, Boolean> {
}
