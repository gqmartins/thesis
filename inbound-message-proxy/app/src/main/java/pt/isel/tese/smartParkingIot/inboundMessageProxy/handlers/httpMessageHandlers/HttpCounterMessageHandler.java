package pt.isel.tese.smartParkingIot.inboundMessageProxy.handlers.httpMessageHandlers;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.handlers.IMessageHandler;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.messages.CounterMessage;

public class HttpCounterMessageHandler implements IMessageHandler<CounterMessage> {

    private final String inboundMessageApiUri;
    private final String inboundMessageApiKey;
    private static final String SENSOR_ID_URI_PART = "{sensor-id}";

    public HttpCounterMessageHandler(String inboundMessageApiUri, String inboundMessageApiKey) {
        this.inboundMessageApiUri = inboundMessageApiUri;
        this.inboundMessageApiKey = inboundMessageApiKey;
    }

    @Override
    public Boolean apply(CounterMessage counterMessage) {
        String inboundMessageUri = inboundMessageApiUri.replace(SENSOR_ID_URI_PART, String.valueOf(counterMessage.getSensorId()));
        HttpPost postRequest = buildPostRequest(inboundMessageUri, counterMessage);
        try(CloseableHttpResponse response = HttpClients.createDefault().execute(postRequest)) {
            int statusCode = response.getStatusLine().getStatusCode();
            return statusCode == 200 || statusCode == 201;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    private HttpPost buildPostRequest(String uri, CounterMessage counterMessage) {
        try {
            HttpPost postRequest = new HttpPost(uri);

            String jsonEntity = "{\"count\": " + counterMessage.getCount() + "}\"";
            StringEntity entity = new StringEntity(jsonEntity);
            postRequest.setEntity(entity);
            postRequest.setHeader("Accept", "application/json");
            postRequest.setHeader("Content-type", "application/json");
            postRequest.setHeader("Authorization", "Basic " + inboundMessageApiKey);

            return postRequest;
        } catch (Exception e) {
            throw new RuntimeException("Error making the request.");
        }

    }
}
