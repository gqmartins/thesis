package pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.TTNMessageSuppliers;

import org.thethingsnetwork.data.common.Connection;
import org.thethingsnetwork.data.common.messages.DataMessage;
import org.thethingsnetwork.data.common.messages.UplinkMessage;
import org.thethingsnetwork.data.mqtt.Client;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.messages.CounterMessage;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.IMessageSupplier;

import java.util.function.Consumer;

public class TTNCounterMessageSupplier implements IMessageSupplier<CounterMessage> {

    private Client client;
    private static final int SENSOR_ID_HIGH_BYTE_INDEX = 0;
    private static final int SENSOR_ID_LOW_BYTE_INDEX = 1;
    private static final int MESSAGE_TYPE_INDEX = 2;
    private static final int COUNTER_INDEX = 3;
    private static final int COUNTER_MESSAGE_TYPE = 1;

    public TTNCounterMessageSupplier(String region, String appId, String accessKey) {
        if(client == null) {
            initClient(region, appId, accessKey);
        }
    }

    @Override
    public void setup(Consumer<CounterMessage> messageConsumer) throws Exception {
        client.onMessage((deviceId, messageData) -> {
            byte[] payload = getDataPayload(messageData);
            if((payload.length == 1 && payload[SENSOR_ID_HIGH_BYTE_INDEX] == 0) ||
                payload[MESSAGE_TYPE_INDEX] != COUNTER_MESSAGE_TYPE){
                return;
            }
            int sensorId = getSensorId(payload);
            CounterMessage message = new CounterMessage(sensorId, payload[COUNTER_INDEX]);
            messageConsumer.accept(message);
        });
        client.start();
    }

    private void initClient(String region, String appId, String accessKey) {
        if(client == null) {
            try {
                client = new Client(region, appId, accessKey);
                client.onActivation((deviceId, data) -> System.out.println("TTNBaseMessageSupplier -> activation: " + deviceId + ", data: " + data.getDevAddr()));
                client.onError((Throwable _error) -> System.err.println("TTNBaseMessageSupplier ->  error: " + _error.getMessage()));
                client.onConnected((Connection _client) -> System.out.println("TTNBaseMessageSupplier -> connected !"));
            } catch (Exception e) {
                throw new RuntimeException("Failed to start client.");
            }
        }
    }

    private byte[] getDataPayload(DataMessage dataMessage) {
        return ((UplinkMessage) dataMessage).getPayloadRaw();
    }

    private int getSensorId(byte[] payload) {
        return ((payload[SENSOR_ID_HIGH_BYTE_INDEX] & 0xFF) << 8) | (payload[SENSOR_ID_LOW_BYTE_INDEX] & 0xFF);
    }
}
