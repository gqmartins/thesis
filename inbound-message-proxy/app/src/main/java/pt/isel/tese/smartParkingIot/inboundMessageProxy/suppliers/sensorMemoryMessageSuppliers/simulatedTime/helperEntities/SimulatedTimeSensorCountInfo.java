package pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.sensorMemoryMessageSuppliers.simulatedTime.helperEntities;

import pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.sensorMemoryMessageSuppliers.entities.SensorInfo;

import java.util.List;

public class SimulatedTimeSensorCountInfo extends SensorInfo {

    private List<Integer> values;

    public SimulatedTimeSensorCountInfo(int sensorId, int baseValue, String configFile, List<Integer> values) {
        super(sensorId, baseValue, configFile);
        this.values = values;
    }

    public List<Integer> getValues() {
        return values;
    }
}
