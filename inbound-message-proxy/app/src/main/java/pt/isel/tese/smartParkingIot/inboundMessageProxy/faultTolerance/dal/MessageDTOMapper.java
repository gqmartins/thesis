package pt.isel.tese.smartParkingIot.inboundMessageProxy.faultTolerance.dal;

import com.google.gson.Gson;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.faultTolerance.entities.MessageDTO;

import java.sql.Timestamp;

public class MessageDTOMapper {

    private class MessageWrapper<T> {

        public T content;
    }

    private Gson gson = new Gson();

    public <T> MessageDTO transformToMessageDTO(T message, String consumerType) {
        String content = gson.toJson(message);
        String contentType = message.getClass().getTypeName();
        Timestamp created = new Timestamp(System.currentTimeMillis());
        return new MessageDTO(consumerType, content, contentType, created, created);
    }

    public <T> T transformToMessage(MessageDTO message) {
        try {
            Class<T> type = (Class<T>) Class.forName(message.getContentType());
            T content = gson.fromJson(message.getContent(), type);
            return content;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
}
