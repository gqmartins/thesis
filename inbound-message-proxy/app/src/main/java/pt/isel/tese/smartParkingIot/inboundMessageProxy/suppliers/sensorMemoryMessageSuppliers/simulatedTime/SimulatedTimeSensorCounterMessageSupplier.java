package pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.sensorMemoryMessageSuppliers.simulatedTime;

import pt.isel.tese.smartParkingIot.inboundMessageProxy.messages.CounterMessage;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.IMessageSupplier;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.sensorMemoryMessageSuppliers.entities.SensorInfo;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.sensorMemoryMessageSuppliers.realTime.helperEntities.CountData;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.sensorMemoryMessageSuppliers.simulatedTime.helperEntities.SimulatedTimeSensorCountInfo;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.sensorMemoryMessageSuppliers.utils.PoissonAlgorithm;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.sensorMemoryMessageSuppliers.utils.TimeUtils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SimulatedTimeSensorCounterMessageSupplier implements IMessageSupplier<CounterMessage> {

    private final Map<Integer, SimulatedTimeSensorCountInfo> sensors;
    private final int valuesCount;
    private final int minutesInterval;
    private final int secondsInterval;
    private static final int MILLISECONDS_IN_SECOND = 1000;

    public SimulatedTimeSensorCounterMessageSupplier(int minutesInterval, int secondsInterval, String sensorConfigFilename) {
        this.sensors = getSensorList(sensorConfigFilename);
        this.minutesInterval = minutesInterval;
        this.secondsInterval = secondsInterval;
        valuesCount = sensors.entrySet().iterator().next().getValue().getValues().size();
    }

    @Override
    public void setup(Consumer<CounterMessage> messageConsumer) {
        final Calendar date = calculateDate();
        final int[] currentIndex = calculateIndex(date);
        new Thread(() -> {
            try {
                while(true) {
                    sensors
                            .values()
                            .stream()
                            .map(sensor -> {
                                int fileCount = sensor.getValues().get(currentIndex[0]);
                                int poissonValue = PoissonAlgorithm.calculateValue(fileCount);
                                String created = getCreatedDate(date);
                                return new CounterMessage(sensor.getSensorId(), poissonValue, created);
                            })
                            .forEach(messageConsumer);
                    updateCurrentIndex(currentIndex);
                    updateDate(date);
                    Thread.sleep(this.secondsInterval * MILLISECONDS_IN_SECOND);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private Map<Integer, SimulatedTimeSensorCountInfo> getSensorList(String sensorConfigFilename) {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(sensorConfigFilename);
        try(Stream<String> stream = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8)).lines()) {
            return stream
                    .skip(1)
                    .map(line -> new ArrayList<>(Arrays.asList(line.split(","))))
                    .map(SensorInfo::getSensorInfo)
                    .map(this::transformToSensorCountInfo)
                    .collect(Collectors.toMap(
                            SimulatedTimeSensorCountInfo::getSensorId,
                            sensor -> sensor
                    ));
        }
    }

    private SimulatedTimeSensorCountInfo transformToSensorCountInfo(SensorInfo sensorInfo) {
        List<Integer> values = getCountValues(sensorInfo.getConfigFile());
        return new SimulatedTimeSensorCountInfo(sensorInfo.getSensorId(), sensorInfo.getBaseValue(),
                sensorInfo.getConfigFile(), values);
    }

    private List<Integer> getCountValues(String filename) {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(filename);
        try(Stream<String> stream = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8)).lines()) {
            return stream
                    .skip(1)
                    .map(line -> new ArrayList<>(Arrays.asList(line.split(","))))
                    .map(list -> Integer.parseInt(list.get(2)))
                    .collect(Collectors.toList());
        }
    }

    private int[] calculateIndex(Calendar date) {
        String filename = sensors.entrySet().iterator().next().getValue().getConfigFile();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(filename);
        try(Stream<String> stream = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8)).lines()) {
            List<CountData> countData = stream
                    .skip(1)
                    .map(line -> new ArrayList<>(Arrays.asList(line.split(","))))
                    .map(CountData::getCountData)
                    .collect(Collectors.toList());
            for(int index = 0; index < countData.size(); ++index) {
                CountData currentCountData = countData.get(index);
                if(currentCountData.getWeekDay() == date.get(Calendar.DAY_OF_WEEK)
                        && currentCountData.getHour() == date.get(Calendar.HOUR_OF_DAY)
                        && currentCountData.getMinute() == date.get(Calendar.MINUTE)) {
                    return new int[]{index};
                }
            }
        }
        return null;
    }

    private Calendar calculateDate() {
        Calendar currentTime = Calendar.getInstance();
        int minute = TimeUtils.getClosestMinute(currentTime.get(Calendar.MINUTE), this.minutesInterval);
        currentTime.set(Calendar.MINUTE, minute);
        if(minute == 0) {
            int hour = currentTime.get(Calendar.HOUR_OF_DAY);
            currentTime.set(Calendar.HOUR_OF_DAY, hour == 23 ? 0 : hour + 1);
        }
        return currentTime;
    }

    private String getCreatedDate(Calendar calendar) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(calendar.getTime());
    }

    private void updateCurrentIndex(int[] currentIndex) {
        ++currentIndex[0];
        if(currentIndex[0] == valuesCount) {
            currentIndex[0] = 0;
        }
    }

    private void updateDate(Calendar lastSentDate) {
        lastSentDate.add(Calendar.MINUTE, this.minutesInterval);
    }
}
