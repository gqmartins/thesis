package pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.sensorMemoryMessageSuppliers.realTime;

import pt.isel.tese.smartParkingIot.inboundMessageProxy.messages.CounterMessage;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.IMessageSupplier;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.sensorMemoryMessageSuppliers.entities.SensorInfo;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.sensorMemoryMessageSuppliers.realTime.helperEntities.CountData;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.sensorMemoryMessageSuppliers.realTime.helperEntities.RealTimeSensorCountInfo;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.sensorMemoryMessageSuppliers.realTime.helperEntities.TimeKey;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.sensorMemoryMessageSuppliers.utils.PoissonAlgorithm;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.sensorMemoryMessageSuppliers.utils.TimeUtils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RealTimeSensorCounterMessageSupplier implements IMessageSupplier<CounterMessage> {

    private final Map<Integer, RealTimeSensorCountInfo> sensors;
    private final int minutesInterval;
    private static final int MILLISECONDS_IN_MINUTE = 60000;

    public RealTimeSensorCounterMessageSupplier(int minutesInterval, String sensorConfigFilename) {
        this.sensors = getSensorList(sensorConfigFilename);
        this.minutesInterval = minutesInterval;
    }

    @Override
    public void setup(Consumer<CounterMessage> messageConsumer) {
        new Thread(() -> {
            try {
                while(true) {
                    sensors
                            .values()
                            .stream()
                            .map(sensor -> {
                                int fileCount = calculateCarsCount(sensor.getId());
                                int poissonValue = PoissonAlgorithm.calculateValue(fileCount);
                                return new CounterMessage(sensor.getId(), sensor.getBaseValue() + poissonValue);
                            })
                            .forEach(messageConsumer);
                    Thread.sleep(this.minutesInterval * MILLISECONDS_IN_MINUTE);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private int calculateCarsCount(int sensorId) {
        Calendar currentTime = Calendar.getInstance();
        int weekDay = currentTime.get(Calendar.DAY_OF_WEEK);
        int hour = currentTime.get(Calendar.HOUR_OF_DAY);
        int minute = TimeUtils.getClosestMinute(currentTime.get(Calendar.MINUTE), this.minutesInterval);
        TimeKey timeKey = new TimeKey(weekDay, hour, minute);
        return sensors.get(sensorId).getCountValues().get(timeKey);
    }

    private Map<Integer, RealTimeSensorCountInfo> getSensorList(String sensorConfigFilename) {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(sensorConfigFilename);
        try(Stream<String> stream = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8)).lines()) {
            return stream
                    .skip(1)
                    .map(line -> new ArrayList<>(Arrays.asList(line.split(","))))
                    .map(SensorInfo::getSensorInfo)
                    .map(this::transformToSensorCountInfo)
                    .collect(Collectors.toMap(
                            RealTimeSensorCountInfo::getId,
                            sensor -> sensor
                    ));
        }
    }

    private RealTimeSensorCountInfo transformToSensorCountInfo(SensorInfo sensorInfo) {
        Map<TimeKey, Integer> valueCounts = getMap(sensorInfo.getConfigFile());
        return new RealTimeSensorCountInfo(sensorInfo.getSensorId(), sensorInfo.getBaseValue(), valueCounts);
    }

    private Map<TimeKey, Integer> getMap(String filename) {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(filename);
        try(Stream<String> stream = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8)).lines()) {
            return stream
                    .skip(1)
                    .map(line -> new ArrayList<>(Arrays.asList(line.split(","))))
                    .map(CountData::getCountData)
                    .collect(Collectors.toMap(
                            countData -> new TimeKey(countData.getWeekDay(), countData.getHour(), countData.getMinute()),
                            CountData::getCount
                    ));
        }
    }
}
