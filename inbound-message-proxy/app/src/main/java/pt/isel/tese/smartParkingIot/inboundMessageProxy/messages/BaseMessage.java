package pt.isel.tese.smartParkingIot.inboundMessageProxy.messages;

public class BaseMessage {

    private int sensorId;

    public BaseMessage(int sensorId) {
        this.sensorId = sensorId;
    }

    public int getSensorId() {
        return sensorId;
    }

    public void setSensorId(int sensorId) {
        this.sensorId = sensorId;
    }
}
