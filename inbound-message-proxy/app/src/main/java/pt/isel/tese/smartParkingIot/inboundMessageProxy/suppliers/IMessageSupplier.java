package pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers;

import java.util.function.Consumer;

public interface IMessageSupplier<T> {

    void setup(Consumer<T> messageConsumer) throws Exception;
}
