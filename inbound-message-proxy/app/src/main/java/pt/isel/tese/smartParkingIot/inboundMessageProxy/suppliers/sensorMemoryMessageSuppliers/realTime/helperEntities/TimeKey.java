package pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.sensorMemoryMessageSuppliers.realTime.helperEntities;

import java.util.Objects;

public class TimeKey {

    private final int weekDay;
    private final int hour;
    private final int minute;

    public TimeKey(int weekDay, int hour, int minute) {
        this.weekDay = weekDay;
        this.hour = hour;
        this.minute = minute;
    }

    public int getWeekDay() {
        return weekDay;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    @Override
    public boolean equals(Object obj) {
        TimeKey timeKey = (TimeKey) obj;
        return timeKey.weekDay == this.weekDay && timeKey.hour == this.hour && timeKey.minute == this.minute;
    }

    @Override
    public int hashCode() {
        return Objects.hash(weekDay, hour, minute);
    }
}
