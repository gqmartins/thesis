package pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.sensorMemoryMessageSuppliers.realTime.helperEntities;

import java.util.List;

public class CountData {

    private final int weekDay;
    private final int hour;
    private final int minute;
    private final int count;

    public CountData(int weekDay, int hour, int minute, int count) {
        this.weekDay = weekDay;
        this.hour = hour;
        this.minute = minute;
        this.count = count;
    }

    public int getWeekDay() {
        return weekDay;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public int getCount() {
        return count;
    }

    // used to create an instance based of a csv line
    public static CountData getCountData(List<String> data) {
        int weekDay = Integer.parseInt(data.get(0));
        String[] time = data.get(1).split(":");
        int hour = Integer.parseInt(time[0]);
        int minute = Integer.parseInt(time[1]);
        int count = Integer.parseInt(data.get(2));

        return new CountData(weekDay, hour, minute, count);
    }
}
