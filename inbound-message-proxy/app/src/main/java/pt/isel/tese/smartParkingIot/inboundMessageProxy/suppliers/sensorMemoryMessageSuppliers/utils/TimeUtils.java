package pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.sensorMemoryMessageSuppliers.utils;

public class TimeUtils {

    public static int getClosestMinute(int currentMinute, int minutesInterval) {
        int closestMinuteUp = 0;
        while(closestMinuteUp < currentMinute || closestMinuteUp == 60) {
            closestMinuteUp += minutesInterval;
        }

        if(closestMinuteUp == currentMinute) {
            return currentMinute;
        }

        int closestMinuteDown = closestMinuteUp - minutesInterval;
        int differenceCurrentClosestUp = closestMinuteUp - currentMinute;
        int differenceCurrentClosestDown = Math.abs(closestMinuteDown - currentMinute);

        int calculatedMinute  = differenceCurrentClosestUp <= differenceCurrentClosestDown ? closestMinuteUp : closestMinuteDown;
        return calculatedMinute == 60 ? 0 : calculatedMinute;
    }
}
