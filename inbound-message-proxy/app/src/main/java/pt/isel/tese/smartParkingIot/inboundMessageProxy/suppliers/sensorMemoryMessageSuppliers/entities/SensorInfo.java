package pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.sensorMemoryMessageSuppliers.entities;

import java.util.List;

public class SensorInfo {

    private final int sensorId;
    private final int baseValue;
    private final String configFile;

    public SensorInfo(int sensorId, int baseValue, String configFile) {
        this.sensorId = sensorId;
        this.baseValue = baseValue;
        this.configFile = configFile;
    }

    public int getSensorId() {
        return sensorId;
    }

    public int getBaseValue() {
        return baseValue;
    }

    public String getConfigFile() {
        return configFile;
    }

    // used to create an instance based of a csv line
    public static SensorInfo getSensorInfo(List<String> data) {
        int sensor = Integer.parseInt(data.get(0));
        int baseValue = Integer.parseInt(data.get(1));
        String configFile = data.get(2);

        return new SensorInfo(sensor, baseValue, configFile);
    }
}
