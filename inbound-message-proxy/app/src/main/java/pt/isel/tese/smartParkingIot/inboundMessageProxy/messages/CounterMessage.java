package pt.isel.tese.smartParkingIot.inboundMessageProxy.messages;

public class CounterMessage extends BaseMessage {

    private int count;
    private String created;

    public CounterMessage(int sensorId, int count) {
        super(sensorId);
        this.count = count;
    }

    public CounterMessage(int sensorId, int count, String created) {
        this(sensorId, count);
        this.created = created;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }
}
