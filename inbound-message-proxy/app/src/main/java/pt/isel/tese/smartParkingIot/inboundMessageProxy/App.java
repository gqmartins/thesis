package pt.isel.tese.smartParkingIot.inboundMessageProxy;

import pt.isel.tese.smartParkingIot.inboundMessageProxy.config.PropertiesConfig;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.faultTolerance.FaultToleranceMechanism;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.handlers.IMessageHandler;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.handlers.httpMessageHandlers.HttpCounterMessageHandler;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.IMessageSupplier;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.TTNMessageSuppliers.TTNCounterMessageSupplier;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.memoryMessageSuppliers.MemoryCounterMessageSupplier;

import java.util.ArrayList;
import java.util.List;

import static pt.isel.tese.smartParkingIot.inboundMessageProxy.config.PropertiesFileKeys.*;

public class App {

    private static final List<IMessageSupplier> suppliers = new ArrayList<>();
    private static final List<IMessageHandler> handlers = new ArrayList<>();

    public static void main(String[] args) throws Exception {
        PropertiesConfig propertiesConfig = new PropertiesConfig(PROPERTIES_FILE);
        createSuppliers(propertiesConfig);
        createHandlers(propertiesConfig);
        matchHandlersToSuppliers();
    }

    private static void createSuppliers(PropertiesConfig properties) throws Exception {
        String ttnRegion = properties.getValue(TTN_REGION_KEY);
        String ttnAppId = properties.getValue(TTN_APP_ID);
        String ttnAccessKey = properties.getValue(TTN_ACCESS_KEY);

        //int minutesInterval = Integer.parseInt(properties.getValue(MINUTES_INTERVAL_KEY));
        //int secondsInterval = Integer.parseInt(properties.getValue(SECONDS_INTERVAL_KEY));
        //String sensorConfigFile = properties.getValue(SENSOR_CONFIG_FILE);

        suppliers.add(new TTNCounterMessageSupplier(ttnRegion, ttnAppId, ttnAccessKey));
        //suppliers.add(new MemoryCounterMessageSupplier());
        //suppliers.add(new RealTimeSensorCounterMessageSupplier(minutesInterval, sensorConfigFile));
        //suppliers.add(new SimulatedTimeSensorCounterMessageSupplier(minutesInterval, secondsInterval, sensorConfigFile));
    }

    private static void createHandlers(PropertiesConfig properties) {
        String inboundMessageApiUri = properties.getValue(INBOUND_MESSAGE_API_URI);
        String inboundMessageApiKey = properties.getValue(INBOUND_MESSAGE_API_KEY);
        HttpCounterMessageHandler httpCounterMessageHandler = new HttpCounterMessageHandler(inboundMessageApiUri,
                inboundMessageApiKey);
        handlers.add(httpCounterMessageHandler);
    }

    private static void matchHandlersToSuppliers() throws Exception {
        FaultToleranceMechanism faultToleranceMechanism = new FaultToleranceMechanism();
        for(int i = 0; i < suppliers.size(); ++i) {
            suppliers.get(i).setup(faultToleranceMechanism.getConsumer(handlers.get(i)));
        }
    }
}
