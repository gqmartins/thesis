package pt.isel.tese.smartParkingIot.inboundMessageProxy.faultTolerance.entities;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "Message")
public class MessageDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "consumer_type")
    private String consumerType;

    @Column(name = "content")
    private String content;

    @Column(name = "content_type")
    private String contentType;

    @Column(name = "created")
    private Timestamp created;

    @Column(name = "last_sent_try")
    private Timestamp lastSentTry;

    public MessageDTO() {
    }

    public MessageDTO(String consumerType, String content, String contentType, Timestamp created, Timestamp lastSentTry) {
        this.consumerType = consumerType;
        this.content = content;
        this.contentType = contentType;
        this.created = created;
        this.lastSentTry = lastSentTry;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getConsumerType() {
        return consumerType;
    }

    public void setConsumerType(String consumerType) {
        this.consumerType = consumerType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Timestamp getLastSentTry() {
        return lastSentTry;
    }

    public void setLastSentTry(Timestamp lastSentTry) {
        this.lastSentTry = lastSentTry;
    }
}
