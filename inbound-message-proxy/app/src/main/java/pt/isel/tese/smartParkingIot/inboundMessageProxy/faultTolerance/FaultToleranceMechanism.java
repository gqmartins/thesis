package pt.isel.tese.smartParkingIot.inboundMessageProxy.faultTolerance;

import pt.isel.tese.smartParkingIot.inboundMessageProxy.faultTolerance.dal.MessageDTOMapper;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.faultTolerance.dal.MessageDataAccessLayer;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.faultTolerance.entities.MessageDTO;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.handlers.IMessageHandler;

import java.sql.Timestamp;
import java.util.function.Consumer;

public class FaultToleranceMechanism {

    private MessageDataAccessLayer messageDAL = new MessageDataAccessLayer();
    private MessageDTOMapper messageMapper = new MessageDTOMapper();

    public <T> Consumer<T> getConsumer(IMessageHandler<T> messageHandler) {
        return message -> {
            tryToSendFailedMessages(messageHandler);
            tryToSendMessage(messageHandler, message);
        };
    }

    private <T> void tryToSendFailedMessages(IMessageHandler<T> messageHandler) {
        String consumerType = getMessageHandlerType(messageHandler);
        messageDAL.read()
                .stream()
                .filter(m -> m.getConsumerType().equals(consumerType))
                .forEach(m -> tryToResendMessage(messageHandler, m));
    }

    private <T> void tryToSendMessage(IMessageHandler<T> messageHandler, T message) {
        boolean success = messageHandler.apply(message);
        if(!success) {
            String consumerType = getMessageHandlerType(messageHandler);
            MessageDTO messageDTO = messageMapper.transformToMessageDTO(message, consumerType);
            messageDAL.insert(messageDTO);
        }
    }

    private <T> void tryToResendMessage(IMessageHandler<T> messageHandler, MessageDTO messageDTO) {
        T message = messageMapper.transformToMessage(messageDTO);
        boolean success = messageHandler.apply(message);
        if(success) {
            messageDAL.delete(messageDTO);
        } else {
            messageDTO.setLastSentTry(new Timestamp(System.currentTimeMillis()));
            messageDAL.update(messageDTO);
        }
    }

    private <T> String getMessageHandlerType(IMessageHandler<T> messageHandler) {
        return messageHandler.getClass().getName();
    }
}
