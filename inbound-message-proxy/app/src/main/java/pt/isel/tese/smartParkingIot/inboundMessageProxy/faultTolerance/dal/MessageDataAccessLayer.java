package pt.isel.tese.smartParkingIot.inboundMessageProxy.faultTolerance.dal;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import pt.isel.tese.smartParkingIot.inboundMessageProxy.faultTolerance.entities.MessageDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class MessageDataAccessLayer {

    private static SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

    public MessageDataAccessLayer() {
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure()
                .build();
        sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
    }

    public void insert(MessageDTO message) {
        execute(session -> session.save(message));
    }

    public List<MessageDTO> read() {
        final List<List<MessageDTO>> messages = new ArrayList<>();
        execute(session -> messages.add(session.createQuery("FROM MessageDTO").list()));
        return messages.get(0);
    }

    public void update(MessageDTO message) {
        execute(session -> session.update(message));
    }

    public void delete(MessageDTO message) {
        execute(session -> session.delete(message));
    }

    private void execute(Consumer<Session> consumer) {
        try(Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            consumer.accept(session);
            session.getTransaction().commit();
        } catch (HibernateException ex) {
            System.out.println("A database related error occurred.");
            ex.printStackTrace();
        }
    }
}
