package pt.isel.tese.smartParkingIot.inboundMessageProxy.suppliers.sensorMemoryMessageSuppliers.realTime.helperEntities;

import java.util.Map;

public class RealTimeSensorCountInfo {

    private int id;
    private int baseValue;
    private Map<TimeKey, Integer> countValues;

    public RealTimeSensorCountInfo(int id, int baseValue, Map<TimeKey, Integer> countValues) {
        this.id = id;
        this.baseValue = baseValue;
        this.countValues = countValues;
    }

    public int getId() {
        return id;
    }

    public int getBaseValue() {
        return baseValue;
    }

    public Map<TimeKey, Integer> getCountValues() {
        return countValues;
    }
}
