\chapter{Aplicação de administração}
\label{cha:administration_tool}

Um dos objetivos para o sistema construído é ter uma interface gráfica capaz de disponibilizar todas as operações para a criação e manipulação das entidades envolvidas na solução. A aplicação que tem a responsabilidade de fazer isso é a \textit{Administration Tool}, uma aplicação \textit{Web} que permite criar zonas e sensores com base num mapa, mostrar listas das entidades como os tipos de sensores, visualizar gráficos da ocupação de percentagem das zonas entre outras funcionalidades.
\par

Foi com base em esboços criados para esta aplicação que a \textit{Administration API} foi criada também. Tudo começou na aplicação de \textit{frontend}, com casos de utilização e uma espécie de levantamento de requisitos relativamente àquilo que a \textit{Administration Tool} tinha de fazer. Neste capítulo explica-se as diferentes partes da aplicação \textit{Web}, com alguns exemplos de utilização e parte do código que está por trás desses exemplos.
\par

A aplicação é feita com base na \textit{framework} de \textit{fontend Angular}. A \textit{framework React} em conjunto com \textit{Redux} também foi considerada para a realização da aplicação e após a aprendizagem de ambas o \textit{Angular} revelou-se a melhor opção por ter padrões bem definidos no que diz respeito à organização do código, não ter tantos conceitos que é preciso dominar e ter uma curva de aprendizagem menor.

\section{Comunicação com Administration API}
\label{sec:administration_tool_comunicacao_administration_api}

A \textit{Administration Tool} é uma aplicação \textit{frontend} que tem a necessidade de comunicar com uma aplicação servidora, através do protocolo \textit{HTTP}. É aqui que entra a \textit{Administration API} já desenvolvida, que contém toda a lógica de negócio necessária para o sistema funcionar como desejado.
\par

O primeiro passo para realizar a comunicação entre as duas componentes era criar código em \textit{Typescript}, linguagem utilizada para a construção da \textit{Administration Tool}, que fizesse os pedidos \textit{HTTP} ao \textit{backend}. Para isto os controladores da aplicação servidora foram decorados com anotações que dizem respeito ao \textit{Swagger}, ferramenta utilizada para gerar uma interface gráfica com os exemplos dos pedidos de uma \textit{API REST}. A partir destas anotações é possível gerar um ficheiro \textit{JSON} ou \textit{YAML} que descreve toda a \textit{API}. Este último ficheiro foi introduzido no \textit{SwaggerHub} e gerou-se um cliente em \textit{Typescript} a partir do mesmo que contém todo o código responsável por fazer os pedidos \textit{HTTP} à aplicação.

\section{Estrutura da aplicação}
\label{sec:administration-tool_estrutura_da_aplicação}

Uma aplicação baseada na \textit{framework Angular} é dividida em componentes. Cada componente é uma parte visual da aplicação e o objetivo é que cada um seja independente dos outros e que sejam reutilizáveis em várias partes da aplicação. Vão ser explicados alguns exemplos desta independência e reutilização os capítulos seguintes. Cada componente é composto então por um ficheiro \textit{HTML}, um ficheiro \textit{CSS} e um ficheiro \textit{Typescript}. É neste último ficheiro onde se diz quais os ficheiros HTML e \textit{CSS} que estão associados ao componente. Um exemplo da declaração de um componente pode ser encontrado na figura \ref{fig:administration-tool/navbar_component}.

\begin{figure}[h]
	\centering
	\includegraphics{administration-tool/navbar_component.png}
	\caption{Componente que lida com a barra de navegação.}
	\label{fig:administration-tool/navbar_component}
\end{figure}

A implementação deste componente é relativamente simples, mas existem casos de implementações bem mais complexas, como é o caso do componente que lida com o mapa onde as zonas e sensores são criados. Associado a um componente pode existir também um serviço, que normalmente é responsável por chamar o código que vai realizar os pedidos à aplicação servidora. Outra peça que se pode encontrar junto de um componente é uma entidade, ou modelo, se um determinado componente for responsável por uma entidade.
\par

Para todas as entidades do sistema existe um componente que trata da sua listagem. Para a criação, edição e remoção das entidades existem outros componentes que dão origem a caixas de diálogo. A título de exemplo, para a entidade zona vai existir um componente chamado \textit{ZoneList} que apresenta a lista de zonas, outro chamado \textit{ZoneCreate} com a resopnsabilidade de apresentar as informações necessárias para criar uma zona e criar a mesma. Por fim existe um último componente chamado \textit{ZoneEdit} que lida com a edição e remoção de uma determinada zona. Este comportamento repete-se para todas as entidades e o mesmo acontece para os nomes dos componentes que seguem todos a nomenclatura explicada.

\subsection{Componentes partilhados}
\label{subsec:administration_tool_componentes_partilhados}

Existem vários componentes que são utilizados por outros, como se fossem componentes utilitários, ou implementações que têm alguns métodos base comuns a diversas classes. O componente \textit{BaseApiService} serve como \textit{wrapper} para todos os serviços que chamam métodos da \textit{API}. O componente \textit{BaseComponent} é utilizado como classe base de alguns outros e tem o objetivo de disponibilizar métodos que tratam erros, ou gerir subscrições assim como encapsular as respostas aos pedidos dos serviços. O componente \textit{Error} é utilizado para mostrar mensagens de erro na interface. A última peça a referir partilhada por diversos artefactos é uma directiva, chamada \textit{Placeholder}, que é colocada nos \textit{templates HTML} que tenham necessidade de mostrar componentes dinâmicos. Estes últimos componentes não são nada mais que componentes criados programaticamente, anexados depois à directiva mencionada. O estilo gráfico destes componentes são, no caso desta aplicação, caixas de diálogo normalmente utilizadas para criar ou editar entidades.

\section{Autenticação}
\label{sec:administration_tool_autenticacao}

A primeira página com que o utilizador se depara é a de autenticação onde insere o seu nome de utilizador e palavra passe para conseguir aceder à aplicação. A imagem da figura \ref{fig:administration-tool_login} é a página onde se realiza o processo de autenticação na aplicação.

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.4]{administration-tool/login.png}
	\caption{Página de login.}
	\label{fig:administration-tool_login}
\end{figure}

O componente responsável pela barra de navegação apresentada no topo da página chama-se \textit{Navbar}, já apresentado na figura \ref{fig:administration-tool/navbar_component}. Este componente aparece em todas as páginas da aplicação e não possui qualquer funcionalidade além do aspeto visual.
\par

O componente responsável por realizar o \textit{login} tem a responsabilidade de garantir que tanto o campo de utilizador como de palavra chave estão preenchidos, assim como realizar o pedido à aplicação servidor para ser feito o \textit{login} na aplicação. Para o pedido ser feito têm de se respeitar as regras impostas pela aplicação servidora, logo tem de ser utilizador o protocolo basic access authentication. Este protocolo consiste em fazer um pedido \textit{HTTP} onde no \textit{header Authorization} é colocada a palavra "Basic"  seguida de um espaço e da concatenação do nome de utilizador e password separados por dois pontos. Se o pedido de autenticação for válido o utilizador da aplicação é reencaminhado para a página explicada no próximo capítulo.

\section{Componentes desenvolvidos}
\label{sec:componentes_desenvolvidos}

A divisão da aplicação em diversos componentes permite uma maior organização do código pois cada componente tem a sua responsabilidade e trata de um conjunto de operações. Assim sendo, as diferentes partes da aplicação estão divididas por componentes, sendo esses apresentados nesta secção.

\subsection{Mapa}
\label{subsec:administration_tool_mapa}

É nesta parte da aplicação que se realizam a criação das zonas e dos sensores, duas entidades essenciais de todo o sistema. Na figura \ref{fig:administration-map} está representada o mapa onde podem ser criadas as entidades referidas.

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.4]{administration-tool/map.png}
	\caption{Mapa de criação de zonas e sensors.}
	\label{fig:administration-map}
\end{figure}

Existem diversos componentes envolvidos na imagem apresentada, sendo o primeiro deles o componente \textit{Board} que engloba a lista de opções que se encontra do lado esquerdo e a área onde se encontra o mapa. A lista de opções é algo fixo, ao passo que a àrea onde se encontra o mapa é dinâmica e o seu conteúdo muda consoante aquilo que for clicado na lista. A mudança do conteúdo desta parte vai ser visualizada nos próximos capítulos onde em vez do mapa vão aparecer as listas das diversas entidades. A mudança do conteúdo consoante aquilo que está selecionado na lista é feita utilizando a biblioteca \textit{Angular Router}.
\par

No mapa estão representadas as entidades zona e sensor, sendo que as zonas são os polígonos com diversas cores e os sensores os pinos a azul. Este mapa cumpre um dos requisitos da tese, que era ter uma representação visual da ocupação das zonas e como se pode ver no mapa cada uma tem uma cor diferente. A zona que está a verde tem uma baixa percentagem de ocupação, a que se encontra a amarelo tem uma média e a vermelha possui uma taxa de ocupação alta. Tanto o mapa como os objetos colocados no mesmo recorrem a uma \textit{framework} de \textit{frontend} chamada \textit{Leaflet}. Na inicialização do componente que é responsável pelo mapa são feitos os pedidos necessários à aplicação servidora para se ter todas as zonas e sensores para posteriormente serem apresentados no mapa. Depois de tudo inserido ficam guardados no componente todos os elementos na forma de uma lista.
\par

Duas funcionalidades que aparecem neste componente é a possibilidade de criar zonas e sensores. Isto é feito clicando num dos dois botões no canto superior direito do mapa. Ao se clicar no botão responsável por criar uma zona é pedido para se colocarem os pontos pertencentes ao perímetro da zona e após estes estarem colocados aparece o ecrã disposto na figura \ref{fig:administration-create_zone}.

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.4]{administration-tool/create_zone.png}
	\caption{Inserção de dados para criação de uma zona.}
	\label{fig:administration-create_zone}
\end{figure}

Esta caixa de diálogo é criada como sendo um componente dinâmico, de seu nome \textit{ZoneCreate} e colocada na diretiva \textit{Placeholder} já referenciada. Para a criação da zona tem de se colocar um nome e o número máximo de veículo que a zona pode ter. Depois dos dados inseridos a nova zona aparece como um polígono no mapa e é possível editar a mesma, como qualquer outra zona, se se clicar em cima da mesma. Neste caso aparece a figura \ref{fig:administration-edit_zone} onde se pode editar o nome da zona e a sua contagem máxima de veículos.

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.7]{administration-tool/edit_zone.png}
	\caption{Edição de uma zona.}
	\label{fig:administration-edit_zone}
\end{figure}

Uma das opções presentes na figura \ref{fig:administration-edit_zone} é a de remoção de uma zona. Após selecionar essa opção é feito um pedido à aplicação servidora para apagar a zona e esta é também apagada no mapa assim como todos os seus sensores. Aqui é novamente utilizado um componente dinâmico para apresentar a caixa de diálogo e este chama-se \textit{EditZone}.
\par

Relativamente aos sensores existem exatamente as mesmas opções que para as zonas. É possível criar um sensor e após a inserção da sua localização no mapa aparece uma caixa de diálogo, apresentada na figura \ref{fig:administration-create_sensor}, onde se tem de preencher os restantes dados relativos ao sensor, nomeadamente o seu nome, a sua zona de entrada e saída. Como cada sensor tem um tipo é necessário também preencher as informações relativas a esse tipo. O tipo do sensor é selecionado na \textit{dropdown} e os campos relativos a esse tipo de sensor vão mudar consoante a escolha.
\par

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.4]{administration-tool/create_sensor.png}
	\caption{Criação de um sensor.}
	\label{fig:administration-create_sensor}
\end{figure}

A edição e remoção de um sensor são feitas clicando no mesmo depois de este estar presente no mapa. A figura \ref{fig:administration-edit_sensor} mostra a caixa de diálogo que aparece quando se clica num sensor no mapa.
\par

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.7]{administration-tool/edit_sensor.png}
	\caption{Edição de um sensor.}
	\label{fig:administration-edit_sensor}
\end{figure}

\subsection{Zonas}
\label{subsec:administration_tool_zonas}

Uma das necessidades que pode existir é visualizar todas as zonas existentes em forma de lista, em vez de se utilizar o mapa. Esta visualização é possível e está representada na figura \ref{fig:administration-zones}.
\par

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.55]{administration-tool/zones.png}
	\caption{Lista de zonas.}
	\label{fig:administration-zones}
\end{figure}

Em cada uma das linhas é apresentado o nome da sua zona, a sua data de criação e modificação. Do lado direito de cada um dos itens existem duas opções, uma que permite editar a zona e outra para a apagar. Clicar na opção de edição leva a que apareça a caixa de diálogo da figura \ref{fig:administration-edit_zone}, o que mais uma vez prova que a utilização de componentes é benéfica pois o mesmo componente está a ser utilizado em dois cenários distintos. Outro exemplo da reutilização de componentes está demonstrado na própria lista, em que o mesmo componente é utilizado para fazer não só esta lista como as restantes apresentadas nos capítulos seguintes. O componente descrito tem o nome de \textit{List}, recebe os elementos a apresentar e eventos para chamar quando são clicados os botões de edição e remoção dos itens.

\subsection{Sensores}
\label{subsec:administration_tool_sensores}

Semelhante à lista das zonas apresentada no capítulo anterior existe também uma lista de sensores. Esta lista é apresentada na figura \ref{fig:administration-sensors} e possui uma diferença relativamente à lista das zonas.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.55]{administration-tool/sensors.png}
	\caption{Lista de sensores.}
	\label{fig:administration-sensors}
\end{figure}

A diferença para a lista anterior está nas opções de cada linha, onde neste caso existe um símbolo de um envelope. Este símbolo é a opção para enviar uma mensagem para o sensor através da \textit{TTN}. Quando clicado aparece a caixa de diálogo da figura \ref{fig:administration-create_outbound_message}.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.7]{administration-tool/create_outbound_message.png}
	\caption{Criação de mensagem de \textit{outbound}.}
	\label{fig:administration-create_outbound_message}
\end{figure}

Como já foi explicado cada sensor tem associado a ele um tipo de sensor. Por sua vez cada tipo de sensor está ligado a diversos tipos de mensagem de \textit{outbound}, que são os tipos de mensagem que podem ser enviados para aquele tipo de sensor. Por esta associação consegue-se chegar a partir de um sensor aos tipos de mensagens que se podem enviar para o mesmo. Estes tipos de mensagens são os que são apresentados na \textit{dropdown} e cada tipo tem diferentes atributos associados, que são os que aparecem quando é selecionar o um tipo de mensagem diferente. Depois de se terem preenchido todos os campos é então enviada a mensagem para o sensor.

\subsection{Tipos de sensor}
\label{subsec:administration_tool_tipos_de_sensor}

Seguindo o mesmo estilo de apresentar as entidades em forma de lista, a que se segue é a que diz respeito aos tipos de sensor. O conteúdo deste componente é não só a lista, mas também a opção de criar um novo tipo sensor. A parte visual do componente é apresentada na figura \ref{fig:administration-sensor_types}.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.7]{administration-tool/sensor_types.png}
	\caption{Lista de tipos de sensor.}
	\label{fig:administration-sensor_types}
\end{figure}

A criação de um sensor é feita clicando no botão que encontra na parte de baixo do componente. Isto vai abrir uma caixa de diálogo, apresentada na figura \ref{fig:administration-create_sensor_type}, que realizar a operação pretendida. O código do componente que realiza a gestão da criação do sensor tem o mecanismo de adicionar e remover os campos que aquele tipo de sensor terá. Para isto existe uma lista com os campos correntes e quando se adiciona um campo a lista cresce. Caso se elimine um dos campos esta ação é refletida na lista que guarda os campos no componente. Após inseridos os dados é feito um pedido à aplicação servidora para criar o tipo de sensor e a lista é atualizada.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.7]{administration-tool/create_sensor_type.png}
	\caption{Criação de um tipo de sensor.}
	\label{fig:administration-create_sensor_type}
\end{figure}

Mais uma vez recorre-se ao componente \textit{List} para preencher a lista de tipos de sensor na qual existem as opções de edição e remoção dos elementos. Na edição de um tipo de sensor aparece a mesma caixa de diálogo da figura \ref{fig:administration-create_sensor_type} com a adição das datas de criação e modificação.

\subsection{Tipos de mensagens de outbound}
\label{subsec:administration_tool_tipos_de_mensagens_de_outbound}

Uma das entidades envolvidas no sistema é o tipo de mensagens de outbound. Tal como os tipos de sensores também têm um componente em que são apresentados os diferentes tipos de mensagem e é possível criar novos tipos. A parte visual deste componente é mostrada na figura \ref{fig:administration-outbound_message_types}.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.55]{administration-tool/outbound_message_types.png}
	\caption{Lista de tipos de mensagem de outbound.}
	\label{fig:administration-outbound_message_types}
\end{figure}

Mais uma vez existe a possibilidade de se apagar ou editar um tipo de mensagem e a listagem dos elementos depende do tipo de sensor selecionado na \textit{dropdown}. A criação do tipo de mensagem envolve uma caixa de diálogo semelhante à apresentada na criação de um tipo de sensor. A única diferença é a inclusão de uma \textit{dropdown} onde se seleciona o tipo de sensor a que aquele tipo de mensagem pertence.

\subsection{Previsão de percentagens das zonas}
\label{subsec:administration_tool_percentagens_zonas}

O componente tratado neste capítulo tem a responsabilidade de mostrar a percentagem de ocupação das zonas num determinado período de tempo. Obter estes valores num período de tempo que já passou era um dos objetivos da tese, no entanto no decorrer da mesma deicidiu-se que também se poderia permitir visualizar os resultados para um tempo futuro. Assim sendo é possível escolher um período de tempo no qual se vão obter as percentagens de ocupação das zonas onde estão contemplados os valores passados e os valores futuros, estes últimos previstos. Um exemplo da obtenção destes resultados e construção de um gráfico a partir dos mesmos pode ser visto na figura \ref{fig:administration-dashboard}.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{administration-tool/dashboard.png}
	\caption{Gráfico com percentagens de ocupação.}
	\label{fig:administration-dashboard}
\end{figure}

O processo da construção do gráfico passa por recolher o que é inserido no formulário, nomeadamente a zona selecionada e o intervalo temporal para depois realizar um pedido à aplicação servidora. A resposta a esse pedido é utilizada para criar os gráficos, pois nela se encontram datas e a percentagem de ocupação para cada uma das datas. O gráfico e construído com a ajuda da framework \textit{Highcharts} e o componente responsável pelo gráfico chama-se \textit{PredictionChart}.

\subsection{Utilizadores}
\label{subsec:administration_tool_utilizadores}

A última entidade da qual resta falar é a que diz respeito aos utilizadores. Novamente é apresentada uma lista com todos os utilizadores do sistema, onde é possível editar e remover os mesmos e também a opção da criação de um novo utilizador.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{administration-tool/users.png}
	\caption{Lista de utilizadores.}
	\label{fig:administration-users}
\end{figure}

Para criação de utilizadores é necessário introduzir os dados referentes aos mesmos, ou seja, o seu nome, nome de utilizador, palavra passe e o seu papel. Um exemplo do formulário para a criação de um utilizador é apresentado na figura \ref{fig:administration-create_user}. Esta funcionalidade está apenas disponível para os administradores do sistema, eles é que podem criar e visualizar utilizadores. A questão de ser o administrador a colocar a palavra passe existe apenas para facilitar o mecanismo de criação de um utilizador, sendo que o mesmo vale se um utilizador se esquecer da palavra passe pois esta pode ser mudada pelo administrador do sistema.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.7]{administration-tool/create_user.png}
	\caption{Criação de utilizador.}
	\label{fig:administration-create_user}
\end{figure}

\subsection{Testes}
\label{subsec:aplicacao_de_administracao_testes}

A aplicação de administração foi testada de forma manual, sem implementação de testes automáticos ou outra forma de testes de interface, pois os testes manuais revelaram-se suficientes tendo em conta o tamanho e complexidade da aplicação em questão. Os testes consistiram em utilizar as diferentes funcionalidades que a aplicação tem como a criação de zonas, adicionar sensores às zonas entre outras e ver se o comportamento obtido da aplicação era o que se esperava.
