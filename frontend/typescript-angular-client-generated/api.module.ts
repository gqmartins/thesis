import { NgModule, ModuleWithProviders, SkipSelf, Optional } from '@angular/core';
import { Configuration } from './configuration';
import { HttpClient } from '@angular/common/http';


import { AuthenticationService } from './api/authentication.service';
import { OutboundMessageService } from './api/outboundMessage.service';
import { OutboundMessageTypeService } from './api/outboundMessageType.service';
import { RoleService } from './api/role.service';
import { SensorService } from './api/sensor.service';
import { SensorTypeService } from './api/sensorType.service';
import { SwaggerControllerService } from './api/swaggerController.service';
import { UserService } from './api/user.service';
import { ZoneService } from './api/zone.service';

@NgModule({
  imports:      [],
  declarations: [],
  exports:      [],
  providers: [
    AuthenticationService,
    OutboundMessageService,
    OutboundMessageTypeService,
    RoleService,
    SensorService,
    SensorTypeService,
    SwaggerControllerService,
    UserService,
    ZoneService ]
})
export class ApiModule {
    public static forRoot(configurationFactory: () => Configuration): ModuleWithProviders {
        return {
            ngModule: ApiModule,
            providers: [ { provide: Configuration, useFactory: configurationFactory } ]
        };
    }

    constructor( @Optional() @SkipSelf() parentModule: ApiModule,
                 @Optional() http: HttpClient) {
        if (parentModule) {
            throw new Error('ApiModule is already loaded. Import in your base AppModule only.');
        }
        if (!http) {
            throw new Error('You need to import the HttpClientModule in your AppModule! \n' +
            'See also https://github.com/angular/angular/issues/20575');
        }
    }
}
