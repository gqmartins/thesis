/**
 * SmartparkingIoT Admin API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional }                      from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams,
         HttpResponse, HttpEvent }                           from '@angular/common/http';
import { CustomHttpUrlEncodingCodec }                        from '../encoder';

import { Observable }                                        from 'rxjs/Observable';

import { ErrorResponse } from '../model/errorResponse';
import { SensorTypeRequest } from '../model/sensorTypeRequest';
import { SensorTypeResponse } from '../model/sensorTypeResponse';

import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';


@Injectable()
export class SensorTypeService {

    protected basePath = 'https://localhost:8080/smart-parking-iot-api';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
            this.basePath = basePath || configuration.basePath || this.basePath;
        }
    }

    /**
     * @param consumes string[] mime-types
     * @return true: consumes contains 'multipart/form-data', false: otherwise
     */
    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (const consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    /**
     * adds a sensor type to the system
     * adds a sensor type to the system
     * @param body body
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public createSensorType(body: SensorTypeRequest, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public createSensorType(body: SensorTypeRequest, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public createSensorType(body: SensorTypeRequest, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public createSensorType(body: SensorTypeRequest, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (body === null || body === undefined) {
            throw new Error('Required parameter body was null or undefined when calling createSensorType.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.post<any>(`${this.basePath}/sensor-type`,
            body,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * deletes the sensor type with the specified sensor-type-id
     * delete the sensor type with the specified sensor-type-id
     * @param sensorTypeId sensor-type-id
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public deleteSensorType(sensorTypeId: number, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public deleteSensorType(sensorTypeId: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public deleteSensorType(sensorTypeId: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public deleteSensorType(sensorTypeId: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (sensorTypeId === null || sensorTypeId === undefined) {
            throw new Error('Required parameter sensorTypeId was null or undefined when calling deleteSensorType.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];

        return this.httpClient.delete<any>(`${this.basePath}/sensor-type/${encodeURIComponent(String(sensorTypeId))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * edits the sensor type with the specified sensor-type-id
     * edits the sensor type with the specified sensor-type-id
     * @param sensorTypeId sensor-type-id
     * @param body body
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public editSensorType(sensorTypeId: number, body: SensorTypeRequest, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public editSensorType(sensorTypeId: number, body: SensorTypeRequest, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public editSensorType(sensorTypeId: number, body: SensorTypeRequest, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public editSensorType(sensorTypeId: number, body: SensorTypeRequest, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (sensorTypeId === null || sensorTypeId === undefined) {
            throw new Error('Required parameter sensorTypeId was null or undefined when calling editSensorType.');
        }

        if (body === null || body === undefined) {
            throw new Error('Required parameter body was null or undefined when calling editSensorType.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.put<any>(`${this.basePath}/sensor-type/${encodeURIComponent(String(sensorTypeId))}`,
            body,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * returns sensor type with the specified sensor-type-id
     * returns sensor type with the specified sensor-type-id
     * @param sensorTypeId sensor-type-id
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public readSensorType(sensorTypeId: number, observe?: 'body', reportProgress?: boolean): Observable<SensorTypeResponse>;
    public readSensorType(sensorTypeId: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<SensorTypeResponse>>;
    public readSensorType(sensorTypeId: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<SensorTypeResponse>>;
    public readSensorType(sensorTypeId: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (sensorTypeId === null || sensorTypeId === undefined) {
            throw new Error('Required parameter sensorTypeId was null or undefined when calling readSensorType.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];

        return this.httpClient.get<SensorTypeResponse>(`${this.basePath}/sensor-type/${encodeURIComponent(String(sensorTypeId))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * returns types of sensors added to the system
     * returns all the sensor types that were added to the system, ordered by creation date
     * @param page page
     * @param size size
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public readSensorTypes(page?: number, size?: number, observe?: 'body', reportProgress?: boolean): Observable<Array<SensorTypeResponse>>;
    public readSensorTypes(page?: number, size?: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<SensorTypeResponse>>>;
    public readSensorTypes(page?: number, size?: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<SensorTypeResponse>>>;
    public readSensorTypes(page?: number, size?: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {



        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (page !== undefined && page !== null) {
            queryParameters = queryParameters.set('page', <any>page);
        }
        if (size !== undefined && size !== null) {
            queryParameters = queryParameters.set('size', <any>size);
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];

        return this.httpClient.get<Array<SensorTypeResponse>>(`${this.basePath}/sensor-type`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

}
