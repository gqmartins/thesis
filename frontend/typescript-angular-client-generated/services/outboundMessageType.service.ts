/**
 * SmartparkingIoT Admin API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional }                      from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams,
         HttpResponse, HttpEvent }                           from '@angular/common/http';
import { CustomHttpUrlEncodingCodec }                        from '../encoder';

import { Observable }                                        from 'rxjs/Observable';

import { ErrorResponse } from '../model/errorResponse';
import { ListItemResponse } from '../model/listItemResponse';
import { OutboundMessageTypeRequest } from '../model/outboundMessageTypeRequest';
import { OutboundMessageTypeResponse } from '../model/outboundMessageTypeResponse';

import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';


@Injectable()
export class OutboundMessageTypeService {

    protected basePath = 'https://localhost:8080/smart-parking-iot-api';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
            this.basePath = basePath || configuration.basePath || this.basePath;
        }
    }

    /**
     * @param consumes string[] mime-types
     * @return true: consumes contains 'multipart/form-data', false: otherwise
     */
    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (const consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    /**
     * adds a outbound message type to the system
     * adds a outbound message type to the system
     * @param sensorTypeId sensor-type-id
     * @param body body
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public createOutboundMessageType(sensorTypeId: number, body: OutboundMessageTypeRequest, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public createOutboundMessageType(sensorTypeId: number, body: OutboundMessageTypeRequest, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public createOutboundMessageType(sensorTypeId: number, body: OutboundMessageTypeRequest, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public createOutboundMessageType(sensorTypeId: number, body: OutboundMessageTypeRequest, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (sensorTypeId === null || sensorTypeId === undefined) {
            throw new Error('Required parameter sensorTypeId was null or undefined when calling createOutboundMessageType.');
        }

        if (body === null || body === undefined) {
            throw new Error('Required parameter body was null or undefined when calling createOutboundMessageType.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.post<any>(`${this.basePath}/sensor-type/${encodeURIComponent(String(sensorTypeId))}/outbound-message-type`,
            body,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * deletes the outbound message type with the specified outboundMessageType-id
     * delete the outbound message type with the specified outbound-message-type-id
     * @param sensorTypeId sensor-type-id
     * @param outboundMessageTypeId outbound-message-type-id
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public deleteOutboundMessageType(sensorTypeId: number, outboundMessageTypeId: number, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public deleteOutboundMessageType(sensorTypeId: number, outboundMessageTypeId: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public deleteOutboundMessageType(sensorTypeId: number, outboundMessageTypeId: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public deleteOutboundMessageType(sensorTypeId: number, outboundMessageTypeId: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (sensorTypeId === null || sensorTypeId === undefined) {
            throw new Error('Required parameter sensorTypeId was null or undefined when calling deleteOutboundMessageType.');
        }

        if (outboundMessageTypeId === null || outboundMessageTypeId === undefined) {
            throw new Error('Required parameter outboundMessageTypeId was null or undefined when calling deleteOutboundMessageType.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];

        return this.httpClient.delete<any>(`${this.basePath}/sensor-type/${encodeURIComponent(String(sensorTypeId))}/outbound-message-type/${encodeURIComponent(String(outboundMessageTypeId))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * edits the outbound message type with the specified outbound-message-type-id
     * edits the outbound message type with the specified outbound-message-type-id
     * @param sensorTypeId sensor-type-id
     * @param outboundMessageTypeId outbound-message-type-id
     * @param body body
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public editOutboundMessageType(sensorTypeId: number, outboundMessageTypeId: number, body: OutboundMessageTypeRequest, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public editOutboundMessageType(sensorTypeId: number, outboundMessageTypeId: number, body: OutboundMessageTypeRequest, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public editOutboundMessageType(sensorTypeId: number, outboundMessageTypeId: number, body: OutboundMessageTypeRequest, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public editOutboundMessageType(sensorTypeId: number, outboundMessageTypeId: number, body: OutboundMessageTypeRequest, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (sensorTypeId === null || sensorTypeId === undefined) {
            throw new Error('Required parameter sensorTypeId was null or undefined when calling editOutboundMessageType.');
        }

        if (outboundMessageTypeId === null || outboundMessageTypeId === undefined) {
            throw new Error('Required parameter outboundMessageTypeId was null or undefined when calling editOutboundMessageType.');
        }

        if (body === null || body === undefined) {
            throw new Error('Required parameter body was null or undefined when calling editOutboundMessageType.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.put<any>(`${this.basePath}/sensor-type/${encodeURIComponent(String(sensorTypeId))}/outbound-message-type/${encodeURIComponent(String(outboundMessageTypeId))}`,
            body,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * returns outbound message type with the specified outbound-message-type-id
     * returns outbound message type with the specified outbound-message-type-id
     * @param sensorTypeId sensor-type-id
     * @param outboundMessageTypeId outbound-message-type-id
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public readOutboundMessageType(sensorTypeId: number, outboundMessageTypeId: number, observe?: 'body', reportProgress?: boolean): Observable<OutboundMessageTypeResponse>;
    public readOutboundMessageType(sensorTypeId: number, outboundMessageTypeId: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<OutboundMessageTypeResponse>>;
    public readOutboundMessageType(sensorTypeId: number, outboundMessageTypeId: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<OutboundMessageTypeResponse>>;
    public readOutboundMessageType(sensorTypeId: number, outboundMessageTypeId: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (sensorTypeId === null || sensorTypeId === undefined) {
            throw new Error('Required parameter sensorTypeId was null or undefined when calling readOutboundMessageType.');
        }

        if (outboundMessageTypeId === null || outboundMessageTypeId === undefined) {
            throw new Error('Required parameter outboundMessageTypeId was null or undefined when calling readOutboundMessageType.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];

        return this.httpClient.get<OutboundMessageTypeResponse>(`${this.basePath}/sensor-type/${encodeURIComponent(String(sensorTypeId))}/outbound-message-type/${encodeURIComponent(String(outboundMessageTypeId))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * returns outbound message types added to the system
     * returns all the outbound message types that were added to the system, ordered by creation date
     * @param sensorTypeId sensor-type-id
     * @param page page
     * @param size size
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public readOutboundMessageTypes(sensorTypeId: number, page?: number, size?: number, observe?: 'body', reportProgress?: boolean): Observable<Array<ListItemResponse>>;
    public readOutboundMessageTypes(sensorTypeId: number, page?: number, size?: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<ListItemResponse>>>;
    public readOutboundMessageTypes(sensorTypeId: number, page?: number, size?: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<ListItemResponse>>>;
    public readOutboundMessageTypes(sensorTypeId: number, page?: number, size?: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (sensorTypeId === null || sensorTypeId === undefined) {
            throw new Error('Required parameter sensorTypeId was null or undefined when calling readOutboundMessageTypes.');
        }



        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (page !== undefined && page !== null) {
            queryParameters = queryParameters.set('page', <any>page);
        }
        if (size !== undefined && size !== null) {
            queryParameters = queryParameters.set('size', <any>size);
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];

        return this.httpClient.get<Array<ListItemResponse>>(`${this.basePath}/sensor-type/${encodeURIComponent(String(sensorTypeId))}/outbound-message-type`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

}
