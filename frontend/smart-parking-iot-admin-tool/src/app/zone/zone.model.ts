export interface Zone {
  id?: number;
  name?: string;
  maxCount?: number;
  currentCount?: number;
  occupiedPercentage?: number;
  perimeter?: any;
  created?: Date;
  modified?: Date;
}