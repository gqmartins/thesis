export interface ZonePrediction {
  date: string;
  percentage: number;
}