import { Component, OnInit, Input, Output, EventEmitter, ComponentFactoryResolver } from '@angular/core';
import { Zone } from '../zone.model';
import { ApiErrorResponse } from 'src/app/shared/base-api-service/response/api-error-response.model';
import { BaseComponent } from 'src/app/shared/base-component/base-component.component';
import { DomainZoneService } from '../zone.service';
import { ApiBodyResponse } from 'src/app/shared/base-api-service/response/api-body-response.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-zone-edit',
  templateUrl: './zone-edit.component.html',
  styleUrls: ['./zone-edit.component.css']
})
export class ZoneEditComponent extends BaseComponent implements OnInit {
  @Input() zoneId: number;
  @Input() showDelete: boolean;
  @Output() edit = new EventEmitter<void>();
  @Output() delete = new EventEmitter<number>();
  @Output() error = new EventEmitter<ApiErrorResponse>();
  private zone: Zone;
  private showDialog = false;
  
  constructor(private zoneService: DomainZoneService, protected componentResolver: ComponentFactoryResolver) {
    super(componentResolver);
   }

  ngOnInit() {
    this.zoneService.readZone(this.zoneId).subscribe(response => {
      this.zone = (<ApiBodyResponse>response).body;
      this.showDialog = true;
    });
  }

  onSubmit(form: NgForm) {
    const newName = form.value.name;
    const newMaxCount = form.value.maxCount;
    const newZone: Zone = this.createFromEditForm(this.zone.id, newName, newMaxCount, this.zone.perimeter);
    this.zoneService.editZone(newZone).subscribe(this.handleSubscription(this.edit, this.error));
  }

  onDelete() {
    this.delete.emit(this.zoneId);
  }

  onCancel() {
    this.edit.emit();
  }

  private createFromEditForm(id: number, name: string, maxCount: number, perimeter: string): Zone {
    return {id: id, name: name, maxCount: maxCount, perimeter: perimeter};
  }
}
