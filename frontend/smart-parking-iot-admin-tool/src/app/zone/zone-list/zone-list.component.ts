import { Component, OnInit, ComponentFactoryResolver, ViewChild, EventEmitter } from '@angular/core';
import { Zone } from '../zone.model';
import { BaseComponent } from 'src/app/shared/base-component/base-component.component';
import { DomainZoneService } from '../zone.service';
import { PlaceholderDirective } from 'src/app/shared/placeholder/placeholder.directive';
import { ZoneEditComponent } from '../zone-edit/zone-edit.component';
import { ListItem } from 'src/app/list/list-item.model';

@Component({
  selector: 'app-zone-list',
  templateUrl: './zone-list.component.html',
  styleUrls: ['./zone-list.component.css']
})
export class ZoneListComponent extends BaseComponent implements OnInit {
  private zones: Array<Zone>;
  private editComplete = new EventEmitter<ListItem>();
  private deleteComplete  = new EventEmitter<number>();
  @ViewChild(PlaceholderDirective, {static: false}) private placeholder: PlaceholderDirective;

  constructor(private zoneService: DomainZoneService, protected componentResolver: ComponentFactoryResolver) {
    super(componentResolver);
  }

  ngOnInit() {
    this.zoneService.listZones().subscribe(this.handleServiceRequest(this.placeholder, response => {
      this.zones = response;
    }));
  }

  onEdit(id: number) {
    const componentFactory = this.componentResolver.resolveComponentFactory(ZoneEditComponent);
    const hostViewContainerRef = this.placeholder.viewContainerRef;
    hostViewContainerRef.clear();
    const zoneEditComponentRef = hostViewContainerRef.createComponent(componentFactory);
    zoneEditComponentRef.instance.zoneId = id;
    zoneEditComponentRef.instance.showDelete = false;
    // in this case cancel and edit run the same code
    this.placeholderSuccessSubscription = zoneEditComponentRef.instance.edit.subscribe(() => {
      this.zoneService.readZone(id).subscribe(this.handleServiceRequest(this.placeholder, response => {
        this.editComplete.emit(response);
      }));
      this.placeholderSuccessSubscription.unsubscribe();
      hostViewContainerRef.clear();
    });
    this.placeholderErrorSubscription = zoneEditComponentRef.instance.error.subscribe(response => {
      this.handleError(response, this.placeholder);
    });
  }

  onDelete(id: number) {
    this.zoneService.deleteZone(id).subscribe(this.handleServiceRequest(this.placeholder, response => {
      this.deleteComplete.emit(id);
    }));
  }
}
