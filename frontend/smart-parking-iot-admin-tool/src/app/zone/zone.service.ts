import { Injectable } from '@angular/core';
import { Zone } from './zone.model';
import { BaseApiService } from '../shared/base-api-service/base-api.service';
import { ZoneService, ZoneRequest, ZoneResponse, ListItemResponse, PredictionResponse } from '../api';
import { BaseApiResponse } from '../shared/base-api-service/response/base-api-response';
import { Observable } from 'rxjs';
import { ZonePrediction } from './zone-prediction';

@Injectable({providedIn: 'root'})
export class DomainZoneService {

  constructor(private apiZoneService: ZoneService, private baseApiService: BaseApiService) {}

  public createZone(zone: Zone): Observable<BaseApiResponse> {
    const body: ZoneRequest = {
      name: zone.name,
      maxCount: zone.maxCount,
      perimeter: JSON.parse(zone.perimeter)
    };
    const request = this.apiZoneService.createZone(body, 'response');
    return this.baseApiService.handlePostRequest(request);
  }

  public listZones() {
    const request = this.apiZoneService.readZones(undefined, undefined, 'response');
    return this.baseApiService.handleGetRequest(request, this.zoneListMapper);
  }

  public readZone(id: number) {
    const request = this.apiZoneService.readZone(id, 'response');
    return this.baseApiService.handleGetRequest(request, DomainZoneService.createFromReadApiRequest);
  }

  public editZone(zone: Zone) {
    const body: ZoneRequest = {
      name: zone.name,
      maxCount: zone.maxCount,
      perimeter: zone.perimeter
    };
    const request = this.apiZoneService.editZone(zone.id, body, 'response');
    return this.baseApiService.handlePutRequest(request);
  }

  public deleteZone(id: number) {
    const request = this.apiZoneService.deleteZone(id, 'response');
    return this.baseApiService.handleDeleteRequest(request);
  }

  public listZonePredictions(zoneId: number, startDate: string, endDate: string, interval: number) {
    const request = this.apiZoneService.readZonePredictions(zoneId, startDate, endDate, interval, 'response');
    return this.baseApiService.handleGetRequest(request, this.zonePredictionListMapper);
  }

  private zoneListMapper(zoneList: ListItemResponse[]): Zone[] {
    return zoneList.map(DomainZoneService.createFromListApiRequest);
  }

  private zonePredictionListMapper(zonePredictionList: Array<PredictionResponse>): Array<ZonePrediction> {
    return zonePredictionList.map(prediction => {return { date: prediction.date, percentage: prediction.percentage }});
  }

  private static createFromListApiRequest(zone: ListItemResponse): Zone {
    return {id: zone.id, name: zone.name, created: new Date(zone.created), modified: new Date(zone.modified)}
  }

  private static createFromReadApiRequest(zone: ZoneResponse): Zone {
    return {id: zone.id, name: zone.name, maxCount: zone.maxCount, currentCount: zone.currentCount, 
      occupiedPercentage: zone.occupiedPercentage, perimeter: zone.perimeter, created: new Date(zone.created), 
      modified: new Date(zone.modified)};
  }
}