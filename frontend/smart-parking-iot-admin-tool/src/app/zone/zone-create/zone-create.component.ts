import { Component, OnInit, Input, Output, EventEmitter, ComponentFactoryResolver } from '@angular/core';
import { DomainZoneService } from '../zone.service';
import { NgForm } from '@angular/forms';
import { Zone } from '../zone.model';
import { ApiErrorResponse } from 'src/app/shared/base-api-service/response/api-error-response.model';
import { BaseComponent } from 'src/app/shared/base-component/base-component.component';

@Component({
  selector: 'app-zone-create',
  templateUrl: './zone-create.component.html',
  styleUrls: ['./zone-create.component.css']
})
export class ZoneCreateComponent extends BaseComponent implements OnInit {
  @Input() zoneArea: string;
  @Output() success = new EventEmitter<void>();
  @Output() cancel = new EventEmitter<void>();
  @Output() error = new EventEmitter<ApiErrorResponse>();

  constructor(private zoneService: DomainZoneService, protected componentResolver: ComponentFactoryResolver) {
    super(componentResolver);
  }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    const name = form.value.name;
    const maxCount = form.value.maxCount;
    const newZone: Zone = this.createFromForm(name, maxCount, this.zoneArea);
    this.zoneService.createZone(newZone).subscribe(this.handleSubscription(this.success, this.error));
  }
  
  onCancel() {
    this.cancel.emit();
  }

  private createFromForm(name: string, maxCount: number, perimeter: string): Zone {
    return {name: name, maxCount: maxCount, perimeter: perimeter};
  }
}
