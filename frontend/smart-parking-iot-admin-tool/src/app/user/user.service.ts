import { UserService, ListItemResponse, UserResponse } from '../api';
import { BaseApiService } from '../shared/base-api-service/base-api.service';
import { Injectable } from '@angular/core';
import { User } from './user.model';
import { UserRole } from './user-role.model';

@Injectable({providedIn: 'root'})
export class DomainUserService {

  constructor(private apiUserService: UserService, private baseApiService: BaseApiService) {}

  public createUser(user: User) {
    const body = {
      name: user.name,
      password: user.password,
      role: user.role.id,
      username: user.username
    };
    const request = this.apiUserService.createUser(body, 'response');
    return this.baseApiService.handlePostRequest(request);
  }

  public listUsers() {
    const request = this.apiUserService.readUsers(undefined, undefined, 'response');
    return this.baseApiService.handleGetRequest(request, this.userListMapper);
  }

  public readUser(userId: number) {
    const request = this.apiUserService.readUser(userId, 'response');
    return this.baseApiService.handleGetRequest(request, this.createFromReadApiRequest);
  }

  public editUser(user: User) {
    const body = {
      name: user.name,
      password: user.password,
      role: user.role.id,
      username: user.username
    };
    const request = this.apiUserService.editUser(user.id, body, 'response');
    return this.baseApiService.handlePutRequest(request);
  }

  public deleteUser(userId: number) {
    const request = this.apiUserService.deleteUser(userId, 'response');
    return this.baseApiService.handleDeleteRequest(request);
  }

  private userListMapper(userList: ListItemResponse[]): Array<User> {
    return userList.map(user => {
      return {id: user.id, name: user.name, created: new Date(user.created), modified: new Date(user.modified)};
    });
  }

  private createFromReadApiRequest(user: UserResponse): User {
    const role: UserRole = {id: user.role.id, name: user.role.name};
    return {id: user.id, name: user.name, username: user.username, password: user.password, role: role, created: new Date(user.created), modified: new Date(user.modified)};
  }
}