import { UserRole } from './user-role.model';

export interface User {
  id?: number;
  name: string;
  username?: string;
  role?: UserRole;
  password?: string;
  created?: Date;
  modified?: Date;
}