import { Component, OnInit, Input, Output, EventEmitter, ComponentFactoryResolver } from '@angular/core';
import { ApiErrorResponse } from 'src/app/shared/base-api-service/response/api-error-response.model';
import { User } from '../user.model';
import { UserRole } from '../user-role.model';
import { DomainUserService } from '../user.service';
import { DomainUserRoleService } from '../user-role.service';
import { BaseComponent } from 'src/app/shared/base-component/base-component.component';
import { ApiBodyResponse } from 'src/app/shared/base-api-service/response/api-body-response.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent extends BaseComponent implements OnInit {
  @Input() userId: number;
  @Output() edit = new EventEmitter<void>();
  @Output() cancel = new EventEmitter<void>();
  @Output() error = new EventEmitter<ApiErrorResponse>();
  private user: User;
  private roles: Array<UserRole>;
  private selectedRole: UserRole;
  private showDialog: boolean = false;

  constructor(private userService: DomainUserService, private roleService: DomainUserRoleService, protected componentResolver: ComponentFactoryResolver) {
    super(componentResolver);
  }

  ngOnInit() {
    this.userService.readUser(this.userId).subscribe(response => {
      this.user = (<ApiBodyResponse>response).body;
      this.roleService.listRoles().subscribe(response => {
        this.roles = (<ApiBodyResponse>response).body;
        this.roleService.readRole(this.user.role.id).subscribe(response => {
          this.selectedRole = (<ApiBodyResponse>response).body;
          this.showDialog = true;
        });
      });
    });
  }

  onSubmit(form: NgForm) {
    const password = form.value.password;
    const newUser = this.createFromForm(this.user.id, this.user.name, this.user.username, password, this.selectedRole.id);
    this.userService.editUser(newUser).subscribe(this.handleSubscription(this.edit, this.error));
  }

  onCancel() {
    this.cancel.emit();
  }
  
  private createFromForm(id:number, name: string, username: string, password: string, role: number) {
    return {id: id, name: name, username: username, password: password, role: {id: role}};
  }
}
