import { Component, OnInit, ViewChild, EventEmitter, ComponentFactoryResolver } from '@angular/core';
import { User } from '../user.model';
import { ListItem } from 'src/app/list/list-item.model';
import { PlaceholderDirective } from 'src/app/shared/placeholder/placeholder.directive';
import { DomainUserService } from '../user.service';
import { BaseComponent } from 'src/app/shared/base-component/base-component.component';
import { UserCreateComponent } from '../user-create/user-create.component';
import { UserEditComponent } from '../user-edit/user-edit.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent extends BaseComponent implements OnInit {
  private users: Array<User>;
  private editComplete = new EventEmitter<ListItem>();
  private deleteComplete  = new EventEmitter<number>();
  @ViewChild(PlaceholderDirective, {static: false}) private placeholder: PlaceholderDirective;
  
  constructor(private userService: DomainUserService, protected componentResolver: ComponentFactoryResolver) {
    super(componentResolver);
  }

  ngOnInit() {
    this.userService.listUsers().subscribe(this.handleServiceRequest(this.placeholder, response => {
      this.users = response;
    }));
  }

  onEdit(id: number) {
    const componentFactory = this.componentResolver.resolveComponentFactory(UserEditComponent);
    const hostViewContainerRef = this.placeholder.viewContainerRef;
    hostViewContainerRef.clear();
    const component = hostViewContainerRef.createComponent(componentFactory);
    component.instance.userId = id;
    component.changeDetectorRef.detectChanges();
    // in this case cancel and edit run the same code
    this.placeholderSuccessSubscription = component.instance.edit.subscribe(() => {
      this.userService.readUser(id).subscribe(this.handleServiceRequest(this.placeholder, response => {
        this.editComplete.emit(response);
      }));
      this.placeholderSuccessSubscription.unsubscribe();
      hostViewContainerRef.clear();
    });
    this.placeholderCancelSubscription = component.instance.cancel.subscribe(() => {
      this.placeholderCancelSubscription.unsubscribe();
      hostViewContainerRef.clear();
    })
    this.placeholderErrorSubscription = component.instance.error.subscribe(response => {
      this.handleError(response, this.placeholder);
    });
  }

  onDelete(id: number) {
    this.userService.deleteUser(id).subscribe(this.handleServiceRequest(this.placeholder, response => {
      this.deleteComplete.emit(id);
    }));
  }

  onCreate() {
    const componentFactory = this.componentResolver.resolveComponentFactory(UserCreateComponent);
    const hostViewContainerRef = this.placeholder.viewContainerRef;
    hostViewContainerRef.clear();
    const component = hostViewContainerRef.createComponent(componentFactory);
    component.changeDetectorRef.detectChanges();
    // in this case cancel and edit run the same code
    this.placeholderSuccessSubscription = component.instance.success.subscribe(location => {
      const id = +location.substring(location.lastIndexOf('/') + 1);
      this.userService.readUser(id).subscribe(this.handleServiceRequest(this.placeholder, response => {
        this.users.push(response);
      }));
      this.placeholderSuccessSubscription.unsubscribe();
      hostViewContainerRef.clear();
    });
    this.placeholderCancelSubscription = component.instance.cancel.subscribe(() => {
      this.placeholderCancelSubscription.unsubscribe();
      hostViewContainerRef.clear();
    })
    this.placeholderErrorSubscription = component.instance.error.subscribe(response => {
      this.handleError(response, this.placeholder);
    });
  }
}
