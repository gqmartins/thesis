import { Injectable } from '@angular/core';
import { RoleService, RoleResponse } from '../api';
import { BaseApiService } from '../shared/base-api-service/base-api.service';
import { UserRole } from './user-role.model';

@Injectable({providedIn: 'root'})
export class DomainUserRoleService {
  
  constructor(private apiRoleService: RoleService, private baseApiService: BaseApiService) {}

  public listRoles() {
    const request = this.apiRoleService.readRoles(undefined, undefined, 'response');
    return this.baseApiService.handleGetRequest(request, this.roleListMapper);
  }

  public readRole(roleId: number) {
    const request = this.apiRoleService.readRole(roleId, 'response');
    return this.baseApiService.handleGetRequest(request, this.roleMapper);
  }

  private roleListMapper(roles: Array<RoleResponse>): Array<UserRole> {
    return roles.map(role => {return { id: role.id, name: role.name }});
  }

  private roleMapper(role): UserRole {
    return { id: role.id, name: role.name };
  }
}