import { Component, OnInit, Output, EventEmitter, ComponentFactoryResolver } from '@angular/core';
import { User } from '../user.model';
import { ApiErrorResponse } from 'src/app/shared/base-api-service/response/api-error-response.model';
import { UserRole } from '../user-role.model';
import { DomainUserService } from '../user.service';
import { DomainUserRoleService } from '../user-role.service';
import { BaseComponent } from 'src/app/shared/base-component/base-component.component';
import { ApiBodyResponse } from 'src/app/shared/base-api-service/response/api-body-response.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent extends BaseComponent implements OnInit {
  @Output() success = new EventEmitter<User>();
  @Output() cancel = new EventEmitter<void>();
  @Output() error = new EventEmitter<ApiErrorResponse>();
  private roles: Array<UserRole>;
  private selectedRole: UserRole;
  private showDialog: boolean = false;
  
  constructor(private userService: DomainUserService, private roleService: DomainUserRoleService, protected componentResolver: ComponentFactoryResolver) {
    super(componentResolver);
  }

  ngOnInit() {
    this.roleService.listRoles().subscribe(response => {
      this.roles = (<ApiBodyResponse>response).body;
      this.roleService.readRole(this.roles[0].id).subscribe(response => {
        this.selectedRole = (<ApiBodyResponse>response).body;
        this.showDialog = true;
      });
    });
  }

  onSubmit(form: NgForm) {
    const name = form.value.name;
    const username = form.value.username;
    const password = form.value.password;
    const newUser = this.createFromForm(name, username, password, this.selectedRole.id);
    this.userService.createUser(newUser).subscribe(this.handleSubscription(this.success, this.error));
  }

  onCancel() {
    this.cancel.emit();
  }
  
  private createFromForm(name: string, username: string, password: string, role: number) {
    return {name: name, username: username, password: password, role: {id: role}};
  }
}
