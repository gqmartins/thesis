import { Component, OnInit } from '@angular/core';
import { BoardItem } from './board-item/board-item.model';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {
  private boardItems: BoardItem[] = [];

  constructor() { }

  ngOnInit() {
    this.boardItems = [
      new BoardItem('map', 'Map'),
      new BoardItem('zones', 'Zones'),
      new BoardItem('sensors', 'Sensors'),
      new BoardItem('sensor-types', 'Sensor types'),
      new BoardItem('outbound-message-types', 'Outbound message types'),
      new BoardItem('dashboard', 'Dashboard'),
      new BoardItem('users', 'Users'),
    ];
  }
}
