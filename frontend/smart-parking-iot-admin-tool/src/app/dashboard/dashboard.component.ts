import { Component, OnInit, ViewChild, ComponentFactoryResolver, EventEmitter } from '@angular/core';
import { DomainZoneService } from '../zone/zone.service';
import { Zone } from '../zone/zone.model';
import { SelectListItem } from './select-list-item.model';
import { BaseComponent } from '../shared/base-component/base-component.component';
import { PlaceholderDirective } from '../shared/placeholder/placeholder.directive';
import { NgForm } from '@angular/forms';
import { ZonePrediction } from '../zone/zone-prediction';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent extends BaseComponent implements OnInit {
  private zones: Array<Zone>;
  private days: Array<SelectListItem> = [];
  private months: Array<SelectListItem> = [];
  private beginTimes: Array<SelectListItem> = [];
  private endTimes: Array<SelectListItem> = [];
  private intervalTimes: Array<SelectListItem> = [];
  private selectedZone: Zone;
  private selectedDay: SelectListItem = {id: 1, name: 'Monday'};
  private selectedMonth: SelectListItem = {id: 1, name: 'January'};
  private selectedBeginTime: SelectListItem = {id: 0, name: '0'};
  private selectedEndTime: SelectListItem = {id: 0, name: '0'};
  private selectedIntervalTime: SelectListItem = {id: 20, name: '20'};
  private showPage: boolean = false;
  private searched = false;
  private zonePredictions;
  private newSearch = new EventEmitter<any>();
  @ViewChild(PlaceholderDirective, {static: false}) private placeholder: PlaceholderDirective;

  constructor(private zoneService: DomainZoneService, protected componentResolver: ComponentFactoryResolver) {
    super(componentResolver);
  }

  ngOnInit() {
    this.zoneService.listZones().subscribe(this.handleServiceRequest(this.placeholder, response => {
      this.zones = response;
      this.zoneService.readZone(this.zones[0].id).subscribe(this.handleServiceRequest(this.placeholder, response => {
        this.selectedZone = response;
        this.fillListItemsWithNumbers(this.days, 1, 31);
        this.fillListItemsWithNames(this.months, ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']);
        this.fillListItemsWithNumbers(this.beginTimes, 0, 23);
        this.fillListItemsWithNumbers(this.endTimes, 0, 23);
        this.fillIntervals();
        this.showPage = true;
      }));
    }));
  }

  onSubmit(form: NgForm) {
    const startDate = this.buildDate(this.selectedBeginTime.id, 0);
    const endDate = this.buildEndDate();
    this.zoneService.listZonePredictions(this.selectedZone.id, startDate, endDate, this.selectedIntervalTime.id).subscribe(this.handleServiceRequest(this.placeholder, response => {
      const predictions = this.formatPredictions(response);
      this.zonePredictions = predictions;
      this.searched = true;
      this.newSearch.emit(this.zonePredictions);
    }));
  }

  private buildEndDate() {
    const endHour = this.selectedEndTime.id === 0 ? 23 : this.selectedEndTime.id - 1;
    return this.buildDate(endHour, 59);
  }

  private fillListItemsWithNames(listItems: Array<SelectListItem>, names: Array<string>) {
    for(let i = 0; i < names.length; ++i) {
      listItems.push({ id: i, name: names[i] });
    }
  }

  private fillListItemsWithNumbers(listItems: Array<SelectListItem>, begin: number, end: number) {
    for(let i = 0; begin <= end; ++i) {
      const name = begin < 10 ? '0' + begin.toString() : begin.toString();
      listItems.push({ id: i, name: name});
      ++begin;
    }
  }

  private fillIntervals() {
    const intervals = [20, 30, 60];
    for(let i = 0; i < intervals.length; ++i) {
      this.intervalTimes.push({ id: intervals[i], name: intervals[i].toString()});
    }
  }

  private buildDate(selectedHour: number, selectedMinute: number) {
    const currentYear = new Date().getFullYear();
    const month = this.getDatePart(this.selectedMonth.id);
    const day = this.getDatePart(this.selectedDay.id);
    const hour = this.getDatePart(selectedHour);
    const minute = this.getDatePart(selectedMinute);
    return currentYear + '-' + month + '-' + day + 'T' + hour + ':' + minute;
  }

  private getDatePart(part: number) {
    return part < 10 ? '0' + part : part;
  }

  private formatPredictions(predictions: Array<ZonePrediction>) {
    return predictions.map(prediction => {
      const time = prediction.date.substring(prediction.date.indexOf(' ') + 1, prediction.date.lastIndexOf(':'));
      prediction.date = time;
      return prediction;
    });
  }
}
