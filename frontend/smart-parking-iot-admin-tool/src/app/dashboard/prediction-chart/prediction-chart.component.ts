import { Component, OnInit, Input, EventEmitter } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-prediction-chart',
  templateUrl: './prediction-chart.component.html',
  styleUrls: ['./prediction-chart.component.css']
})
export class PredictionChartComponent implements OnInit {
  @Input() predictions;
  @Input() newSearch: EventEmitter<any>;
  private Highcharts: typeof Highcharts = Highcharts;
  private showGraph: boolean = false;
  private chartOptions: Highcharts.Options;
  
  constructor() { }

  ngOnInit() {
    const times = this.predictions.map(prediction => prediction.date);
    const percentages = this.predictions.map(prediction => prediction.percentage);
    const searchEventEmitter = this.newSearch;
    this.chartOptions = {
      chart: {
        events: {
          load() {
            searchEventEmitter.subscribe(predictions => {
              const newTimes = predictions.map(prediction => prediction.date);
              const newPercentages = predictions.map(prediction => prediction.percentage);
              this.xAxis[0].setCategories(newTimes);
              this.series[0].setData(newPercentages);
            })
          }
        }
      },
      title: {text: ''},
      xAxis: {categories: times},
      series: [
        {data: percentages, type: 'line', showInLegend: false}
      ]
    };
    this.showGraph = true;
  }


}
