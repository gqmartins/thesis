import { Component, OnInit, ComponentFactoryResolver, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { tileLayer, latLng, Map, marker, Layer, Marker, polygon, LatLng, Polygon} from 'leaflet';
import { PlaceholderDirective } from '../shared/placeholder/placeholder.directive';
import { LayerItem } from './layer-item.model';
import { Zone } from '../zone/zone.model';
import { BaseComponent } from '../shared/base-component/base-component.component';
import { DomainZoneService } from '../zone/zone.service';
import { ZoneCreateComponent } from '../zone/zone-create/zone-create.component';
import { ZoneEditComponent } from '../zone/zone-edit/zone-edit.component';
import { SensorCreateComponent } from '../sensor/sensor-create/sensor-create.component';
import { DomainSensorService } from '../sensor/sensor.service';
import { Sensor } from '../sensor/sensor.model';
import { SensorEditComponent } from '../sensor/sensor-edit/sensor-edit.component';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent extends BaseComponent implements OnInit {
  private leafletOptions = {
    layers: [tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' })],
    zoom: 5,
    center: latLng(46.879966, -121.726909)
  };
  private leafletLayers: Layer[] = [];
  private zoneLayers: LayerItem[] = [];
  private sensorLayers: LayerItem[] = [];
  private showButton: boolean = false;
  private editing: boolean = false;
  private creatingSensor = false;
  private zoneAreaPoints: number = 0;
  private mapClick: Function = () => {};
  private finish: Function = (layer: Layer) => {};

  @ViewChild(PlaceholderDirective, {static: false}) private placeholder: PlaceholderDirective;
  
  private GREEN_ZONE_OCCUPATION_LIMIT = 50;
  private YELLOW_ZONE_OCCUPATION_LIMIT = 85;

  constructor(private zoneService: DomainZoneService, private SensorService: DomainSensorService, protected componentResolver: ComponentFactoryResolver, private changeDetector: ChangeDetectorRef) {
    super(componentResolver);
   }

  ngOnInit() {
  }

  // Map Events
  onMapClick(evt) {
    this.mapClick(evt);
  }

  onMapReady(map: Map) {
    this.populateZones();
    this.populateSensors();
    this.showButton = true;
  }

  onCreateZone() {
    this.finish = this.launchCreateZoneComponent;
    const zoneArea = polygon([]);
    zoneArea.setStyle({color: this.getZoneColor(0)});
    this.leafletLayers.push(zoneArea);
    this.mapClickWrapperCreate(evt => {
      zoneArea.addLatLng(evt.latlng);
      let areaPoint = marker(evt.latlng);
      this.leafletLayers.push(areaPoint);
      this.zoneAreaPoints += 1;
    });
  }

  onCreateSensor() {
    this.finish = this.launchCreateSensorComponent;
    this.creatingSensor = true;
    let clicked: boolean = false;
    this.mapClickWrapperCreate(evt => {
      if(clicked) {
        this.leafletLayers.splice(-1,1);
      }
      let newSensor = marker(evt.latlng);
      this.leafletLayers.push(newSensor);
      clicked = true;
    });
  }

  onCancel() {
    this.removeEditMarkers(1);
    this.mapClickWrapperFinishEditing();
  }

  onFinish() {
    this.removeEditMarkers(0);
    this.mapClickWrapperFinishEditing();
    const createdLayer = this.leafletLayers[this.leafletLayers.length - 1];
    this.finish(createdLayer);
  }

  private removeEditMarkers(initialCount: number) {
    let elementsToRemove = initialCount;
    if(this.zoneAreaPoints > 0) {
      elementsToRemove += this.zoneAreaPoints;
      this.zoneAreaPoints = 0;
    }
    let leaftletLayersSize = this.leafletLayers.length;
    this.leafletLayers.splice(leaftletLayersSize - elementsToRemove, leaftletLayersSize);
  }

  private mapClickWrapperCreate(func: Function) {
    this.mapClick = func;
    this.editing = true;
    event.stopPropagation();
  }

  private mapClickWrapperFinishEditing() {
    this.editing = false;
    this.mapClick = () => {};
    event.stopPropagation();
  }

  // Zone related stuff
  private populateZones() {
    this.zoneService.listZones().subscribe(this.handleServiceRequest(this.placeholder, response => {
      response.forEach(zone => {
        this.zoneService.readZone(zone.id).subscribe(this.handleServiceRequest(this.placeholder, response => {
          this.addZoneToLayers(response);
        }));
      });
    }));
  }

  private launchCreateZoneComponent(layer: Layer) {
    const componentFactory = this.componentResolver.resolveComponentFactory(ZoneCreateComponent);
    const hostViewContainerRef = this.placeholder.viewContainerRef;
    hostViewContainerRef.clear();
    const zoneCreateComponentRef = hostViewContainerRef.createComponent(componentFactory);
    const zoneArea = (<Polygon>layer).toGeoJSON().geometry;
    zoneCreateComponentRef.instance.zoneArea = JSON.stringify(zoneArea);
    this.placeholderSuccessSubscription = zoneCreateComponentRef.instance.success.subscribe(() => {
      this.leafletLayers.splice(-1, 1);
      this.repopulateZones();
      this.placeholderSuccessSubscription.unsubscribe();
      hostViewContainerRef.clear();
    });
    this.placeholderCancelSubscription = zoneCreateComponentRef.instance.cancel.subscribe(() => {
      this.leafletLayers.splice(-1, 1);
      this.placeholderCancelSubscription.unsubscribe();
      hostViewContainerRef.clear();
    });
    this.placeholderErrorSubscription = zoneCreateComponentRef.instance.error.subscribe(response => {
      this.handleError(response, this.placeholder);
    });
  }

  private launchEditZoneComponent(id: number) {
    const componentFactory = this.componentResolver.resolveComponentFactory(ZoneEditComponent);
    const hostViewContainerRef = this.placeholder.viewContainerRef;
    hostViewContainerRef.clear();
    const zoneEditComponentRef = hostViewContainerRef.createComponent(componentFactory);
    zoneEditComponentRef.instance.zoneId = id;
    zoneEditComponentRef.instance.showDelete = true;
    // in this case cancel and edit run the same code
    this.placeholderCancelSubscription = zoneEditComponentRef.instance.edit.subscribe(() => {
      this.placeholderCancelSubscription.unsubscribe();
      hostViewContainerRef.clear();
    });
    // in this case sucess is used for delete operation
    this.placeholderSuccessSubscription = zoneEditComponentRef.instance.delete.subscribe((id: number) => {
      this.removeZone(id);
      this.placeholderSuccessSubscription.unsubscribe();
      hostViewContainerRef.clear();
      /*
      this.openConfirmationDialog(hostViewContainerRef, 'Are you sure you want to delete this zone?', () => {
        this.removeZone(id);
        this.placeholderSuccessSubscription.unsubscribe();
        hostViewContainerRef.clear();
      });
      */
    });
    this.placeholderErrorSubscription = zoneEditComponentRef.instance.error.subscribe(response => {
      this.handleError(response, this.placeholder);
    });
  }

  private repopulateZones() {
    let index = 0;
    this.zoneLayers.forEach(layerItem => {
      this.leafletLayers.splice(layerItem.layerIndex - index, 1);
      index += 1;
    })
    this.zoneLayers = [];
    this.populateZones();
  }

  private addZoneToLayers(zone: Zone) {
    const zonePerimeter = this.swapCoordinateArray(zone.perimeter.coordinates[0]);
    const zonePerimeterLayer = polygon(zonePerimeter);
    zonePerimeterLayer.setStyle({color: this.getZoneColor(zone.occupiedPercentage)});
    this.leafletLayers.push(zonePerimeterLayer);
    this.zoneLayers.push(new LayerItem(zone.id, this.leafletLayers.length - 1));
    zonePerimeterLayer.on('click', evt => {
      if(!this.creatingSensor) {
        this.launchEditZoneComponent(zone.id);
      }
    });
    zonePerimeterLayer.on('contextmenu', evt => {
      console.log('zone right click');
    })
  }

  private getZoneColor(occupiedPercentage: number) {
    if(occupiedPercentage > this.YELLOW_ZONE_OCCUPATION_LIMIT) {
      return "red";
    }
    if(occupiedPercentage > this.GREEN_ZONE_OCCUPATION_LIMIT) {
      return "yellow";
    }
    return "green";
  }

  private removeZone(id: any) {
    this.zoneService.deleteZone(id).subscribe(this.handleServiceRequest(this.placeholder, () => {  
      const layerIndex = this.zoneLayers.find(layerItem => layerItem.layerId == id).layerIndex;
      this.leafletLayers.splice(layerIndex, 1);
    }));
  }

  // sensor related stuff
  
  private populateSensors() {
    this.SensorService.listSensors().subscribe(this.handleServiceRequest(this.placeholder, response => {
      response.forEach(sensor => {
        this.SensorService.readSensor(sensor.id).subscribe(this.handleServiceRequest(this.placeholder, response => {
          this.addSensorToLayers(response);
        }));
      });
    }));
  }

  private addSensorToLayers(sensor: Sensor) {
    const sensorLocation = this.swapCoordinates(sensor.location.coordinates);
    const sensorLocationLayer = marker([sensorLocation[0], sensorLocation[1]]);
    this.leafletLayers.push(sensorLocationLayer);
    this.sensorLayers.push(new LayerItem(sensor.id, this.leafletLayers.length - 1));
    sensorLocationLayer.on('click', evt => {
      this.launchEditSensorComponent(sensor.id);
    });
    sensorLocationLayer.on('contextmenu', evt => {
      console.log('right click');
    });
  }

  private launchCreateSensorComponent(layer: Layer) {
    const componentFactory = this.componentResolver.resolveComponentFactory(SensorCreateComponent);
    const hostViewContainerRef = this.placeholder.viewContainerRef;
    hostViewContainerRef.clear();
    const sensorCreateComponentRef = hostViewContainerRef.createComponent(componentFactory);
    const sensorLocation = (<Marker>layer).toGeoJSON().geometry;
    sensorCreateComponentRef.instance.sensorLocation = JSON.stringify(sensorLocation);
    this.placeholderSuccessSubscription = sensorCreateComponentRef.instance.success.subscribe(location => {
      const sensorId = +location.substring(location.lastIndexOf('/') + 1);
      this.sensorLayers.push({layerId: sensorId, layerIndex: this.leafletLayers.length - 1});
      this.leafletLayers[this.leafletLayers.length - 1].on('click', evt => {
        this.launchEditSensorComponent(sensorId);
      });
      this.placeholderSuccessSubscription.unsubscribe();
      hostViewContainerRef.clear();
    });
    this.placeholderCancelSubscription = sensorCreateComponentRef.instance.cancel.subscribe(() => {
      this.leafletLayers.splice(-1, 1);
      this.placeholderCancelSubscription.unsubscribe();
      hostViewContainerRef.clear();
    })
    this.placeholderErrorSubscription = sensorCreateComponentRef.instance.error.subscribe(response => {
      this.handleError(response, this.placeholder);
    });
   this.creatingSensor = false;
  }

  private launchEditSensorComponent(id: number) {
    const componentFactory = this.componentResolver.resolveComponentFactory(SensorEditComponent);
    const hostViewContainerRef = this.placeholder.viewContainerRef;
    hostViewContainerRef.clear();
    const sensorEditComponentRef = hostViewContainerRef.createComponent(componentFactory);
    sensorEditComponentRef.instance.sensorId = id;
    sensorEditComponentRef.instance.showDelete = true;
    sensorEditComponentRef.changeDetectorRef.detectChanges();
    // in this case cancel and edit run the same code
    this.placeholderCancelSubscription = sensorEditComponentRef.instance.edit.subscribe(() => {
      this.placeholderCancelSubscription.unsubscribe();
      hostViewContainerRef.clear();
    });
    // in this case sucess is used for delete operation
    this.placeholderSuccessSubscription = sensorEditComponentRef.instance.delete.subscribe((id: number) => {
      this.removeSensor(id);
      this.placeholderSuccessSubscription.unsubscribe();
      hostViewContainerRef.clear();
    });
    this.placeholderErrorSubscription = sensorEditComponentRef.instance.error.subscribe(response => {
      this.handleError(response, this.placeholder);
    });
  }

  private removeSensor(sensor: number) {
    this.SensorService.deleteSensor(sensor).subscribe(this.handleServiceRequest(this.placeholder, () => {  
      const layerIndex = this.sensorLayers.find(layerItem => layerItem.layerId == sensor).layerIndex;
      this.leafletLayers.splice(layerIndex, 1);
      this.changeDetector.detectChanges();
    }));
  }

  // General methods
  private swapCoordinateArray(coordinates: number[][]) {
    const latLngCoordinates: LatLng[] = [];
    for(let index = 0; index < coordinates.length; ++index) {
      this.swapCoordinates(coordinates[index]);
      const newLatLng = new LatLng(coordinates[index][0], coordinates[index][1]);
      latLngCoordinates.push(newLatLng);
    }
    return latLngCoordinates;
  }

  private swapCoordinates(coordinates: number[]) {
    const firstCoordinate = coordinates[0];
    coordinates[0] = coordinates[1];
    coordinates[1] = firstCoordinate;
    return coordinates;
  }
}
