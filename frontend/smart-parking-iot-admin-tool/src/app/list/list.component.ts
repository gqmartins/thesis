import { Component, OnInit, Input, EventEmitter, Output, OnDestroy } from '@angular/core';
import { faEdit, faTrashAlt } from '@fortawesome/free-solid-svg-icons'
import { ListItem } from './list-item.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit, OnDestroy {
  @Input() list: Array<ListItem>;
  @Output() editInit = new EventEmitter<number>();
  @Input() editComplete = new EventEmitter<ListItem>();
  @Output() deleteInit = new EventEmitter<number>();
  @Input() deleteComplete = new EventEmitter<number>();
  @Input() spaceLeft;
  private faEdit = faEdit;
  private faTrashAlt = faTrashAlt;
  private tableHeight;

  constructor() { }

  ngOnInit() {
    // subscribe to edit completion
    this.editComplete.subscribe(response => {
      const index = this.list.findIndex(listItem => listItem.id == response.id);
      this.list[index] = response;
    });
    // subscribe to delete completion
    this.deleteComplete.subscribe(response => {
      const index = this.list.findIndex(listItem => listItem.id == response);
      this.list.splice(index, 1);
    });
    this.tableHeight = this.spaceLeft ? "calc(100vh - 9.28vh - " + this.spaceLeft + "vh)" : "calc(100vh - 9.28vh)";
  }

  ngOnDestroy(): void {
    this.editComplete.unsubscribe();
    this.deleteComplete.unsubscribe();
  }

  onEdit(id: number) {
    this.editInit.emit(id);
  }

  onDelete(id: number) {
    this.deleteInit.emit(id);
  }


}
