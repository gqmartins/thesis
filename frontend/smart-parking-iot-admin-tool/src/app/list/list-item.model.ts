export interface ListItem {
  id: number,
  name: string,
  created: Date,
  modified: Date
}