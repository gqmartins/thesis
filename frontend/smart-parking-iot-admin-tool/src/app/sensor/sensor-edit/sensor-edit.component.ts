import { Component, OnInit, Input, Output, EventEmitter, ComponentFactoryResolver, OnChanges, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { ApiErrorResponse } from 'src/app/shared/base-api-service/response/api-error-response.model';
import { Sensor } from '../sensor.model';
import { DomainZoneService } from 'src/app/zone/zone.service';
import { DomainSensorTypeService } from 'src/app/sensor-type/sensor-type.service';
import { DomainSensorService } from '../sensor.service';
import { SensorType } from 'src/app/sensor-type/sensor-type.model';
import { Zone } from 'src/app/zone/zone.model';
import { ApiBodyResponse } from 'src/app/shared/base-api-service/response/api-body-response.model';
import { NgForm } from '@angular/forms';
import { BaseComponent } from 'src/app/shared/base-component/base-component.component';
import { SensorZone } from '../sensor-zone-model';

@Component({
  selector: 'app-sensor-edit',
  templateUrl: './sensor-edit.component.html',
  styleUrls: ['./sensor-edit.component.css']
})
export class SensorEditComponent extends BaseComponent implements OnInit {
  @Input() sensorId: number;
  @Input() showDelete: boolean;
  @Output() edit = new EventEmitter<void>();
  @Output() delete = new EventEmitter<number>();
  @Output() error = new EventEmitter<ApiErrorResponse>();
  private sensor: Sensor;
  private zones: Array<Zone> = [{id: 0, name: 'None'}];
  private selectedEnterZone: Zone;
  private selectedExitZone: Zone;
  private sensorTypes: Array<SensorType> = [];
  private selectedSensorType: SensorType;
  private showDialog: boolean = false;

  constructor(private sensorService: DomainSensorService, private sensorTypeService: DomainSensorTypeService, private zoneService: DomainZoneService, protected componentResolver: ComponentFactoryResolver, private changeDetector: ChangeDetectorRef) {
    super(componentResolver);
  }

  ngOnInit() {
    this.sensorService.readSensor(this.sensorId).subscribe(response => {
      this.sensor = (<ApiBodyResponse>response).body;
      this.sensorTypeService.listSensorTypes().subscribe(response => {
        this.sensorTypes = (<ApiBodyResponse>response).body;
        this.sensorTypeService.readSensorType(this.sensor.type).subscribe(response => {
          this.selectedSensorType = (<ApiBodyResponse>response).body;
          this.zoneService.listZones().subscribe(response => {
            const zones = (<ApiBodyResponse>response).body;
            zones.forEach(zone => this.zones.push(zone));
            this.selectedEnterZone = this.findZone(this.sensor.zones, true);
            this.selectedExitZone = this.findZone(this.sensor.zones, false);
            this.showDialog = true;
            this.changeDetector.detectChanges();
          });
        })
      });
    });
  }
  
  onSubmit(form: NgForm) {
    const name = form.value.name;
    const enterZone = form.value.enterZone;
    const exitZone = form.value.exitZone;
    const sensorType = form.value.sensorType;
    const fields: {[key: string]: string} = {};
    this.selectedSensorType.fields.forEach(field => {
      fields[field] = form.value[field];
    });
    const newSensor = this.createFromForm(this.sensorId, name, fields, this.sensor.location, sensorType, enterZone, exitZone);
    this.sensorService.editSensor(newSensor).subscribe(this.handleSubscription(this.edit, this.error));
  }

  onDelete() {
    this.delete.emit(this.sensorId);
  }

  onCancel() {
    this.edit.emit();
  }

  onSensorTypeChange(value) {
    this.sensorTypeService.readSensorType(value).subscribe(response => {
      this.selectedSensorType = (<ApiBodyResponse>response).body;
      this.changeDetector.detectChanges();
    });
  }

  private createFromForm(id: number, name: string, fieldValues: {[key: string]: string}, location: any, type: number, enterZoneId: number, exitZoneId: number): Sensor {
    const zones: Array<SensorZone> = [];
    this.addZoneToSensorZones(zones, enterZoneId, true);
    this.addZoneToSensorZones(zones, exitZoneId, false);
    return {id: id, name: name, fieldValues: fieldValues, location: location, type: type, zones: zones};
  }

  private findZone(zones, enterExit) {
    const zone = zones.find(zone => zone.enterExit == enterExit);
    return zone === undefined ? {id: 0, name: 'None'} : zone;
  }

  private addZoneToSensorZones(zones: Array<SensorZone>, zoneId: number, enterExit: boolean) {
    if(zoneId != 0) {
      zones.push({id: zoneId, enterExit: enterExit});
    }
  }
}
