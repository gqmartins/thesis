import { Injectable } from '@angular/core';
import { SensorService, SensorRequest, SensorZoneModel, ListItemResponse, SensorResponse } from '../api';
import { BaseApiService } from '../shared/base-api-service/base-api.service';
import { Sensor } from './sensor.model';
import { Observable } from 'rxjs';
import { BaseApiResponse } from '../shared/base-api-service/response/base-api-response';
import { SensorZone } from './sensor-zone-model';

@Injectable({providedIn: 'root'})
export class DomainSensorService {

  constructor(private apiSensorService: SensorService, private baseApiService: BaseApiService) {}

  public createSensor(sensor: Sensor): Observable<BaseApiResponse> {
    const body = {
      name: sensor.name,
      location: JSON.parse(sensor.location),
      type: sensor.type,
      zones: DomainSensorService.mapSensorZones(sensor.zones),
      fieldValues: sensor.fieldValues
    };
    const request = this.apiSensorService.createSensor(body, 'response');
    return this.baseApiService.handlePostRequest(request);
  }

  public listSensors() {
    const request = this.apiSensorService.readSensors(undefined, undefined, 'response');
    return this.baseApiService.handleGetRequest(request, DomainSensorService.sensorListMapper);
  }

  public readSensor(id: number) {
    const request = this.apiSensorService.readSensor(id, 'response');
    return this.baseApiService.handleGetRequest(request, DomainSensorService.createFromReadApiRequest);
  }

  public editSensor(sensor: Sensor) {
    const body = {
      name: sensor.name,
      location: sensor.location,
      type: sensor.type,
      zones: DomainSensorService.mapSensorZones(sensor.zones),
      fieldValues: sensor.fieldValues
    };
    const request = this.apiSensorService.editSensor(sensor.id, body, 'response');
    return this.baseApiService.handlePutRequest(request);
  }

  public deleteSensor(id: number) {
    const request = this.apiSensorService.deleteSensor(id, 'response');
    return this.baseApiService.handleDeleteRequest(request);
  }

  private static mapSensorZones(sensorZones: Array<SensorZone>): Array<SensorZoneModel> {
    return sensorZones.map(sensorZone => {return { id: sensorZone.id, enterExit: sensorZone.enterExit }});
  }

  private static sensorListMapper(sensorList: Array<ListItemResponse>) {
    return sensorList.map(DomainSensorService.createFromListApiRequest);
  }

  private static createFromListApiRequest(sensor: ListItemResponse): Sensor {
    return {id: sensor.id, name: sensor.name, created: new Date(sensor.created), modified: new Date(sensor.modified)};
  }

  private static createFromReadApiRequest(sensor: SensorResponse): Sensor {
    return {id: sensor.id, name: sensor.name, location: sensor.location, type: sensor.type, zones: sensor.zones, fieldValues: sensor.fieldValues, created: new Date(sensor.created), modified: new Date(sensor.modified)};
  }
}