import { SensorZone } from './sensor-zone-model';

export interface Sensor {
  id?: number;
  name: string;
  location?: any;
  type?: number;
  zones?: Array<SensorZone>;
  fieldValues?: { [key: string]: string; };
  created?: Date;
  modified?: Date;
}