import { Component, OnInit, Input, Output, EventEmitter, ComponentFactoryResolver } from '@angular/core';
import { SensorZone } from '../sensor-zone-model';
import { Sensor } from '../sensor.model';
import { DomainSensorService } from '../sensor.service';
import { DomainSensorTypeService } from 'src/app/sensor-type/sensor-type.service';
import { DomainZoneService } from 'src/app/zone/zone.service';
import { SensorType } from 'src/app/sensor-type/sensor-type.model';
import { Zone } from 'src/app/zone/zone.model';
import { ApiBodyResponse } from 'src/app/shared/base-api-service/response/api-body-response.model';
import { NgForm } from '@angular/forms';
import { ApiErrorResponse } from 'src/app/shared/base-api-service/response/api-error-response.model';
import { BaseComponent } from 'src/app/shared/base-component/base-component.component';

@Component({
  selector: 'app-sensor-create',
  templateUrl: './sensor-create.component.html',
  styleUrls: ['./sensor-create.component.css']
})
export class SensorCreateComponent extends BaseComponent implements OnInit {
  @Input() sensorLocation: string;
  @Output() success = new EventEmitter<number>();
  @Output() cancel = new EventEmitter<void>();
  @Output() error = new EventEmitter<ApiErrorResponse>();
  private showDialog: boolean = false;
  private sensorTypes: Array<SensorType> = [];
  private selectedSensorType: SensorType;
  private zones: Array<Zone> = [{id: 0, name: 'None'}];
  private defaultEnterZone: Zone = {id: 0};
  private defaultExitZone: Zone = {id: 0};
  
  constructor(private sensorService: DomainSensorService, private sensorTypeService: DomainSensorTypeService, private zoneService: DomainZoneService, protected componentResolver: ComponentFactoryResolver) {
    super(componentResolver)
  }

  ngOnInit() {
    this.sensorTypeService.listSensorTypes().subscribe(response => {
      this.sensorTypes = (<ApiBodyResponse>response).body;
      this.sensorTypeService.readSensorType(this.sensorTypes[0].id).subscribe(response => {
        this.selectedSensorType = (<ApiBodyResponse>response).body;
        this.zoneService.listZones().subscribe(response => {
          const responseBody = (<ApiBodyResponse>response).body;
          responseBody.forEach(zone => this.zones.push(zone));
          this.showDialog = true;
        });
      })
    });
  }

  onSubmit(form: NgForm) {
    const name = form.value.name;
    const enterZone = form.value.enterZone;
    const exitZone = form.value.exitZone;
    const sensorType = form.value.sensorType;
    const fields: {[key: string]: string} = {};
    this.selectedSensorType.fields.forEach(field => {
      fields[field] = form.value[field];
    });
    const newSensor = this.createFromForm(name, fields, this.sensorLocation, sensorType, enterZone, exitZone);
    this.sensorService.createSensor(newSensor).subscribe(this.handleSubscription(this.success, this.error));
  }

  onSensorTypeChange(value) {
    this.sensorTypeService.readSensorType(value).subscribe(response => {
      this.selectedSensorType = (<ApiBodyResponse>response).body;
    });
  }

  onCancel() {
    this.cancel.emit();
  }

  private createFromForm(name: string, fieldValues: {[key: string]: string}, location: string, type: number, enterZoneId: number, exitZoneId: number): Sensor {
    const zones: Array<SensorZone> = [];
    this.addZoneToSensorZones(zones, enterZoneId, true);
    this.addZoneToSensorZones(zones, exitZoneId, false);
    return {name: name, fieldValues: fieldValues, location: location, type: type, zones: zones};
  }

  private addZoneToSensorZones(zones: Array<SensorZone>, zoneId: number, enterExit: boolean) {
    if(zoneId != 0) {
      zones.push({id: zoneId, enterExit: enterExit});
    }
  }
}
