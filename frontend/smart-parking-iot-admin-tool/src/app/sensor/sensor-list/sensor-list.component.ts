import { Component, OnInit, EventEmitter, ViewChild, ComponentFactoryResolver } from '@angular/core';
import { Sensor } from '../sensor.model';
import { ListItem } from 'src/app/list/list-item.model';
import { PlaceholderDirective } from 'src/app/shared/placeholder/placeholder.directive';
import { DomainSensorService } from '../sensor.service';
import { BaseComponent } from 'src/app/shared/base-component/base-component.component';
import { SensorEditComponent } from '../sensor-edit/sensor-edit.component';
import { faEdit, faTrashAlt, faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { OutboundMessageCreateComponent } from 'src/app/outbound-message/outbound-message-create/outbound-message-create.component';

@Component({
  selector: 'app-sensor-list',
  templateUrl: './sensor-list.component.html',
  styleUrls: ['./sensor-list.component.css']
})
export class SensorListComponent extends BaseComponent implements OnInit {
  private sensors: Array<Sensor>;
  private editComplete = new EventEmitter<ListItem>();
  private deleteComplete  = new EventEmitter<number>();
  @ViewChild(PlaceholderDirective, {static: false}) private placeholder: PlaceholderDirective;
  private faEdit = faEdit;
  private faTrashAlt = faTrashAlt;
  private faEnvelope = faEnvelope;
  
  constructor(private sensorService: DomainSensorService, protected componentResolver: ComponentFactoryResolver) {
    super(componentResolver);
  }

  ngOnInit() {
    this.sensorService.listSensors().subscribe(this.handleServiceRequest(this.placeholder, response => {
      this.sensors = response;
    }))
  }

  onEdit(id: number) {
    const componentFactory = this.componentResolver.resolveComponentFactory(SensorEditComponent);
    const hostViewContainerRef = this.placeholder.viewContainerRef;
    hostViewContainerRef.clear();
    const sensorEditComponentRef = hostViewContainerRef.createComponent(componentFactory);
    sensorEditComponentRef.instance.sensorId = id;
    sensorEditComponentRef.instance.showDelete = false;
    sensorEditComponentRef.changeDetectorRef.detectChanges();
    // in this case cancel and edit run the same code
    this.placeholderCancelSubscription = sensorEditComponentRef.instance.edit.subscribe(() => {
      this.sensorService.readSensor(id).subscribe(this.handleServiceRequest(this.placeholder, response => {
        this.editComplete.emit(response);
      }));
      this.placeholderCancelSubscription.unsubscribe();
      hostViewContainerRef.clear();
    });
    this.placeholderErrorSubscription = sensorEditComponentRef.instance.error.subscribe(response => {
      this.handleError(response, this.placeholder);
    });
  }

  onDelete(id: number) {
    this.sensorService.deleteSensor(id).subscribe(this.handleServiceRequest(this.placeholder, response => {
      this.deleteComplete.emit(id);
    }));
  }

  onMessageSend(id: number) {
    this.sensorService.readSensor(id).subscribe(this.handleServiceRequest(this.placeholder, response => {
      const componentFactory = this.componentResolver.resolveComponentFactory(OutboundMessageCreateComponent);
      const hostViewContainerRef = this.placeholder.viewContainerRef;
      hostViewContainerRef.clear();
      const component = hostViewContainerRef.createComponent(componentFactory);
      component.instance.sensorTypeId = response.type;
      component.instance.sensorId = id;
      component.changeDetectorRef.detectChanges();
      // in this case cancel and edit run the same code
      this.placeholderSuccessSubscription = component.instance.success.subscribe(location => {
        this.placeholderSuccessSubscription.unsubscribe();
        hostViewContainerRef.clear();
      });
      this.placeholderCancelSubscription = component.instance.cancel.subscribe(() => {
        this.placeholderCancelSubscription.unsubscribe();
        hostViewContainerRef.clear();
      })
      this.placeholderErrorSubscription = component.instance.error.subscribe(response => {
        this.handleError(response, this.placeholder);
      });
    }));
  }
}
