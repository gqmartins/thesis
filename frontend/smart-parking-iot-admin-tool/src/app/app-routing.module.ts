import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login.component';
import { BoardComponent } from './board/board.component';
import { MapComponent } from './map/map.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ZoneListComponent } from './zone/zone-list/zone-list.component';
import { SensorListComponent } from './sensor/sensor-list/sensor-list.component';
import { SensorTypeListComponent } from './sensor-type/sensor-type-list/sensor-type-list.component';
import { OutboundMessageTypeListComponent } from './outbound-message-type/outbound-message-type-list/outbound-message-type-list.component';
import { UserListComponent } from './user/user-list/user-list.component';

const routes: Routes = [
  { path: '', component: LoginComponent, pathMatch: 'full'},
  { path: 'board', component: BoardComponent, children: [
    { path: 'map', component: MapComponent },
    { path: 'zones', component: ZoneListComponent },
    { path: 'sensors', component: SensorListComponent },
    { path: 'sensor-types', component: SensorTypeListComponent },
    { path: 'outbound-message-types', component: OutboundMessageTypeListComponent },
    { path: 'dashboard', component: DashboardComponent },
    { path: 'users', component: UserListComponent }
  ] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
