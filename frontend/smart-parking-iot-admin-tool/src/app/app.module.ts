import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Provider, forwardRef } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login.component';
import { MapComponent } from './map/map.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ApiModule } from './api/api.module';
import { NavbarComponent } from './navbar/navbar.component';
import { BoardComponent } from './board/board.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BoardItemComponent } from './board/board-item/board-item.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { ZoneCreateComponent } from './zone/zone-create/zone-create.component';
import { PlaceholderDirective } from './shared/placeholder/placeholder.directive';
import { BaseApiService } from './shared/base-api-service/base-api.service';
import { BaseComponent } from './shared/base-component/base-component.component';
import { ZoneEditComponent } from './zone/zone-edit/zone-edit.component';
import { ErrorComponent } from './shared/error/error.component';
import { ConfirmDlialogComponent } from './shared/confirm-dlialog/confirm-dlialog.component';
import { SensorCreateComponent } from './sensor/sensor-create/sensor-create.component';
import { SensorEditComponent } from './sensor/sensor-edit/sensor-edit.component';
import { ListComponent } from './list/list.component';
import { ZoneListComponent } from './zone/zone-list/zone-list.component';
import { SensorListComponent } from './sensor/sensor-list/sensor-list.component';
import { SensorTypeListComponent } from './sensor-type/sensor-type-list/sensor-type-list.component';
import { OutboundMessageTypeListComponent } from './outbound-message-type/outbound-message-type-list/outbound-message-type-list.component';
import { UserListComponent } from './user/user-list/user-list.component';
import { SensorTypeCreateComponent } from './sensor-type/sensor-type-create/sensor-type-create.component';
import { SensorTypeEditComponent } from './sensor-type/sensor-type-edit/sensor-type-edit.component';
import { OutboundMessageTypeCreateComponent } from './outbound-message-type/outbound-message-type-create/outbound-message-type-create.component';
import { OutboundMessageTypeEditComponent } from './outbound-message-type/outbound-message-type-edit/outbound-message-type-edit.component';
import { UserCreateComponent } from './user/user-create/user-create.component';
import { UserEditComponent } from './user/user-edit/user-edit.component';
import { OutboundMessageCreateComponent } from './outbound-message/outbound-message-create/outbound-message-create.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { PredictionChartComponent } from './dashboard/prediction-chart/prediction-chart.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MapComponent,
    NavbarComponent,
    BoardComponent,
    DashboardComponent,
    UserListComponent,
    BoardItemComponent,
    ZoneCreateComponent,
    PlaceholderDirective,
    ErrorComponent,
    BaseComponent,
    ZoneEditComponent,
    ConfirmDlialogComponent,
    SensorCreateComponent,
    SensorEditComponent,
    ListComponent,
    ZoneListComponent,
    SensorListComponent,
    SensorTypeListComponent,
    OutboundMessageTypeListComponent,
    UserListComponent,
    SensorTypeCreateComponent,
    SensorTypeEditComponent,
    OutboundMessageTypeCreateComponent,
    OutboundMessageTypeEditComponent,
    UserCreateComponent,
    UserEditComponent,
    OutboundMessageCreateComponent,
    PredictionChartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ApiModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    LeafletModule.forRoot(),
    HighchartsChartModule,
    HttpClientModule
  ],
  providers: [BaseApiService],
  bootstrap: [AppComponent],
  entryComponents: [
    ZoneCreateComponent, ZoneEditComponent, ErrorComponent, SensorCreateComponent, SensorEditComponent, 
    SensorTypeCreateComponent, SensorTypeEditComponent, OutboundMessageTypeCreateComponent,
    OutboundMessageTypeEditComponent, UserCreateComponent, UserEditComponent, OutboundMessageCreateComponent
  ]
})
export class AppModule { }
