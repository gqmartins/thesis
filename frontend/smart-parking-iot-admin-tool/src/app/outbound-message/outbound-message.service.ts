import { Injectable } from '@angular/core';
import { OutboundMessageService } from '../api';
import { BaseApiService } from '../shared/base-api-service/base-api.service';
import { OutboundMessage } from './outbound-message.model';

@Injectable({providedIn: 'root'})
export class DomainOutboundMessageService {

  constructor(private apiOutboundMessage: OutboundMessageService, private baseApiService: BaseApiService) {}

  public createOutboundMessage(sensorId: number, outboundMessage: OutboundMessage) {
    const body = {
      type: outboundMessage.type,
      fieldValues: outboundMessage.fieldValues
    };
    const request = this.apiOutboundMessage.createOutboundMessage(sensorId, body, 'response');
    return this.baseApiService.handlePostRequest(request);
  }
}