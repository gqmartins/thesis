import { Component, OnInit, Input, Output, EventEmitter, ComponentFactoryResolver } from '@angular/core';
import { SensorEditComponent } from 'src/app/sensor/sensor-edit/sensor-edit.component';
import { ApiErrorResponse } from 'src/app/shared/base-api-service/response/api-error-response.model';
import { DomainOutboundMessageService } from '../outbound-message.service';
import { DomainOutboundMessageTypeService } from 'src/app/outbound-message-type/outbound-message-type.service';
import { BaseComponent } from 'src/app/shared/base-component/base-component.component';
import { OutboundMessageType } from 'src/app/outbound-message-type/outbound-message-type.model';
import { ApiBodyResponse } from 'src/app/shared/base-api-service/response/api-body-response.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-outbound-message-create',
  templateUrl: './outbound-message-create.component.html',
  styleUrls: ['./outbound-message-create.component.css']
})
export class OutboundMessageCreateComponent extends BaseComponent implements OnInit {
  @Input() sensorTypeId: number;
  @Input() sensorId: number;
  @Output() success = new EventEmitter<number>();
  @Output() cancel = new EventEmitter<void>();
  @Output() error = new EventEmitter<ApiErrorResponse>();
  private outboundMessageTypes: Array<OutboundMessageType>;
  private selectedOutboundMessageType: OutboundMessageType;
  private showDialog: boolean = false;

  constructor(private outboundMessageService: DomainOutboundMessageService, private outboundMessageTypeService: DomainOutboundMessageTypeService, protected componentResolver: ComponentFactoryResolver) {
    super(componentResolver);
  }

  ngOnInit() {
    this.outboundMessageTypeService.listOutboundMessageTypes(this.sensorTypeId).subscribe(response =>{
      this.outboundMessageTypes = (<ApiBodyResponse>response).body;
      const outboundMessageTypeId = this.outboundMessageTypes[0].id;
      this.outboundMessageTypeService.readOutboundMessageType(this.sensorTypeId, outboundMessageTypeId).subscribe(response => {
        this.selectedOutboundMessageType = (<ApiBodyResponse>response).body;
        this.showDialog = true;
      });
    });
  }

  onOutboundMessageTypeChange() {
    
  }

  onSubmit(form: NgForm) {
    const type = this.selectedOutboundMessageType.id;
    const fieldsValues: {[key: string]: string} = {};
    this.selectedOutboundMessageType.fields.forEach(field => {
      fieldsValues[field] = form.value[field];
    });
    const newOutboundMessage = this.createFromForm(type, fieldsValues);
    this.outboundMessageService.createOutboundMessage(this.sensorId, newOutboundMessage).subscribe(this.handleSubscription(this.success, this.error));
  }

  onCancel() {
    this.cancel.emit();
  }

  private createFromForm(type: number, fieldValues: {[key: string]: string}) {
    return {type: type, fieldValues: fieldValues};
  }
}
