export interface OutboundMessage {
  id?: number;
  type?: number;
  fieldValues?: { [key: string]: string; };
}