import { EventEmitter, ComponentFactoryResolver, Component } from '@angular/core';
import { Subscription } from 'rxjs';
import { ApiLocationResponse } from '../base-api-service/response/api-location-response.model';
import { ApiBodyResponse } from '../base-api-service/response/api-body-response.model';
import { ApiErrorResponse } from '../base-api-service/response/api-error-response.model';
import { BaseApiResponse } from '../base-api-service/response/base-api-response';
import { ErrorComponent } from '../error/error.component';
import { ConfirmDlialogComponent } from '../confirm-dlialog/confirm-dlialog.component';

@Component({
  selector: 'app-base-component',
  template: ''
})
export class BaseComponent {
  protected placeholderSuccessSubscription: Subscription;
  protected placeholderErrorSubscription: Subscription;
  protected placeholderCancelSubscription: Subscription;

  constructor(protected componentResolver: ComponentFactoryResolver) {}

  protected handleServiceRequest(placeholder, success) {
    return response => {
      switch (response.constructor) {
        case ApiLocationResponse:
          success(response.location);
          break;
        case ApiBodyResponse:
          success(response.body);
          break;
        case BaseApiResponse:
          success();
          break;
        default:
          this.handleError(response, placeholder);
      }
    }
  }

  protected handleSubscription(success: EventEmitter<any>, error: EventEmitter<ApiErrorResponse>) {
    return response => {
      switch (response.constructor) {
        case ApiLocationResponse:
          success.emit(response.location);
          break;
        case ApiBodyResponse:
          success.emit(response.body);
          break;
        case BaseApiResponse:
          success.emit();
          break;
        default:
          error.emit(response);
      }
    }
  }

  protected handleError(response, placeholder) {
    const hostViewContainerRef = placeholder.viewContainerRef;
    hostViewContainerRef.clear();
    const componentFactory = this.componentResolver.resolveComponentFactory(ErrorComponent);
    const errorComponentRef = hostViewContainerRef.createComponent(componentFactory);
    errorComponentRef.instance.title = response.errorTitle;
    errorComponentRef.instance.detail = response.errorDetail;
    this.placeholderSuccessSubscription = errorComponentRef.instance.close.subscribe(() => {
      this.placeholderSuccessSubscription.unsubscribe();
      hostViewContainerRef.clear();
    });
  }
  /*
  protected openConfirmationDialog(hostViewContainerRef, message: string, yesOperation) {
    const componentFactory = this.componentResolver.resolveComponentFactory(ConfirmDlialogComponent);
    hostViewContainerRef.clear();
    const confirmDialogComponent = hostViewContainerRef.createComponent(componentFactory);
    confirmDialogComponent.instance.message = message;
    this.placeholderSuccessSubscription = confirmDialogComponent.instance.yesOperation.subscribe(() => {
      yesOperation();
      this.placeholderSuccessSubscription.unsubscribe();
      hostViewContainerRef.clear();
    });
    confirmDialogComponent.instance.noOperation.subscribe(() => {
      hostViewContainerRef.clear();
    });
  }
  */
}
