import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {
  @Input() title: string;
  @Input() detail: string;
  @Output() close = new EventEmitter<void>();
  
  constructor() { }

  ngOnInit() {
  }

  onClose() {
    this.close.emit();
  }
}
