import { BaseApiResponse } from './base-api-response';

export class ApiErrorResponse extends BaseApiResponse {
  constructor(public errorTitle: string, public errorDetail: string) {
    super(true);
  }
}