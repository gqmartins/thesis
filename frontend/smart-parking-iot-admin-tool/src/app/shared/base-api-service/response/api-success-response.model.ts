import { BaseApiResponse } from './base-api-response';

export class ApiSuccessResponse extends BaseApiResponse {
  constructor(public statusCode: number) {
    super(false);
  }
}