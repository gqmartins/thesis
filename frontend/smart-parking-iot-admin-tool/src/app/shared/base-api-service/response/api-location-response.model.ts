import { BaseApiResponse } from './base-api-response';

export class ApiLocationResponse extends BaseApiResponse {
  constructor(public location: string) {
    super(false);
  }
}