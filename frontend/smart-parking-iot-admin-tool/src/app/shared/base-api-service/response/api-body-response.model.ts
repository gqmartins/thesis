import { BaseApiResponse } from './base-api-response';

export class ApiBodyResponse extends BaseApiResponse {
  constructor(public body: any) {
    super(true);
  }
}