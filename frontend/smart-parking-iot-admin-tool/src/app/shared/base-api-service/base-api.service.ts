import { ApiLocationResponse } from './response/api-location-response.model';
import { ApiErrorResponse } from './response/api-error-response.model';
import { ErrorResponse } from 'src/app/api';
import { map, catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { BaseApiResponse } from './response/base-api-response';
import { ApiBodyResponse } from './response/api-body-response.model';

export class BaseApiService {

  public handlePostRequest(request): Observable<BaseApiResponse> {
    return this.handleRequest(request, response => {
      let location = response.headers.get('Location');
      return new ApiLocationResponse(location);
    });
  }

  public handleGetRequest(request, mapper): Observable<BaseApiResponse> {
    return this.handleRequest(request, response => {
      let mappedResponse = mapper(response.body);
      return new ApiBodyResponse(mappedResponse);
    });
  }

  public handlePutRequest(request): Observable<BaseApiResponse> {
    return this.handleRequest(request, response => new BaseApiResponse(false));
  }

  public handleDeleteRequest(request): Observable<BaseApiResponse> {
    return this.handleRequest(request, response => new BaseApiResponse(false));
  }

  private handleRequest(request, handleSuccess): Observable<BaseApiResponse> {
    return request.pipe(
      map(response => handleSuccess(response)),
      catchError(error => of(this.handleError(error)))
    );
  }

  private handleError(errorResponse) {
    let error = <ErrorResponse> errorResponse.error;
    return new ApiErrorResponse(error.title, error.detail);
  }
}