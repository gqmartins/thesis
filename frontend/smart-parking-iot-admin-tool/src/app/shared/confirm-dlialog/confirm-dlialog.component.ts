import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-confirm-dlialog',
  templateUrl: './confirm-dlialog.component.html',
  styleUrls: ['./confirm-dlialog.component.css']
})
export class ConfirmDlialogComponent implements OnInit {
  @Input() message: string;
  @Output() yesOperation = new EventEmitter<void>();
  @Output() noOperation = new EventEmitter<void>();
  
  constructor() { }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    this.yesOperation.emit();
  }

  onCancel() {
    this.noOperation.emit();
  }

}
