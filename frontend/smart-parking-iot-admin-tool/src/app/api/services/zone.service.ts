/**
 * SmartparkingIoT Admin API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional }                      from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams,
         HttpResponse, HttpEvent }                           from '@angular/common/http';
import { CustomHttpUrlEncodingCodec }                        from '../encoder';

import { Observable }                                        from 'rxjs/Observable';

import { ErrorResponse } from '../model/errorResponse';
import { ListItemResponse } from '../model/listItemResponse';
import { ZoneRequest } from '../model/zoneRequest';
import { ZoneResponse } from '../model/zoneResponse';

import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';


@Injectable()
export class ZoneService {

    protected basePath = 'http://localhost:8080/smart-parking-iot-api';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
            this.basePath = basePath || configuration.basePath || this.basePath;
        }
    }

    /**
     * @param consumes string[] mime-types
     * @return true: consumes contains 'multipart/form-data', false: otherwise
     */
    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (const consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    /**
     * adds a zone to the system
     * adds a zone to the system
     * @param body body
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public createZone(body: ZoneRequest, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public createZone(body: ZoneRequest, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public createZone(body: ZoneRequest, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public createZone(body: ZoneRequest, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (body === null || body === undefined) {
            throw new Error('Required parameter body was null or undefined when calling createZone.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.post<any>(`${this.basePath}/zone`,
            body,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * deletes the zone with the specified zone-id
     * delete the zone with the specified zone-id
     * @param zoneId zone-id
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public deleteZone(zoneId: number, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public deleteZone(zoneId: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public deleteZone(zoneId: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public deleteZone(zoneId: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (zoneId === null || zoneId === undefined) {
            throw new Error('Required parameter zoneId was null or undefined when calling deleteZone.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];

        return this.httpClient.delete<any>(`${this.basePath}/zone/${encodeURIComponent(String(zoneId))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * edits the zone with the specified zone-id
     * edits the zone with the specified zone-id
     * @param zoneId zone-id
     * @param body body
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public editZone(zoneId: number, body: ZoneRequest, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public editZone(zoneId: number, body: ZoneRequest, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public editZone(zoneId: number, body: ZoneRequest, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public editZone(zoneId: number, body: ZoneRequest, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (zoneId === null || zoneId === undefined) {
            throw new Error('Required parameter zoneId was null or undefined when calling editZone.');
        }

        if (body === null || body === undefined) {
            throw new Error('Required parameter body was null or undefined when calling editZone.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.put<any>(`${this.basePath}/zone/${encodeURIComponent(String(zoneId))}`,
            body,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * returns zone with the specified zone-id
     * returns zone with the specified zone-id
     * @param zoneId zone-id
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public readZone(zoneId: number, observe?: 'body', reportProgress?: boolean): Observable<ZoneResponse>;
    public readZone(zoneId: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<ZoneResponse>>;
    public readZone(zoneId: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<ZoneResponse>>;
    public readZone(zoneId: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (zoneId === null || zoneId === undefined) {
            throw new Error('Required parameter zoneId was null or undefined when calling readZone.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];

        return this.httpClient.get<ZoneResponse>(`${this.basePath}/zone/${encodeURIComponent(String(zoneId))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * returns predictions for the zone with the specified zone-id, a start, end date and an interval
     * returns zone predictions with the specified zone-id
     * @param zoneId zone-id
     * @param start start
     * @param end end
     * @param interval interval
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public readZonePredictions(zoneId: number, start?: string, end?: string, interval?: number, observe?: 'body', reportProgress?: boolean): Observable<ZoneResponse>;
    public readZonePredictions(zoneId: number, start?: string, end?: string, interval?: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<ZoneResponse>>;
    public readZonePredictions(zoneId: number, start?: string, end?: string, interval?: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<ZoneResponse>>;
    public readZonePredictions(zoneId: number, start?: string, end?: string, interval?: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (zoneId === null || zoneId === undefined) {
            throw new Error('Required parameter zoneId was null or undefined when calling readZonePredictions.');
        }




        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (start !== undefined && start !== null) {
            queryParameters = queryParameters.set('start', <any>start);
        }
        if (end !== undefined && end !== null) {
            queryParameters = queryParameters.set('end', <any>end);
        }
        if (interval !== undefined && interval !== null) {
            queryParameters = queryParameters.set('interval', <any>interval);
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];

        return this.httpClient.get<ZoneResponse>(`${this.basePath}/zone/${encodeURIComponent(String(zoneId))}/prediction`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * returns zones added to the system
     * returns all the zones that were added to the system, ordered by creation date
     * @param page page
     * @param size size
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public readZones(page?: number, size?: number, observe?: 'body', reportProgress?: boolean): Observable<Array<ListItemResponse>>;
    public readZones(page?: number, size?: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<ListItemResponse>>>;
    public readZones(page?: number, size?: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<ListItemResponse>>>;
    public readZones(page?: number, size?: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {



        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (page !== undefined && page !== null) {
            queryParameters = queryParameters.set('page', <any>page);
        }
        if (size !== undefined && size !== null) {
            queryParameters = queryParameters.set('size', <any>size);
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];

        return this.httpClient.get<Array<ListItemResponse>>(`${this.basePath}/zone`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

}
