export * from './authentication.service';
import { AuthenticationService } from './authentication.service';
export * from './outboundMessage.service';
import { OutboundMessageService } from './outboundMessage.service';
export * from './outboundMessageType.service';
import { OutboundMessageTypeService } from './outboundMessageType.service';
export * from './role.service';
import { RoleService } from './role.service';
export * from './sensor.service';
import { SensorService } from './sensor.service';
export * from './sensorType.service';
import { SensorTypeService } from './sensorType.service';
export * from './swaggerController.service';
import { SwaggerControllerService } from './swaggerController.service';
export * from './user.service';
import { UserService } from './user.service';
export * from './zone.service';
import { ZoneService } from './zone.service';
export const APIS = [AuthenticationService, OutboundMessageService, OutboundMessageTypeService, RoleService, SensorService, SensorTypeService, SwaggerControllerService, UserService, ZoneService];
