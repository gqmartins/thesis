export interface OutboundMessageType {
  id?: number;
  name: string;
  fields?: Array<string>;
  sensorType?: number;
  created?: Date;
  modified?: Date;
}