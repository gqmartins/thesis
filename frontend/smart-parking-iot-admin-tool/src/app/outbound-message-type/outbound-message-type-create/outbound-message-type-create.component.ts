import { Component, OnInit, EventEmitter, Output, ComponentFactoryResolver } from '@angular/core';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { OutboundMessageType } from '../outbound-message-type.model';
import { ApiErrorResponse } from 'src/app/shared/base-api-service/response/api-error-response.model';
import { SensorType } from 'src/app/sensor-type/sensor-type.model';
import { DomainSensorTypeService } from 'src/app/sensor-type/sensor-type.service';
import { DomainOutboundMessageTypeService } from '../outbound-message-type.service';
import { BaseComponent } from 'src/app/shared/base-component/base-component.component';
import { ApiBodyResponse } from 'src/app/shared/base-api-service/response/api-body-response.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-outbound-message-type-create',
  templateUrl: './outbound-message-type-create.component.html',
  styleUrls: ['./outbound-message-type-create.component.css']
})
export class OutboundMessageTypeCreateComponent extends BaseComponent implements OnInit {
  @Output() success = new EventEmitter<OutboundMessageType>();
  @Output() cancel = new EventEmitter<void>();
  @Output() error = new EventEmitter<ApiErrorResponse>();
  private sensorTypes: Array<SensorType>;
  private selectedSensorType: SensorType;
  private fields = [{value: ''}];
  private faTrashAlt = faTrashAlt;
  private showDialog: boolean = false;

  constructor(private outboundMessageTypeService: DomainOutboundMessageTypeService, private sensorTypeService: DomainSensorTypeService, protected componentResolver: ComponentFactoryResolver) {
    super(componentResolver);
  }

  ngOnInit() {
    this.sensorTypeService.listSensorTypes().subscribe(response => {
      this.sensorTypes = (<ApiBodyResponse>response).body;
      this.sensorTypeService.readSensorType(this.sensorTypes[0].id).subscribe(response => {
        this.selectedSensorType = (<ApiBodyResponse>response).body;
        this.showDialog = true;
      });
    });
  }

  onSubmit(form: NgForm) {
    const name = form.value.name;
    const outboundMessageTypeFields = this.fields
      .filter(field => field.value !== '')
      .map(field => field.value);
    const newOutboundMessageType = this.createFromForm(name, this.selectedSensorType.id, outboundMessageTypeFields);
    this.outboundMessageTypeService.createOutboundMessageType(newOutboundMessageType).subscribe(this.handleSubscription(this.success, this.error));
  }

  onAddField() {
    this.fields.push({value: ''});
  }

  onDeleteField(index: number) {
    this.fields.splice(index, 1);
  }

  onCancel() {
    this.cancel.emit();
  }

  private createFromForm(name: string, sensorType: number, outboundMessageTypeFields: Array<string>) {
    return {name: name, sensorType: sensorType, fields: outboundMessageTypeFields};
  }
}
