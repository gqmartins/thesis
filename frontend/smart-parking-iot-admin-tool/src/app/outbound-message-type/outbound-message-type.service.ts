import { Injectable } from '@angular/core';
import { OutboundMessageTypeService, ListItemResponse, OutboundMessageTypeResponse } from '../api';
import { BaseApiService } from '../shared/base-api-service/base-api.service';
import { OutboundMessageType } from './outbound-message-type.model';

@Injectable({providedIn: 'root'})
export class DomainOutboundMessageTypeService {

  constructor(private apiOutboundMessageType: OutboundMessageTypeService, private baseApiService: BaseApiService) {}

  public createOutboundMessageType(outboundMessageType: OutboundMessageType) {
    const body = {
      name: outboundMessageType.name,
      fields: outboundMessageType.fields
    };
    const request = this.apiOutboundMessageType.createOutboundMessageType(outboundMessageType.sensorType, body, 'response');
    return this.baseApiService.handlePostRequest(request);
  }

  public listOutboundMessageTypes(sensorId: number) {
    const request = this.apiOutboundMessageType.readOutboundMessageTypes(sensorId, undefined, undefined, 'response');
    return this.baseApiService.handleGetRequest(request, this.outboundMessageTypeMapper);
  }
  
  public readOutboundMessageType(sensorId: number, outboundMessageTypeId: number) {
    const request = this.apiOutboundMessageType.readOutboundMessageType(sensorId, outboundMessageTypeId, 'response');
    return this.baseApiService.handleGetRequest(request, DomainOutboundMessageTypeService.createFromReadApiRequest);
  }

  public editOutbundMessageType(sensorId: number, outboundMessageType: OutboundMessageType) {
    const body = {
      name: outboundMessageType.name,
      fields: outboundMessageType.fields
    };
    const request = this.apiOutboundMessageType.editOutboundMessageType(sensorId, outboundMessageType.id, body, 'response');
    return this.baseApiService.handlePutRequest(request);
  }

  public deleteOutboundMessageType(sensorId: number, outboundMessageTypeId: number) {
    const request = this.apiOutboundMessageType.deleteOutboundMessageType(sensorId, outboundMessageTypeId);
    return this.baseApiService.handleDeleteRequest(request);
  }

  private outboundMessageTypeMapper(outboundMessageTypeList: ListItemResponse[]): Array<OutboundMessageType> {
    const outboundMessageTypes: Array<OutboundMessageType> = [];
    outboundMessageTypeList.forEach(outboundMessageType => {
      const mappedOutoundMessageType: OutboundMessageType = DomainOutboundMessageTypeService.createFromListApiRequest(outboundMessageType);
      outboundMessageTypes.push(mappedOutoundMessageType);
    });
    return outboundMessageTypes;
  }

  private static createFromListApiRequest(outboundMessageType: ListItemResponse): OutboundMessageType {
    return {id: outboundMessageType.id, name: outboundMessageType.name, created: new Date(outboundMessageType.created), modified: new Date(outboundMessageType.modified)};
  }

  private static createFromReadApiRequest(outboundMessageType: OutboundMessageTypeResponse): OutboundMessageType {
    return {id: outboundMessageType.id, name: outboundMessageType.name, fields: outboundMessageType.fields, sensorType: outboundMessageType.sensorType, created: new Date(outboundMessageType.created), modified: new Date(outboundMessageType.modified)};
  }
}