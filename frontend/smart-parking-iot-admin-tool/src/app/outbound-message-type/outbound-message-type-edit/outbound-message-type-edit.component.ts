import { Component, OnInit, Input, Output, EventEmitter, ComponentFactoryResolver } from '@angular/core';
import { ApiErrorResponse } from 'src/app/shared/base-api-service/response/api-error-response.model';
import { OutboundMessageType } from '../outbound-message-type.model';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { DomainOutboundMessageTypeService } from '../outbound-message-type.service';
import { DomainSensorTypeService } from 'src/app/sensor-type/sensor-type.service';
import { BaseComponent } from 'src/app/shared/base-component/base-component.component';
import { ApiBodyResponse } from 'src/app/shared/base-api-service/response/api-body-response.model';
import { SensorType } from 'src/app/sensor-type/sensor-type.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-outbound-message-type-edit',
  templateUrl: './outbound-message-type-edit.component.html',
  styleUrls: ['./outbound-message-type-edit.component.css']
})
export class OutboundMessageTypeEditComponent extends BaseComponent implements OnInit {
  @Input() outboundMessageTypeId: number;
  @Input() sensorTypeId: number;
  @Output() edit = new EventEmitter<void>();
  @Output() cancel = new EventEmitter<void>();
  @Output() error = new EventEmitter<ApiErrorResponse>();
  private outboundMessageType: OutboundMessageType;
  private fields = [];
  private showDialog: boolean = false;
  private faTrashAlt = faTrashAlt;

  constructor(private outboundMessageTypeService: DomainOutboundMessageTypeService, private sensorTypeService: DomainSensorTypeService, protected componentResolver: ComponentFactoryResolver) {
    super(componentResolver);
  }

  ngOnInit() {
    this.outboundMessageTypeService.readOutboundMessageType(this.sensorTypeId, this.outboundMessageTypeId).subscribe(response => {
      this.outboundMessageType = (<ApiBodyResponse>response).body;
      this.fields = this.outboundMessageType.fields.map(field => {return {value: field}});
      this.showDialog = true;
    });
  }

  onSubmit(form: NgForm) {
    const name = form.value.name;
    const outboundMessageTypefields = this.fields
      .filter(field => field.value !== '')
      .map(field => field.value);
    const newOutboundMessageType = this.createFromForm(this.outboundMessageTypeId, name, outboundMessageTypefields);
    this.outboundMessageTypeService.editOutbundMessageType(this.sensorTypeId, newOutboundMessageType).subscribe(this.handleSubscription(this.edit, this.error));
  }

  onAddField() {
    this.fields.push({value: ''});
  }

  onDeleteField(index: number) {
    this.fields.splice(index, 1);
  }

  onCancel() {
    this.cancel.emit();
  }

  private createFromForm(id: number, name: string, outboundMessageTypeFields: Array<string>) {
    return {id: id, name: name, fields: outboundMessageTypeFields};
  }
}