import { Component, OnInit, EventEmitter, ViewChild, ComponentFactoryResolver, ChangeDetectorRef } from '@angular/core';
import { OutboundMessageType } from '../outbound-message-type.model';
import { ListItem } from 'src/app/list/list-item.model';
import { PlaceholderDirective } from 'src/app/shared/placeholder/placeholder.directive';
import { BaseComponent } from 'src/app/shared/base-component/base-component.component';
import { DomainOutboundMessageTypeService } from '../outbound-message-type.service';
import { DomainSensorTypeService } from 'src/app/sensor-type/sensor-type.service';
import { SensorType } from 'src/app/sensor-type/sensor-type.model';
import { OutboundMessageTypeCreateComponent } from '../outbound-message-type-create/outbound-message-type-create.component';
import { OutboundMessageTypeEditComponent } from '../outbound-message-type-edit/outbound-message-type-edit.component';

@Component({
  selector: 'app-outbound-message-type-list',
  templateUrl: './outbound-message-type-list.component.html',
  styleUrls: ['./outbound-message-type-list.component.css']
})
export class OutboundMessageTypeListComponent extends BaseComponent implements OnInit {
  private outboundMessageTypes: Array<OutboundMessageType>;
  private sensorTypes: Array<SensorType>;
  private selectedSensorType: SensorType;
  private editComplete = new EventEmitter<ListItem>();
  private deleteComplete  = new EventEmitter<number>();
  @ViewChild(PlaceholderDirective, {static: false}) private placeholder: PlaceholderDirective;
  private show: boolean = false;

  constructor(private outboundMessageTypeService: DomainOutboundMessageTypeService, private sensorTypeService: DomainSensorTypeService, protected componentResolver: ComponentFactoryResolver) {
    super(componentResolver);
  }

  ngOnInit() {
    this.sensorTypeService.listSensorTypes().subscribe(this.handleServiceRequest(this.placeholder, response => {
      this.sensorTypes = response;
      this.sensorTypeService.readSensorType(this.sensorTypes[0].id).subscribe(this.handleServiceRequest(this.placeholder, response => {
        this.selectedSensorType = response;
        this.outboundMessageTypeService.listOutboundMessageTypes(this.selectedSensorType.id).subscribe(this.handleServiceRequest(this.placeholder, response => {
          this.outboundMessageTypes = response;
          this.show = true;
        }));
      }));
    }));
  }

  onSensorTypeChange(value) {
    this.outboundMessageTypeService.listOutboundMessageTypes(this.selectedSensorType.id).subscribe(this.handleServiceRequest(this.placeholder, response => {
      this.outboundMessageTypes = response;
    }));
  }

  onEdit(id: number) {
    const componentFactory = this.componentResolver.resolveComponentFactory(OutboundMessageTypeEditComponent);
    const hostViewContainerRef = this.placeholder.viewContainerRef;
    hostViewContainerRef.clear();
    const outboundMessageTypeEditComponent = hostViewContainerRef.createComponent(componentFactory);
    outboundMessageTypeEditComponent.instance.outboundMessageTypeId = id;
    outboundMessageTypeEditComponent.instance.sensorTypeId = this.selectedSensorType.id;
    // in this case cancel and edit run the same code
    this.placeholderSuccessSubscription = outboundMessageTypeEditComponent.instance.edit.subscribe(location => {
      this.outboundMessageTypeService.listOutboundMessageTypes(this.selectedSensorType.id).subscribe(this.handleServiceRequest(this.placeholder, response => {
        this.outboundMessageTypes = response;
      }));
      this.placeholderSuccessSubscription.unsubscribe();
      hostViewContainerRef.clear();
    });
    this.placeholderCancelSubscription = outboundMessageTypeEditComponent.instance.cancel.subscribe(() => {
      this.placeholderCancelSubscription.unsubscribe();
      hostViewContainerRef.clear();
    })
    this.placeholderErrorSubscription = outboundMessageTypeEditComponent.instance.error.subscribe(response => {
      this.handleError(response, this.placeholder);
    });
  }

  onDelete(id: number) {
    this.outboundMessageTypeService.deleteOutboundMessageType(this.selectedSensorType.id, id).subscribe(this.handleServiceRequest(this.placeholder, response => {
      this.deleteComplete.emit(id);
    }));
  }

  onCreate() {
    const componentFactory = this.componentResolver.resolveComponentFactory(OutboundMessageTypeCreateComponent);
    const hostViewContainerRef = this.placeholder.viewContainerRef;
    hostViewContainerRef.clear();
    const outboundMessageTypeCreateComponent = hostViewContainerRef.createComponent(componentFactory);
    outboundMessageTypeCreateComponent.changeDetectorRef.detectChanges();
    // in this case cancel and edit run the same code
    this.placeholderSuccessSubscription = outboundMessageTypeCreateComponent.instance.success.subscribe(location => {
      this.outboundMessageTypeService.listOutboundMessageTypes(this.selectedSensorType.id).subscribe(this.handleServiceRequest(this.placeholder, response => {
        this.outboundMessageTypes = response;
      }));
      this.placeholderSuccessSubscription.unsubscribe();
      hostViewContainerRef.clear();
    });
    this.placeholderCancelSubscription = outboundMessageTypeCreateComponent.instance.cancel.subscribe(() => {
      this.placeholderCancelSubscription.unsubscribe();
      hostViewContainerRef.clear();
    })
    this.placeholderErrorSubscription = outboundMessageTypeCreateComponent.instance.error.subscribe(response => {
      this.handleError(response, this.placeholder);
    });
  }

}
