import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { DomainAuthenticationService } from './authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private faUser = faUser;

  constructor(private authorizationService: DomainAuthenticationService, private router: Router) { }

  ngOnInit() {}

  onSubmit(form: NgForm) {
    const username = form.value.username;
    const password = form.value.password;
    this.authorizationService.login(username, password).subscribe(loggedIn => {
      if(loggedIn) {
        this.router.navigate(['/board/map']);
      }
    });
  }
}
