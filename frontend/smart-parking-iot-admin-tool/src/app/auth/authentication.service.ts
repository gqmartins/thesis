import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { AuthenticationService } from '../api';

@Injectable({providedIn: 'root'})
export class DomainAuthenticationService {
  public user = new BehaviorSubject<string>(null);

  constructor(private apiAuthenticationService: AuthenticationService) {}

  public login(username: string, password: string) {
    const authorizationHeader = 'Basic ' + btoa(username + ':' + password);
    return this.apiAuthenticationService.loginUser(authorizationHeader, 'response')
      .pipe(tap(response  => {
        const token = response.headers.get('Authorization');
        this.user.next(token);
      }), map(response => response.status == 200));
  }
}