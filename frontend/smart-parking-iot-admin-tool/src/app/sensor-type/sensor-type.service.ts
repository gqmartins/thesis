import { Injectable } from '@angular/core';
import { SensorType } from './sensor-type.model';
import { SensorTypeService, ListItemResponse, SensorTypeResponse } from '../api';
import { BaseApiService } from '../shared/base-api-service/base-api.service';

@Injectable({providedIn: 'root'})
export class DomainSensorTypeService {

  constructor(private apiSensorTypeService: SensorTypeService, private baseApiService: BaseApiService) {}

  public createSensorType(sensorType: SensorType) {
    const body = {
      name: sensorType.name,
      fields: sensorType.fields
    };
    const request = this.apiSensorTypeService.createSensorType(body, 'response');
    return this.baseApiService.handlePostRequest(request);
  }

  public listSensorTypes() {
    const request = this.apiSensorTypeService.readSensorTypes(undefined, undefined, 'response');
    return this.baseApiService.handleGetRequest(request, this.sensorTypeListMapper);
  }

  public readSensorType(id: number) {
    const request = this.apiSensorTypeService.readSensorType(id, 'response');
    return this.baseApiService.handleGetRequest(request, DomainSensorTypeService.createFromReadApiRequest);
  }

  public updateSensorType(sensorType: SensorType) {
    const body = {
      name: sensorType.name,
      fields: sensorType.fields
    };
    const request = this.apiSensorTypeService.editSensorType(sensorType.id, body, 'response');
    return this.baseApiService.handlePutRequest(request);
  }

  public deleteSensorType(id: number) {
    const request = this.apiSensorTypeService.deleteSensorType(id, 'response');
    return this.baseApiService.handleDeleteRequest(request);
  }

  private sensorTypeListMapper(sensorTypeList: ListItemResponse[]): Array<SensorType> {
    const sensorTypes: Array<SensorType> = [];
    sensorTypeList.forEach(sensorType => {
      const mappedSensorType: SensorType = DomainSensorTypeService.createFromListApiRequest(sensorType);
      sensorTypes.push(mappedSensorType);
    });
    return sensorTypes;
  }

  private static createFromListApiRequest(sensorType: ListItemResponse): SensorType {
    return {id: sensorType.id, name: sensorType.name, created: new Date(sensorType.created), modified: new Date(sensorType.modified)};
  }

  private static createFromReadApiRequest(sensorType: SensorTypeResponse): SensorType {
    return {id: sensorType.id, name: sensorType.name, fields: sensorType.fields, created: new Date(sensorType.created), modified: new Date(sensorType.modified)};
  }
}