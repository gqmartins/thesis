export interface SensorType {
  id?: number;
  name: string;
  created?: Date;
  modified?: Date;
  fields?: Array<string>;
}