import { Component, OnInit, Output, EventEmitter, ComponentFactoryResolver } from '@angular/core';
import { ApiErrorResponse } from 'src/app/shared/base-api-service/response/api-error-response.model';
import { SensorType } from '../sensor-type.model';
import { BaseComponent } from 'src/app/shared/base-component/base-component.component';
import { NgForm } from '@angular/forms';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { ValueConverter } from '@angular/compiler/src/render3/view/template';
import { DomainSensorService } from 'src/app/sensor/sensor.service';
import { DomainSensorTypeService } from '../sensor-type.service';

@Component({
  selector: 'app-sensor-type-create',
  templateUrl: './sensor-type-create.component.html',
  styleUrls: ['./sensor-type-create.component.css']
})
export class SensorTypeCreateComponent extends BaseComponent implements OnInit {
  @Output() success = new EventEmitter<SensorType>();
  @Output() cancel = new EventEmitter<void>();
  @Output() error = new EventEmitter<ApiErrorResponse>();
  private fields = [{value: ''}];
  private faTrashAlt = faTrashAlt;

  constructor(private sensorTypeService: DomainSensorTypeService, protected componentResolver: ComponentFactoryResolver) {
    super(componentResolver);
  }

  ngOnInit() {
  }

  onAddField() {
    this.fields.push({value: ''});
  }

  onDeleteField(index: number) {
    this.fields.splice(index, 1);
  }

  onSubmit(form: NgForm) {
    const name = form.value.name;
    const sensorTypefields = this.fields
      .filter(field => field.value !== '')
      .map(field => field.value);
    const sensorType = this.createFromForm(name, sensorTypefields);
    this.sensorTypeService.createSensorType(sensorType).subscribe(this.handleSubscription(this.success, this.error));
  }

  onCancel() {
    this.cancel.emit();
  }

  private createFromForm(name: string, sensorTypefields: Array<string>) {
    return {name: name, fields: sensorTypefields};
  }
}
