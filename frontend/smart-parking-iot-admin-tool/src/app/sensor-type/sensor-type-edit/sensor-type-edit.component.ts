import { Component, OnInit, Input, EventEmitter, Output, ComponentFactoryResolver } from '@angular/core';
import { SensorType } from '../sensor-type.model';
import { DomainSensorTypeService } from '../sensor-type.service';
import { BaseComponent } from 'src/app/shared/base-component/base-component.component';
import { ApiBodyResponse } from 'src/app/shared/base-api-service/response/api-body-response.model';
import { NgForm } from '@angular/forms';
import { ApiErrorResponse } from 'src/app/shared/base-api-service/response/api-error-response.model';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-sensor-type-edit',
  templateUrl: './sensor-type-edit.component.html',
  styleUrls: ['./sensor-type-edit.component.css']
})
export class SensorTypeEditComponent extends BaseComponent implements OnInit {
  @Input() sensorTypeId: number;
  @Output() edit = new EventEmitter<void>();
  @Output() cancel = new EventEmitter<void>();
  @Output() error = new EventEmitter<ApiErrorResponse>();
  private sensorType: SensorType;
  private fields = [];
  private showDialog: boolean = false;
  private faTrashAlt = faTrashAlt;

  constructor(private sensorTypeService: DomainSensorTypeService, protected componentResolver: ComponentFactoryResolver) {
    super(componentResolver);
  }

  ngOnInit() {
    this.sensorTypeService.readSensorType(this.sensorTypeId).subscribe(response => {
      this.sensorType = (<ApiBodyResponse>response).body;
      this.fields = this.sensorType.fields.map(field => {return {value: field}});
      this.showDialog = true;
    });
  }

  onAddField() {
    this.fields.push({value: ''});
  }

  onDeleteField(index: number) {
    this.fields.splice(index, 1);
  }

  onSubmit(form: NgForm) {
    const name = form.value.name;
    const sensorTypefields = this.fields
      .filter(field => field.value !== '')
      .map(field => field.value);
    const sensorType = this.createFromForm(this.sensorType.id, name, sensorTypefields);
    this.sensorTypeService.updateSensorType(sensorType).subscribe(this.handleSubscription(this.edit, this.error));
  }

  onCancel() {
    this.cancel.emit();
  }

  private createFromForm(id: number, name: string, sensorTypefields: Array<string>) {
    return {id: id, name: name, fields: sensorTypefields};
  }
}
