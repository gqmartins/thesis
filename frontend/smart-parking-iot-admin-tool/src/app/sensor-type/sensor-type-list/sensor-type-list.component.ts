import { Component, OnInit, EventEmitter, ViewChild, ComponentFactoryResolver } from '@angular/core';
import { PlaceholderDirective } from 'src/app/shared/placeholder/placeholder.directive';
import { ListItem } from 'src/app/list/list-item.model';
import { BaseComponent } from 'src/app/shared/base-component/base-component.component';
import { DomainSensorTypeService } from '../sensor-type.service';
import { SensorType } from '../sensor-type.model';
import { SensorTypeCreateComponent } from '../sensor-type-create/sensor-type-create.component';
import { SensorTypeEditComponent } from '../sensor-type-edit/sensor-type-edit.component';

@Component({
  selector: 'app-sensor-type-list',
  templateUrl: './sensor-type-list.component.html',
  styleUrls: ['./sensor-type-list.component.css']
})
export class SensorTypeListComponent extends BaseComponent implements OnInit {
  private sensorTypes: Array<SensorType>;
  private editComplete = new EventEmitter<ListItem>();
  private deleteComplete  = new EventEmitter<number>();
  @ViewChild(PlaceholderDirective, {static: false}) private placeholder: PlaceholderDirective;
  
  constructor(private sensorTypeService: DomainSensorTypeService, protected componentResolver: ComponentFactoryResolver) {
    super(componentResolver);
  }

  ngOnInit() {
    this.sensorTypeService.listSensorTypes().subscribe(this.handleServiceRequest(this.placeholder, response => {
      this.sensorTypes = response;
    }))
  }

  onEdit(id: number) {
    const componentFactory = this.componentResolver.resolveComponentFactory(SensorTypeEditComponent);
    const hostViewContainerRef = this.placeholder.viewContainerRef;
    hostViewContainerRef.clear();
    const sensorTypeEditComponentRef = hostViewContainerRef.createComponent(componentFactory);
    sensorTypeEditComponentRef.instance.sensorTypeId = id;
    // in this case cancel and edit run the same code
    this.placeholderSuccessSubscription = sensorTypeEditComponentRef.instance.edit.subscribe(() => {
      this.sensorTypeService.readSensorType(id).subscribe(this.handleServiceRequest(this.placeholder, response => {
        this.editComplete.emit(response);

      }));
      this.placeholderSuccessSubscription.unsubscribe();
      hostViewContainerRef.clear();
    });
    this.placeholderCancelSubscription = sensorTypeEditComponentRef.instance.cancel.subscribe(() => {
      this.placeholderCancelSubscription.unsubscribe();
      hostViewContainerRef.clear();
    })
    this.placeholderErrorSubscription = sensorTypeEditComponentRef.instance.error.subscribe(response => {
      this.handleError(response, this.placeholder);
    });
  }

  onDelete(id: number) {
    this.sensorTypeService.deleteSensorType(id).subscribe(this.handleServiceRequest(this.placeholder, response => {
      this.deleteComplete.emit(id);
    }));
  }

  onCreate() {
    const componentFactory = this.componentResolver.resolveComponentFactory(SensorTypeCreateComponent);
    const hostViewContainerRef = this.placeholder.viewContainerRef;
    hostViewContainerRef.clear();
    const sensorTypeCreateComponentRef = hostViewContainerRef.createComponent(componentFactory);
    sensorTypeCreateComponentRef.changeDetectorRef.detectChanges();
    // in this case cancel and edit run the same code
    this.placeholderSuccessSubscription = sensorTypeCreateComponentRef.instance.success.subscribe(location => {
      const id = +location.substring(location.lastIndexOf('/') + 1);
      this.sensorTypeService.readSensorType(id).subscribe(this.handleServiceRequest(this.placeholder, response => {
        this.sensorTypes.push(response);
      }));
      this.placeholderSuccessSubscription.unsubscribe();
      hostViewContainerRef.clear();
    });
    this.placeholderCancelSubscription = sensorTypeCreateComponentRef.instance.cancel.subscribe(() => {
      this.placeholderCancelSubscription.unsubscribe();
      hostViewContainerRef.clear();
    })
    this.placeholderErrorSubscription = sensorTypeCreateComponentRef.instance.error.subscribe(response => {
      this.handleError(response, this.placeholder);
    });
  }
}
