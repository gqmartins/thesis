package pt.isel.tese.smartParkingIoT.notificationSender.logic;

import pt.isel.tese.smartParkingIoT.notificationSender.logic.contract.ISubscriptionService;
import pt.isel.tese.smartParkingIoT.notificationSender.logic.contract.IZoneService;
import pt.isel.tese.smartParkingIoT.notificationSender.logic.implementation.SubscriptionService;
import pt.isel.tese.smartParkingIoT.notificationSender.logic.implementation.ZoneService;
import pt.isel.tese.smartParkingIoT.notificationSender.logic.manager.IPercentageAlgorithm;
import pt.isel.tese.smartParkingIoT.notificationSender.logic.manager.PercentageAlgorithm;
import pt.isel.tese.smartParkingIoT.notificationSender.repository.RepositoryFacade;

public class ServiceFacade {

    private IZoneService zoneService;
    private ISubscriptionService subscriptionService;

    public ServiceFacade() {
        RepositoryFacade repositoryFacade = new RepositoryFacade();
        IPercentageAlgorithm algorithm = new PercentageAlgorithm();
        this.zoneService = new ZoneService(repositoryFacade, algorithm);
        this.subscriptionService = new SubscriptionService(repositoryFacade);
    }

    public IZoneService getZoneService() {
        return zoneService;
    }

    public ISubscriptionService getSubscriptionService() {
        return subscriptionService;
    }
}
