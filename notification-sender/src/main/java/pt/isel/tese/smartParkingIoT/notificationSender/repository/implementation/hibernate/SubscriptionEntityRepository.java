package pt.isel.tese.smartParkingIoT.notificationSender.repository.implementation.hibernate;

import pt.isel.tese.smartParkingIoT.notificationSender.repository.implementation.hibernate.dto.SubscriptionEntityDTO;
import pt.isel.tese.smartParkingIoT.notificationSender.repository.implementation.hibernate.mapper.SubscriptionEntityRepositoryMapper;

public class SubscriptionEntityRepository extends BaseRepository<SubscriptionEntityDTO, SubscriptionEntityDTO> {

    public SubscriptionEntityRepository() {
        super(SubscriptionEntityDTO.class, new SubscriptionEntityRepositoryMapper(), "hibernate-subscription-db.cfg.xml");
    }
}
