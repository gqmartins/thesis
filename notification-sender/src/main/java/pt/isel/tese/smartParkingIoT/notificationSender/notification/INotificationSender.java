package pt.isel.tese.smartParkingIoT.notificationSender.notification;

public interface INotificationSender {

    void sendNotifications();
}
