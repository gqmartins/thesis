package pt.isel.tese.smartParkingIoT.notificationSender.logic.manager;

import pt.isel.tese.smartParkingIoT.notificationSender.model.Zone;
import pt.isel.tese.smartParkingIoT.notificationSender.repository.RepositoryFacade;

public class PercentageAlgorithm implements IPercentageAlgorithm {

    @Override
    public int calculate(RepositoryFacade repositoryFacade, int zone) {
        Zone currentZone = repositoryFacade.getZoneRepository().read(zone);
        return (currentZone.getCurrentCount() * 100) / currentZone.getMaxCount();
    }
}
