package pt.isel.tese.smartParkingIoT.notificationSender.repository.implementation.hibernate;

import pt.isel.tese.smartParkingIoT.notificationSender.model.Subscription;
import pt.isel.tese.smartParkingIoT.notificationSender.model.SubscriptionEntity;
import pt.isel.tese.smartParkingIoT.notificationSender.repository.contract.ISubscriptionRepository;
import pt.isel.tese.smartParkingIoT.notificationSender.repository.implementation.hibernate.dto.SubscriptionDTO;
import pt.isel.tese.smartParkingIoT.notificationSender.repository.implementation.hibernate.mapper.SubscriptionEntityRepositoryMapper;
import pt.isel.tese.smartParkingIoT.notificationSender.repository.implementation.hibernate.mapper.SubscriptionRepositoryMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SubscriptionRepository extends BaseRepository<Subscription, SubscriptionDTO> implements ISubscriptionRepository {

    private SubscriptionEntityRepository subscriptionEntityRepository;

    public SubscriptionRepository() {
        super(SubscriptionDTO.class, new SubscriptionRepositoryMapper(), "hibernate-subscription-db.cfg.xml");
        this.subscriptionEntityRepository = new SubscriptionEntityRepository();
    }

    @Override
    public List<Subscription> read() {
        List<Subscription> subscriptions = super.read();
        subscriptions.forEach(subscription -> {
            List<SubscriptionEntity> subscriptionEntities = this.readEntities(subscription.getId());
            subscription.setSubscriptionEntities(subscriptionEntities);
        });
        return subscriptions;
    }

    @Override
    public Subscription read(int id) {
        Subscription subscription = super.read(id);
        List<SubscriptionEntity> subscriptionEntities = this.readEntities(subscription.getId());
        subscription.setSubscriptionEntities(subscriptionEntities);
        return subscription;
    }

    private List<SubscriptionEntity> readEntities(int subscriptionId) {
        return subscriptionEntityRepository
                .read()
                .stream()
                .filter(dto -> dto.getSubscriptionId() == subscriptionId)
                .map(SubscriptionEntityRepositoryMapper::transformToSubscriptionEntity)
                .collect(Collectors.toList());
    }
}
