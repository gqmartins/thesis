package pt.isel.tese.smartParkingIoT.notificationSender.model;

import pt.isel.tese.smartParkingIoT.notificationSender.model.util.GeoJsonPolygon;

public class Zone extends Model {

    private int zoneId;
    private String name;
    private int maxCount;
    private int currentCount;
    private int occupiedPercentage;
    private GeoJsonPolygon perimeter;

    public Zone(int zoneId, String name, int maxCount, int currentCount, int occupiedPercentage, GeoJsonPolygon perimeter) {
        super("zone-" + zoneId, "Zone");
        this.zoneId = zoneId;
        this.name = name;
        this.maxCount = maxCount;
        this.currentCount = currentCount;
        this.occupiedPercentage = occupiedPercentage;
        this.perimeter = perimeter;
    }

    public int getZoneId() {
        return zoneId;
    }

    public void setZoneId(int zoneId) {
        this.zoneId = zoneId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(int maxCount) {
        this.maxCount = maxCount;
    }

    public int getCurrentCount() {
        return currentCount;
    }

    public void setCurrentCount(int currentCount) {
        this.currentCount = currentCount;
    }

    public int getOccupiedPercentage() {
        return occupiedPercentage;
    }

    public void setOccupiedPercentage(int occupiedPercentage) {
        this.occupiedPercentage = occupiedPercentage;
    }

    public GeoJsonPolygon getPerimeter() {
        return perimeter;
    }

    public void setPerimeter(GeoJsonPolygon perimeter) {
        this.perimeter = perimeter;
    }
}
