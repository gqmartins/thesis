package pt.isel.tese.smartParkingIoT.notificationSender.repository.contract;

import pt.isel.tese.smartParkingIoT.notificationSender.model.Subscription;

import java.util.List;

public interface ISubscriptionRepository {

    List<Subscription> read();
    Subscription read(int id);
    void update(Subscription subscription);
}
