package pt.isel.tese.smartParkingIoT.notificationSender.model;

import pt.isel.tese.smartParkingIoT.notificationSender.model.util.GeoJsonPolygon;

public class SubscriptionEntity {

    private int id;
    private String entityId;
    private int subscriptionId;

    public SubscriptionEntity(String entityId) {
        this.entityId = entityId;
    }

    public SubscriptionEntity(int id, String entityId, int subscriptionId) {
        this(entityId);
        this.id = id;
        this.subscriptionId = subscriptionId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public int getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(int subscriptionId) {
        this.subscriptionId = subscriptionId;
    }
}
