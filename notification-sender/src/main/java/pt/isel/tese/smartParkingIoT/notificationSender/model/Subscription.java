package pt.isel.tese.smartParkingIoT.notificationSender.model;

import java.util.Date;
import java.util.List;

public class Subscription {

    private int id;
    private String status;
    private String url;
    private Date expires;
    private List<SubscriptionEntity> subscriptionEntities;

    public Subscription(int id, String status, String url, Date expires, List<SubscriptionEntity> subscriptionEntities) {
        this.id = id;
        this.status = status;
        this.url = url;
        this.expires = expires;
        this.subscriptionEntities = subscriptionEntities;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getExpires() {
        return expires;
    }

    public void setExpires(Date expires) {
        this.expires = expires;
    }

    public List<SubscriptionEntity> getSubscriptionEntities() {
        return subscriptionEntities;
    }

    public void setSubscriptionEntities(List<SubscriptionEntity> subscriptionEntities) {
        this.subscriptionEntities = subscriptionEntities;
    }
}
