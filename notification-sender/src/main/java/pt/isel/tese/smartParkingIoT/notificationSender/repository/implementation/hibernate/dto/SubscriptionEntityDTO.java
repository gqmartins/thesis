package pt.isel.tese.smartParkingIoT.notificationSender.repository.implementation.hibernate.dto;

import javax.persistence.*;

@Entity
@Table(name = "Entity")
public class SubscriptionEntityDTO {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "entity_id")
    private String entityId;

    @Column(name = "subscription_id")
    private int subscriptionId;

    public SubscriptionEntityDTO() { }

    public SubscriptionEntityDTO(int id, String entityId, int subscriptionId) {
        this.id = id;
        this.entityId = entityId;
        this.subscriptionId = subscriptionId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public int getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(int subscriptionId) {
        this.subscriptionId = subscriptionId;
    }
}
