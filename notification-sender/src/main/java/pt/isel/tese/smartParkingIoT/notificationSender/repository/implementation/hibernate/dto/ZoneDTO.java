package pt.isel.tese.smartParkingIoT.notificationSender.repository.implementation.hibernate.dto;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "Zone")
public class ZoneDTO {

    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "max_count")
    private int maxCount;

    @Column(name = "current_count")
    private int currentCount;

    @Column(name = "perimeter")
    private String perimeter;

    public ZoneDTO() { }

    public ZoneDTO(int id, String name, int maxCount, int currentCount, String perimeter) {
        this.id = id;
        this.name = name;
        this.maxCount = maxCount;
        this.currentCount = currentCount;
        this.perimeter = perimeter;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(int maxCount) {
        this.maxCount = maxCount;
    }

    public int getCurrentCount() {
        return currentCount;
    }

    public void setCurrentCount(int currentCount) {
        this.currentCount = currentCount;
    }

    public String getPerimeter() {
        return perimeter;
    }

    public void setPerimeter(String perimeter) {
        this.perimeter = perimeter;
    }
}
