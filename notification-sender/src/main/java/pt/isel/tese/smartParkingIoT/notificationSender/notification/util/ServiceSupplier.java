package pt.isel.tese.smartParkingIoT.notificationSender.notification.util;

import pt.isel.tese.smartParkingIoT.notificationSender.logic.ServiceFacade;
import pt.isel.tese.smartParkingIoT.notificationSender.logic.contract.IBaseService;

import java.util.HashMap;
import java.util.Map;

public class ServiceSupplier {

    private final Map<String, IBaseService> serviceMap = new HashMap<>();

    public ServiceSupplier(ServiceFacade serviceFacade) {
        this.serviceMap.put("zone", serviceFacade.getZoneService());
    }

    public IBaseService getService(String key) {
        return serviceMap.get(key);
    }
}
