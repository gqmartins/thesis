package pt.isel.tese.smartParkingIoT.notificationSender;

import pt.isel.tese.smartParkingIoT.notificationSender.logic.ServiceFacade;
import pt.isel.tese.smartParkingIoT.notificationSender.notification.HttpNotificationSender;
import pt.isel.tese.smartParkingIoT.notificationSender.notification.INotificationSender;
import pt.isel.tese.smartParkingIoT.notificationSender.notification.mapper.MapperFacade;

public class App {

    private static final int MINUTES_TO_MILLISECONDS = 60000;

    public static void main(String[] args) throws InterruptedException {
        int timeToSleep = Integer.parseInt(args[0]) * MINUTES_TO_MILLISECONDS;
        INotificationSender notificationSender = new HttpNotificationSender(new ServiceFacade(), new MapperFacade());
        while(true) {
            notificationSender.sendNotifications();
            Thread.sleep(timeToSleep);
        }
    }
}
