package pt.isel.tese.smartParkingIoT.notificationSender.notification.model.util;

import java.util.List;

public class GeoJsonPolygon {

    private String type = "Polygon";
    private List<List<List<Float>>> coordinates;

    public GeoJsonPolygon(List<List<List<Float>>> coordinates) {
        this.coordinates = coordinates;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<List<List<Float>>> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<List<List<Float>>> coordinates) {
        this.coordinates = coordinates;
    }
}
