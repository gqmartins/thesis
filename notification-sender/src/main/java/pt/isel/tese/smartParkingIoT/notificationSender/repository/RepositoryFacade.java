package pt.isel.tese.smartParkingIoT.notificationSender.repository;

import pt.isel.tese.smartParkingIoT.notificationSender.repository.contract.ISubscriptionRepository;
import pt.isel.tese.smartParkingIoT.notificationSender.repository.contract.IZoneRepository;
import pt.isel.tese.smartParkingIoT.notificationSender.repository.implementation.hibernate.SubscriptionRepository;
import pt.isel.tese.smartParkingIoT.notificationSender.repository.implementation.hibernate.ZoneRepository;

public class RepositoryFacade {

    private ISubscriptionRepository subscriptionRepository;
    private IZoneRepository zoneRepository;

    public RepositoryFacade() {
        this.subscriptionRepository = new SubscriptionRepository();
        this.zoneRepository = new ZoneRepository();
    }

    public ISubscriptionRepository getSubscriptionRepository() {
        return subscriptionRepository;
    }

    public IZoneRepository getZoneRepository() {
        return zoneRepository;
    }
}
