package pt.isel.tese.smartParkingIoT.notificationSender.logic.implementation;

import pt.isel.tese.smartParkingIoT.notificationSender.logic.contract.IZoneService;
import pt.isel.tese.smartParkingIoT.notificationSender.logic.manager.IPercentageAlgorithm;
import pt.isel.tese.smartParkingIoT.notificationSender.model.Model;
import pt.isel.tese.smartParkingIoT.notificationSender.model.Zone;
import pt.isel.tese.smartParkingIoT.notificationSender.repository.RepositoryFacade;
import pt.isel.tese.smartParkingIoT.notificationSender.repository.contract.IZoneRepository;
import pt.isel.tese.smartParkingIoT.notificationSender.utils.ListUtils;

import java.util.HashMap;
import java.util.List;

public class ZoneService implements IZoneService {

    private IZoneRepository zoneRepository;
    private IPercentageAlgorithm percentageAlgorithm;
    private RepositoryFacade repositoryFacade;

    public ZoneService(RepositoryFacade repositoryFacade, IPercentageAlgorithm percentageAlgorithm) {
        this.zoneRepository = repositoryFacade.getZoneRepository();
        this.percentageAlgorithm = percentageAlgorithm;
        this.repositoryFacade = repositoryFacade;
    }

    @Override
    public List<Model> read() {
        List<Zone> zones = zoneRepository.read();
        return ListUtils.mapList(zones, this::addPercentageToZone);
    }

    @Override
    public Zone read(int id) {
        Zone zone = zoneRepository.read(id);
        return addPercentageToZone(zone);
    }

    private Zone addPercentageToZone(Zone zone) {
        int occupiedPercentage = percentageAlgorithm.calculate(repositoryFacade, zone.getZoneId());
        zone.setOccupiedPercentage(occupiedPercentage);
        return zone;
    }
}
