package pt.isel.tese.smartParkingIoT.notificationSender.repository.contract;

import pt.isel.tese.smartParkingIoT.notificationSender.model.Zone;

import java.util.List;

public interface IZoneRepository {

    List<Zone> read();
    Zone read(int id);
}
