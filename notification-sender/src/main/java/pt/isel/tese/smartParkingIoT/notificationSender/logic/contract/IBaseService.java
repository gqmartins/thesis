package pt.isel.tese.smartParkingIoT.notificationSender.logic.contract;

import pt.isel.tese.smartParkingIoT.notificationSender.model.Model;

import java.util.List;

public interface IBaseService {

    List<Model> read();
    Model read(int id);
}
