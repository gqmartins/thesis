package pt.isel.tese.smartParkingIoT.notificationSender.logic.contract;

import pt.isel.tese.smartParkingIoT.notificationSender.model.Model;
import pt.isel.tese.smartParkingIoT.notificationSender.model.Subscription;

import java.util.List;

public interface ISubscriptionService {

    List<Subscription> read();
    Subscription read(int id);
    void updateExpiredSubscriptions();
}
