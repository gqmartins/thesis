package pt.isel.tese.smartParkingIoT.notificationSender.repository.implementation.hibernate.mapper;

import com.google.gson.Gson;
import pt.isel.tese.smartParkingIoT.notificationSender.model.Zone;
import pt.isel.tese.smartParkingIoT.notificationSender.model.util.GeoJsonPolygon;
import pt.isel.tese.smartParkingIoT.notificationSender.repository.implementation.hibernate.dto.ZoneDTO;

public class ZoneRepositoryMapper implements IRepositoryMapper<Zone, ZoneDTO> {

    private final Gson gson = new Gson();

    @Override
    public Zone transformToObject(ZoneDTO dto) {
        GeoJsonPolygon perimeter = gson.fromJson(dto.getPerimeter(), GeoJsonPolygon.class);
        return new Zone(dto.getId(), dto.getName(), dto.getMaxCount(), dto.getCurrentCount(), 0,
                perimeter);
    }

    @Override
    public ZoneDTO transformToDto(Zone object) {
        String perimeter = gson.toJson(object.getPerimeter());
        return new ZoneDTO(object.getZoneId(), object.getName(), object.getMaxCount(), object.getCurrentCount(), perimeter);
    }

    @Override
    public int getIdentifier(Zone model) {
        return model.getZoneId();
    }
}
