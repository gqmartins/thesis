package pt.isel.tese.smartParkingIoT.notificationSender.notification.mapper;

public class MapperFacade {

    private ZoneNotificationMapper zoneNotificationMapper;

    public MapperFacade() {
        this.zoneNotificationMapper = new ZoneNotificationMapper();
    }

    public ZoneNotificationMapper getZoneNotificationMapper() {
        return zoneNotificationMapper;
    }
}
