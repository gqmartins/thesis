package pt.isel.tese.smartParkingIoT.notificationSender.notification.model;

import pt.isel.tese.smartParkingIoT.notificationSender.notification.model.util.GeoJsonPolygon;

public class ZoneNotification extends Notification {

    private String name;
    private int occupiedPercentage;
    private GeoJsonPolygon location;

    public ZoneNotification(String id, String name, int occupiedPercentage, GeoJsonPolygon location) {
        super(id, "Zone");
        this.name = name;
        this.occupiedPercentage = occupiedPercentage;
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOccupiedPercentage() {
        return occupiedPercentage;
    }

    public void setOccupiedPercentage(int occupiedPercentage) {
        this.occupiedPercentage = occupiedPercentage;
    }

    public GeoJsonPolygon getLocation() {
        return location;
    }

    public void setLocation(GeoJsonPolygon location) {
        this.location = location;
    }
}
