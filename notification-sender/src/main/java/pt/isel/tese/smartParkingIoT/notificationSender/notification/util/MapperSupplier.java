package pt.isel.tese.smartParkingIoT.notificationSender.notification.util;

import pt.isel.tese.smartParkingIoT.notificationSender.notification.mapper.INotificationMapper;
import pt.isel.tese.smartParkingIoT.notificationSender.notification.mapper.MapperFacade;

import java.util.HashMap;
import java.util.Map;

public class MapperSupplier {

    private final Map<String, INotificationMapper> mapperMap = new HashMap<>();

    public MapperSupplier(MapperFacade mapperFacade) {
        this.mapperMap.put("zone", mapperFacade.getZoneNotificationMapper());
    }

    public INotificationMapper getMapper(String key) {
        return this.mapperMap.get(key);
    }
}
