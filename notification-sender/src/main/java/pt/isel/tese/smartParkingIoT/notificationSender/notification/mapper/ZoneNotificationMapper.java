package pt.isel.tese.smartParkingIoT.notificationSender.notification.mapper;

import pt.isel.tese.smartParkingIoT.notificationSender.model.Model;
import pt.isel.tese.smartParkingIoT.notificationSender.model.Zone;
import pt.isel.tese.smartParkingIoT.notificationSender.notification.model.Notification;
import pt.isel.tese.smartParkingIoT.notificationSender.notification.model.ZoneNotification;
import pt.isel.tese.smartParkingIoT.notificationSender.notification.model.util.GeoJsonPolygon;

public class ZoneNotificationMapper implements INotificationMapper {

    @Override
    public Notification transformToNotification(Model model) {
        Zone zone = (Zone) model;
        GeoJsonPolygon location = new GeoJsonPolygon(((Zone) model).getPerimeter().getCoordinates());
        return new ZoneNotification(zone.getId(), zone.getName(), zone.getOccupiedPercentage(), location);
    }
}
