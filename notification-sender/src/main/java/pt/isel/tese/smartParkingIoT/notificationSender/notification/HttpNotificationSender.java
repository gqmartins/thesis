package pt.isel.tese.smartParkingIoT.notificationSender.notification;

import com.google.gson.Gson;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import pt.isel.tese.smartParkingIoT.notificationSender.logic.ServiceFacade;
import pt.isel.tese.smartParkingIoT.notificationSender.logic.contract.ISubscriptionService;
import pt.isel.tese.smartParkingIoT.notificationSender.logic.contract.IZoneService;
import pt.isel.tese.smartParkingIoT.notificationSender.model.Model;
import pt.isel.tese.smartParkingIoT.notificationSender.model.Subscription;
import pt.isel.tese.smartParkingIoT.notificationSender.model.util.SubscriptionStatus;
import pt.isel.tese.smartParkingIoT.notificationSender.notification.mapper.MapperFacade;
import pt.isel.tese.smartParkingIoT.notificationSender.notification.model.Notification;
import pt.isel.tese.smartParkingIoT.notificationSender.notification.util.MapperSupplier;
import pt.isel.tese.smartParkingIoT.notificationSender.notification.util.ServiceSupplier;
import pt.isel.tese.smartParkingIoT.notificationSender.repository.contract.ISubscriptionRepository;
import pt.isel.tese.smartParkingIoT.notificationSender.repository.contract.IZoneRepository;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class HttpNotificationSender implements INotificationSender {

    private ISubscriptionService subscriptionService;
    private ServiceSupplier serviceSupplier;
    private MapperSupplier mapperSupplier;
    private Gson gson;

    public HttpNotificationSender(ServiceFacade serviceFacade, MapperFacade mapperFacade) {
        this.subscriptionService = serviceFacade.getSubscriptionService();
        this.serviceSupplier = new ServiceSupplier(serviceFacade);
        this.mapperSupplier = new MapperSupplier(mapperFacade);
        this.gson = new Gson();
    }

    @Override
    public void sendNotifications() {
        subscriptionService.updateExpiredSubscriptions();
        List<Subscription> validSubscriptions = subscriptionService.read();
        validSubscriptions.forEach(subscription -> {
            subscription.getSubscriptionEntities().stream().map(entity -> {
                String entityId = entity.getEntityId();
                String type = entityId.substring(0, entityId.lastIndexOf("-"));
                int id = Integer.parseInt(entityId.substring(entityId.lastIndexOf("-") + 1));
                Model model = serviceSupplier.getService(type).read(id);
                return mapperSupplier.getMapper(type).transformToNotification(model);
            }).forEach(notification -> sendNotification(notification, subscription.getUrl()));
        });
    }

    private void sendNotification(Notification notification, String url) {
        try {
            HttpPost request = new HttpPost(url);
            String body = gson.toJson(notification);
            request.setEntity(new StringEntity(body));
            request.setHeader("Accept", "application/json");
            request.setHeader("Content-type", "application/json");
            try(CloseableHttpResponse response = HttpClients.createDefault().execute(request)) {
                int statusCode = response.getStatusLine().getStatusCode();
                System.out.println("request sent to " + url + " for entity " + notification.getId() + " has status code " + statusCode);
            }
        } catch (Exception e) {
            throw new RuntimeException("Error making the request.");
        }
    }
}
