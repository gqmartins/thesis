package pt.isel.tese.smartParkingIoT.notificationSender.notification.mapper;

import pt.isel.tese.smartParkingIoT.notificationSender.model.Model;
import pt.isel.tese.smartParkingIoT.notificationSender.notification.model.Notification;

public interface INotificationMapper {

    Notification transformToNotification(Model model);
}
