package pt.isel.tese.smartParkingIoT.notificationSender.repository.implementation.hibernate.mapper;

public interface IRepositoryMapper<T, R> {

    T transformToObject(R dto);
    R transformToDto(T model);
    int getIdentifier(T model);
}
