package pt.isel.tese.smartParkingIoT.notificationSender.repository.implementation.hibernate;

import pt.isel.tese.smartParkingIoT.notificationSender.model.Zone;
import pt.isel.tese.smartParkingIoT.notificationSender.repository.contract.IZoneRepository;
import pt.isel.tese.smartParkingIoT.notificationSender.repository.implementation.hibernate.dto.ZoneDTO;
import pt.isel.tese.smartParkingIoT.notificationSender.repository.implementation.hibernate.mapper.ZoneRepositoryMapper;

public class ZoneRepository extends BaseRepository<Zone, ZoneDTO> implements IZoneRepository {

    public ZoneRepository() {
        super(ZoneDTO.class, new ZoneRepositoryMapper(), "hibernate-smartparkingiot-db.cfg.xml");
    }
}
