package pt.isel.tese.smartParkingIoT.notificationSender.logic.implementation;

import pt.isel.tese.smartParkingIoT.notificationSender.logic.contract.ISubscriptionService;
import pt.isel.tese.smartParkingIoT.notificationSender.model.Model;
import pt.isel.tese.smartParkingIoT.notificationSender.model.Subscription;
import pt.isel.tese.smartParkingIoT.notificationSender.model.util.SubscriptionStatus;
import pt.isel.tese.smartParkingIoT.notificationSender.repository.RepositoryFacade;
import pt.isel.tese.smartParkingIoT.notificationSender.repository.contract.ISubscriptionRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class SubscriptionService implements ISubscriptionService {

    private ISubscriptionRepository subscriptionRepository;

    public SubscriptionService(RepositoryFacade repositoryFacade) {
        this.subscriptionRepository = repositoryFacade.getSubscriptionRepository();
    }

    @Override
    public List<Subscription> read() {
        return subscriptionRepository.read();
    }

    @Override
    public Subscription read(int id) {
        return subscriptionRepository.read(id);
    }

    @Override
    public void updateExpiredSubscriptions() {
        final Date currentDate = new Date();
        subscriptionRepository
                .read()
                .stream()
                .filter(subscription -> subscription.getExpires().before(currentDate))
                .forEach(subscription -> {
                    subscription.setStatus(SubscriptionStatus.EXPIRED);
                    subscriptionRepository.update(subscription);
                });
    }
}
