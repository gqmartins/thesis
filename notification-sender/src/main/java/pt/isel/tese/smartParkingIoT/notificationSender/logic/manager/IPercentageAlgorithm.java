package pt.isel.tese.smartParkingIoT.notificationSender.logic.manager;

import pt.isel.tese.smartParkingIoT.notificationSender.repository.RepositoryFacade;

public interface IPercentageAlgorithm {

    int calculate(RepositoryFacade repositoryFacade, int zone);
}
