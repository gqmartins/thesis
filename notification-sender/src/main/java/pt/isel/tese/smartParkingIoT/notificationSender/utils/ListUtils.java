package pt.isel.tese.smartParkingIoT.notificationSender.utils;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ListUtils {

    public static <T,R> List<R> mapList(List<T> list, Function<T, R> mapper) {
        return list
                .stream()
                .map(mapper)
                .collect(Collectors.toList());
    }
}
