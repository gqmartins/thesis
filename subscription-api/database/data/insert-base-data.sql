BEGIN;

INSERT INTO Type (id, type) VALUES (1, 'Zone');
INSERT INTO Attribute(id, name, type, type_id) VALUES (1, 'name', 'Number', 1),
											 		  (2, 'occupiedPercentage', 'Number', 1),
										 	          (3, 'location', 'geo:polygon', 1);

COMMIT;