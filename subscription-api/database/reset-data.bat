SET DBNAME=subscriptiondb
SET USERNAME=subscriptiondb_user
SET USERPASSWORD=subscriptiondb_password

psql -h localhost -p 5432 -d %DBNAME% -U postgres -f data\delete-data.sql
psql -h localhost -p 5432 -d %DBNAME% -U postgres -f data\insert-base-data.sql

pause