BEGIN;

CREATE TABLE Type (
	id INT NOT NULL,
	type TEXT NOT NULL,

	CONSTRAINT PK_Type PRIMARY KEY (id)
);

CREATE TABLE Attribute (
	id INT NOT NULL,
	name TEXT NOT NULL,
	type TEXT NOT NULL,
	type_id INT NOT NULL,

	CONSTRAINT PK_Attribute PRIMARY KEY (id),
	CONSTRAINT FK_Attribute_Entity FOREIGN KEY (type_id) REFERENCES Type (id)
);

CREATE TABLE AppUser (
	id SERIAL,
	username TEXT NOT NULL UNIQUE,
	password TEXT NOT NULL,
	api_key_prefix TEXT NULL,
	api_key TEXT NULL,
	created TIMESTAMP NOT NULL,
	modified TIMESTAMP NOT NULL,

	CONSTRAINT PK_AppUser PRIMARY KEY (id)
);

CREATE TABLE Subscription (
	id SERIAL,
	status TEXT NOT NULL,
	url TEXT NOT NULL,
	expires TIMESTAMP NOT NULL,
	user_id INT NOT NULL,

	CONSTRAINT PK_Subscription PRIMARY KEY (id),
	CONSTRAINT FK_Subscription_AppUser FOREIGN KEY (user_id) REFERENCES AppUser (id)
);

CREATE TABLE Entity (
	id SERIAL,
	entity_id TEXT NOT NULL,
	subscription_id INT NOT NULL,

	CONSTRAINT PK_Entity PRIMARY KEY (id),
	CONSTRAINT FK_Entity_Subscription FOREIGN KEY (subscription_id) REFERENCES Subscription (id)
);

CREATE TABLE Request (
	id SERIAL,
	user_id INT NOT NULL,
	created TIMESTAMP NOT NULL,

	CONSTRAINT PK_Request PRIMARY KEY (id),
	CONSTRAINT FK_Request_AppUser FOREIGN KEY (user_id) REFERENCES AppUser (id)
);

COMMIT;