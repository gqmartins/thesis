package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResourceResponse {

    @JsonProperty("entities_url")
    private String entitiesUrl = null;

    @JsonProperty("types_url")
    private String typesUrl = null;

    @JsonProperty("subscriptions_url")
    private String subscriptionsUrl = null;

    public ResourceResponse(String entitiesUrl, String typesUrl, String subscriptionsUrl) {
        this.entitiesUrl = entitiesUrl;
        this.typesUrl = typesUrl;
        this.subscriptionsUrl = subscriptionsUrl;
    }

    public String getEntitiesUrl() {
        return entitiesUrl;
    }

    public void setEntitiesUrl(String entitiesUrl) {
        this.entitiesUrl = entitiesUrl;
    }

    public String getTypesUrl() {
        return typesUrl;
    }

    public void setTypesUrl(String typesUrl) {
        this.typesUrl = typesUrl;
    }

    public String getSubscriptionsUrl() {
        return subscriptionsUrl;
    }

    public void setSubscriptionsUrl(String subscriptionsUrl) {
        this.subscriptionsUrl = subscriptionsUrl;
    }
}
