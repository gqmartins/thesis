package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.contracts;

import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.Type;

public interface ITypeRepository extends IBaseRepository<Type> { }
