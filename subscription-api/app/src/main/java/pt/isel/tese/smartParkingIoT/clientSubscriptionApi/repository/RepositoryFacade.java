package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository;

import org.springframework.stereotype.Component;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.contracts.*;

@Component
public class RepositoryFacade {

    private IUserRepository userRepository;
    private IZoneRepository zoneRepository;
    private IRequestRepository requestRepository;
    private ITypeRepository typeRepository;
    private IAttributeRepository attributeRepository;
    private ISubscriptionRepository subscriptionRepository;

    public RepositoryFacade(IUserRepository userRepository, IZoneRepository zoneRepository, IRequestRepository requestRepository,
                            ITypeRepository typeRepository, IAttributeRepository attributeRepository, ISubscriptionRepository subscriptionRepository) {
        this.userRepository = userRepository;
        this.zoneRepository = zoneRepository;
        this.requestRepository = requestRepository;
        this.typeRepository = typeRepository;
        this.attributeRepository = attributeRepository;
        this.subscriptionRepository = subscriptionRepository;
    }

    public IUserRepository getUserRepository() {
        return userRepository;
    }

    public IZoneRepository getZoneRepository() {
        return zoneRepository;
    }

    public IRequestRepository getRequestRepository() {
        return requestRepository;
    }

    public ITypeRepository getTypeRepository() {
        return typeRepository;
    }

    public IAttributeRepository getAttributeRepository() {
        return attributeRepository;
    }

    public ISubscriptionRepository getSubscriptionRepository() {
        return subscriptionRepository;
    }
}
