package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.request.subscription.SubscriptionEntityRequest;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.request.subscription.SubscriptionNotificationRequest;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.request.subscription.SubscriptionNotificationUrlRequest;

public class SubscriptionRequest {

    @JsonProperty("subject")
    private SubscriptionEntityRequest subject = null;

    @JsonProperty("notification")
    private SubscriptionNotificationRequest notification = null;

    @JsonProperty("expires")
    private String expires;

    @JsonProperty("status")
    private String status;

    public SubscriptionRequest() { }

    public SubscriptionRequest(SubscriptionEntityRequest subject, SubscriptionNotificationRequest notification, String expires, String status) {
        this.subject = subject;
        this.notification = notification;
        this.expires = expires;
        this.status = status;
    }

    /**
     * Get subject
     * @return subject
     **/
    @ApiModelProperty(example = "", required = true, value = "")
    public SubscriptionEntityRequest getSubject() {
        return subject;
    }

    public void setSubject(SubscriptionEntityRequest subject) {
        this.subject = subject;
    }

    /**
     * Get notification
     * @return notification
     **/
    @ApiModelProperty(example = "", required = true, value = "")
    public SubscriptionNotificationRequest getNotification() {
        return notification;
    }

    public void setNotification(SubscriptionNotificationRequest notification) {
        this.notification = notification;
    }

    /**
     * Get expires
     * @return expires
     **/
    @ApiModelProperty(example = "2019-12-14T10:40:52", required = true, value = "")
    public String getExpires() {
        return expires;
    }

    public void setExpires(String expires) {
        this.expires = expires;
    }

    /**
     * Get status
     * @return status
     **/
    @ApiModelProperty(example = "active", required = true, value = "")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
