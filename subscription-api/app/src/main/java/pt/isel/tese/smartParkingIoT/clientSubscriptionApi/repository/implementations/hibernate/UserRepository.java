package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate;

import org.springframework.stereotype.Repository;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.User;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.contracts.IUserRepository;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.dto.UserDTO;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.mappers.UserRepositoryMapper;

@Repository
public class UserRepository extends BaseRepository<User, UserDTO> implements IUserRepository {

    public UserRepository() {
        super(UserDTO.class, new UserRepositoryMapper(), "hibernate-subscription-db.cfg.xml");
    }
}
