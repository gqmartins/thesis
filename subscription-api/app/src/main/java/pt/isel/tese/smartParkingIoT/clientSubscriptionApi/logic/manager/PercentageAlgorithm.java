package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.manager;

import org.springframework.stereotype.Component;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.Zone;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.RepositoryFacade;

@Component
public class PercentageAlgorithm implements IPercentageAlgorithm {

    @Override
    public int calculate(RepositoryFacade repositoryFacade, int zone) {
        Zone currentZone = repositoryFacade.getZoneRepository().read(zone);
        return (currentZone.getCurrentCount() * 100) / currentZone.getMaxCount();
    }
}
