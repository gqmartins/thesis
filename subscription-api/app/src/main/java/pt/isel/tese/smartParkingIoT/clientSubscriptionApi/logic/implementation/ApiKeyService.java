package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.implementation;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.config.AppConfiguration;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.contract.IApiKeyService;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.exceptions.ApiKeyException;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.Request;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.User;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.RepositoryFacade;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.contracts.IRequestRepository;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.contracts.IUserRepository;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ApiKeyService implements IApiKeyService {

    private IUserRepository userRepository;
    private IRequestRepository requestRepository;
    private AppConfiguration appConfiguration;
    private static final int API_KEY_PREFIX_LENGTH = 8;
    private static final int API_KEY_LENGTH = 64;
    private static final String API_KEY_VALUES = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-_";

    public ApiKeyService(RepositoryFacade repositoryFacade, AppConfiguration appConfiguration) {
        this.userRepository = repositoryFacade.getUserRepository();
        this.requestRepository = repositoryFacade.getRequestRepository();
        this.appConfiguration = appConfiguration;
    }

    @Override
    public String buildApiKey(int user) {
        User savedUser = userRepository.read(user);
        String apiKeyPrefix = generateRandomString(API_KEY_PREFIX_LENGTH);
        String apiKey = generateRandomString(API_KEY_LENGTH);
        String hashedApiKey = generateHashedApiKey(apiKey);
        savedUser.setApiKeyPrefix(apiKeyPrefix);
        savedUser.setApiKey(hashedApiKey);
        savedUser.setModified(new Date());
        userRepository.update(savedUser);
        return "{" + apiKeyPrefix + "}" + apiKey;
    }

    @Override
    public void validateRequest(String fullApiKey) {
        String prefix = fullApiKey.substring(1, fullApiKey.indexOf("}"));
        String apiKey = fullApiKey.substring(fullApiKey.indexOf("}") + 1);
        User savedUser = findUser(prefix);
        if(!verifyApiKey(apiKey, savedUser.getApiKey())) {
            throw new ApiKeyException("Invalid api key", "The passed api key is no longer valid.");
        }
        long requestsInLastHour = getRequestsInLastHour(savedUser);
        if(requestsInLastHour >= Long.parseLong(appConfiguration.requestsPerHour)) {
            throw new ApiKeyException("Invalid request", "You already exceeded the number of requests per hour.");
        }
        Request request = new Request(savedUser.getId(), new Date());
        this.requestRepository.create(request);
    }

    @Override
    public User findUser(String prefix) {
        List<User> users = userRepository.read(new HashMap<>())
                .stream()
                .filter(user -> user.getApiKeyPrefix().equals(prefix))
                .collect(Collectors.toList());
        if(users.size() == 0) {
            throw new ApiKeyException("Invalid api key", "There's no user with suck api key.");
        }
        return users.get(0);
    }

    private long getRequestsInLastHour(User savedUser) {
        deleteOlderRequests(savedUser);
        return this.requestRepository
                .read(new HashMap<>())
                .stream()
                .filter(request -> request.getUser() == savedUser.getId())
                .count();
    }

    private void deleteOlderRequests(User savedUser) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY, -1);
        final Date currentDate = calendar.getTime();
        this.requestRepository
                .read(new HashMap<>())
                .stream()
                .filter(request -> request.getUser() == savedUser.getId() && request.getCreated().before(currentDate))
                .forEach(this.requestRepository::delete);
    }

    private String generateRandomString(int length) {
        StringBuilder apiKey = new StringBuilder();
        Random random = new Random();
        for(int i = 0; i < length; ++i) {
            char randomCharacter = API_KEY_VALUES.charAt(random.nextInt(API_KEY_VALUES.length()));
            apiKey.append(randomCharacter);
        }
        return apiKey.toString();
    }

    private String generateHashedApiKey(String apiKey) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(apiKey);
    }

    private boolean verifyApiKey(String rawApiKey, String savedApiKey) {
        BCryptPasswordEncoder passwordDecoder = new BCryptPasswordEncoder();
        return passwordDecoder.matches(rawApiKey, savedApiKey);
    }
}
