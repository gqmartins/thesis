package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.implementations;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.config.UriConfiguration;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.contracts.IResourceController;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response.ResourceResponse;

@RestController
public class ResourceController implements IResourceController {

    @Override
    public ResponseEntity<ResourceResponse> readResource() {
        ResourceResponse response = new ResourceResponse(UriConfiguration.ENTITIES_URI, UriConfiguration.TYPES_URI, UriConfiguration.SUBSCRIPTIONS_URI);
        return ResponseEntity.ok(response);
    }
}
