package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate;

import org.springframework.stereotype.Repository;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.Zone;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.contracts.IZoneRepository;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.dto.ZoneDTO;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.mappers.ZoneRepositoryMapper;

@Repository
public class ZoneRepository extends BaseRepository<Zone, ZoneDTO> implements IZoneRepository {

    public ZoneRepository() {
        super(ZoneDTO.class, new ZoneRepositoryMapper(), "hibernate-smartparkingiot-db.cfg.xml");
    }
}
