package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.contracts;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.config.UriConfiguration;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response.ApiKeyResponse;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response.ErrorResponse;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response.ZoneResponse;

import javax.validation.Valid;

public interface IApiKeyController {

    @ApiOperation(value = "returns the api-key for the specified user", nickname = "readApiKey", notes = "returns zone with the specified zone-id", response = ApiKeyResponse.class, tags={ "Api Key" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "api-key to use in other requests", response = ZoneResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "The specified zone does not exist", response = ErrorResponse.class) })
    @GetMapping(value = UriConfiguration.API_KEY_URI, produces = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<ApiKeyResponse> readApiKey(@ApiParam(value = "User info",required=true) @Valid @RequestHeader(value="Authorization") String basicAuth);
}
