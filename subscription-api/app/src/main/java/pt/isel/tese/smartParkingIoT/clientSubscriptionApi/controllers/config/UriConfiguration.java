package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.config;

public class UriConfiguration {

    public static final String RESOURCE_URI = "/";

    public static final String REGISTER_USER_URI = "/register";

    public static final String API_KEY_URI = "/api-key";

    public static final String TYPES_URI = "/types";
    public static final String TYPES_ID_URI = "/types/{type-name}";

    public static final String ENTITIES_URI = "/entities";
    public static final String ENTITIES_ID_URI = "/entities/{entity-id}";

    public static final String SUBSCRIPTIONS_URI = "/subscriptions";
    public static final String SUBSCRIPTIONS_ID_URI = "/subscriptions/{subscription-id}";

    public static final String LOGIN_URI = "/login";
    public static final String LOGOUT_URI = "/logout";
}
