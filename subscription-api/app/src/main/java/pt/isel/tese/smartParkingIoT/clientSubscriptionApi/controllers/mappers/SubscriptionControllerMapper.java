package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.mappers;

import org.springframework.stereotype.Component;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.request.SubscriptionRequest;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.request.subscription.SubscriptionEntityListItemRequest;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.request.subscription.SubscriptionEntityRequest;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.request.subscription.SubscriptionNotificationRequest;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.request.subscription.SubscriptionNotificationUrlRequest;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response.SubscriptionResponse;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.Subscription;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.SubscriptionEntity;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.utils.ListUtils;

import java.time.Instant;
import java.util.Date;
import java.util.List;

@Component
public class SubscriptionControllerMapper {

    public Subscription transformToModel(SubscriptionRequest subscription, int userId) {
        List<SubscriptionEntity> subscriptionEntities = mapToSubscriptionEntities(subscription.getSubject().getEntities());
        String url = subscription.getNotification().getHttp().getUrl();
        Date expires = Date.from(Instant.parse(subscription.getExpires()));
        return new Subscription("active", url, expires, subscriptionEntities, userId);
    }

    public SubscriptionResponse transformToResponse(Subscription subscription) {
        SubscriptionEntityRequest subject = new SubscriptionEntityRequest(mapToSubscriptionListTem(subscription.getSubscriptionEntities()));
        SubscriptionNotificationRequest notification = new SubscriptionNotificationRequest(new SubscriptionNotificationUrlRequest(subscription.getUrl()));
        String expires = subscription.getExpires().toString();
        return new SubscriptionResponse(subscription.getId(), subject, notification, expires, subscription.getStatus());
    }

    private List<SubscriptionEntity> mapToSubscriptionEntities(List<SubscriptionEntityListItemRequest> entities) {
        return ListUtils.mapList(entities, entity -> new SubscriptionEntity(entity.getId()));
    }

    private List<SubscriptionEntityListItemRequest> mapToSubscriptionListTem(List<SubscriptionEntity> entities) {
        return ListUtils.mapList(entities, entity -> {
            String entityId = entity.getEntityId();
            String type = entityId.substring(0, entityId.lastIndexOf("-"));
            return new SubscriptionEntityListItemRequest(entityId, type);
        });
    }
}
