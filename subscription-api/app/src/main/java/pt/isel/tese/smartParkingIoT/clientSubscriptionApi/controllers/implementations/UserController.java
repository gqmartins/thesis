package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.implementations;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.contracts.IUserController;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.mappers.UserControllerMapper;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.request.UserRequest;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.ServiceFacade;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.contract.IUserService;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.User;

import javax.validation.Valid;

@RestController
public class UserController implements IUserController {

    private IUserService userService;
    private UserControllerMapper userMapper;

    public UserController(ServiceFacade serviceFacade, UserControllerMapper userMapper) {
        this.userService = serviceFacade.getUserService();
        this.userMapper = userMapper;
    }

    @Override
    public ResponseEntity<Void> createUser(@Valid @RequestBody UserRequest body) {
        User user = userMapper.transformToModel(body);
        userService.create(user);
        return ResponseEntity.ok().build();
    }
}
