package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.mappers;

import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.SubscriptionEntity;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.dto.SubscriptionEntityDTO;

public class SubscriptionEntityRepositoryMapper implements IRepositoryMapper<SubscriptionEntityDTO, SubscriptionEntityDTO> {

    @Override
    public SubscriptionEntityDTO transformToObject(SubscriptionEntityDTO dto) {
        return dto;
    }

    @Override
    public SubscriptionEntityDTO transformToDto(SubscriptionEntityDTO model) {
        return model;
    }

    @Override
    public int getIdentifier(SubscriptionEntityDTO model) {
        return model.getId();
    }

    public static SubscriptionEntityDTO transformToSubscriptionEntityDTO(int subscriptionId, SubscriptionEntity model) {
        return new SubscriptionEntityDTO(model.getId(), model.getEntityId(), subscriptionId);
    }

    public static SubscriptionEntity transformToSubscriptionEntity(SubscriptionEntityDTO dto) {
        return new SubscriptionEntity(dto.getId(), dto.getEntityId(), dto.getSubscriptionId());
    }
}
