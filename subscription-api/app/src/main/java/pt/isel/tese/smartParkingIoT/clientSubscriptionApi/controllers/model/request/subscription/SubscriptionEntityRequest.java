package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.request.subscription;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;

public class SubscriptionEntityRequest {

    @JsonProperty("entities")
    private List<SubscriptionEntityListItemRequest> entities = new ArrayList<>();

    public SubscriptionEntityRequest() { }

    public SubscriptionEntityRequest(List<SubscriptionEntityListItemRequest> entities) {
        this.entities = entities;
    }

    /**
     * Get entities
     * @return entities
     **/
    @ApiModelProperty(example = "", required = true, value = "")
    public List<SubscriptionEntityListItemRequest> getEntities() {
        return entities;
    }

    public void setEntities(List<SubscriptionEntityListItemRequest> entities) {
        this.entities = entities;
    }
}
