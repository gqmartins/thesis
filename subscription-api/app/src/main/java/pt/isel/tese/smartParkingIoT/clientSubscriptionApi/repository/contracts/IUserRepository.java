package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.contracts;

import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.User;

public interface IUserRepository extends IBaseRepository<User> { }
