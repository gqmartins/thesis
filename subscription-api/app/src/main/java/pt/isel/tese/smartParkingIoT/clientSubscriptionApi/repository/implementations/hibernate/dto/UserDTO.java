package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.dto;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "AppUser")
public class UserDTO {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "api_key_prefix")
    private String apiKeyPrefix;

    @Column(name = "api_key")
    private String apiKey;

    @Column(name = "created")
    private Date created;

    @Column(name = "modified")
    private Date modified;

    public UserDTO() { }

    public UserDTO(int id, String username, String password, String apiKeyPrefix, String apiKey, Date created, Date modified) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.apiKeyPrefix = apiKeyPrefix;
        this.apiKey = apiKey;
        this.created = created;
        this.modified = modified;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getApiKeyPrefix() {
        return apiKeyPrefix;
    }

    public void setApiKeyPrefix(String apiKeyPrefix) {
        this.apiKeyPrefix = apiKeyPrefix;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }
}
