package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.implementation;

import org.springframework.stereotype.Service;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.contract.ISubscriptionService;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.exceptions.SubscriptionException;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.Subscription;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.RepositoryFacade;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.contracts.ISubscriptionRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SubscriptionService implements ISubscriptionService {

    private ISubscriptionRepository subscriptionRepository;

    public SubscriptionService(RepositoryFacade repositoryFacade) {
        this.subscriptionRepository = repositoryFacade.getSubscriptionRepository();
    }

    @Override
    public int create(Subscription object) {
        return subscriptionRepository.create(object);
    }

    @Override
    public List<Subscription> read(int userId) {
        return subscriptionRepository.read(new HashMap<>())
                .stream()
                .filter(subscription -> subscription.getUserId() == userId)
                .collect(Collectors.toList());
    }

    @Override
    public Subscription read(int id, int userId) {
        Subscription subscription = subscriptionRepository.read(id);
        if(subscription.getUserId() != userId) {
            throw new SubscriptionException("Invalid request", "You can't read the specified subscription because the subscription belongs to other user.");
        }
        return subscription;
    }

    @Override
    public void delete(int id, int userId) {
        Subscription subscription = read(id, userId);
        subscriptionRepository.delete(subscription);
    }
}
