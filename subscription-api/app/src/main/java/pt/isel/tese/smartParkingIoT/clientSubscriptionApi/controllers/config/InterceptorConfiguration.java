package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.interceptor.SecurityInterceptor;

@Configuration
public class InterceptorConfiguration extends WebMvcConfigurerAdapter {

    private SecurityInterceptor securityInterceptor;
    private AppConfiguration appConfiguration;

    public InterceptorConfiguration(SecurityInterceptor securityInterceptor, AppConfiguration appConfiguration) {
        this.securityInterceptor = securityInterceptor;
        this.appConfiguration = appConfiguration;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        super.addInterceptors(registry);

        if(Boolean.valueOf(appConfiguration.useSecurity)) {
            registry.addInterceptor(securityInterceptor);
        }
    }
}
