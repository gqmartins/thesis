package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.mappers;

import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.Attribute;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.dto.AttributeDTO;

public class AttributeRepositoryMapper implements IRepositoryMapper<Attribute, AttributeDTO> {

    @Override
    public Attribute transformToObject(AttributeDTO dto) {
        return new Attribute(dto.getId(), dto.getName(), dto.getType(), dto.getEntityId());
    }

    @Override
    public AttributeDTO transformToDto(Attribute model) {
        return new AttributeDTO(model.getId(), model.getName(), model.getType(), model.getId());
    }

    @Override
    public int getIdentifier(Attribute model) {
        return model.getId();
    }
}
