package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model;

public class Attribute {

    private int id;
    private String name;
    private String type;
    private int typeId;

    public Attribute(int id, String name, String type, int typeId) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.typeId = typeId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }
}
