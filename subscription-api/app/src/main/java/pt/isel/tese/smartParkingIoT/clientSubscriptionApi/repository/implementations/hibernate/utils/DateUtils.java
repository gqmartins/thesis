package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.utils;

import java.sql.Timestamp;
import java.util.Date;

public class DateUtils {

    public static Timestamp convertDateToTimestamp(Date date) {
        return new Timestamp(date.getTime());
    }
}
