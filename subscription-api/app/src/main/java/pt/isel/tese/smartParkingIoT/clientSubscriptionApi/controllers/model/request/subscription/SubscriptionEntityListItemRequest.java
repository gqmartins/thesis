package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.request.subscription;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

public class SubscriptionEntityListItemRequest {

    @JsonProperty("id")
    private String id;

    @JsonProperty("type")
    private String type;

    public SubscriptionEntityListItemRequest() { }

    public SubscriptionEntityListItemRequest(String id, String type) {
        this.id = id;
        this.type = type;
    }

    /**
     * Get id
     * @return id
     **/
    @ApiModelProperty(example = "zone-1", required = true, value = "")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /**
     * Get type
     * @return type
     **/
    @ApiModelProperty(example = "Zone", required = true, value = "")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
