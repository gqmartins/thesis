package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate;

import org.springframework.stereotype.Repository;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.Subscription;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.SubscriptionEntity;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.contracts.ISubscriptionRepository;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.dto.SubscriptionDTO;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.dto.SubscriptionEntityDTO;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.mappers.SubscriptionEntityRepositoryMapper;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.mappers.SubscriptionRepositoryMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class SubscriptionRepository extends BaseRepository<Subscription, SubscriptionDTO> implements ISubscriptionRepository {

    private SubscriptionEntityRepository subscriptionEntityRepository;

    public SubscriptionRepository(SubscriptionEntityRepository subscriptionEntityRepository) {
        super(SubscriptionDTO.class, new SubscriptionRepositoryMapper(), "hibernate-subscription-db.cfg.xml");
        this.subscriptionEntityRepository = subscriptionEntityRepository;
    }

    @Override
    public int create(Subscription object) {
        int createdId = super.create(object);
        object.setId(createdId);
        createEntities(object);
        return createdId;
    }

    @Override
    public List<Subscription> read(Map<String, String> filter) {
        List<Subscription> subscriptions = super.read(filter);
        subscriptions.forEach(subscription -> {
            List<SubscriptionEntity> subscriptionEntities = this.readEntities(subscription.getId());
            subscription.setSubscriptionEntities(subscriptionEntities);
        });
        return subscriptions;
    }

    @Override
    public Subscription read(int id) {
        Subscription subscription = super.read(id);
        List<SubscriptionEntity> subscriptionEntities = this.readEntities(subscription.getId());
        subscription.setSubscriptionEntities(subscriptionEntities);
        return subscription;
    }

    @Override
    public void delete(Subscription object) {
        deleteEntities(object.getId());
        super.delete(object);
    }

    private void createEntities(Subscription object) {
        object.getSubscriptionEntities().forEach(entity -> {
            SubscriptionEntityDTO dto = SubscriptionEntityRepositoryMapper.transformToSubscriptionEntityDTO(object.getId(), entity);
            subscriptionEntityRepository.create(dto);
        });
    }

    private List<SubscriptionEntity> readEntities(int subscriptionId) {
        return subscriptionEntityRepository
                .read(new HashMap<>())
                .stream()
                .filter(dto -> dto.getSubscriptionId() == subscriptionId)
                .map(SubscriptionEntityRepositoryMapper::transformToSubscriptionEntity)
                .collect(Collectors.toList());
    }

    private void deleteEntities(int subscriptionId) {
        subscriptionEntityRepository
                .read(new HashMap<>())
                .stream()
                .filter(dto -> dto.getSubscriptionId() == subscriptionId)
                .forEach(subscriptionEntityRepository::delete);
    }
}
