package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.implementation;

import org.springframework.stereotype.Service;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.contract.IZoneService;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.manager.IPercentageAlgorithm;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.Zone;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.util.Model;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.RepositoryFacade;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.contracts.IZoneRepository;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.utils.ListUtils;

import java.util.HashMap;
import java.util.List;

@Service
public class ZoneService implements IZoneService {

    private IZoneRepository zoneRepository;
    private IPercentageAlgorithm percentageAlgorithm;
    private RepositoryFacade repositoryFacade;

    public ZoneService(RepositoryFacade repositoryFacade, IPercentageAlgorithm percentageAlgorithm) {
        this.zoneRepository = repositoryFacade.getZoneRepository();
        this.percentageAlgorithm = percentageAlgorithm;
        this.repositoryFacade = repositoryFacade;
    }

    @Override
    public List<Model> read() {
        List<Zone> zones = zoneRepository.read(new HashMap<>());
        return ListUtils.mapList(zones, this::addPercentageToZone);
    }

    @Override
    public Zone read(int id) {
        Zone zone = zoneRepository.read(id);
        return addPercentageToZone(zone);
    }

    private Zone addPercentageToZone(Zone zone) {
        int occupiedPercentage = percentageAlgorithm.calculate(repositoryFacade, zone.getId());
        zone.setOccupiedPercentage(occupiedPercentage);
        return zone;
    }
}
