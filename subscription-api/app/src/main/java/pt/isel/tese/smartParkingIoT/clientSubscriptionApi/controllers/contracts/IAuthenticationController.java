package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.contracts;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.config.UriConfiguration;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response.ErrorResponse;

import javax.validation.Valid;

public interface IAuthenticationController {

    @ApiOperation(value = "Login", nickname = "loginUser", notes = "Operation login given the user representation passed in the header authorization, using http basic authentication. The content-type in this request MUST be application/json.", tags={ "Authentication" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User was logged in successfully. Make sure to used the returned JWT token in all the requests. The token can be found in the header Authorization"),
            @ApiResponse(code = 400, message = "Invalid input. A description of the error will be in the returned JSON.", response = ErrorResponse.class),
            @ApiResponse(code = 412, message = "User already logged in.", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "The requested user does not exist.", response = ErrorResponse.class) })
    @PostMapping(value = UriConfiguration.LOGIN_URI, produces = { MediaType.APPLICATION_JSON_VALUE } )
    ResponseEntity<Void> loginUser(@ApiParam(value = "User info",required=true) @Valid @RequestHeader(value="Authorization") String basicAuth);

    @ApiOperation(value = "Logout", nickname = "logoutUser", notes = "Operation logout given the JWT passed in the header authorization.", tags={ "Authentication", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User was logged off successfully."),
            @ApiResponse(code = 400, message = "Invalid input. A description of the error will be in the returned JSON.", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "The requested user does not exist.", response = ErrorResponse.class) })
    @PostMapping(value = UriConfiguration.LOGOUT_URI, produces = { MediaType.APPLICATION_JSON_VALUE } )
    ResponseEntity<Void> logoutUser(@ApiParam(value = "Current token",required=true) @Valid @RequestHeader(value="Authorization") String basicAuth);
}
