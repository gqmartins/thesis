package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

public class UserRequest {

    @JsonProperty("username")
    @NotNull(message = "A username must be provided.")
    private String username = null;

    @JsonProperty("password")
    @NotNull(message = "A password must be provided.")
    private String password = null;

    public UserRequest() { }

    /**
     * Get username
     * @return username
     **/
    @ApiModelProperty(example = "c_jorge", required = true, value = "")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Get password
     * @return password
     **/
    @ApiModelProperty(example = "*****", required = true, value = "")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
