package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@JsonSerialize(using = AttributeResponse.AttributeResponseSerializer.class)
public class AttributeResponse {

    private String name = null;
    private List<String> types = new ArrayList<>();

    public AttributeResponse(String name, List<String> types) {
        this.name = name;
        this.types = types;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public static class AttributeResponseSerializer extends JsonSerializer<AttributeResponse> {

        @Override
        public void serialize(AttributeResponse attributeResponse, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeObjectFieldStart(attributeResponse.getName());
            jsonGenerator.writeArrayFieldStart("types");
            for(String type : attributeResponse.getTypes()) {
                jsonGenerator.writeString(type);
            }
            jsonGenerator.writeEndArray();
            jsonGenerator.writeEndObject();
            jsonGenerator.writeEndObject();
        }
    }
}
