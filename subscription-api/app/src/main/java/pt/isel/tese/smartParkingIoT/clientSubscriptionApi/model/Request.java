package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model;

import java.util.Date;

public class Request {

    private int id;
    private int user;
    private Date created;

    public Request(int user, Date created) {
        this.user = user;
        this.created = created;
    }

    public Request(int id, int user, Date created) {
        this.id = id;
        this.user = user;
        this.created = created;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
