package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.request.SubscriptionRequest;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.request.subscription.SubscriptionEntityRequest;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.request.subscription.SubscriptionNotificationRequest;

public class SubscriptionResponse extends SubscriptionRequest {

    @JsonProperty("id")
    private String id;

    public SubscriptionResponse(int id, SubscriptionEntityRequest subject, SubscriptionNotificationRequest notification, String expires, String status) {
        super(subject, notification, expires, status);
        this.id = String.valueOf(id);
    }

    /**
     * Get id
     * @return id
     **/
    @ApiModelProperty(example = "7623", required = true, value = "")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
