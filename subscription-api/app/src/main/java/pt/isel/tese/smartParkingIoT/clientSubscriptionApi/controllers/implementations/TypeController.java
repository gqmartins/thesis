package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.implementations;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.contracts.ITypeController;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.mappers.TypeControllerMapper;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response.TypeResponse;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.utils.ServiceSupplier;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.ServiceFacade;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.contract.ITypeService;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.Type;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class TypeController implements ITypeController {

    private ITypeService typeService;
    private TypeControllerMapper typeMapper;
    private ServiceSupplier serviceSupplier;

    public TypeController(ServiceFacade serviceFacade, TypeControllerMapper typeMapper, ServiceSupplier serviceSupplier) {
        this.typeService = serviceFacade.getTypeService();
        this.typeMapper = typeMapper;
        this.serviceSupplier = serviceSupplier;
    }

    @Override
    public ResponseEntity<List<TypeResponse>> readTypes(@RequestParam("api-key") String apiKey) {
        List<Type> entities = this.typeService.read();
        List<TypeResponse> typeResponse = entities.stream().map(entity -> {
            String type = entity.getType().toLowerCase();
            int count = this.serviceSupplier.getService(type).read().size();
            return this.typeMapper.transformToResponse(entity, count);
        }).collect(Collectors.toList());
        return ResponseEntity.ok(typeResponse);
    }

    @Override
    public ResponseEntity<TypeResponse> readType(@PathVariable("type-name") String typeName, @RequestParam("api-key") String apiKey) {
        Type model = typeService.read(typeName);
        String type = model.getType().toLowerCase();
        int count = this.serviceSupplier.getService(type).read().size();
        TypeResponse typeResponse = this.typeMapper.transformToResponse(model, count);
        return ResponseEntity.ok(typeResponse);
    }
}
