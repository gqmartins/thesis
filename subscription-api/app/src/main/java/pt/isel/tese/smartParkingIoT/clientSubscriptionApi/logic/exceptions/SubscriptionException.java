package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.exceptions;

public class SubscriptionException extends LogicException {

    public SubscriptionException(String title, String detail) {
        super(title, detail);
    }
}
