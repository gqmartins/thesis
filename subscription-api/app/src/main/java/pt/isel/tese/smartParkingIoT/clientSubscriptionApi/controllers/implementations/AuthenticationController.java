package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.implementations;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.config.AppConfiguration;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.contracts.IAuthenticationController;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.utils.JwtUtils;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.ServiceFacade;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.contract.IAuthenticationService;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.contract.IUserService;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.User;

import javax.validation.Valid;

@RestController
public class AuthenticationController implements IAuthenticationController {

    private IAuthenticationService authenticationService;
    private IUserService userService;
    private AppConfiguration appConfiguration;

    public AuthenticationController(ServiceFacade serviceFacade, AppConfiguration appConfiguration) {
        this.authenticationService = serviceFacade.getAuthenticationService();
        this.userService = serviceFacade.getUserService();
        this.appConfiguration = appConfiguration;
    }

    @Override
    public ResponseEntity<Void> loginUser(@Valid @RequestHeader(value="Authorization") String basicAuth) {
        basicAuth = basicAuth.replace("Basic ", "");
        String[] values = new String(Base64.decodeBase64(basicAuth)).split(":");
        String username = values[0];
        String password = values[1];
        User user = authenticationService.login(username, password);
        return getResponseWithToken(user);
    }

    @Override
    public ResponseEntity<Void> logoutUser(@Valid @RequestHeader(value="Authorization") String basicAuth) {
        return ResponseEntity.ok().header("Authorization", "").build();
    }

    private ResponseEntity<Void> getResponseWithToken(User user) {
        String jwtToken = JwtUtils.generateToken(user, Integer.parseInt(appConfiguration.jwtExpirationMinutes),
                appConfiguration.jwtSecret);

        String header = "Bearer " + jwtToken;
        return ResponseEntity.ok().header("Authorization", header).build();
    }
}
