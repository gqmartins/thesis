package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.exceptions;

import org.springframework.http.HttpStatus;

public class NotImplementedException extends ApiException {

    public NotImplementedException(){
        this("This method is not yet implemented.", "Not Implemented", HttpStatus.NOT_IMPLEMENTED, "about:blank", "string");
    }

    public NotImplementedException(String title, String detail) {
        super(title, detail, HttpStatus.NOT_IMPLEMENTED, null);
    }

    public NotImplementedException(String title, String detail, HttpStatus status, String instance, String type) {
        super(title, detail, status, instance, type,null);
    }
}
