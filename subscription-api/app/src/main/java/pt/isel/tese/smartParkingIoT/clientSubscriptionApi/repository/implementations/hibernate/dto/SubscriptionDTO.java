package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.dto;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "Subscription")
public class SubscriptionDTO {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "status")
    private String status;

    @Column(name = "url")
    private String url;

    @Column(name = "expires")
    private Timestamp expires;

    @Column(name = "user_id")
    private int userId;

    public SubscriptionDTO() { }

    public SubscriptionDTO(int id, String status, String url, Timestamp expires, int userId) {
        this.id = id;
        this.status = status;
        this.url = url;
        this.expires = expires;
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Timestamp getExpires() {
        return expires;
    }

    public void setExpires(Timestamp expires) {
        this.expires = expires;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
