package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.implementation;

import org.springframework.stereotype.Service;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.contract.ITypeService;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.exceptions.TypeException;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.Attribute;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.Type;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.RepositoryFacade;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.contracts.IAttributeRepository;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.contracts.ITypeRepository;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TypeService implements ITypeService {

    private ITypeRepository typeRepository;
    private IAttributeRepository attributeRepository;

    public TypeService(RepositoryFacade repositoryFacade) {
        this.typeRepository = repositoryFacade.getTypeRepository();
        this.attributeRepository = repositoryFacade.getAttributeRepository();
    }

    @Override
    public List<Type> read() {
        List<Type> entities = typeRepository.read(new HashMap<>());
        return entities
                .stream()
                .map(this::addAttributesToEntity)
                .collect(Collectors.toList());
    }

    @Override
    public Type read(String type) {
        Type entity = getEntity(type);
        return addAttributesToEntity(entity);
    }

    private Type addAttributesToEntity(Type type) {
        type.setAttributes(getAttribute(type.getId()));
        return type;
    }

    private List<Attribute> getAttribute(int entityId) {
        return attributeRepository
                .read(new HashMap<>())
                .stream()
                .filter(attribute -> attribute.getTypeId() == entityId)
                .collect(Collectors.toList());
    }

    private Type getEntity(String type) {
        List<Type> types = this.typeRepository
                .read(new HashMap<>())
                .stream()
                .filter(entity -> entity.getType().equals(type))
                .collect(Collectors.toList());
        if(types.size() == 0) {
            throw new TypeException("Invalid request", "The requested type does not exist.");
        }
        return types.get(0);
    }
}
