package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.mappers;

import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.Type;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.dto.TypeDTO;

public class TypeRepositoryMapper implements IRepositoryMapper<Type, TypeDTO> {

    @Override
    public Type transformToObject(TypeDTO dto) {
        return new Type(dto.getId(), dto.getType());
    }

    @Override
    public TypeDTO transformToDto(Type model) {
        return new TypeDTO(model.getId(), model.getType());
    }

    @Override
    public int getIdentifier(Type model) {
        return model.getId();
    }
}
