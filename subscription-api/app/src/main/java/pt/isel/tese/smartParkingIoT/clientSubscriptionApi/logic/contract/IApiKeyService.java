package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.contract;

import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.User;

public interface IApiKeyService {

    String buildApiKey(int user);
    void validateRequest(String fullApiKey);
    User findUser(String apiKey);
}
