package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.mappers;

import org.springframework.stereotype.Component;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response.AttributeResponse;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response.TypeResponse;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.Attribute;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.Type;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.utils.ListUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class TypeControllerMapper {

    public TypeResponse transformToResponse(Type type, int count) {
        List<AttributeResponse> attributes = ListUtils.mapList(type.getAttributes(), this::mapAttribute);
        return new TypeResponse(type.getType(), attributes, count);
    }

    private AttributeResponse mapAttribute(Attribute attribute) {
        List<String> attributeTypes = new ArrayList<>(Arrays.asList(attribute.getType()));
        return new AttributeResponse(attribute.getName(), attributeTypes);
    }
}
