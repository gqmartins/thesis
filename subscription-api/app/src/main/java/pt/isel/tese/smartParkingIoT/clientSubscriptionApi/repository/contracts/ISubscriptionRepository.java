package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.contracts;

import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.Subscription;

public interface ISubscriptionRepository extends IBaseRepository<Subscription> { }
