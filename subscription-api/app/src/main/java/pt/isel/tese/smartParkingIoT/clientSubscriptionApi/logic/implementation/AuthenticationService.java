package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.implementation;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.contract.IAuthenticationService;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.exceptions.IncorrectLoginException;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.User;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.RepositoryFacade;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.contracts.IUserRepository;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuthenticationService implements IAuthenticationService {

    private IUserRepository userRepository;

    public AuthenticationService(RepositoryFacade repositoryFacade) {
        this.userRepository = repositoryFacade.getUserRepository();
    }

    @Override
    public User login(String username, String password) {
        int userId = getUserId(username);
        User savedUser = userRepository.read(userId);

        if(!verifyPassword(password, savedUser.getPassword())) {
            throw new IncorrectLoginException();
        }

        return savedUser;
    }

    @Override
    public void logout(String username) {

    }

    private int getUserId(String username) {
        List<User> users = userRepository
                .read(new HashMap<>())
                .stream()
                .filter(user -> user.getUsername().equals(username))
                .collect(Collectors.toList());
        if(users.size() == 0) {
            throw new IncorrectLoginException();
        }
        return users.get(0).getId();
    }

    private boolean verifyPassword(String rawPassword, String encodedPassword) {
        BCryptPasswordEncoder passwordDecoder = new BCryptPasswordEncoder();
        return passwordDecoder.matches(rawPassword, encodedPassword);
    }
}
