package pt.isel.tese.smartParkingIoT.clientSubscriptionApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
public class ClientSubscriptionApi {

    public static void main(String[] args) {
        System.out.println("::STARTING::");
        SpringApplication.run(ClientSubscriptionApi.class, args);
        System.out.println("::RUNNING::");
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry
                        .addMapping("/**")
                        .allowedOrigins("*")
                        .exposedHeaders("Authorization")
                        .allowedMethods("*");
            }
        };
    }
}
