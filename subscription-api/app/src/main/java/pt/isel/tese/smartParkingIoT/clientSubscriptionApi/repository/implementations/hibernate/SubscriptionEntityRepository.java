package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate;

import org.springframework.stereotype.Repository;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.dto.SubscriptionEntityDTO;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.mappers.SubscriptionEntityRepositoryMapper;

@Repository
public class SubscriptionEntityRepository extends BaseRepository<SubscriptionEntityDTO, SubscriptionEntityDTO> {

    public SubscriptionEntityRepository() {
        super(SubscriptionEntityDTO.class, new SubscriptionEntityRepositoryMapper(), "hibernate-subscription-db.cfg.xml");
    }
}
