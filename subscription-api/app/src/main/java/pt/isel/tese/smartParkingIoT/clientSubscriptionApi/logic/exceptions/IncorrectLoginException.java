package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.exceptions;

public class IncorrectLoginException extends LogicException {

    public IncorrectLoginException() {
        super("Invalid login.", "The user does not exist, or the password in incorrect.");
    }

    public IncorrectLoginException(String title, String detail) {
        super(title, detail);
    }
}
