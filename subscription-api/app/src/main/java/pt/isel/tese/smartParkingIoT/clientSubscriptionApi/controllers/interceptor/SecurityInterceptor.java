package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.config.AppConfiguration;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.utils.JwtUtils;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.contract.IApiKeyService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@CrossOrigin
@Component
public class SecurityInterceptor extends HandlerInterceptorAdapter {

    private AppConfiguration appConfiguration;
    private IApiKeyService apiKeyService;
    private Set<String> unprotectedUris;
    private Set<String> tokenProtectedUris;
    private static final String AUTHORIZATION_HEADER = "Authorization";

    public SecurityInterceptor(AppConfiguration appConfiguration, IApiKeyService apiKeyService) {
        this.appConfiguration = appConfiguration;
        this.apiKeyService = apiKeyService;
        this.unprotectedUris = buildUriSet(appConfiguration.unprotectedUris);
        this.tokenProtectedUris = buildUriSet(appConfiguration.tokenProtectedUris);
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String requestUri = getUri(request.getRequestURI());
        if(unprotectedUris.contains(requestUri)) {
            return true;
        }
        if(tokenProtectedUris.contains(requestUri)) {
            handleJwtToken(request);
            return super.preHandle(request, response, handler);
        }
        handleApiKey(request);
        return super.preHandle(request, response, handler);
    }

    private void handleJwtToken(HttpServletRequest request) {
        String authorizationHeader = request.getHeader(AUTHORIZATION_HEADER);
        JwtUtils.parseToken(authorizationHeader, appConfiguration.jwtSecret);
    }

    private void handleApiKey(HttpServletRequest request) {
        String apiKey = request.getParameter("api-key");
        this.apiKeyService.validateRequest(apiKey);
    }

    private Set<String> buildUriSet(String uris) {
        Set<String> result = new HashSet<>();
        Arrays.stream(uris.split(";")).forEach(result::add);
        return result;
    }

    private String getUri(String requestUri) {
        int baseUriLength = appConfiguration.serverContextPath.length();
        return requestUri.substring(baseUriLength);
    }
}
