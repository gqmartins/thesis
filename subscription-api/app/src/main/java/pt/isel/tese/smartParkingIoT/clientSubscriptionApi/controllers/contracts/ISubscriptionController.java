package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.contracts;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.config.UriConfiguration;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.request.SubscriptionRequest;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response.ErrorResponse;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response.SubscriptionResponse;

import javax.validation.Valid;
import java.util.List;

public interface ISubscriptionController {

    @ApiOperation(value = "adds a subscrpition to the system", nickname = "createSubscription", notes = "adds a subscription to the system", tags={ "Subscription" })
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "subscription successfully added to the system. to get the subscription added to the system make a get request to the response header location URI."),
            @ApiResponse(code = 400, message = "Bad request parameter", response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class) })
    @PostMapping(value = UriConfiguration.SUBSCRIPTIONS_URI, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<Void> createSubscription(@ApiParam(value = "Subscription to add")  @Valid @RequestBody SubscriptionRequest body, @ApiParam(value = "api key", required = true) @RequestParam("api-key") String apiKey);

    @ApiOperation(value = "deletes the subscription with the specified subscription-id", nickname = "deleteSubscription", notes = "delete the subscription with the specified subscription-id", tags={ "Subscription" })
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "subscription successfully deleted."),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "The specified subscription does not exist", response = ErrorResponse.class) })
    @DeleteMapping(value = UriConfiguration.SUBSCRIPTIONS_ID_URI, produces = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<Void> deleteSubscription(@ApiParam(value = "subscription id",required=true) @PathVariable("subscription-id") Integer subscriptionId, @ApiParam(value = "api key", required = true) @RequestParam("api-key") String apiKey);

    @ApiOperation(value = "returns subscription with the specified subscription-id", nickname = "readSubscription", notes = "returns subscription with the specified subscription-id", response = SubscriptionResponse.class, tags={ "Subscription" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "subscription with the specified subscription-id", response = SubscriptionResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "The specified subscription does not exist", response = ErrorResponse.class) })
    @GetMapping(value = UriConfiguration.SUBSCRIPTIONS_ID_URI, produces = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<SubscriptionResponse> readSubscription(@ApiParam(value = "subscription id",required=true) @PathVariable("subscription-id") Integer subscriptionId, @ApiParam(value = "api key", required = true) @RequestParam("api-key") String apiKey);

    @ApiOperation(value = "returns subscriptions added to the system", nickname = "readsubscriptions", notes = "returns all the subscriptions that were added to the system, ordered by creation date", response = SubscriptionResponse.class, responseContainer = "List", tags={ "Subscription" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "subscription result matching search criteria", response = SubscriptionResponse.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad request parameter", response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class) })
    @GetMapping(value = UriConfiguration.SUBSCRIPTIONS_URI, produces = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<List<SubscriptionResponse>> readSubscriptions(@ApiParam(value = "api key", required = true) @RequestParam("api-key") String apiKey);
}
