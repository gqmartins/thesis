package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.request.subscription;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

public class SubscriptionNotificationUrlRequest {

    @JsonProperty("url")
    private String url;

    public SubscriptionNotificationUrlRequest() { }

    public SubscriptionNotificationUrlRequest(String url) {
        this.url = url;
    }

    /**
     * Get url
     * @return url
     **/
    @ApiModelProperty(example = "http://localhost:8080/notification", required = true, value = "")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
