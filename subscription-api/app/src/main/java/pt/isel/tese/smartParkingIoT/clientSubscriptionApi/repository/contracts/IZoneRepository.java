package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.contracts;

import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.Zone;

public interface IZoneRepository extends IBaseRepository<Zone> { }
