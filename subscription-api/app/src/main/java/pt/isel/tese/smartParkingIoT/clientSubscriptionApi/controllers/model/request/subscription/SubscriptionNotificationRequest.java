package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.request.subscription;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

public class SubscriptionNotificationRequest {

    @JsonProperty("http")
    private SubscriptionNotificationUrlRequest http;

    public SubscriptionNotificationRequest() { }

    public SubscriptionNotificationRequest(SubscriptionNotificationUrlRequest http) {
        this.http = http;
    }

    public SubscriptionNotificationUrlRequest getHttp() {
        return http;
    }

    /**
     * Get http
     * @return http
     **/
    @ApiModelProperty(example = "", required = true, value = "")
    public void setHttp(SubscriptionNotificationUrlRequest http) {
        this.http = http;
    }
}
