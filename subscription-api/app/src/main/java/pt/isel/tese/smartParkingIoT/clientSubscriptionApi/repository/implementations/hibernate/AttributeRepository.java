package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate;

import org.springframework.stereotype.Repository;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.Attribute;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.contracts.IAttributeRepository;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.dto.AttributeDTO;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.mappers.AttributeRepositoryMapper;

@Repository
public class AttributeRepository extends BaseRepository<Attribute, AttributeDTO> implements IAttributeRepository {

    public AttributeRepository() {
        super(AttributeDTO.class, new AttributeRepositoryMapper(), "hibernate-subscription-db.cfg.xml");
    }
}
