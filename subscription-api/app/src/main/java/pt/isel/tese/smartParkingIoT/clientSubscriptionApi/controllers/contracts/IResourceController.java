package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.contracts;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.config.UriConfiguration;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response.EntityResponse;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response.ErrorResponse;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response.ResourceResponse;

import java.util.List;

public interface IResourceController {

    @ApiOperation(value = "returns the base urls for the NGSI v2 endpoints", nickname = "readResource", notes = "returns the base urls for the NGSI v2 endpoints", response = ResourceResponse.class, tags={ "Resource" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "returns entities added to the system", response = EntityResponse.class, responseContainer = "List")
    })
    @GetMapping(value = UriConfiguration.RESOURCE_URI, produces = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<ResourceResponse> readResource();
}
