package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Type")
public class TypeDTO {

    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "type")
    private String type;

    public TypeDTO() { }

    public TypeDTO(int id, String type) {
        this.id = id;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
