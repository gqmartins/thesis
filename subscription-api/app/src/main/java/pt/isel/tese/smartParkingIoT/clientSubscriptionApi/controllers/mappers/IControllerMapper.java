package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.mappers;

import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response.EntityResponse;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.util.Model;

public interface IControllerMapper {

    EntityResponse transformToResponse(Model model);
}
