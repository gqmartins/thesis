package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.contracts;

import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.Request;

public interface IRequestRepository extends IBaseRepository<Request> { }
