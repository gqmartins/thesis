package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class TypeResponse {

    @JsonProperty("type")
    private String type = null;

    @JsonProperty("attrs")
    private List<AttributeResponse> attrs = new ArrayList<>();

    @JsonProperty("count")
    private Integer count = null;

    public TypeResponse(String type, List<AttributeResponse> attrs, Integer count) {
        this.type = type;
        this.attrs = attrs;
        this.count = count;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<AttributeResponse> getAttrs() {
        return attrs;
    }

    public void setAttrs(List<AttributeResponse> attrs) {
        this.attrs = attrs;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
