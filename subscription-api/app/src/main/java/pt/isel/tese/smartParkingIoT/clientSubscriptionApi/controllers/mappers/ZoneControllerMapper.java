package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.mappers;

import org.springframework.stereotype.Component;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.config.UriConfiguration;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response.EntityResponse;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response.ListItemResponse;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response.ZoneResponse;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.utils.GeoJsonPolygonModel;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.utils.UriBuilder;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.Zone;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.util.Model;

@Component
public class ZoneControllerMapper implements IControllerMapper {

    public EntityResponse transformToResponse(Model model) {
        Zone zone = (Zone) model;
        GeoJsonPolygonModel perimeter = new GeoJsonPolygonModel(zone.getPerimeter().getCoordinates());
        return new ZoneResponse(zone.getName(), perimeter, zone.getId(), zone.getOccupiedPercentage());
    }
}
