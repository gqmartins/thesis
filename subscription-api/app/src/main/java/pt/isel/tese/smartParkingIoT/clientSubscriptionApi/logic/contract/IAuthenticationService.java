package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.contract;

import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.User;

public interface IAuthenticationService {

    User login(String username, String password);
    void logout(String username);
}
