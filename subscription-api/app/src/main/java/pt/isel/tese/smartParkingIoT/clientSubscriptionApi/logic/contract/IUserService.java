package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.contract;

import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.User;

public interface IUserService {

    void create(User user);
}
