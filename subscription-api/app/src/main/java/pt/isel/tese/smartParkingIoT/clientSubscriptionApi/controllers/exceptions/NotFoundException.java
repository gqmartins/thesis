package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.exceptions;

import org.springframework.http.HttpStatus;

public class NotFoundException extends ApiException {

    public NotFoundException(String title, String detail) {
        this(title, detail,null);
    }

    public NotFoundException(String title, String detail, Exception ex) {
        super(title, detail, HttpStatus.NOT_FOUND,ex);
    }

    public NotFoundException(String title, String detail, HttpStatus status, String instance, String type, Exception ex) {
        super(title, detail, status, instance, type,ex);
    }
}
