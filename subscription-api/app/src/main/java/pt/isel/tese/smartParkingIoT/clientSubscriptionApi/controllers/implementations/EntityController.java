package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.implementations;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.contracts.IEntityController;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.mappers.IControllerMapper;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response.EntityResponse;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.utils.MapperSupplier;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.utils.ServiceSupplier;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.contract.IBaseService;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.util.Model;

import java.util.ArrayList;
import java.util.List;

@RestController
public class EntityController implements IEntityController {

    private ServiceSupplier serviceSupplier;
    private MapperSupplier mapperSupplier;

    public EntityController(ServiceSupplier serviceSupplier, MapperSupplier mapperSupplier) {
        this.serviceSupplier = serviceSupplier;
        this.mapperSupplier = mapperSupplier;
    }

    @Override
    public ResponseEntity<List<EntityResponse>> readEntities(
            @RequestParam("api-key") String apiKey) {
        List<EntityResponse> result = new ArrayList<>();
        this.serviceSupplier.getServiceKeys().forEach(key -> {
            IBaseService service = this.serviceSupplier.getService(key);
            List<Model> models = service.read();
            models.forEach(model -> {
                IControllerMapper mapper = this.mapperSupplier.getMapper(key);
                EntityResponse entityResponse = mapper.transformToResponse(model);
                result.add(entityResponse);
            });
        });
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<EntityResponse> readEntity(
            @PathVariable("entity-id") String entityId,
            @RequestParam("api-key") String apiKey) {
        String type = entityId.substring(0, entityId.indexOf("-"));
        int id = Integer.parseInt(entityId.substring(entityId.indexOf("-") + 1));
        Model model = this.serviceSupplier.getService(type).read(id);
        EntityResponse entityResponse = this.mapperSupplier
                .getMapper(type).transformToResponse(model);
        return ResponseEntity.ok(entityResponse);
    }
}
