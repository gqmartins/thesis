package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.implementations;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.config.UriConfiguration;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.contracts.ISubscriptionController;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.mappers.SubscriptionControllerMapper;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.request.SubscriptionRequest;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response.SubscriptionResponse;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.utils.UriBuilder;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.ServiceFacade;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.contract.IApiKeyService;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.contract.ISubscriptionService;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.Subscription;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.User;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.utils.ListUtils;

import java.net.URI;
import java.util.List;

@RestController
public class SubscriptionController implements ISubscriptionController {

    private ISubscriptionService subscriptionService;
    private IApiKeyService apiKeyService;
    private SubscriptionControllerMapper entityMapper;

    public SubscriptionController(ServiceFacade serviceFacade, SubscriptionControllerMapper entityMapper) {
        this.subscriptionService = serviceFacade.getSubscriptionService();
        this.apiKeyService = serviceFacade.getApiKeyService();
        this.entityMapper = entityMapper;
    }

    @Override
    public ResponseEntity<Void> createSubscription(@RequestBody SubscriptionRequest body, @RequestParam("api-key") String apiKey) {
        int userId = getUserId(apiKey);
        Subscription subscription = entityMapper.transformToModel(body, userId);
        int createdSubscriptionId = subscriptionService.create(subscription);
        URI createdUri = UriBuilder.getCreatedURI(UriConfiguration.SUBSCRIPTIONS_ID_URI, createdSubscriptionId);
        return ResponseEntity.created(createdUri).build();
    }

    @Override
    public ResponseEntity<Void> deleteSubscription(@PathVariable("subscription-id") Integer subscriptionId, @RequestParam("api-key") String apiKey) {
        int userId = getUserId(apiKey);
        subscriptionService.delete(subscriptionId, userId);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<SubscriptionResponse> readSubscription(@PathVariable("subscription-id") Integer subscriptionId, @RequestParam("api-key") String apiKey) {
        int userId = getUserId(apiKey);
        Subscription subscription = subscriptionService.read(subscriptionId, userId);
        SubscriptionResponse response = this.entityMapper.transformToResponse(subscription);
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<List<SubscriptionResponse>> readSubscriptions(@RequestParam("api-key") String apiKey) {
        int userId = getUserId(apiKey);
        List<Subscription> subscriptions = subscriptionService.read(userId);
        List<SubscriptionResponse> response = ListUtils.mapList(subscriptions, entityMapper::transformToResponse);
        return ResponseEntity.ok(response);
    }

    private int getUserId(String apiKey) {
        String prefix = apiKey.substring(1, apiKey.indexOf("}"));
        User user = apiKeyService.findUser(prefix);
        return user.getId();
    }
}
