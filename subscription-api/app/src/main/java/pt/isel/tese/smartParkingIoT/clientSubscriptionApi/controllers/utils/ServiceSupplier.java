package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.utils;

import org.springframework.stereotype.Component;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.ServiceFacade;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.contract.IBaseService;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Component
public class ServiceSupplier {

    private final Map<String, IBaseService> servicesMap = new HashMap<>();

    public ServiceSupplier(ServiceFacade serviceFacade) {
        buildServiceMap(serviceFacade);
    }

    public IBaseService getService(String key) {
        return this.servicesMap.get(key);
    }

    public Set<String> getServiceKeys() {
        return this.servicesMap.keySet();
    }

    private void buildServiceMap(ServiceFacade serviceFacade) {
        servicesMap.put("zone", serviceFacade.getZoneService());
    }
}
