package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.mappers;

import org.springframework.stereotype.Component;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.request.UserRequest;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.User;

@Component
public class UserControllerMapper {

    public User transformToModel(UserRequest request) {
        return new User(request.getUsername(), request.getPassword());
    }
}
