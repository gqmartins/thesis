package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

public class EntityResponse {

    @JsonProperty("type")
    private String type;

    @JsonProperty("id")
    private String id;

    public EntityResponse(String type, String id) {
        this.type = type;
        this.id = id;
    }

}
