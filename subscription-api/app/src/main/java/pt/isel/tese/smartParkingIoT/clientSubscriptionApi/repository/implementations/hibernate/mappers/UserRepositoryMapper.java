package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.mappers;

import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.User;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.dto.UserDTO;

public class UserRepositoryMapper implements IRepositoryMapper<User, UserDTO> {

    @Override
    public User transformToObject(UserDTO dto) {
        return new User(dto.getId(), dto.getUsername(), dto.getPassword(), dto.getApiKeyPrefix(), dto.getApiKey(),
                dto.getCreated(), dto.getModified());
    }

    @Override
    public UserDTO transformToDto(User object) {
        return new UserDTO(object.getId(), object.getUsername(), object.getPassword(), object.getApiKeyPrefix(),
                object.getApiKey(), object.getCreated(), object.getModified());
    }

    @Override
    public int getIdentifier(User model) {
        return model.getId();
    }
}
