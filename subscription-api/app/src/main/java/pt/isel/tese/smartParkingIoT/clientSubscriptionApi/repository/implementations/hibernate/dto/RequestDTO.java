package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.dto;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Request")
public class RequestDTO {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "user_id")
    private int user;

    @Column(name = "created")
    private Date created;

    public RequestDTO() { }

    public RequestDTO(int id, int user, Date created) {
        this.id = id;
        this.user = user;
        this.created = created;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
