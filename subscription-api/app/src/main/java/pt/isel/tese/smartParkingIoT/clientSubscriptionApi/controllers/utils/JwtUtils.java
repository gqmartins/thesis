package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.utils;

import io.jsonwebtoken.*;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.exceptions.UnauthorizedException;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.utils.JsonWebToken;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.User;

import java.util.Date;

public class JwtUtils {

    private static final int MILLISECONDS_IN_MINUTES = 60000;

    public static String generateToken(User user, int expirationTime, String secretKey) {
        return Jwts.builder()
                .setSubject(String.valueOf(user.getId()))
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + (expirationTime * MILLISECONDS_IN_MINUTES)))
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
    }

    public static JsonWebToken parseToken(String header, String secretKey) {
        if(header == null) {
            throw new UnauthorizedException("Token error.", "A token must be provided to make such request.");
        }
        try {
            String token = header.substring(header.indexOf(" ") + 1);
            Jws<Claims> claimsJws = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);

            int user = Integer.parseInt(claimsJws.getBody().getSubject());
            Date issued = claimsJws.getBody().getIssuedAt();
            Date expiration = claimsJws.getBody().getExpiration();

            return new JsonWebToken(user, issued, expiration);
        } catch (SignatureException ex) {
            throw new UnauthorizedException("Token error.", "The token is not valid.", ex);
        } catch (ExpiredJwtException ex){
            throw new UnauthorizedException("Token error.", "The token has expired.", ex);
        }
    }
}
