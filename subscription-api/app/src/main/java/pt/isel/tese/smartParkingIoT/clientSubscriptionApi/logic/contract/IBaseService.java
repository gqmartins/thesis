package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.contract;

import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.util.Model;

import java.util.List;

public interface IBaseService {

    List<Model> read();
    Model read(int id);
}
