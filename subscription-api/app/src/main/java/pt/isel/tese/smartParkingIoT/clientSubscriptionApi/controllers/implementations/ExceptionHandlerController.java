package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.implementations;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.exceptions.*;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response.ErrorResponse;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.exceptions.LogicException;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.exceptions.RepositoryException;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionHandlerController {

    /**
     * API Specific Exceptions
     * */

    /**
     * Bad request. Http status = 400
     * */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = { BadRequestException.class })
    protected ErrorResponse handleBadRequest(HttpServletRequest request, ApiException ex) {
        return ex.getError();
    }

    /**
     * Unauthorized. Http status = 401
     * */
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(value = { UnauthorizedException.class })
    protected ErrorResponse handleUnauthorized(HttpServletRequest request, ApiException ex) {
        return ex.getError();
    }

    /**
     * Resource not found. Http status = 404
     * */
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = {NotFoundException.class })
    protected ErrorResponse resourceNotFoundException(HttpServletRequest request, NotFoundException ex) {
        return ex.getError();
    }

    /**
     * Type mismatch. Http status = 403
     *
     * */
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(value = {ForbiddenException.class })
    protected ErrorResponse handleForbiddenException(HttpServletRequest request, ForbiddenException ex) {
        return new ErrorResponse("The request is forbidden.", ex.getMessage(), HttpStatus.FORBIDDEN);
    }

    /**
     * Not Implemented. Http status = 501
     * */
    @ResponseStatus(HttpStatus.NOT_IMPLEMENTED)
    @ExceptionHandler(value = {NotImplementedException.class })
    public ErrorResponse handleNotImplementedException(HttpServletRequest request, NotImplementedException ex) {
        return new ErrorResponse("This request is not yet implemented.", ex.getMessage(), HttpStatus.NOT_IMPLEMENTED);
    }

    /**
     * SPRING and JAVA Specific Exceptions
     * Partial list: http://docs.spring.io/spring/docs/3.2.x/spring-framework-reference/html/mvc.html#mvc-ann-rest-spring-mvc-exceptions
     * */

    /**
     * Missing a required header/parameter. Http status = 400
     * */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = { ServletRequestBindingException.class,MissingServletRequestParameterException.class,
            MissingServletRequestPartException.class })
    protected ErrorResponse handleServletRequestBindingException(HttpServletRequest request,ServletRequestBindingException ex) {
        return new ErrorResponse("Missing required header/parameter", ex, request, HttpStatus.BAD_REQUEST);
    }

    /**
     * Type mismatch. Http status = 400
     *
     * */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    protected ErrorResponse handleTypeMismatchException(HttpServletRequest request,MethodArgumentNotValidException ex) {
        String message = ex.getBindingResult().getAllErrors().get(0).getDefaultMessage();
        return new ErrorResponse("Invalid input.", message, HttpStatus.BAD_REQUEST);
    }

    /**
     * Type mismatch. Http status = 404
     *
     * */
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = {NoHandlerFoundException.class })
    protected ErrorResponse handleResourceNotFoundException(HttpServletRequest request,NoHandlerFoundException ex) {
        return new ErrorResponse("No handler for request path and/or http method", ex.getMessage(),HttpStatus.NOT_FOUND);
    }

    /**
     * Default error handler. All unheld exceptions will be treated with Http status = 500
     * */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Throwable.class)
    public ErrorResponse handleThrowable(HttpServletRequest request, final Throwable ex) {
        return new ErrorResponse("Internal server error.","An unexpected internal server error occured.", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Domain related exceptions handled here!
     * */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {LogicException.class})
    protected ErrorResponse handleDomainException(HttpServletRequest request, LogicException ex) {
        return mapLogicException(ex, request, HttpStatus.BAD_REQUEST);
    }

    /**
     * Repository related exceptions handled here!
     * */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {RepositoryException.class})
    protected ErrorResponse handleRepositoryException(HttpServletRequest request,RepositoryException ex) {
        return mapRepositoryException(ex, request, HttpStatus.BAD_REQUEST);
    }

    private static ErrorResponse mapLogicException(LogicException ex, HttpServletRequest req, HttpStatus code){
        return new ErrorResponse(ex.getTitle(), ex, req, code);
    }

    private static ErrorResponse mapRepositoryException(RepositoryException ex, HttpServletRequest req,HttpStatus code){
        return new ErrorResponse(ex.getTitle(),ex, req, code);
    }
}
