package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.implementations;

import io.swagger.annotations.ApiParam;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.config.AppConfiguration;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.contracts.IApiKeyController;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response.ApiKeyResponse;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.utils.JsonWebToken;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.utils.JwtUtils;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.contract.IApiKeyService;

import javax.validation.Valid;

@RestController
public class ApiKeyController implements IApiKeyController {

    private AppConfiguration appConfiguration;
    private IApiKeyService apiKeyService;

    public ApiKeyController(AppConfiguration appConfiguration, IApiKeyService apiKeyService) {
        this.appConfiguration = appConfiguration;
        this.apiKeyService = apiKeyService;
    }

    @Override
    public ResponseEntity<ApiKeyResponse> readApiKey(@ApiParam(value = "User info",required=true) @Valid @RequestHeader(value="Authorization") String basicAuth) {
        JsonWebToken token = JwtUtils.parseToken(basicAuth, appConfiguration.jwtSecret);
        String apiKey = apiKeyService.buildApiKey(token.getSubject());
        ApiKeyResponse apiKeyResponse = new ApiKeyResponse(apiKey);
        return ResponseEntity.ok(apiKeyResponse);
    }
}
