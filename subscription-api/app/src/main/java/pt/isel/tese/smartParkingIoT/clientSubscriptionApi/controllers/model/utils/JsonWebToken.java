package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.utils;

import java.util.Date;

public class JsonWebToken {

    private int subject;
    private Date issued;
    private Date expiration;

    public JsonWebToken(int subject, Date issued, Date expiration) {
        this.subject = subject;
        this.issued = issued;
        this.expiration = expiration;
    }

    public int getSubject() {
        return subject;
    }

    public void setSubject(int subject) {
        this.subject = subject;
    }

    public Date getIssued() {
        return issued;
    }

    public void setIssued(Date issued) {
        this.issued = issued;
    }

    public Date getExpiration() {
        return expiration;
    }

    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }
}
