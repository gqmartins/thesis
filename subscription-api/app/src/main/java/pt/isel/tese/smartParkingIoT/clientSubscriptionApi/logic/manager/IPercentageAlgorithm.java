package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.manager;

import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.RepositoryFacade;

public interface IPercentageAlgorithm {

    int calculate(RepositoryFacade repositoryFacade, int zone);
}
