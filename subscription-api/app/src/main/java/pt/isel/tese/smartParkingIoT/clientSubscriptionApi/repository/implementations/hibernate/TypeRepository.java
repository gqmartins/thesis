package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate;

import org.springframework.stereotype.Repository;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.Type;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.contracts.ITypeRepository;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.dto.TypeDTO;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.mappers.TypeRepositoryMapper;

@Repository
public class TypeRepository extends BaseRepository<Type, TypeDTO> implements ITypeRepository {

    protected TypeRepository() {
        super(TypeDTO.class, new TypeRepositoryMapper(), "hibernate-subscription-db.cfg.xml");
    }
}
