package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.contracts;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.config.UriConfiguration;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response.EntityResponse;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response.ErrorResponse;

import java.util.List;

public interface IEntityController {

    @ApiOperation(value = "returns entities added to the system", nickname = "readEntities", notes = "returns entities added to the system", response = EntityResponse.class, responseContainer = "List", tags={ "Entity" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "returns entities added to the system", response = EntityResponse.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad request parameter", response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class) })
    @GetMapping(value = UriConfiguration.ENTITIES_URI, produces = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<List<EntityResponse>> readEntities(@ApiParam(value = "api key", required = true) @RequestParam("api-key") String apiKey);

    @ApiOperation(value = "returns entity with the specified entity-id", nickname = "readEntity", notes = "returns entity with the specified entity-id", response = EntityResponse.class, tags={ "Entity" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "entity with the specified entity-id", response = EntityResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "The specified zone does not exist", response = ErrorResponse.class) })
    @GetMapping(value = UriConfiguration.ENTITIES_ID_URI, produces = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<EntityResponse> readEntity(@ApiParam(value = "entity id",required=true) @PathVariable("entity-id") String entityId, @ApiParam(value = "api key", required = true) @RequestParam("api-key") String apiKey);
}
