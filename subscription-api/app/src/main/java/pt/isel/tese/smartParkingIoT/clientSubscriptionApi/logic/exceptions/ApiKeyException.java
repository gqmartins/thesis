package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.exceptions;

public class ApiKeyException extends LogicException {

    public ApiKeyException(String title, String detail) {
        super(title, detail);
    }
}
