package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.utils;

import org.springframework.stereotype.Component;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.mappers.IControllerMapper;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.mappers.MapperFacade;

import java.util.HashMap;
import java.util.Map;

@Component
public class MapperSupplier {

    private final Map<String, IControllerMapper> mappersMap = new HashMap<>();

    public MapperSupplier(MapperFacade mapperFacade) {
        buildMappersMap(mapperFacade);
    }

    public IControllerMapper getMapper(String key) {
        return mappersMap.get(key);
    }

    private void buildMappersMap(MapperFacade mapperFacade) {
        mappersMap.put("zone", mapperFacade.getZoneControllerMapper());
    }
}
