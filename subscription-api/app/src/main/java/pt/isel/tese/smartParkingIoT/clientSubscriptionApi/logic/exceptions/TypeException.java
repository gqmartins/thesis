package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.exceptions;

public class TypeException extends LogicException {

    public TypeException(String title, String detail) {
        super(title, detail);
    }
}
