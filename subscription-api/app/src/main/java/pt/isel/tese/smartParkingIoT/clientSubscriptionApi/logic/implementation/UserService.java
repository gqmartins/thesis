package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.implementation;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.contract.IUserService;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.User;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.RepositoryFacade;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.contracts.IUserRepository;

import java.util.Date;

@Service
public class UserService implements IUserService {

    private IUserRepository userRepository;

    public UserService(RepositoryFacade repositoryFacade) {
        this.userRepository = repositoryFacade.getUserRepository();
    }

    @Override
    public void create(User user) {
        String generatedPassword = generatePassword(user.getPassword());
        user.setPassword(generatedPassword);
        user.setCreated(new Date());
        user.setModified(user.getCreated());
        userRepository.create(user);
    }

    private String generatePassword(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }
}
