package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.mappers;

public interface IRepositoryMapper<T, R> {

    T transformToObject(R dto);
    R transformToDto(T model);
    int getIdentifier(T model);
}
