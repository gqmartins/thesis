package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.mappers;

import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.Subscription;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.SubscriptionEntity;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.dto.SubscriptionDTO;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.utils.DateUtils;

import java.sql.Timestamp;
import java.util.ArrayList;

public class SubscriptionRepositoryMapper implements IRepositoryMapper<Subscription, SubscriptionDTO> {

    @Override
    public Subscription transformToObject(SubscriptionDTO dto) {
        return new Subscription(dto.getId(), dto.getStatus(), dto.getUrl(), dto.getExpires(), new ArrayList<SubscriptionEntity>(), dto.getUserId());
    }

    @Override
    public SubscriptionDTO transformToDto(Subscription model) {
        Timestamp expires = DateUtils.convertDateToTimestamp(model.getExpires());
        return new SubscriptionDTO(model.getId(), model.getStatus(), model.getUrl(), expires, model.getUserId());
    }

    @Override
    public int getIdentifier(Subscription model) {
        return model.getId();
    }
}
