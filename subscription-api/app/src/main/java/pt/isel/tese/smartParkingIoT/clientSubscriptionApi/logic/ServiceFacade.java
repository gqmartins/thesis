package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic;

import org.springframework.stereotype.Component;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.contract.*;

@Component
public class ServiceFacade {

    private IUserService userService;
    private IAuthenticationService authenticationService;
    private IZoneService zoneService;
    private ITypeService typeService;
    private ISubscriptionService subscriptionService;
    private IApiKeyService apiKeyService;

    public ServiceFacade(IUserService userService, IAuthenticationService authenticationService, IZoneService zoneService,
                         ITypeService typeService, ISubscriptionService subscriptionService, IApiKeyService apiKeyService) {
        this.userService = userService;
        this.authenticationService = authenticationService;
        this.zoneService = zoneService;
        this.typeService = typeService;
        this.subscriptionService = subscriptionService;
        this.apiKeyService = apiKeyService;
    }

    public IUserService getUserService() {
        return userService;
    }

    public IAuthenticationService getAuthenticationService() {
        return authenticationService;
    }

    public IZoneService getZoneService() {
        return zoneService;
    }

    public ITypeService getTypeService() {
        return typeService;
    }

    public ISubscriptionService getSubscriptionService() {
        return subscriptionService;
    }

    public IApiKeyService getApiKeyService() {
        return apiKeyService;
    }
}
