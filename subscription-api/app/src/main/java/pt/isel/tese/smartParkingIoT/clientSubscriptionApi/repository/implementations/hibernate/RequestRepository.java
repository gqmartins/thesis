package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate;

import org.springframework.stereotype.Repository;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.Request;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.contracts.IRequestRepository;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.dto.RequestDTO;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.mappers.RequestRepositoryMapper;

@Repository
public class RequestRepository extends BaseRepository<Request, RequestDTO> implements IRequestRepository {

    public RequestRepository() {
        super(RequestDTO.class, new RequestRepositoryMapper(), "hibernate-subscription-db.cfg.xml");
    }
}
