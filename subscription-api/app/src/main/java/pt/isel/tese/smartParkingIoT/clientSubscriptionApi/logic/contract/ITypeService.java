package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.contract;

import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.Type;

import java.util.List;

public interface ITypeService {

    List<Type> read();
    Type read(String name);
}
