package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.mappers;

import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.Request;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.implementations.hibernate.dto.RequestDTO;

public class RequestRepositoryMapper implements IRepositoryMapper<Request, RequestDTO> {

    @Override
    public Request transformToObject(RequestDTO dto) {
        return new Request(dto.getId(), dto.getUser(), dto.getCreated());
    }

    @Override
    public RequestDTO transformToDto(Request model) {
        return new RequestDTO(model.getId(), model.getUser(), model.getCreated());
    }

    @Override
    public int getIdentifier(Request model) {
        return model.getId();
    }
}
