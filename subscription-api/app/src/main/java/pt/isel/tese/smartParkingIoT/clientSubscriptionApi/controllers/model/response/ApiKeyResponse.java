package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

public class ApiKeyResponse {

    @JsonProperty("apiKey")
    private String apiKey = null;

    public ApiKeyResponse(String apiKey) {
        this.apiKey = apiKey;
    }

    /**
     * Get apiKey
     * @return apiKey
     **/
    @ApiModelProperty(example = "a7te6f87huiahberofoiwknjefq87eur", required = true, value = "")
    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}
