package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.repository.exceptions;

public class NameAlreadyExistsException extends RepositoryException {

    public NameAlreadyExistsException() {
        super("Invalid name.", "There's already an entity with that name.");
    }

    public NameAlreadyExistsException(String title, String detail) {
        super(title, detail);
    }
}
