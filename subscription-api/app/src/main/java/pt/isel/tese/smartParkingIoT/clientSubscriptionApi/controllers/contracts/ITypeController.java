package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.contracts;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.config.UriConfiguration;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response.ErrorResponse;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response.TypeResponse;

import java.util.List;

public interface ITypeController {

    @ApiOperation(value = "returns types that belong to the system", nickname = "readTypes", notes = "returns types that belong to the system", response = TypeResponse.class, responseContainer = "List", tags={ "Type" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "returns types that belong to the system", response = TypeResponse.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad request parameter", response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class) })
    @GetMapping(value = UriConfiguration.TYPES_URI, produces = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<List<TypeResponse>> readTypes(@ApiParam(value = "api key", required = true) @RequestParam("api-key") String apiKey);

    @ApiOperation(value = "returns type with the specified entity-name", nickname = "readType", notes = "returns type with the specified entity-name", response = TypeResponse.class, tags={ "Type" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "type with the specified type-name", response = TypeResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "The specified zone does not exist", response = ErrorResponse.class) })
    @GetMapping(value = UriConfiguration.TYPES_ID_URI, produces = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<TypeResponse> readType(@ApiParam(value = "type name",required=true) @PathVariable("type-name") String typeName, @ApiParam(value = "api key", required = true) @RequestParam("api-key") String apiKey);
}
