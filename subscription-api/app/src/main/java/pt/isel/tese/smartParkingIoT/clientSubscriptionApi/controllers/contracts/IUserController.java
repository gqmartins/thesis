package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.contracts;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.config.UriConfiguration;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.request.UserRequest;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response.ErrorResponse;

import javax.validation.Valid;

@Api(value = "user", description = "the user API")
public interface IUserController {

    @ApiOperation(value = "Registers a user into the system", nickname = "registerUser", notes = "Registers a user into the system", tags={ "User" })
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "User successfully registered into the system."),
            @ApiResponse(code = 400, message = "Bad request parameter", response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class) })
    @PostMapping(value = UriConfiguration.REGISTER_USER_URI, produces = { "application/json" }, consumes = { "application/json" })
    ResponseEntity<Void> createUser(@ApiParam(value = "User to register") @Valid @RequestBody UserRequest body);
}
