package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.model.utils.GeoJsonPolygonModel;

import javax.validation.constraints.NotNull;

public class ZoneResponse extends EntityResponse {

    @JsonProperty("name")
    private String name = null;

    @JsonProperty("occupiedPercentage")
    private Integer occupiedPercentage = null;

    @JsonProperty("location")
    private GeoJsonPolygonModel perimeter = null;

    public ZoneResponse(String name, GeoJsonPolygonModel perimeter, Integer id, Integer occupiedPercentage) {
        super("Zone", "zone-" + id);
        this.name = name;
        this.perimeter = perimeter;
        this.occupiedPercentage = occupiedPercentage;
    }

    /**
     * Get name
     * @return name
     **/
    @ApiModelProperty(example = "zone 1", required = true, value = "")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get perimeter
     * @return perimeter
     **/
    @ApiModelProperty(required = true, value = "")
    public GeoJsonPolygonModel getPerimeter() {
        return perimeter;
    }

    public void setPerimeter(GeoJsonPolygonModel perimeter) {
        this.perimeter = perimeter;
    }

    /**
     * Get occupiedPercentage
     * @return occupiedPercentage
     **/
    @ApiModelProperty(example = "75", required = true, value = "")
    @NotNull

    public Integer getOccupiedPercentage() {
        return occupiedPercentage;
    }

    public void setOccupiedPercentage(Integer occupiedPercentage) {
        this.occupiedPercentage = occupiedPercentage;
    }
}
