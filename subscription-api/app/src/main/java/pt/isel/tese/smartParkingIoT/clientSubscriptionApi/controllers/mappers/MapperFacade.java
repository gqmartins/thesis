package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.controllers.mappers;

import org.springframework.stereotype.Component;

@Component
public class MapperFacade {

    private ZoneControllerMapper zoneControllerMapper;
    private TypeControllerMapper typeControllerMapper;

    public MapperFacade(ZoneControllerMapper zoneControllerMapper, TypeControllerMapper typeControllerMapper) {
        this.zoneControllerMapper = zoneControllerMapper;
        this.typeControllerMapper = typeControllerMapper;
    }

    public ZoneControllerMapper getZoneControllerMapper() {
        return zoneControllerMapper;
    }

    public TypeControllerMapper getTypeControllerMapper() {
        return typeControllerMapper;
    }
}
