package pt.isel.tese.smartParkingIoT.clientSubscriptionApi.logic.contract;

import pt.isel.tese.smartParkingIoT.clientSubscriptionApi.model.Subscription;

import java.util.List;
import java.util.Map;

public interface ISubscriptionService {

    int create(Subscription object);
    List<Subscription> read(int userId);
    Subscription read(int id, int userId);
    void delete(int id, int userId);
}
