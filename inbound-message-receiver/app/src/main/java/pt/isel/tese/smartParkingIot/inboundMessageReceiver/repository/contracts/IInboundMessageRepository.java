package pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.contracts;

import pt.isel.tese.smartParkingIot.inboundMessageReceiver.model.InboundMessage;

public interface IInboundMessageRepository extends IBaseRepository<InboundMessage> { }
