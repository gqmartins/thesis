package pt.isel.tese.smartParkingIot.inboundMessageReceiver.model;

import java.util.Date;

public class Zone {

    private int id;
    private String name;
    private int maxCount;
    private int currentCount;
    private Date created;
    private Date modified;

    public Zone(int id) {
        this.id = id;
    }

    public Zone(int id, String name, int maxCount, int currentCount, Date created, Date modified) {
        this.id = id;
        this.name = name;
        this.maxCount = maxCount;
        this.currentCount = currentCount;
        this.created = created;
        this.modified = modified;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(int maxCount) {
        this.maxCount = maxCount;
    }

    public int getCurrentCount() {
        return currentCount;
    }

    public void setCurrentCount(int currentCount) {
        this.currentCount = currentCount;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }
}
