package pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.utils;

import java.sql.Timestamp;
import java.util.Date;

public class DateUtils {

    public static Timestamp convertDateToTimestamp(Date date) {
        return new Timestamp(date.getTime());
    }
}
