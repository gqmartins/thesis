package pt.isel.tese.smartParkingIot.inboundMessageReceiver.logic.contracts;

public interface IBaseService<T> {

    int create(T model);
}
