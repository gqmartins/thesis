package pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.exceptions;

public class EntityDoesNotExistException extends RepositoryException {

    public EntityDoesNotExistException() {
        super("The entity doesn't exist.", "The entity with the requested id does not exist on the system.");
    }

    public EntityDoesNotExistException(String title, String detail) {
        super(title, detail);
    }
}
