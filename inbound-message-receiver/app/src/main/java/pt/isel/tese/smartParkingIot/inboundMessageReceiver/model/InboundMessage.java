package pt.isel.tese.smartParkingIot.inboundMessageReceiver.model;

import java.util.Date;

public class InboundMessage {

    private int id;
    private int sensorId;
    private int count;
    private Date created;

    public InboundMessage(int sensorId, int count) {
        this.sensorId = sensorId;
        this.count = count;
    }

    public InboundMessage(int id, int sensorId, int count, Date created) {
        this(sensorId, count);
        this.id = id;
        this.created = created;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSensorId() {
        return sensorId;
    }

    public void setSensorId(int sensorId) {
        this.sensorId = sensorId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
