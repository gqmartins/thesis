package pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.contracts;

import pt.isel.tese.smartParkingIot.inboundMessageReceiver.logic.contracts.IBaseService;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.model.Sensor;

public interface ISensorRepository extends IBaseRepository<Sensor> { }
