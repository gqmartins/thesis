package pt.isel.tese.smartParkingIot.inboundMessageReceiver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InboundMessageReceiverApi {

    public static void main(String[] args) {
        System.out.println("::STARTING::");
        SpringApplication.run(InboundMessageReceiverApi.class, args);
        System.out.println("::RUNNING::");
    }
}
