package pt.isel.tese.smartParkingIot.inboundMessageReceiver.controller.mapper;

import org.springframework.stereotype.Component;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.controller.model.request.InboundMessageRequest;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.model.InboundMessage;

@Component
public class InboundMessageControllerMapper {

    public InboundMessage transformToModel(InboundMessageRequest request, int sensor) {
        return new InboundMessage(sensor, request.getCount());
    }
}
