package pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.implementations.hibernate.mappers;

import pt.isel.tese.smartParkingIot.inboundMessageReceiver.model.SensorZone;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.implementations.hibernate.dto.SensorZoneDTO;

public class SensorZoneRepositoryMapper implements IRepositoryMapper<SensorZoneDTO, SensorZoneDTO> {

    @Override
    public SensorZoneDTO transformToObject(SensorZoneDTO dto) {
        return dto;
    }

    @Override
    public SensorZoneDTO transformToDto(SensorZoneDTO model) {
        return model;
    }

    @Override
    public int getIdentifier(SensorZoneDTO model) {
        return model.getId();
    }

    public static SensorZone transformToSensorZone(SensorZoneDTO dto) {
        return new SensorZone(dto.getZone(), dto.isEnterExit());
    }
}
