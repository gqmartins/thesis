package pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.implementations.hibernate;

import org.springframework.stereotype.Repository;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.model.Sensor;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.model.SensorZone;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.contracts.ISensorRepository;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.implementations.hibernate.dto.SensorDTO;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.implementations.hibernate.mappers.SensorRepositoryMapper;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.implementations.hibernate.mappers.SensorZoneRepositoryMapper;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class SensorRepository extends BaseRepository<Sensor, SensorDTO> implements ISensorRepository {

    private SensorZoneRepository sensorZoneRepository;

    public SensorRepository(SensorZoneRepository sensorZoneRepository) {
        super(SensorDTO.class, new SensorRepositoryMapper());
        this.sensorZoneRepository = sensorZoneRepository;
    }
    /*
    @Override
    public int create(Sensor object) {
        throw new NotImplementedException();
    }

    @Override
    public List<Sensor> read(Map<String, String> filter) {
        throw new NotImplementedException();
    }

    @Override
    public void update(Sensor object) {
        throw new NotImplementedException();
    }
    */
    @Override
    public Sensor read(int id) {
        Sensor sensor = super.read(id);
        sensor.setZones(getAllSensorZones(sensor.getId()));
        return sensor;
    }

    private List<SensorZone> getAllSensorZones(int sensorId) {
        return sensorZoneRepository
                .read(new HashMap<>())
                .stream()
                .filter(sensorZoneDTO -> sensorZoneDTO.getSensor() == sensorId)
                .map(SensorZoneRepositoryMapper::transformToSensorZone)
                .collect(Collectors.toList());
    }
}
