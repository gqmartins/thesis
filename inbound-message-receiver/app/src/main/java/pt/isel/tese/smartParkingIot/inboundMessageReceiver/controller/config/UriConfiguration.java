package pt.isel.tese.smartParkingIot.inboundMessageReceiver.controller.config;

public class UriConfiguration {

    public static final String INBOUND_MESSAGES_URI = "/sensor/{sensor-id}/inbound-message";
    public static final String INBOUND_MESSAGES_ID_URI = "/sensor/{sensor-id}/inbound-message/{inbound-message-id}";
}
