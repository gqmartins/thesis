package pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.implementations.hibernate;

import org.springframework.stereotype.Repository;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.model.InboundMessage;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.contracts.IInboundMessageRepository;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.implementations.hibernate.dto.InboundMessageDTO;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.implementations.hibernate.mappers.InboundMessageRepositoryMapper;

import java.util.function.Function;

@Repository
public class InboundMessageRepository extends BaseRepository<InboundMessage, InboundMessageDTO> implements IInboundMessageRepository {

    public InboundMessageRepository() {
        super(InboundMessageDTO.class, new InboundMessageRepositoryMapper());
    }
}
