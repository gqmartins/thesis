package pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository;

import org.springframework.stereotype.Component;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.contracts.IInboundMessageRepository;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.contracts.ISensorRepository;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.contracts.IZoneRepository;

@Component
public class RepositoryFacade {

    private IInboundMessageRepository inboundMessageRepository;
    private IZoneRepository zoneRepository;
    private ISensorRepository sensorRepository;

    public RepositoryFacade(IInboundMessageRepository inboundMessageRepository, IZoneRepository zoneRepository, ISensorRepository sensorRepository) {
        this.inboundMessageRepository = inboundMessageRepository;
        this.zoneRepository = zoneRepository;
        this.sensorRepository = sensorRepository;
    }

    public IInboundMessageRepository getInboundMessageRepository() {
        return inboundMessageRepository;
    }

    public IZoneRepository getZoneRepository() {
        return zoneRepository;
    }

    public ISensorRepository getSensorRepository() {
        return sensorRepository;
    }
}
