package pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.implementations.hibernate.dto;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Sensor")
public class SensorDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    public SensorDTO() { }

    public SensorDTO(int id) {
        this.id = id;
    }

    public SensorDTO(int id, String name, List<SensorZoneDTO> zones) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
