package pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.implementations.hibernate.mappers;

import pt.isel.tese.smartParkingIot.inboundMessageReceiver.model.Sensor;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.model.SensorZone;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.implementations.hibernate.dto.SensorDTO;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.implementations.hibernate.dto.SensorZoneDTO;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.implementations.hibernate.dto.ZoneDTO;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.utils.ListUtils;

import java.util.ArrayList;
import java.util.List;

public class SensorRepositoryMapper implements IRepositoryMapper<Sensor, SensorDTO> {

    @Override
    public Sensor transformToObject(SensorDTO dto) {
        return new Sensor(dto.getId(), dto.getName(), new ArrayList<>());
    }

    @Override
    public SensorDTO transformToDto(Sensor model) {
        List<SensorZoneDTO> sensorZones = ListUtils.mapList(model.getZones(), sensorZone ->
                this.transformToSensorZoneDTO(sensorZone, model.getId()));
        return new SensorDTO(model.getId(), model.getName(), sensorZones);
    }

    @Override
    public int getIdentifier(Sensor model) {
        return model.getId();
    }

    private SensorZone transformToSensorZone(SensorZoneDTO dto) {
        return new SensorZone(dto.getZone(), dto.isEnterExit());
    }

    private SensorZoneDTO transformToSensorZoneDTO(SensorZone model, int sensorId) {
        return new SensorZoneDTO(model.getId(), sensorId, model.isEnterExit());
    }
}
