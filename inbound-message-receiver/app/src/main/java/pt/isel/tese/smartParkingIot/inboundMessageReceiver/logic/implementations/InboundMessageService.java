package pt.isel.tese.smartParkingIot.inboundMessageReceiver.logic.implementations;

import org.springframework.stereotype.Service;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.logic.contracts.IInboundMessageService;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.model.InboundMessage;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.model.Sensor;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.model.SensorZone;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.model.Zone;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.RepositoryFacade;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.contracts.IInboundMessageRepository;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.contracts.ISensorRepository;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.contracts.IZoneRepository;

import java.util.Date;
import java.util.List;

@Service
public class InboundMessageService implements IInboundMessageService {

    private IInboundMessageRepository inboundMessageRepository;
    private IZoneRepository zoneRepository;
    private ISensorRepository sensorRepository;

    public InboundMessageService(RepositoryFacade repositoryFacade) {
        this.inboundMessageRepository = repositoryFacade.getInboundMessageRepository();
        this.zoneRepository = repositoryFacade.getZoneRepository();
        this.sensorRepository = repositoryFacade.getSensorRepository();
    }

    @Override
    public int create(InboundMessage model) {
        Sensor sensor = sensorRepository.read(model.getSensorId());
        updateZoneCount(sensor.getZones(), model.getCount());
        model.setCreated(new Date());
        return inboundMessageRepository.create(model);
    }

    private void updateZoneCount(List<SensorZone> zones, int count) {
        zones.forEach(zone -> {
            Zone sensorZone = zoneRepository.read(zone.getId());
            int currentCount = sensorZone.getCurrentCount();
            int newCurrentCount = zone.isEnterExit() ? currentCount + count : currentCount - count;
            if(newCurrentCount < 0) {
                newCurrentCount = 100;
            }
            sensorZone.setCurrentCount(newCurrentCount);
            zoneRepository.update(sensorZone);
        });
    }
}
