package pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.implementations.hibernate;

import org.springframework.stereotype.Repository;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.implementations.hibernate.dto.SensorZoneDTO;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.implementations.hibernate.mappers.IRepositoryMapper;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.implementations.hibernate.mappers.SensorZoneRepositoryMapper;

@Repository
public class SensorZoneRepository extends BaseRepository<SensorZoneDTO, SensorZoneDTO> {

    public SensorZoneRepository() {
        super(SensorZoneDTO.class, new SensorZoneRepositoryMapper());
    }
}
