package pt.isel.tese.smartParkingIot.inboundMessageReceiver.logic.exceptions;

public class LogicException extends RuntimeException {

    private String title;
    private String detail;

    public LogicException(String title, String detail) {
        super(detail);
        this.title = title;
        this.detail = detail;
    }

    public String getTitle() {
        return title;
    }

    public String getDetail() {
        return detail;
    }
}
