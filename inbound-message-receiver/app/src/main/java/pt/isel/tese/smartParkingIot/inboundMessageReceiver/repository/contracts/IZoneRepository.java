package pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.contracts;

import pt.isel.tese.smartParkingIot.inboundMessageReceiver.model.Zone;

public interface IZoneRepository extends IBaseRepository<Zone> { }
