package pt.isel.tese.smartParkingIot.inboundMessageReceiver.controller.implementation;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.controller.config.UriConfiguration;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.controller.contract.IInboundMessageController;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.controller.mapper.InboundMessageControllerMapper;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.controller.model.request.InboundMessageRequest;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.controller.utils.URIBuilder;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.logic.LogicFacade;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.logic.contracts.IInboundMessageService;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.model.InboundMessage;

import javax.validation.Valid;
import java.net.URI;

@RestController
public class InboundMessageController implements IInboundMessageController {

    private IInboundMessageService messageService;
    private InboundMessageControllerMapper messageMapper;

    public InboundMessageController(LogicFacade logicFacade, InboundMessageControllerMapper messageMapper) {
        this.messageService = logicFacade.getInboundMessageService();
        this.messageMapper = messageMapper;
    }

    @Override
    public ResponseEntity<Void> createMessage(@PathVariable("sensor-id") Integer sensorId, @Valid @RequestBody InboundMessageRequest body) {
        InboundMessage inboundMessage = messageMapper.transformToModel(body, sensorId);
        int createdMessageId = messageService.create(inboundMessage);
        URI createdURI = URIBuilder.getCreatedURI(UriConfiguration.INBOUND_MESSAGES_ID_URI, sensorId, createdMessageId);
        return ResponseEntity.created(createdURI).build();
    }
}
