package pt.isel.tese.smartParkingIot.inboundMessageReceiver.controller.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfiguration {

    @Value("${server.contextPath}")
    public String serverContextPath;

    @Value("${server.port}")
    public String serverPort;

    @Value("${useSecurity}")
    public String useSecurity;

    @Value("${apiKey}")
    public String apiKey;
}
