package pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.implementations.hibernate.mappers;

import pt.isel.tese.smartParkingIot.inboundMessageReceiver.model.InboundMessage;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.implementations.hibernate.dto.InboundMessageDTO;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.utils.DateUtils;

import java.sql.Timestamp;
import java.util.Date;

public class InboundMessageRepositoryMapper implements IRepositoryMapper<InboundMessage, InboundMessageDTO> {

    @Override
    public InboundMessage transformToObject(InboundMessageDTO dto) {
        Date created = DateUtils.convertDateToTimestamp(dto.getCreated());
        return new InboundMessage(dto.getId(), dto.getSensor(), dto.getCount(), created);
    }

    @Override
    public InboundMessageDTO transformToDto(InboundMessage model) {
        Timestamp created = new Timestamp(model.getCreated().getTime());
        return new InboundMessageDTO(model.getId(), model.getSensorId(), model.getCount(), created);
    }

    @Override
    public int getIdentifier(InboundMessage model) {
        return model.getId();
    }
}
