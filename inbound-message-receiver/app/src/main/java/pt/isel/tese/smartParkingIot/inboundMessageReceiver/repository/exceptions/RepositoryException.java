package pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.exceptions;

public class RepositoryException extends RuntimeException {

    private String title;
    private String detail;

    public RepositoryException(String title, String detail) {
        super(detail);
        this.title = title;
        this.detail = detail;
    }

    public String getTitle() {
        return title;
    }

    public String getDetail() {
        return detail;
    }
}
