package pt.isel.tese.smartParkingIot.inboundMessageReceiver.controller.exceptions;

import org.springframework.http.HttpStatus;

public class BadRequestException extends ApiException {

    public BadRequestException(String title, String detail, Exception ex) {
        super(title, detail, HttpStatus.UNAUTHORIZED, ex);
    }

    public BadRequestException(String title, String detail) {
        super(title, detail, HttpStatus.UNAUTHORIZED);
    }

    public BadRequestException(String title, String detail,  String instance, String type, Exception ex) {
        this(title, detail, HttpStatus.UNAUTHORIZED, instance, type,ex);
    }

    public BadRequestException(String title, String detail,  String instance, String type) {
        this(title, detail, HttpStatus.UNAUTHORIZED, instance, type,null);
    }

    public BadRequestException(String title, String detail, HttpStatus status, String instance, String type, Exception ex) {
        super(title, detail, status, instance, type,ex);
    }
}
