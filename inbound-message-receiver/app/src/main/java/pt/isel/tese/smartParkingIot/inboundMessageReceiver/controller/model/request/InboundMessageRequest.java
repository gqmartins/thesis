package pt.isel.tese.smartParkingIot.inboundMessageReceiver.controller.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

public class InboundMessageRequest {

    @JsonProperty("count")
    @NotNull(message = "A count must be provided.")
    private Integer count = null;

    public InboundMessageRequest() { }

    public InboundMessageRequest(Integer sensorId, Integer count) {
        this.count = count;
    }

    /**
     * Get count
     * @return count
     **/
    @ApiModelProperty(example = "27", required = true, value = "")
    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
