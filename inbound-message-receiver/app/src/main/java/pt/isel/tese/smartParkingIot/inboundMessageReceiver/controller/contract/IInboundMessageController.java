package pt.isel.tese.smartParkingIot.inboundMessageReceiver.controller.contract;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.controller.config.UriConfiguration;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.controller.model.request.InboundMessageRequest;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.controller.model.response.ErrorResponse;

import javax.validation.Valid;

@Api(value = "message", description = "the message API")
public interface IInboundMessageController {

    @ApiOperation(value = "adds message to the system", nickname = "createMessage", notes = "adds a message to the system", tags={ "message", })
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "message successfully added to the system. to get the message added to the system make a get request to the response header location URI."),
            @ApiResponse(code = 400, message = "Bad request parameter", response = ErrorResponse.class),
            @ApiResponse(code = 401, message = "You're not authorized to make that request", response = ErrorResponse.class) })
    @PostMapping(value = UriConfiguration.INBOUND_MESSAGES_URI, produces = { "application/json" }, consumes = { "application/json" })
    ResponseEntity<Void> createMessage(@ApiParam(value = "sensor id",required=true) @PathVariable("sensor-id") Integer sensorId, @ApiParam(value = "InboundMessage to add")  @Valid @RequestBody InboundMessageRequest body);
}
