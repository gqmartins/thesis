package pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.implementations.hibernate.dto;

import javax.persistence.*;

@Entity
@Table(name = "Sensor_Zone")
public class SensorZoneDTO {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "zone_id")
    private int zone;

    @Column(name = "sensor_id")
    private int sensor;

    @Column(name = "enter_exit")
    private boolean enterExit;

    public SensorZoneDTO() { }

    public SensorZoneDTO(int id) {
        this.id = id;
    }

    public SensorZoneDTO(int zone, int sensor, boolean enterExit) {
        this.zone = zone;
        this.sensor = sensor;
        this.enterExit = enterExit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getZone() {
        return zone;
    }

    public void setZone(int zone) {
        this.zone = zone;
    }

    public int getSensor() {
        return sensor;
    }

    public void setSensor(int sensor) {
        this.sensor = sensor;
    }

    public boolean isEnterExit() {
        return enterExit;
    }

    public void setEnterExit(boolean enterExit) {
        this.enterExit = enterExit;
    }
}
