package pt.isel.tese.smartParkingIot.inboundMessageReceiver.controller.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.controller.config.AppConfiguration;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.controller.exceptions.UnauthorizedException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@CrossOrigin
@Component
public class SecurityInterceptor extends HandlerInterceptorAdapter {

    private AppConfiguration appConfiguration;
    private static final String AUTHORIZATION_HEADER = "Authorization";

    public SecurityInterceptor(AppConfiguration appConfiguration) {
        this.appConfiguration = appConfiguration;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        String authorizationHeader = request.getHeader(AUTHORIZATION_HEADER);
        String apiKey = authorizationHeader.substring(authorizationHeader.indexOf(" ") + 1);
        if(!apiKey.equals(appConfiguration.apiKey)) {
            throw new UnauthorizedException("Unauthorized request.", "The api key used is incorrect");
        }
        return true;
    }
}
