package pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.contracts;

import java.util.List;
import java.util.Map;

public interface IBaseRepository<T> {

    int create(T object);
    List<T> read(Map<String, String> filters);
    T read(int id);
    void update(T object);
}
