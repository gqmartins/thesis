package pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.implementations.hibernate.mappers;

import pt.isel.tese.smartParkingIot.inboundMessageReceiver.model.Zone;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.implementations.hibernate.dto.ZoneDTO;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.utils.DateUtils;

import java.sql.Timestamp;
import java.util.ArrayList;

public class ZoneRepositoryMapper implements IRepositoryMapper<Zone, ZoneDTO> {

    @Override
    public Zone transformToObject(ZoneDTO dto) {
        return new Zone(dto.getId(), dto.getName(), dto.getMaxCount(), dto.getCurrentCount(), dto.getCreated(),
                dto.getModified());
    }

    @Override
    public ZoneDTO transformToDto(Zone model) {
        Timestamp created = DateUtils.convertDateToTimestamp(model.getCreated());
        Timestamp modified = DateUtils.convertDateToTimestamp(model.getModified());
        return new ZoneDTO(model.getId(), model.getName(), model.getMaxCount(), model.getCurrentCount(), created,
                modified);
    }

    @Override
    public int getIdentifier(Zone model) {
        return model.getId();
    }
}
