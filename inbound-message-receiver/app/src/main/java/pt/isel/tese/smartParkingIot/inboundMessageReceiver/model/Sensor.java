package pt.isel.tese.smartParkingIot.inboundMessageReceiver.model;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class Sensor {

    private int id;
    private String name;
    private List<SensorZone> zones;

    public Sensor(int id, String name, List<SensorZone> zones) {
        this.id = id;
        this.name = name;
        this.zones = zones;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SensorZone> getZones() {
        return zones;
    }

    public void setZones(List<SensorZone> zones) {
        this.zones = zones;
    }
}
