package pt.isel.tese.smartParkingIot.inboundMessageReceiver.logic;

import org.springframework.stereotype.Component;
import pt.isel.tese.smartParkingIot.inboundMessageReceiver.logic.contracts.IInboundMessageService;

@Component
public class LogicFacade {

    private IInboundMessageService inboundMessageService;

    public LogicFacade(IInboundMessageService inboundMessageService) {
        this.inboundMessageService = inboundMessageService;
    }

    public IInboundMessageService getInboundMessageService() {
        return inboundMessageService;
    }
}
