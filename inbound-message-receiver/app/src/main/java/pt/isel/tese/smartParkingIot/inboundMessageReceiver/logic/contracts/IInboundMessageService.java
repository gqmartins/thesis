package pt.isel.tese.smartParkingIot.inboundMessageReceiver.logic.contracts;

import pt.isel.tese.smartParkingIot.inboundMessageReceiver.model.InboundMessage;

public interface IInboundMessageService extends IBaseService<InboundMessage> { }
