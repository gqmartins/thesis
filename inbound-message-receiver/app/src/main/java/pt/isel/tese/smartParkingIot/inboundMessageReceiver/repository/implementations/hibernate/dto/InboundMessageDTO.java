package pt.isel.tese.smartParkingIot.inboundMessageReceiver.repository.implementations.hibernate.dto;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "InboundMessage")
public class InboundMessageDTO {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "sensor_id")
    private int sensor;

    @Column(name = "count")
    private int count;

    @Column(name = "created")
    private Timestamp created;

    public InboundMessageDTO(int id, int sensor, int count, Timestamp created) {
        this.id = id;
        this.sensor = sensor;
        this.count = count;
        this.created = created;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSensor() {
        return sensor;
    }

    public void setSensor(int sensor) {
        this.sensor = sensor;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }
}
