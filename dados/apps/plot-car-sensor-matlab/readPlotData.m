% 1 faixa 1 sentido
%plotData('../../1faixa-1sentido/10hz/10hz.csv', false)
%plotData('../../1faixa-1sentido/10hz/10hz.csv', true)
%plotData('../../1faixa-1sentido/50hz/50hz.csv', false)
%plotData('../../1faixa-1sentido/50hz/50hz.csv', true)
%plotData('../../1faixa-1sentido/100hz/100hz.csv', false)
%plotData('../../1faixa-1sentido/100hz/100hz.csv', true)
%plotData('../../1faixa-1sentido/200hz/200hz.csv', false)
%plotData('../../1faixa-1sentido/200hz/200hz.csv', true)
%plotData('../../1faixa-1sentido/500hz/500hz.csv', false)
%plotData('../../1faixa-1sentido/500hz/500hz.csv', true)
%
% 2 faixas 2 sentidos
%plotData('../../2faixas-2sentidos/10hz/10hz.csv', false)
%plotData('../../2faixas-2sentidos/10hz/10hz.csv', true)
%plotData('../../2faixas-2sentidos/50hz/50hz.csv', false)
%plotData('../../2faixas-2sentidos/50hz/50hz.csv', true)
%plotData('../../2faixas-2sentidos/100hz/100hz.csv', false)
%plotData('../../2faixas-2sentidos/100hz/100hz.csv', true)
%plotData('../../2faixas-2sentidos/200hz/200hz.csv', false)
%plotData('../../2faixas-2sentidos/200hz/200hz.csv', true)
%plotData('../../2faixas-2sentidos/500hz/500hz.csv', false)
%plotData('../../2faixas-2sentidos/500hz/500hz.csv', true)
%
% 1 faixa 1 sentido carros parados
%plotData('../../1faixa-1sentido-semaforo/100hz/100hz.csv', false)
%plotData('../../1faixa-1sentido-semaforo/100hz/100hz.csv', true)
%plotData('../../1faixa-1sentido-semaforo/200hz/200hz.csv', false)
%plotData('../../1faixa-1sentido-semaforo/200hz/200hz.csv', true)
%plotData('../../1faixa-1sentido-semaforo/500hz/500hz.csv', false)
%plotData('../../1faixa-1sentido-semaforo/500hz/500hz.csv', true)
%
% 1 faixa 2 sentidos bicicletas
%plotData('../../1faixa-2sentidos-bicicleta/100hz/100hz.csv', false)
%plotData('../../1faixa-2sentidos-bicicleta/100hz/100hz.csv', true)
%plotData('../../1faixa-2sentidos-bicicleta/200hz/200hz.csv', false)
%plotData('../../1faixa-2sentidos-bicicleta/200hz/200hz.csv', true)
%
% 2 faixas 2 sentidos com autocarros
%plotData('../../2-faixas-2sentidos-autocarros/200hz/200hz.csv', false)
%plotData('../../2-faixas-2sentidos-autocarros/200hz/200hz.csv', true)
%
% sensor debaixo do carro
%plotData('../../sensor-debaixo-carro/90ms/90ms.csv', false);
%plotData('../../sensor-debaixo-carro/90ms/90ms.csv', true);
%plotData('../../sensor-debaixo-carro/10hz/10hz.csv', false);
%plotData('../../sensor-debaixo-carro/10hz/10hz.csv', true);
%plotData('../../sensor-debaixo-carro/10hz/outros-dados/input.csv', false);
%plotData('../../sensor-debaixo-carro/50hz/50hz.csv', false);
%plotData('../../sensor-debaixo-carro/50hz/50hz.csv', true);
%plotData('../../sensor-debaixo-carro/100hz/100hz.csv', false);
%plotData('../../sensor-debaixo-carro/100hz/100hz.csv', true);
%plotData('../../sensor-debaixo-carro/200hz/200hz.csv', false);
%plotData('../../sensor-debaixo-carro/200hz/200hz.csv', true);
%plotData('../../sensor-debaixo-carro/500hz/500hz.csv', false);
%plotData('../../sensor-debaixo-carro/500hz/500hz.csv', true);
plotData('../../test-data/input.csv', false);


function plotData(filename, print_results)
    % plot data
    data = readtable(filename);
    data_time = datetime(data.time, "InputFormat", "yyyy-MM-dd HH:mm:ss.SSS");
    bars = strfind(filename, '/');
    figure('Name', char(extractBetween(filename, bars(end) + 1, length(filename))), 'NumberTitle', 'off');
    hold on;
    %x = plot(data_time, data.x);
    y = plot(data_time, data.y);
    %z = plot(data_time, data.z);
    %magnitude = plot(data_time, data.magnitude);
    %legend([x, y, z, magnitude], ["x", "y", "z", "magnitude"]);
    % plot vertical lines
    if(print_results)
        dotIndex = strfind(filename, ".");
        classifier = readtable(char(strcat(extractBetween(filename, 1, dotIndex(end) - 1), '-resultados.csv')));
        classifier_time = datetime(classifier.time, 'InputFormat', 'yyyy-MM-dd HH:mm:ss.SSS');

        ymax = get(gca,'ylim');
        for index = 1:length(classifier_time)
            time = classifier_time(index);
            plot([time time], ymax, 'HandleVisibility', 'off');
        end 
    end
end