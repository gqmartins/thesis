import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;
import java.io.File;
import java.io.PrintWriter;
import java.util.List;
import java.util.LinkedList;

public class App {

	private static final String CSV_FILE_EXTENSION = ".csv";

	public static void main(String[] args) throws IOException {
		String path = args[0];
		File file = new File(path);

		if(file.isDirectory()) {
			getAllFilesWithinFolder(file).forEach(App::mapFileToCsv);
		} else {
			mapFileToCsv(file);
		}
	}

	private static Stream<File> getAllFilesWithinFolder(File folder) {
		File[] folderFiles = folder.listFiles();
		return Stream.of(folderFiles)
			.filter(file -> file.isFile() && file.getName().endsWith(CSV_FILE_EXTENSION));
	}

	private static void mapFileToCsv(File file) {
		try(Stream<String> lines = Files.lines(Paths.get(file.getAbsolutePath())); 
			PrintWriter output = new PrintWriter(getNewFilename(file))) {
			output.println("time,x,y,z");
			lines
				.skip(1)
				.map(App::mapFileLine)
				.forEachOrdered(output::println);
		} catch(IOException ex) {
			throw new RuntimeException(ex.getMessage());
		}
	}

	private static String getNewFilename(File file) {
		String filepath = file.getAbsolutePath();
		int pointIndex = filepath.indexOf(".");
		return filepath.substring(0, pointIndex) + "-original" + filepath.substring(pointIndex);
	}

	private static String mapFileLine(String line) {
		return line.substring(0, line.lastIndexOf(","));
	}
}