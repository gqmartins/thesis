import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.stream.Stream;
import java.io.File;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;

public class App {

	private static final int MILLISECONDS_IN_A_SECOND = 1000;
	private static final Map<String, List<String>> timeCountMap = new LinkedHashMap<String, List<String>>();

	public static void main(String[] args) {
		String path = args[0];
		File file = new File(path);

		System.out.println("BEGIN...");
		gatherData(file);
		addMilisecondsToTime(file);
		System.out.println("FINISH...");
	}

	private static void gatherData(File file) {
		try(Stream<String> lines = Files.lines(Paths.get(file.getAbsolutePath()))) {
			lines
				.skip(1)
				.forEachOrdered(App::processLine);
		} catch(IOException ex) {
			throw new RuntimeException(ex.getMessage());
		}
	}

	private static void processLine(String line) {
		int commaIndex = line.indexOf(",");
		String dateTime = line.substring(0, commaIndex);
		String values = line.substring(commaIndex);

		if(timeCountMap.containsKey(dateTime)) {
			timeCountMap.get(dateTime).add(values);
		} else {
			timeCountMap.put(dateTime, new ArrayList<String>(Arrays.asList(values)));
		}
	}

	private static void addMilisecondsToTime(File file) {
		try(PrintWriter output = new PrintWriter(getNewFilename(file))) {
			String firstLine = getFileFirstLine(file);
			output.println(firstLine);
			timeCountMap.entrySet().stream().forEach(set -> {
				int valuesSize = set.getValue().size();
				if(valuesSize > MILLISECONDS_IN_A_SECOND) {
					reduceValues(set.getValue());
					valuesSize = set.getValue().size();
				}
				int timeBetweenSamples = MILLISECONDS_IN_A_SECOND / valuesSize;
				for(int milliseconds = 0, index = 0; index < valuesSize; milliseconds += timeBetweenSamples, ++index) {
					String line = set.getKey() + "." + getMillisecondsAsString(milliseconds) + set.getValue().get(index);
					output.println(line);
				}
			});
		} catch(Exception ex) {
			throw new RuntimeException(ex.getMessage());
		}
	}

	private static String getNewFilename(File file) {
		String filepath = file.getAbsolutePath();
		int pointIndex = filepath.indexOf(".");
		return filepath.substring(0, pointIndex) + "-result" + filepath.substring(pointIndex);
	}

	private static String getFileFirstLine(File file) throws Exception {
		BufferedReader bf = new BufferedReader(new FileReader(file));
		return bf.readLine();
	}

	private static void reduceValues(List<String> values) {
		HashSet<Integer> selectedNumbers = new HashSet<Integer>();
		Random numberGenerator = new Random();
		int numbersToGenerate = values.size() - MILLISECONDS_IN_A_SECOND;
		for(int i = 0; i < numbersToGenerate; ++i) {
			int index = getRandomIndex(selectedNumbers, numberGenerator, values.size());
			selectedNumbers.add(index);
			values.remove(index);
		}
	}

	private static int getRandomIndex(HashSet<Integer> selectedNumbers, Random numberGenerator, int max) {
		int selectedIndex;
		do {
			selectedIndex = numberGenerator.nextInt(max);
		} while(selectedNumbers.contains(selectedIndex));
		return selectedIndex;
	}

	private static String getMillisecondsAsString(long milliseconds) {
		String millisecondsText = String.valueOf(milliseconds);
		if(millisecondsText.length() == 3) {
			return String.valueOf(milliseconds);
		}

		return millisecondsText.length() == 2 ? "0" + milliseconds : "00" + milliseconds;
	}
}