import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class App {

	static class PredictedValue {

		private final String beginTime;
		private final String endTime;

		public PredictedValue(String beginTime, String endTime) {
			this.beginTime = beginTime;
			this.endTime = endTime;
		}

		public String getBeginTime() {
			return beginTime;
		}

		public String getEndTime() {
			return endTime;
		}
	}

	public static void main(String[] args) {
		String realValuesFilename = args[0];
		String predictedValuesFilename = args[1];

		List<String> realValues = getFileValues(realValuesFilename, App::getRealValue);
		List<PredictedValue> predictedValues = getFileValues(predictedValuesFilename, App::getPredictedValue);
		/*
		realValues.stream().forEach(System.out::println);
		predictedValues.stream().forEach(value -> System.out.println(value.getBeginTime() + ", " + value.getEndTime()));
		*/
		calculateConfusionMatrix(realValues, predictedValues);
	}

	private static <T> List<T> getFileValues(String filename, Function<String, T> mapper) {
		try(Stream<String> lines = Files.lines(Paths.get(filename))) {
			return lines
				.skip(1)
				.map(mapper)
				.collect(Collectors.toList());
		} catch(IOException ex) {
			throw new RuntimeException(ex.getMessage());
		}
	}

	private static String getRealValue(String line) {
		return line.split(",")[0];
	}

	private static PredictedValue getPredictedValue(String line) {
		String[] times = line.split(",");
		return new PredictedValue(times[0], times[1]);
	}

	private static void calculateConfusionMatrix(List<String> realValues, List<PredictedValue> predictedValues) {
		int[] results = getTruePositivesAndFalseNegatives(realValues, predictedValues);
		int falsePositives = getFalsePositives(realValues, predictedValues);

		System.out.println("true positives: " + results[0]);
		System.out.println("false negatives: " + results[1]);
		System.out.println("false positives: " + falsePositives);
		System.out.println("true negatives: 0");
	}

	private static int[] getTruePositivesAndFalseNegatives(List<String> realValues, List<PredictedValue> predictedValues) {
		int truePostives = 0, falseNegatives = 0;

		for(String real : realValues) {
			boolean found = false;
			for(PredictedValue predicted : predictedValues) {
				int compareBegin = real.compareTo(predicted.getBeginTime());
				int compareEnd = real.compareTo(predicted.getEndTime());
				if(compareBegin == 0 || (compareBegin > 0 && compareEnd < 0)) {
					found = true;
					break;
				}
			}
			if(found) {
				++truePostives;
			} else {
				++falseNegatives;
			}
		}

		return new int[] {truePostives, falseNegatives};
	}

	private static int getFalsePositives(List<String> realValues, List<PredictedValue> predictedValues) {
		int falsePositives = 0;
		for(PredictedValue predicted : predictedValues) {
			boolean found = false;
			for(String real : realValues) {
				int compareBegin = real.compareTo(predicted.getBeginTime());
				int compareEnd = real.compareTo(predicted.getEndTime());
				if(compareBegin == 0 || (compareBegin > 0 && compareEnd < 0)) {
					found = true;
					break;
				}
			}
			if(!found) {
				++falsePositives;
			}
		}
		return falsePositives;
	}
}
