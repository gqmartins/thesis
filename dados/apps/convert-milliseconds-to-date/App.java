import java.io.File;
import java.io.PrintWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.LinkedList;
import java.util.stream.Stream;
import java.util.Date;
import java.util.Calendar;
import java.text.SimpleDateFormat;

public class App {

	private static final int FILE_INDEX = 0;
	private static final int START_DATE_INDEX = 1;
	private static final int START_TIME_INDEX = 2;
	private static final String CSV_FILE_EXTENSION = ".csv";

	public static void main(String[] args) {
		String filename = args[FILE_INDEX];
		String startDate = args[START_DATE_INDEX];
		String startTime = args[START_TIME_INDEX];

		System.out.println("BEGIN...");

		File file = new File(filename);
		if(file.isDirectory()) {
			getAllFilesWithinFolder(file).forEach(f -> addTimeToFile(f, startDate, startTime));
		} else {
			addTimeToFile(file, startDate, startTime);
		}

		System.out.println("FINISHED...");
	}

	private static Stream<File> getAllFilesWithinFolder(File folder) {
		File[] folderFiles = folder.listFiles();
		return Stream.of(folderFiles)
			.filter(file -> file.isFile() && file.getName().endsWith(CSV_FILE_EXTENSION));
	}

	private static void addTimeToFile(File file, String startDate, String startTime) {
		try(Stream<String> lines = Files.lines(Paths.get(file.getAbsolutePath())); 
			PrintWriter output = new PrintWriter(getNewFilename(file))) {
			output.println("time,x,y,z");
			lines
				.skip(1)
				.map(line -> mapFileLine(line, startDate, startTime))
				.forEachOrdered(output::println);
		} catch(IOException ex) {
			throw new RuntimeException(ex.getMessage());
		}
	}

	private static String getNewFilename(File file) {
		String filepath = file.getAbsolutePath();
		int pointIndex = filepath.indexOf(".");
		return filepath.substring(0, pointIndex) + "-result" + filepath.substring(pointIndex);
	}

	private static String mapFileLine(String line, String startDate, String startTime) {
		try {
			int indexOfComma = line.indexOf(",");
			int millis = Integer.parseInt(line.substring(0, indexOfComma));
			String values = line.substring(indexOfComma + 1);
			String date = getDate(startDate, startTime, millis);
			return date + "," + values;	
		} catch(Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	private static String getDate(String startDate, String startTime, int millis) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String dateText = startDate + " " + startTime + ".000";
		Date date = sdf.parse(dateText);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MILLISECOND, millis);
		return sdf.format(calendar.getTime());
	}
}