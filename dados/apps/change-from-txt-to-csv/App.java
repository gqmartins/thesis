import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;
import java.io.File;
import java.io.PrintWriter;

public class App {

	private static final String TEXT_FILE_EXTENSION = ".txt";
	private static final String CSV_FILE_EXTENSION = ".csv";

	public static void main(String[] args) throws IOException {
		String path = args[0];
		File file = new File(path);

		if(file.isDirectory()) {
			getAllFilesWithinFolder(file).forEach(App::mapFileToCsv);
		} else {
			mapFileToCsv(file);
		}
	}

	private static Stream<File> getAllFilesWithinFolder(File folder) {
		File[] folderFiles = folder.listFiles();
		return Stream.of(folderFiles)
			.filter(file -> file.isFile() && file.getName().endsWith(TEXT_FILE_EXTENSION));
	}

	private static void mapFileToCsv(File file) {
		try(Stream<String> lines = Files.lines(Paths.get(file.getAbsolutePath())); 
			PrintWriter output = new PrintWriter(file.getAbsolutePath().replace(TEXT_FILE_EXTENSION, CSV_FILE_EXTENSION))) {
			output.println("time,x,y,z,");
			lines
				.skip(1)
				.map(App::mapFileLine)
				.forEachOrdered(output::println);
		} catch(IOException ex) {
			throw new RuntimeException(ex.getMessage());
		}
	}

	private static String mapFileLine(String line) {
		String lineWithoutTabs = line.replace("\t", "");
		int commaIndex = lineWithoutTabs.lastIndexOf(":") + 3;
		String beforeComma = lineWithoutTabs.substring(0, commaIndex);
		String afterComma = lineWithoutTabs.substring(commaIndex);

		return beforeComma + "," + afterComma;
	}
}