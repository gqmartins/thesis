%plotData('../../contagens-lisboa/E1/E1-result.csv', false);
plotData('../../contagens-lisboa/E1/E1-result.csv', true);
%plotData('../../contagens-lisboa/E2/E2-result.csv', false);
%plotData('../../contagens-lisboa/E2/E2-result.csv', true);
%plotData('../../contagens-lisboa/E4/E4-result.csv', false);
%plotData('../../contagens-lisboa/E4/E4-result.csv', true);
%plotData('../../contagens-lisboa/E5/E5-result.csv', false);
%plotData('../../contagens-lisboa/E5/E5-result.csv', true);
%plotData('../../contagens-lisboa/E10/E10-result.csv', false);
%plotData('../../contagens-lisboa/E10/E10-result.csv', true);
%plotData('../../sensor-debaixo-carro/10hz/citroen_10hz_varios.csv', false);

function plotData(filename, multiple_graphs)
    data = readtable(filename);
    days = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];
    time = datetime(unique(data.time), "InputFormat", "HH:mm");
    if (not(multiple_graphs))
        hold on;
    end
    for day = 1:length(days)
        day_data = data(data.weekday == day,:).count;
        if (multiple_graphs)
            figure('Name', days(day));
        end
        plot(time, day_data);
    end
end