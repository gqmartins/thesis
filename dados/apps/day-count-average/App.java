import java.io.IOException;
import java.util.stream.Stream;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.Map;
import java.util.Date;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.LinkedList;
import java.util.stream.Collectors;

public class App {

	private static final String CSV_FILE_EXTENSION = ".csv";
	private static final Map<String, List<Integer>> countMap = new TreeMap<>(String::compareTo);

	public static void main(String[] args) throws IOException {
		File file = new File(args[0]);

		if(file.isDirectory()) {
			getAllFilesWithinFolder(file).forEach(App::countDays);
		} else {
			countDays(file);
		}

		Map<String, Integer> averages = getAverages();
		storeResults(averages, file);
	}

	private static Stream<File> getAllFilesWithinFolder(File folder) {
		File[] folderFiles = folder.listFiles();
		return Stream.of(folderFiles)
			.filter(file -> file.isFile() && file.getName().endsWith(CSV_FILE_EXTENSION));
	}

	private static void countDays(File file) {
		try(Stream<String> lines = Files.lines(Paths.get(file.getAbsolutePath()))) {
			lines
				.skip(1)
				.forEachOrdered(App::mapFileLine);
		} catch(IOException ex) {
			throw new RuntimeException(ex.getMessage());
		}
	}

	private static void mapFileLine(String line) {
		try {
			String[] values = line.split(",");
			int weekDay = getWeekDay(values[0]);
			String time = values[1];
			int count = Integer.parseInt(values[2]);

			String key = weekDay + " " + time;
			if(!countMap.containsKey(key)) {
				countMap.put(key, new LinkedList<Integer>());
			}
			countMap.get(key).add(count);	
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}

	private static int getWeekDay(String time) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Date date = sdf.parse(time);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.DAY_OF_WEEK);
	}

	private static Map<String, Integer> getAverages() {
		return countMap
			.entrySet()
			.stream()
			.collect(Collectors.toMap(
				entry -> entry.getKey(),
				entry -> (int) entry.getValue().stream().mapToInt(val -> val).average().orElse(0.0),
				(v1, v2) -> { throw new RuntimeException(); },
				TreeMap::new
			));
	}

	private static void storeResults(Map<String, Integer> averages, File file) {
		try(PrintWriter output = new PrintWriter("simultation-result.csv")) {
			output.println("weekday,time,count");
			averages.entrySet().stream().forEach(entry -> {
				String[] keySplit = entry.getKey().split(" ");
				int weekday = Integer.parseInt(keySplit[0]);
				String time = keySplit[1];
				output.println(weekday + "," + time + "," + entry.getValue());
			});
		} catch(IOException ex) {
			throw new RuntimeException(ex.getMessage());
		}
	}

	private static String getNewFilename(File file) {
		String filepath = file.getAbsolutePath();
		int pointIndex = filepath.indexOf(".");
		return filepath.substring(0, pointIndex) + "-result" + filepath.substring(pointIndex);
	}
}